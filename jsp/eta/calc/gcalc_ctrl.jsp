
<%@page import="sun.security.jca.GetInstance"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page
	import="fr.gouv.education.antares.calc.ConstantesCalculette.MESSAGE"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="fr.gouv.education.antares.outils.FileUtils"%>
<%@page
	import="fr.gouv.education.antares.calc.ConstantesCalculette,java.io.*"%>


<%--
Version : 1.0
--%>

<%@page language="java" info="Controleur - Calculette de classement"
	contentType="text/html; charset=iso-8859-1"
	extends="fr.gouv.education.antares.framework.jsp.JspSession"
	isErrorPage="false" errorPage="/erreur.jsp"%>
<%@page
	import="fr.gouv.education.antares.Constantes,
               java.util.ArrayList,
               java.util.BitSet,
               java.util.List,
               java.util.HashMap,
               java.util.Set,
               java.util.Date,
               java.text.DateFormat,
               java.util.Collection,
               java.io.File,
               fr.gouv.education.antares.calc.objets.Calculette,               
               fr.gouv.education.antares.calc.objets.Periode,
               fr.gouv.education.antares.calc.objets.Fonction,
               fr.gouv.education.antares.calc.objets.DureeA30Jours,
               fr.gouv.education.antares.calc.metier.CalculetteBS,
               fr.gouv.education.antares.outils.sort.Sort,   
               fr.gouv.education.antares.outils.MiscUtils,                           
               fr.gouv.education.antares.Constantes,               
               fr.gouv.education.antares.calc.ConstantesCalculette.DETAIL_CALCUL_MESSAGE,
               fr.gouv.education.antares.calc.ConstantesCalculette.DETAIL_CALCUL_MESSAGE,
               fr.gouv.education.antares.calc.ConstantesCalculette.FONCTION_BITSET_INDX,
               fr.gouv.education.antares.calc.ConstantesCalculette.MESSAGE,
               fr.gouv.education.antares.calc.edition.EditionCalcul,
               fr.gouv.education.antares.outils.Conversion,
               fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_ACTION,
               fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_ID,
               fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_NAME,
               fr.gouv.education.antares.calc.ConstantesCalculette.VALEUR_CALCUL,
               fr.gouv.education.antares.calc.ConstantesCalculette.CALCULETTE_FORWARD,               
               fr.gouv.education.antares.outils.StringUtils,
               fr.gouv.education.antares.outils.Utils,               
               fr.gouv.education.antares.simpleLog"%>

<%

    //simpleLog.println(" ==============");
    //simpleLog.println(" request attribut");
    //this.printRequestAttributes(request);
    //simpleLog.println(" ==============");
    //simpleLog.println(" request parametter ");
    //this.printRequestParameters(request);
    //simpleLog.println(" ==============");
    //simpleLog.println(" session parametter ");
    //this.printSessionParameters(request.getSession());

    // Reinitialiser et quitter
    String a = request.getParameter("a") ;
    // lancement du calcul
    String calculer = request.getParameter(FORM_CALCULETTE_ACTION.CALCULER.getNom()) ;
    //edition
    String editerDecompte = request.getParameter(FORM_CALCULETTE_ACTION.EDIT_DECOMPTE.getNom()) ;
    // calcul duree
    String calculDuree = request.getParameter(FORM_CALCULETTE_ACTION.CALCULER_DUREE.getNom()) ;
    // lire info formulaire a partir d'un fichier
    String recuperer = request.getParameter(FORM_CALCULETTE_ACTION.RECUPERER.getNom()) ;
    // stocker les infos dans un fichier
    String enregistrerInfos = request.getParameter(FORM_CALCULETTE_ACTION.ENREG_INFO.getNom()) ;
    
    String dateNomin=request.getParameter(FORM_CALCULETTE_NAME.DT_NOMIN.getNom()) ;
    
    String dateNais=request.getParameter(FORM_CALCULETTE_NAME.DT_NAISS.getNom()) ;
    
    simpleLog.println("====== GCALC_CTRL : [a="+a+"]    [calculer="+calculer+"]    [editerDecompte="+editerDecompte+"]    [calculDuree="+calculDuree+"]    [recuperer="+recuperer+"]    [enregistrerInfos="+enregistrerInfos+"]"   );
    
    MESSAGE tmpMessage = MESSAGE.OK;
    Date tmpDateDeb;
    Date tmpDateFin;
    Periode tmpPeriode ;
    
    if(a != null && a.equals("reinit"))
      {
       simpleLog.println("====== GCALC_CTRL : reinit"); 
       session.removeAttribute(ConstantesCalculette.CALCULETTE_KEY) ;
       forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
       return ;
      }
    else if(a != null && a.equals("quit"))
      {
       simpleLog.println("====== GCALC_CTRL : quit");  
       forward("/con04.jsp?ecranDemande=calculette",request, response);
       return ;
      }
    
    
    
    Calculette cal = (Calculette) session.getAttribute(ConstantesCalculette.CALCULETTE_KEY); 
    
    //Date date= Conversion.stringToDate(dateNomin);
    cal.setDateFormat(dateNomin);
    cal.setDate_Format_Nais(dateNais);
    session.setAttribute(ConstantesCalculette.CALCULETTE_KEY,cal);
    CalculetteBS calBS = CalculetteBS.getSingleton();
   
    if(calculer != null)
      {
       simpleLog.println("====== GCALC_CTRL : calcul");  
       String tmpStr   = request.getParameter("nbFonctions");
       int nbFonction = Conversion.stringToInteger(tmpStr);
       mapDatasCalculette(request,cal);
       /* efface les calculs precedents */
       cal.resetCalcul();
		//List<Fonction> fonctions=cal.getLstFonction();
       
       int j=0;
       for(int i=0; i<cal.getLstFonction().size();i++ ) 
         {
    	  j=i+1;
    	  if( request.getParameter("f_"+ j) != null )
    	  {
    	  String fonctDateDeb=request.getParameter(FORM_CALCULETTE_NAME.FONC_DT_DEB_PREFIX.getNom()+ j);
    	  String fonctDateFin=request.getParameter(FORM_CALCULETTE_NAME.FONC_DT_FIN_PREFIX.getNom()+ j);
    	  String quotitereelle=request.getParameter(FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()+ j);
    	  String dureeRetenuAA=request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_AA_PREFIX.getNom()+ j);
    	  String dureeRetenuMM=request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_MM_PREFIX.getNom()+ j);
    	  String dureeRetenuJJ=request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_JJ_PREFIX.getNom()+ j);
    	  String tmpsCodeArt =ConstantesCalculette.ARTICLE.ARTICLE_15_II.getLabel();
    	  if( !fonctDateDeb.isEmpty() && !fonctDateDeb.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$") )
    	    {
    		 tmpMessage=MESSAGE.CONTROLE_FORMAT_DATE_DEBUT;
    	    }
    	  else if(!fonctDateFin.isEmpty()&& !fonctDateFin.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$") )
    	    {
    		 tmpMessage=MESSAGE.CONTROLE_FORMAT_DATE_FIN;
    	    }
    	  else if(fonctDateDeb.isEmpty()
    			  &&!cal.getLstFonction().get(i).getCodeArticle().equals(ConstantesCalculette.ARTICLE.ARTICLE_15_II.getLabel())
				  && !cal.getLstFonction().get(i).getCodeArticle().equals(ConstantesCalculette.ARTICLE.ARTICLE_LIBRE.getLabel())
				  && !cal.getLstFonction().get(i).getCodeArticle().equals(ConstantesCalculette.ARTICLE.ARTICLE_5_1.getLabel()))
    	    {
    		 tmpMessage=MESSAGE.CALCUL_FONCTIONE_DATE_DEBUT_MANDATORY;
    	    }
    	  else if(fonctDateFin.isEmpty()
    			  &&(!cal.getLstFonction().get(i).getCodeArticle().equals(tmpsCodeArt)
    			  && !cal.getLstFonction().get(i).getCodeArticle().equals(ConstantesCalculette.ARTICLE.ARTICLE_LIBRE.getLabel()) 
    			  && !cal.getLstFonction().get(i).getCodeArticle().equals(ConstantesCalculette.ARTICLE.ARTICLE_5_1.getLabel())))
    	    {
    	     tmpMessage=MESSAGE.CALCUL_FONCTIONE_DATE_FIN_MANDATORY;
    	    }
    	  else if(!fonctDateDeb.isEmpty() && cal.getLstFonction().get(i).getDateDeb()==null)
    	    {
    	     tmpMessage=MESSAGE.CALCUL_FONCTIONE_FORMAT_DATE_MANDATORY;
    	    }
    	  else if(!fonctDateFin.isEmpty() && cal.getLstFonction().get(i).getDateFin()==null) 
    	    {
  		     tmpMessage=MESSAGE.CALCUL_FONCTIONE_FORMAT_DATE_MANDATORY;
  	        }
    	  else if(!quotitereelle.isEmpty() &&(!quotitereelle.matches("^100|[0-9]{1,2}$")||Integer.parseInt(quotitereelle)>100||Integer.parseInt(quotitereelle)<0 ))
    	    {
    		 tmpMessage=MESSAGE.QUOTITE_REELLE_FORMAT;
    	    }
    	  else if(!dureeRetenuAA.equals("0")&& !dureeRetenuAA.isEmpty() 
    			  &&(!dureeRetenuAA.matches("^[0-9]{1,2}$")||Integer.parseInt(dureeRetenuAA)>100||Integer.parseInt(dureeRetenuAA)<0 ))
    	    {
	         tmpMessage=MESSAGE.DUREE_RETENUE_AA;
     	    }
    	  else if(!dureeRetenuMM.equals("0")&& !dureeRetenuMM.isEmpty()&&(!dureeRetenuMM.matches("^[0-9]{1,2}$")||Integer.parseInt(dureeRetenuMM)>=12||Integer.parseInt(dureeRetenuMM)<0 ))
    	    {
	         tmpMessage=MESSAGE.DUREE_RETENUE_MM;
            }
    	  else if(!dureeRetenuJJ.equals("0")&& !dureeRetenuJJ.isEmpty() &&(!dureeRetenuJJ.matches("^[0-9]{1,2}$")||Integer.parseInt(dureeRetenuJJ)>=31||Integer.parseInt(dureeRetenuJJ)<0 ))
    	    {
	         tmpMessage=MESSAGE.DUREE_RETENUE_JJ;
            }
    	  }
    	 }
       if(tmpMessage.getLevel().isErrorLevel())
         {        	
          cal.setUserMessage(tmpMessage);
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
          return ;
         }     
    
       cal.setUserMessage(MESSAGE.OK);
       tmpMessage = calBS.controleData(cal);
       
       
       if(tmpMessage.getLevel().isErrorLevel())
         {        	
          cal.setUserMessage(tmpMessage);
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
          return ;
         }            
       cal.setUserMessage(MESSAGE.OK);
       tmpMessage = calBS.calculeAll(cal);
       
       if(tmpMessage.getLevel().isErrorLevel())
         {        	
          cal.setUserMessage(tmpMessage);         
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response); 
          return ;
         }  
       
      forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
       return ;
      }
    else if(editerDecompte != null)
      {
       simpleLog.println("====== GCALC_CTRL : editdecompte"); 
       tmpMessage = calBS.controleData(cal);
        
       if(tmpMessage.getLevel().isErrorLevel())
         {
          cal.setUserMessage(MESSAGE.OK);
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
          return ;
         }
       
       SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
       String fname = "Decompte_"+cal.getNumEtablissement()+"_"+cal.getPrenom()+"_"+cal.getNom()+"_"+df.format(cal.getDateNaissance()) ;  
       
       File tmpFile = File.createTempFile(fname , ".pdf");
       simpleLog.println(" ---- getGradeOrigineLibelleOrId ----"+cal.getGradeOrigineLibelleOrId());               
       tmpMessage = EditionCalcul.lanceEdition(cal,tmpFile);
       
       //response.setHeader( "Content-Disposition", "inline; filename=\""+tmpFile+"\"" );      
       response.setHeader( "Content-Disposition", "attachement; filename=\""+fname+".pdf\"" );
       response.setHeader("Cache-Control","max-age=1");
       response.setDateHeader("Last-Modified",MiscUtils.today().getTime());
       response.setHeader("Version",Long.toHexString(System.currentTimeMillis()));
       response.setContentType("application/pdf");
       ServletOutputStream outPut = response.getOutputStream();
       try {    	   
           FileUtils.writeFileToStream(outPut,tmpFile);
           outPut.flush();
       }
       finally {
    	   outPut.close();
           response.flushBuffer();
           tmpFile.delete();
       }       
              
       cal.setUserMessage(MESSAGE.OK);       
       return ;
              
      }
    else if(calculDuree != null)
      {
       simpleLog.println("====== GCALC_CTRL : calcul_duree"); 
    	
       String tmpStr = (String)request.getParameter(FORM_CALCULETTE_NAME.CALCUL_DUREE_DATE_DEB.getNom());
       tmpDateDeb = Conversion.stringToDate(tmpStr);
       String tmpStr1 = (String)request.getParameter(FORM_CALCULETTE_NAME.CALCUL_DUREE_DATE_FIN.getNom());
       tmpDateFin = Conversion.stringToDate(tmpStr1);
        
       tmpPeriode = cal.getPeriodePourCalcul();
       tmpPeriode.setDateDebut(tmpDateDeb);
       tmpPeriode.setDateFin(tmpDateFin);    
       
        
       if(!tmpStr.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$")&& !tmpStr.isEmpty()){
       	tmpMessage=MESSAGE.CONTROLE_FORMAT_DATE_DEBUT;
       }
       else if(!tmpStr1.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$") && !tmpStr1.isEmpty()){
       	tmpMessage=MESSAGE.CONTROLE_FORMAT_DATE_FIN;
       }
       else if( tmpDateDeb == null && tmpDateFin == null )
          {      	        
	         tmpMessage = MESSAGE.CALCUL_DUREE_PERIODE_MANDATORY;
	        }
        else if(tmpDateDeb == null){
    	   tmpMessage = MESSAGE.CALCUL_DUREE_DATE_DEBUT_MANDATORY;
       }
        
       else if(tmpDateFin == null){
    	   tmpMessage = MESSAGE.CALCUL_DUREE_DATE_FIN_MANDATORY;
       }
       else if( tmpPeriode.isEmpty())
         {
          tmpMessage = MESSAGE.CALCUL_DUREE_PERIODE_MANDATORY;         
         }
      
          else
            {
             if( !tmpDateDeb.before(tmpDateFin) )
               {
                tmpMessage = MESSAGE.CALCUL_DUREE_DT_DEB_BEFORE_DT_FIN;
               }
                                              
         }
        
       mapDatasCalculette(request,cal);
         
       if(tmpMessage.getLevel().isUserErrorLevel())
         {
          cal.setUserMessage(tmpMessage);  
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
          return ;
         }
       
       cal.setUserMessage(MESSAGE.OK);
       
      }
    else if(recuperer != null || (a!=null && a.equals("recuperer")) )
      {
       simpleLog.println("====== GCALC_CTRL : recup");       
       simpleLog.println("             file : " + request.getParameter("recupinfo") );
       simpleLog.println("      contentType : " + request.getContentType() ); 
       
       String numuai = (String) session.getAttribute(Constantes.SESSION_NUME_ETA); //N� d'etab
       String libuai = (String) session.getAttribute(Constantes.SESSION_LIB_ETA); //libelle
       //String fff   = request.getParameter("recupinfo") ;

       //cal = new Calculette();
       cal.setNomEtablissement(libuai);
       cal.setNumEtablissement(numuai);
       session.setAttribute(ConstantesCalculette.CALCULETTE_KEY,cal);

       String contentType = request.getContentType();
       if( contentType != null && contentType.indexOf("multipart/form-data") >= 0 )
         {
          String fname = "gcalc_infos_"+cal.getNumEtablissement() ;   
          File tmpFile = File.createTempFile(fname+"_tmp" , ".xml");
          File tmpFile2 = File.createTempFile(fname, ".xml");
          try
            {
             ByteArrayInputStream in_ = null;
             DataInputStream in = new DataInputStream(request.getInputStream());
             int formDataLength = request.getContentLength();
             byte dataBytes[] = new byte[formDataLength];
             int byteRead = 0;
             int totalBytesRead = 0;
             while (totalBytesRead < formDataLength)
               {
                byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                totalBytesRead += byteRead;
               }
             String file = new String(dataBytes);

             // RECUPERE LE NON DU FICHIER
             String saveFile = file.substring(file.indexOf("filename=\"") + 10);
             saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
             saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,saveFile.indexOf("\""));
             
             // RECUPERE LA POSITION DU CONTENU DU FICHIER
             int lastIndex = contentType.lastIndexOf("=");
             String boundary = contentType.substring(lastIndex + 1,contentType.length());
             int pos;
             pos = file.indexOf("filename=\"");
             pos = file.indexOf("\n", pos) + 1;
             pos = file.indexOf("\n", pos) + 1;
             pos = file.indexOf("\n", pos) + 1;
             int boundaryLocation = file.indexOf(boundary, pos) - 4;
             int startPos = ((file.substring(0, pos)).getBytes()).length;
             int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

             // VEIRIFCATION QUE LE FICHIER NE SOIT PAS VIDE (soit parce qu'il n'existe pas soit parce qu'il n'a pas ete initialise)
             if( (endPos-startPos)==0)
               {
                simpleLog.println("                  : fichier vide" ); 
               }
             else
               {
                in_ = new ByteArrayInputStream(dataBytes,startPos,(endPos - startPos));
                // ECRITURE DU FICHIER
                
                
                FileUtils.readFileFromStream(in_,tmpFile);
                BufferedReader input =  new BufferedReader(new FileReader(tmpFile));
                BufferedWriter output =  new BufferedWriter(new FileWriter(tmpFile2));
                try 
                  {
                   String line = null;
                   while ( ( line = input.readLine()) != null)
                     {
                      if( !line.trim().equals("") )
                        {
                         output.write(line) ;
                         output.newLine() ;
                        }
                     }
                  }
                catch(Exception e){}
                finally 
                  {
                   input.close();
                   output.close() ;
                  }

                calBS.importDatasFromFile(cal,tmpFile2);
               }
            }
          catch(Exception e)
            {
             e.printStackTrace() ;
            }
          finally
            {
             //a voir si on supprime les 2 fichiers tmp qui sont crees 
            }
         }
        
       forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
       return ;
      }
    else if(enregistrerInfos != null)
      {
       simpleLog.println("====== GCALC_CTRL : enreg_info"); 

       mapDatasCalculette(request,cal);
    	
       tmpMessage = calBS.controleData(cal);
      
       if(tmpMessage.getLevel().isUserErrorLevel())
         {
          cal.setUserMessage(tmpMessage);
          forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response); 
          return ;
         }
       
       
       SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
       String fname = "gcalc_"+cal.getNumEtablissement()+"_"+cal.getPrenom()+"_"+cal.getNom()+"_"+df.format(cal.getDateNaissance())+".xml" ;   
       File tmpFile = File.createTempFile("gcalc_infos_"+cal.getNumEtablissement()+"_"+cal.getPrenom()+"_"+cal.getNom()+"_"+df.format(cal.getDateNaissance()),".xml");
       tmpMessage = calBS.exportDatasToFile(cal,tmpFile);
       
       
   
       
       ServletContext      context  = getServletConfig().getServletContext();
       String              mimetype = context.getMimeType( tmpFile.getName() );
       
       response.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
       response.setHeader( "Content-Disposition", "attachment; filename=\"" + fname + "\"" );
       response.setDateHeader("Last-Modified",MiscUtils.today().getTime());
       response.setHeader("Version",Long.toHexString(System.currentTimeMillis()));
       ServletOutputStream outPut = response.getOutputStream();
       try
       {    	   
           FileUtils.writeFileToStream(outPut,tmpFile);
           outPut.flush();
       }
       finally
       {
    	   outPut.close();
           response.flushBuffer();
       }       
              
       cal.setUserMessage(MESSAGE.OK);
       return ;
       
      }

    simpleLog.println("====== GCALC_CTRL : retour par default"); 
    forward(CALCULETTE_FORWARD.CALCULETTE_ECRAN.getForward(),request, response);
%>

<%!
  private void mapDatasCalculette(HttpServletRequest request , Calculette cal){
     simpleLog.println(">>> mapDatasCalculette");
     CalculetteBS calBS = CalculetteBS.getSingleton();
     calBS.mapDatasCalculette(request,cal);
     
     simpleLog.println(cal.getCorpsOrigineLibelleOrId()+"<<< mapDatasCalculette"+cal.getGradeOrigineLibelleOrId());
 }
%>





