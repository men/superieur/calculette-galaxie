<%--
Version : 1.0
--%>
<%@page language="java" info="Calculette de classement"
   contentType="text/html; charset=iso-8859-1"
   extends="fr.gouv.education.antares.framework.jsp.JspSession"
   isErrorPage="false" errorPage="/erreur.jsp"
%>
<%@page import="java.util.List,
           java.util.HashMap,
           java.util.ArrayList,
           java.util.Iterator,
            fr.gouv.education.antares.Messages,
            fr.gouv.education.antares.Constantes,
            fr.gouv.education.antares.calc.ConstantesCalculette,
            fr.gouv.education.antares.calc.objets.Calculette,
            fr.gouv.education.antares.calc.objets.Fonction,
            fr.gouv.education.antares.calc.objets.Periode,
            fr.gouv.education.antares.calc.objets.DureeA30Jours,
            fr.gouv.education.antares.calc.outils.ref.objects.CorpsRef,
            fr.gouv.education.antares.calc.outils.ref.objects.ArticleRef,
            fr.gouv.education.antares.calc.outils.ref.objects.GradeRef,
            fr.gouv.education.antares.calc.outils.ref.objects.FonctionRef,
            fr.gouv.education.antares.calc.outils.ref.objects.GrilleIndiciaireRef,
            fr.gouv.education.antares.calc.outils.ref.ConstantesRef,
            fr.gouv.education.antares.calc.outils.ref.metier.TableRefHelper,
            fr.gouv.education.antares.calc.outils.ref.ConstantesRef.TAB_REF,
            fr.gouv.education.antares.calc.outils.ref.objects.FonctionArticleRef,
            fr.gouv.education.antares.outils.Conversion,
            fr.gouv.education.antares.outils.sort.Sort,
            fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_ACTION,
            fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_ID,
            fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_NAME,
            fr.gouv.education.antares.calc.ConstantesCalculette.CALCULETTE_FORWARD,
            fr.gouv.education.antares.calc.ConstantesCalculette.LEVEL,
            fr.gouv.education.antares.simpleLog"
%>
<!--<%@taglib uri="weblogic-tags.tld"   prefix="wlt" %>-->
<%@taglib uri="wlTag.tld"           prefix="wl" %>

<%!
  protected void setIdAction() {
    _idAction = "META_GCALC";
  }
%>
<%
    String numuai = (String) session.getAttribute(Constantes.SESSION_NUME_ETA); //N� d'etab
    String libuai = (String) session.getAttribute(Constantes.SESSION_LIB_ETA); //libelle
    String nomEcran = "Calculette de classement (GCALC)";
    
    Calculette cal = null ;
    
    if( request.getSession().getAttribute(ConstantesCalculette.CALCULETTE_KEY) != null )
      cal = (Calculette) request.getSession().getAttribute(ConstantesCalculette.CALCULETTE_KEY);
    else
      {
       cal = new Calculette() ;
       request.getSession().setAttribute(ConstantesCalculette.CALCULETTE_KEY,cal);
      }

    cal.setNomEtablissement(libuai);
    cal.setNumEtablissement(numuai);
    
    simpleLog.println("====== GCALC : cal = [" + cal.getNom()+" "+cal.getPrenom()+" | "+(cal.getLstFonction()!=null?cal.getLstFonction().size()+"":"0")+"fonctions] - "+cal.getNumEtablissement() );
    
    DureeA30Jours tmpDureeA30Jours;
    String tmpNbAnne;
    String tmpNbMois;
    String tmpNbJours;
    Periode tmpPeriode;
%>

<wl:summary>
   <html>
   <head>
   <title>Calculette de classement</title>
   <meta http-equiv="Content-Type"  content="text/html; charset=iso-8859-1">
   <meta http-equiv="Content-Language" content="fr">
   <link href="<%=Constantes.CONTEXT_URI%>/includes/style.css" rel="stylesheet" type="text/css">
   
<style type="text/css">
   
.deleteButton {
  border : 0 ;
  background : url(<%=Constantes.CONTEXT_URI%>/Images/delete.gif) no-repeat top left ;
  width : 16px ;
  height : 16px ;
  cursor : pointer ;
}
   
.libelle{
  background-color: #C0C0C0;
  width: 155;
}
</style>

<script type="text/javascript">

  var compteur = 0 ;
  var fdiv = new Array() ;

  // Tableau des articles
  var arrayA = new Array() ;
<%
  List<ArticleRef> arts = (List<ArticleRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.ARTICLE);
  for (ArticleRef aa : arts)
    {
%>   arrayA["<%=aa.getLibelleCourt()%>"] = {libc:"<%=aa.getLibelleCourt()%>",libl:"<%=aa.getLibelleLong()%>"} ;
<%  }
%>

  //tableau des correspondance fonction / articles
  var arrayFA = new Array() ;
<%
  List<FonctionRef> fff = (List<FonctionRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.FONCTION);
  for (FonctionRef ff : fff)
    {
%>   arrayFA["<%=ff.getLibelleCourt()%>"] = {libc:"<%=ff.getLibelleCourt()%>", libl:"<%=ff.getLibelleLong()%>", arts:[<%
                                                                    List<FonctionArticleRef> lstfid = TableRefHelper.getFonctionArticleByFonctionId(ff.getLibelleCourt());
                                                                    Iterator<FonctionArticleRef> it = lstfid.iterator() ;
                                                                    while(it.hasNext())
                                                                      out.print("\""+it.next().getLibelleCourtArticle()+"\""+(it.hasNext()?",":"") ) ; 
                                                                  %>] } ;
<%  }
%>

  // tableau des valeurs possibles pour le grade d'acceuil
  var arrayGradeAcceuil = new Array() ;       
<%
  HashMap<Long,ArrayList<GradeRef>> aaa = new HashMap<Long,ArrayList<GradeRef>>() ;
  List<GradeRef> lstgrade = (List<GradeRef>) TableRefHelper.getGradeAcceuilRef();
  for(GradeRef grade : lstgrade)
    { 
     if( !aaa.containsKey(grade.getCorpsId()) )
       aaa.put(grade.getCorpsId(),new ArrayList<GradeRef>() ) ;
     aaa.get(grade.getCorpsId()).add(grade) ;
    }
  Iterator<Long> it = aaa.keySet().iterator() ;
  while(it.hasNext())
    {
     Long id = it.next() ;
     int count = 0 ;
%>    arrayGradeAcceuil[<%=id%>] = new Array() ;
<%    for(GradeRef g : aaa.get(id)) 
        { 
%>      arrayGradeAcceuil[<%=id%>][<%=count++%>] = { gradeId:<%=g.getGradeId().intValue() %>, corpsId:<%=g.getCorpsId().intValue() %>, libelleCourt:"<%=g.getLibelleCourt() %>", libelleLong:"<%=g.getLibelleLong() %>", gradeAcceuil:"<%=g.getGradeAcceuil() %>" } ;
<%   }} %>   


  // Tableau de liste des corps
  var arrayCorpsOrigine = new Array() ;
<%
  List<CorpsRef> lco = (List<CorpsRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.CORPS);
  for (CorpsRef tmpCorp : lco)
    {
%>   arrayCorpsOrigine["<%=tmpCorp.getCorpsId()%>"] = {id:"<%=tmpCorp.getCorpsId()%>", libc:"<%=tmpCorp.getLibelleCourt()%>", libl:"<%=tmpCorp.getLibelleLong()%>"} ;
<%  }
%>

  /**
  *
  */
  function updateGrade(selectCorpsId, arrayGrade, eltName){

   var option;	  
   var tableau;

   var selectGrade;
   selectGrade = document.getElementsByName(eltName)[0];
        
   while(selectGrade.hasChildNodes())
     {	   
 	  selectGrade.removeChild(selectGrade.lastChild);	   
     }
 
   corpsId = selectCorpsId;

   if(corpsId == "")
     {
      for (corpId in arrayGrade)
        {
   	     tableau = arrayGrade[corpId];
   	     for(ind in tableau)
           {   	           
            option = document.createElement("option" );
            option.value =tableau[ind].gradeId;
            option.innerHTML = tableau[ind].libelleCourt + " - " + tableau[ind].libelleLong;
            selectGrade.appendChild(option);
           }
        }   	    
     }
   	      	 
   if(corpsId != "")
     {    
      theId = parseInt(corpsId);                         
      tableau = arrayGrade[theId];	  

      if(tableau == null || tableau == undefined)
        return;
      	 
      for(var ii = 0; ii <tableau.length; ii++)
        {
   		 option = document.createElement("option" );
         option.value =tableau[ii].gradeId;
         option.innerHTML = tableau[ii].libelleCourt + " - " + tableau[ii].libelleLong;
         if(ii==0)
           option.selected=true;
         selectGrade.appendChild(option);            
        }   	            
     }   
  }

  var arrayGradeOrigine = new Array();
<%
  aaa = new HashMap<Long,ArrayList<GradeRef>>() ;
  lstgrade = (List<GradeRef>) TableRefHelper.getAllRef(TAB_REF.GRADE);
  for(GradeRef grade : lstgrade)
    { 
     if( !aaa.containsKey(grade.getCorpsId()) )
       aaa.put(grade.getCorpsId(),new ArrayList<GradeRef>() ) ;
     aaa.get(grade.getCorpsId()).add(grade) ;
    }
  it = aaa.keySet().iterator() ;
  while(it.hasNext())
    {
     Long id = it.next() ;
     int count = 0 ;
%>    arrayGradeOrigine[<%=id%>] = new Array() ;
<%    for(GradeRef g : aaa.get(id)) 
       { 
%>      arrayGradeOrigine[<%=id%>][<%=count++%>] = { gradeId:<%=g.getGradeId().intValue() %>, corpsId:<%=g.getCorpsId().intValue() %>, libc:"<%=g.getLibelleCourt() %>", libl:"<%=g.getLibelleLong() %>", gradeAcceuil:"<%=g.getGradeAcceuil() %>" } ;
<%     }}%>   

  var arrayGrilleIndiciaire = new Array();
<%
  HashMap<Long,List<GrilleIndiciaireRef>> mapGrille = new HashMap<Long,List<GrilleIndiciaireRef>>() ;
    
  List<GrilleIndiciaireRef> lstGrille = (List<GrilleIndiciaireRef>) TableRefHelper.getAllRef(TAB_REF.GRILLE_INDICIAIRE);
  for(GrilleIndiciaireRef grille : lstGrille)
    { 
     if( !mapGrille.containsKey(grille.getGradeId()) )
       {
        mapGrille.put(grille.getGradeId(),new ArrayList<GrilleIndiciaireRef>() ) ;
       }
     mapGrille.get(grille.getGradeId()).add(grille) ;
    }
  it = mapGrille.keySet().iterator() ;
  while(it.hasNext())
    {
     Long id = it.next() ;   
     int count = 0 ;
%>    arrayGrilleIndiciaire[<%=id%>] = new Array() ;
<%    for(GrilleIndiciaireRef g : mapGrille.get(id)) { 
%>      arrayGrilleIndiciaire[<%=id%>][<%=count++%>] = { gradeId:<%=g.getGradeId().intValue() %>, grilleIndiciaireId:<%=g.getGrilleIndiciaireId().intValue() %>, chevron:"<%=Conversion.integerToString(g.getChevron())%>", echelon:"<%=Conversion.integerToString(g.getEchelon()) %>", indiceBrut:"<%=Conversion.integerToString(g.getIndiceBrut()) %>", tmpPassage:"<%=Conversion.integerToString(g.getTmpsPassageAnciennete()) %>" } ;
<%   }} %>   
   


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  OnLoad
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  function doOnLoad() {
    //updateGrade(this.value,arrayGradeOrigine,'<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>');
    //updateEchelon(document.getElementsByName('<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>')[0].value);
    //updateChevron(document.getElementsByName('<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>')[0].value);
    //updateIBRDetenu(document.getElementsByName('<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>')[0].value);
<%
    if(!cal.isCalculDone()) {
%>
    hideResult();
<%  }  %>
  }

  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Bloc accueil
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  function updateGradeAccueil(){

       
  }

  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Tab Fonctions
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  function tabfonAddRow() {
      
  if( compteur == 0)
    compteur = parseInt( document.getElementById("nbFonctions").value ) ; 
  
  compteur++;
  document.getElementById("nbFonctions").value = ""+compteur ;
       
  //var rows = document.getElementById("tabfonrows") ;
  var rows = document.getElementById("tabfonrows_body") ;
 
  var trid =  "fonction_"+compteur;
  //alert("index "+ trid);
  var tr = document.createElement("tr") ;
  tr.setAttribute("id" , trid );

  var f_ = document.createElement("input") ;
    f_.type = "hidden" ;
    f_.name = "f_"+compteur ;
    f_.value = "f_"+compteur ;
  tr.appendChild(f_);
  
  for(var i=0;i<21;i++)
    {
     var td = document.createElement("td") ;
     if(i==0)
       {
        var but = document.createElement("input") ;
          but.type="button";
          but.style.border = "0" ;
          but.style.background = "url(<%=Constantes.CONTEXT_URI%>/Images/delete.gif) no-repeat top left" ;
          but.style.width = "16px" ;
          but.style.height = "16px" ;
          but.style.cursor = "pointer" ;
          but.title = "Supprimer cette fonction" ;
          but.onclick = function(){deleteRow(trid);} ;
        td.appendChild(but) ;
       }
     else if(i == 1)
       {        
        generateInput(td,"","dateDeb" +compteur,false,10,"70px");
       }
     else if(i == 2)
       {
        generateInput(td,"","dateFin"+compteur,false,10,"70px");
       }
     else if(i == 3)
       {         
        generateFonctionList(td,compteur,"",50,false);                         
       }
     else if(i == 4)
       {
        generateArticleList(td,compteur,"",30,false);            
       } 
     else if(i == 5)
       {    
        td.nowrap = "nowrap" ;     
        generateInput(td,"100","quotitereelle"+compteur,false,3,"30px","quotite",compteur);   
        generateDivString(td,"%");     
       }
     else if(i == 6) 
       { 
        td.nowrap = "nowrap" ;           
        generateInput(td,"100","quotiteretenue"+compteur,false,3,"30px");    
        generateDivString(td,"%");         
       } 
     else if(i == 7) 
       {                          
        generateInput(td,"","", true,2,"30px");
       } 
     else if(i == 8) 
       {    
        generateInput(td,"","", true,2,"30px");
       } 
     else if(i == 9) 
       { 
        generateInput(td,"","", true,2,"30px");            
       } 
     else if(i == 10) 
       { 
        td.innerHTML=" ";
        tr.appendChild(td);
       }
     else if(i == 11) 
       {         
        generateInput(td,"","", true,2,"30px");
       } 
     else if(i == 12) 
       {
        generateInput(td,"","", true,2,"30px");
       }
     else if(i == 13) 
       {
        generateInput(td,"","", true,2,"30px");     
       } 
     else if(i == 14) 
       { 
        td.innerHTML=" ";
        tr.appendChild(td);
       }
     else if(i == 15)
       { 
        td.nowrap = "nowrap" ;         
        generateInput(td,"","dureeRetenueAA"+compteur, false,2,"30px");
        generateDivString(td,"a");  
       }  
     else if(i == 16)
       {
        td.nowrap = "nowrap" ; 
        generateInput(td,"","dureeRetenueMM"+compteur, false,2,"30px");
        generateDivString(td,"m");  
       } 
     else if(i == 17) 
       {
        td.nowrap = "nowrap" ;
        generateInput(td,"","dureeRetenueJJ"+compteur, false,2,"30px");
        generateDivString(td,"j");  
       }  
     else if(i == 18) 
       {                              
        generateInput(td,"","", true,2,"30px");
       }
     else if(i == 19) 
       {             
        generateInput(td,"","", true,2,"30px");
       }
     else if(i == 20) 
       {    
        generateInput(td,"","", true,2,"30px");                
       } 
     tr.appendChild(td);
    }
   rows.appendChild(tr);          
  }


  function deleteRow(id) {
    var row = document.getElementById(id) ;
    if(row != null)
      {
       var rows = document.getElementById("tabfonrows_body") ;
       rows.removeChild(row);
       hideResult();          
      }
    else
      {
       alert("cannot find row '"+id+"'");
      }      
  }
    

  function generateInput(eltPere,value,name,isDisable,maxLength,sWidth,extra,indx){
    var input = document.createElement("input" );
    input.onchange = function(){hideResult();} ;
    input.name = (name!="")?name:"input_no_name" ;
    input.id = name ;
    input.value = value;      	  
    input.disabled=isDisable;
    if(isDisable)
      input.style.backgroundColor = "#EBEBE4" ;
    if(sWidth != null)
      input.style.width=sWidth ;
    if(maxLength>0)
      {
       input.maxLength=maxLength;
       //input.size=maxLength;
      }
    eltPere.appendChild(input);

    // ajouter le champ en hidden si il est disable
    if(isDisable){
       var f_ = document.createElement("input") ;
       f_.type = "hidden" ;
       f_.name = (name!="")?name:"input_no_name" ;
       f_.value = value ;
       eltPere.appendChild(f_);
    }
    if(extra != null && extra == "quotite")
      {
       input.onkeyup = function(){quotiteFonction('<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()%>'+indx,'<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_RET_PREFIX.getNom()%>'+indx);} ;
      }
  }


  function generateDivString(eltPere,s){
    var p = document.createElement("span" );
    p.innerHTML = ""+s ;
    eltPere.appendChild(p);
  }



  function generateFonctionList(eltPere,index,value,size,selected){
    var hidden = document.createElement("input");
      hidden.type = "hidden" ;
      hidden.name = "id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>"+index ;
      hidden.id = "id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>"+index ;
      hidden.value = "" ;
      
	var select = document.createElement("input");
    var id = "<%=FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom() %>" + index ;
    var said = "<%=FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom() %>" + index ;
    
    select.name=id ;
    select.id = "sf_"+id ;             
    select.style.width="300px";
    select.onkeyup = function(){majFreeDivFonction('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>'+index,'id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>'+index,'fonction_article','sa_'+said);filtreFA('sf'+id,'id_'+id,'sa_'+said);} ;
    select.onblur = function(){closeFreeDiv('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>'+index);} ;
    select.onmousedown = function(){clicFreeDivFonction('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>'+index,'id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()%>'+index,'fonction_article','sa_'+said);} ;
    
	eltPere.appendChild(hidden);
    eltPere.appendChild(select);
  }


  function generateArticleList(eltPere,index,value,size,selected){
    //alert("generateArticleList("+eltPere+","+index+","+value+","+size+","+selected+")");
	var select = document.createElement("select" );	 
    var id =  "<%=FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom() %>" + index;     
	  select.name= id ;
      select.id = "sa_"+id ;   
      //select.style.fontsize= size;
      select.style.width="100px";
      select.onchange = function(e){ checkSelectForObs(this); } ;
       
      opt = document.createElement("option" );	   
  	  opt.value="";
  	  opt.innerHTML="";	   
    select.appendChild(opt);

<%
	List<ArticleRef> lstArticle = (List<ArticleRef>)TableRefHelper.getAllRef(ConstantesRef.TAB_REF.ARTICLE);
	for (ArticleRef arti  :  lstArticle) {  
%>	       	      	   
      opt = document.createElement("option" );	   
      opt.value="<%=arti.getLibelleCourt()%>";   
      opt.innerHTML="<%=arti.getLibelleCourt()%>&nbsp;&nbsp;-&nbsp;&nbsp;<%=arti.getLibelleLong()%>";
      if(opt.value==selected)
        opt.selected = "selected" ;
      select.appendChild(opt);
<%  }  %>

	child = eltPere.firstChild;
	if(child != null)
	  eltPere.replaceChild(select, elt.firstChild);
	else
	   eltPere.appendChild(select);
  }

  

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   Saisie libre - common
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  
  function openFreeDiv(inputid) {
    if(fdiv[inputid] != null)
      {
       fdiv[inputid].style.display = "block" ;
      }
  }
    
  function closeFreeDiv(inputid) {
    if(fdiv[inputid] != null)
      {
       fdiv[inputid].style.display = "none" ;
      }
  }

    
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Saisie libre - Bloc Origine
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * * * * * * * * * CORPS  * * * * * * * * * * * * * * * * * * * * * */

  function keyFreeDivCorpsori() {
    var inputCorps = document.getElementById("<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
    var inputCorpsid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
    var inputGrade = document.getElementById("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
    var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
    var inputIbrEchSup = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>") ;
    var inputIbrDetenu = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>") ;

    inputCorpsid.value = "" ;
    inputGradeid.value = "" ;
    inputIbrEchSup.value = "" ;
    inputIbrDetenu.value = "" ;
    updateIBR() ;
  }
  

  function clicFreeDivCorpsori() {
     
     var inputid = "<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>" ;
     var inputCorps = document.getElementById("<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
     var inputCorpsid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
     var inputGrade = document.getElementById("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
     var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;

     if(fdiv[inputid] != null)
       {
        if( fdiv[inputid].style.display == "block" )
          fdiv[inputid].style.display = "none" ;
        else
          fdiv[inputid].style.display = "block" ;
        return ;
       }
     
     var xy = getAbsolutePos(inputCorps) ;
     var w = inputCorps.clientWidth ;
     var h = inputCorps.clientHeight ;
     
     if(fdiv[inputid] == null)
       {
        fdiv[inputid] = document.createElement("div") ;
        fdiv[inputid].style.position = "absolute" ;
        fdiv[inputid].style.left = xy.x+"px" ;
        fdiv[inputid].style.top = (xy.y+h+4)+"px";
        fdiv[inputid].style.width = (w*2)+"px" ;
        fdiv[inputid].style.height = "200px" ;
        fdiv[inputid].style.border = "1px solid #000000" ;
        fdiv[inputid].style.background = "#FFFFFF" ;
        fdiv[inputid].style.overflow = "auto" ;
        document.body.appendChild(fdiv[inputid]);
       }
     fdiv[inputid].innerHTML = "" ;
     var row = null ;

     for(cc in arrayCorpsOrigine)
       {
        var ccc = arrayCorpsOrigine[cc] ;
        var chaine = ccc.libc + " - " + ccc.libl ;
      
        row = document.createElement("div") ;
        row.style.width = "100%" ;
        row.style.borderBottom = "1px solid #C0C0C0" ;
        row.innerHTML = chaine ;
        row.style.cursor = "pointer" ;
        row.id = cc ;
        row.chaine = chaine ;
        row.onmouseover = function(){this.style.background = "#316AC5";this.style.color = "#FFFFFF";};
        row.onmouseout = function(){this.style.background = "#FFFFFF";this.style.color = "#000000";};
        row.onmousedown = function(){selectFreeDivCorpsori(this.id);};
        fdiv[inputid].appendChild( row ) ;
     }
   
   fdiv[inputid].style.display = "block" ;
  }

  
  function selectFreeDivCorpsori(corpsid) {
	var inputCorps = document.getElementById("<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
	var inputCorpsid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
	var inputGrade = document.getElementById("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputIbrEchSup = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>") ;
	var inputIbrDetenu = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>") ;

	inputCorpsid.value = corpsid ;
	inputCorps.value = arrayCorpsOrigine[corpsid].libc + " - " + arrayCorpsOrigine[corpsid].libl ;

	closeFreeDiv("<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>");
   
    if( arrayGradeOrigine[corpsid] != null && arrayGradeOrigine[corpsid][0] != null )
	  selectFreeDivGradeori(corpsid,0,arrayGradeOrigine[corpsid][0].gradeId);
  }
  
/* * * * * * * * * * * * * * * * * * * * * * * * GRADE  * * * * * * * * * * * * * * * * * * * * * */
  
  function keyFreeDivGradeori() {

    updateIBR() ;
  }

  
  function clicFreeDivGradeori() {
    var inputid = "<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>" ;
	var inputCorpsid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>") ;
	var inputGrade = document.getElementById("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
    
	var xy = getAbsolutePos(inputGrade) ;
	var w = inputGrade.clientWidth ;
	var h = inputGrade.clientHeight ;
	     
	if(fdiv[inputid] == null)
	  {
	   fdiv[inputid] = document.createElement("div") ;
	   fdiv[inputid].style.position = "absolute" ;
	   fdiv[inputid].style.left = xy.x+"px" ;
	   fdiv[inputid].style.top = (xy.y+h+4)+"px";
	   fdiv[inputid].style.width = (w*2)+"px" ;
	   fdiv[inputid].style.height = "200px" ;
	   fdiv[inputid].style.border = "1px solid #000000" ;
	   fdiv[inputid].style.background = "#FFFFFF" ;
	   fdiv[inputid].style.overflow = "auto" ;
	   document.body.appendChild(fdiv[inputid]);
	  }
	else
	  {
	   if( fdiv[inputid].style.display == "block" )
	     {
          fdiv[inputid].style.display = "none" ;
	        return ;
	     } 
	  }
     
    fdiv[inputid].innerHTML = "" ;
	var row = null ;
    var corpsid = inputCorpsid.value ;

    if( corpsid != null && corpsid != "" && arrayGradeOrigine[corpsid] != null )
      {
	   for(cc in arrayGradeOrigine[corpsid] )
	     {
	      var ccc = arrayGradeOrigine[corpsid][cc] ;
	      var chaine = ccc.libc + " - " + ccc.libl ;
	      
	      row = document.createElement("div") ;
	      row.style.width = "100%" ;
	      row.style.borderBottom = "1px solid #C0C0C0" ;
	      row.innerHTML = chaine ;
	      row.style.cursor = "pointer" ;
	      row.id = cc ;
	      row.corpsid = ccc.corpsId ;
          row.gradeid = ccc.gradeId ;
	      row.chaine = chaine ;
	      row.onmouseover = function(){this.style.background = "#316AC5";this.style.color = "#FFFFFF";};
	      row.onmouseout = function(){this.style.background = "#FFFFFF";this.style.color = "#000000";};
	      row.onmousedown = function(){selectFreeDivGradeori(this.corpsid,this.id,this.gradeid);};
	      fdiv[inputid].appendChild( row ) ;
	     }
      }
	fdiv[inputid].style.display = "block" ;
  }


  function selectFreeDivGradeori(corpsid,tabid,gradeid) {
	var inputGrade = document.getElementById("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputIbrEchSup = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>") ;
	var inputIbrDetenu = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>") ;

	inputGradeid.value = gradeid ;
	inputGrade.value = arrayGradeOrigine[corpsid][tabid].libc + " - " + arrayGradeOrigine[corpsid][tabid].libl ;

	closeFreeDiv("<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>");

    if( arrayGrilleIndiciaire[gradeid] != null && arrayGrilleIndiciaire[gradeid][0] != null )
      selectFreeDivEchelonori(gradeid,arrayGrilleIndiciaire[gradeid][0].echelon);
  }


/* * * * * * * * * * * * * * * * * * * * * * * * ECHELON  * * * * * * * * * * * * * * * * * * * * */


  function keyFreeDivEchelonori() {
     
    updateIBR() ;
  }

  
  function clicFreeDivEchelonori() {
	var inputid = "<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>" ;
	var inputEchelon = document.getElementById("<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>") ;
	var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	    
	var xy = getAbsolutePos(inputEchelon) ;
	var w = inputEchelon.clientWidth ;
	var h = inputEchelon.clientHeight ;
	         
	if(fdiv[inputid] == null)
	  {
	   fdiv[inputid] = document.createElement("div") ;
	   fdiv[inputid].style.position = "absolute" ;
	   fdiv[inputid].style.left = xy.x+"px" ;
	   fdiv[inputid].style.top = (xy.y+h+4)+"px";
	   fdiv[inputid].style.width = (w*2)+"px" ;
	   fdiv[inputid].style.height = "200px" ;
	   fdiv[inputid].style.border = "1px solid #000000" ;
	   fdiv[inputid].style.background = "#FFFFFF" ;
	   fdiv[inputid].style.overflow = "auto" ;
	   document.body.appendChild(fdiv[inputid]);
	  }
	else
      {
       if( fdiv[inputid].style.display == "block" )
         {
          fdiv[inputid].style.display = "none" ;
            return ;
         } 
      }
	fdiv[inputid].innerHTML = "" ;
	var row = null ;
	var gradeid = inputGradeid.value ;

	if( gradeid != null && gradeid != "" && arrayGrilleIndiciaire[gradeid] != null )
	  {
       var used = "" ; 
	   for(cc in arrayGrilleIndiciaire[gradeid] )
	     {
	      var ccc = arrayGrilleIndiciaire[gradeid][cc] ;
          if( used.indexOf( ccc.echelon ) == -1 )
            {
	         var chaine = ccc.echelon ;
	          
	         row = document.createElement("div") ;
	         row.style.width = "100%" ;
	         row.style.borderBottom = "1px solid #C0C0C0" ;
	         row.innerHTML = chaine ;
	         row.style.cursor = "pointer" ;
	         row.id = ccc.echelon ;
	         row.gradeid = ccc.gradeId ;
	         row.chaine = chaine ;
	         row.onmouseover = function(){this.style.background = "#316AC5";this.style.color = "#FFFFFF";};
	         row.onmouseout = function(){this.style.background = "#FFFFFF";this.style.color = "#000000";};
	         row.onmousedown = function(){selectFreeDivEchelonori(this.gradeid,this.id);};
	         fdiv[inputid].appendChild( row ) ;
             used += ccc.echelon ;
            }
	     }
	  }
	fdiv[inputid].style.display = "block" ;
  }

  
  function selectFreeDivEchelonori(gradeid,echelon) {
	var inputEchelon = document.getElementById("<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>") ;
	var inputEchelonid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>") ;

	inputEchelonid.value = echelon ;
    inputEchelon.value = echelon ;

	closeFreeDiv("<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>");

	if( arrayGrilleIndiciaire[gradeid] != null && arrayGrilleIndiciaire[gradeid][0] != null )
      for(cc in arrayGrilleIndiciaire[gradeid] )
        if( arrayGrilleIndiciaire[gradeid][cc].echelon == echelon )
		   selectFreeDivChevronori(gradeid,echelon,arrayGrilleIndiciaire[gradeid][cc].chevron );  
  }

/* * * * * * * * * * * * * * * * * * * * * * * * CHEVRON  * * * * * * * * * * * * * * * * * * * * */

  function keyFreeDivChevronori() {
     
    updateIBR() ;
  }

  
  function clicFreeDivChevronori() {
	var inputid = "<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>" ;
	var inputChevron = document.getElementById("<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>") ;
	var inputGradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>") ;
	var inputEchelonid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>") ;
	        
	var xy = getAbsolutePos(inputChevron) ;
	var w = inputChevron.clientWidth ;
	var h = inputChevron.clientHeight ;
	             
	if(fdiv[inputid] == null)
	  {
	   fdiv[inputid] = document.createElement("div") ;
	   fdiv[inputid].style.position = "absolute" ;
	   fdiv[inputid].style.left = xy.x+"px" ;
	   fdiv[inputid].style.top = (xy.y+h+4)+"px";
	   fdiv[inputid].style.width = (w*2)+"px" ;
	   fdiv[inputid].style.height = "200px" ;
	   fdiv[inputid].style.border = "1px solid #000000" ;
	   fdiv[inputid].style.background = "#FFFFFF" ;
	   fdiv[inputid].style.overflow = "auto" ;
	   document.body.appendChild(fdiv[inputid]);
	  }
	else
	  {
	   if( fdiv[inputid].style.display == "block" )
	     {
	      fdiv[inputid].style.display = "none" ;
	        return ;
	     } 
	  }
	fdiv[inputid].innerHTML = "" ;
	var row = null ;
	var gradeid = inputGradeid.value ;
	var echelonid = inputEchelonid.value ;

	if( gradeid != null && gradeid != "" && echelonid != null && echelonid != "" && arrayGrilleIndiciaire[gradeid] != null )
	  {
	   for(cc in arrayGrilleIndiciaire[gradeid] )
	     {
	      var ccc = arrayGrilleIndiciaire[gradeid][cc] ;
	      if( ccc.echelon == echelonid )
	        {
	         var chaine = ccc.chevron ;
	              
	         row = document.createElement("div") ;
	         row.style.width = "100%" ;
	         row.style.borderBottom = "1px solid #C0C0C0" ;
	         row.innerHTML = chaine ;
	         row.style.cursor = "pointer" ;
	         row.id = ccc.chevron ;
	         row.echelonid = ccc.echelon ;
	         row.gradeid = ccc.gradeId ;
	         row.chaine = chaine ;
	         row.onmouseover = function(){this.style.background = "#316AC5";this.style.color = "#FFFFFF";};
	         row.onmouseout = function(){this.style.background = "#FFFFFF";this.style.color = "#000000";};
	         row.onmousedown = function(){selectFreeDivChevronori(this.gradeid,this.echelonid,this.id);};
	         fdiv[inputid].appendChild( row ) ;
	        }
	     }
	  }
	fdiv[inputid].style.display = "block" ;
  }

  
  function selectFreeDivChevronori(gradeid,echelon,chevron) {
	var inputChevron = document.getElementById("<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>") ;
	var inputChevronid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>") ;

	inputChevronid.value = chevron ;
	inputChevron.value = chevron ;

	closeFreeDiv("<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>");

	updateIBR();  
  }

  
/* * * * * * * * * * * * * * * * * * * * * * * * IBR  * * * * * * * * * * * * * * * * * * * * * * */  

  function updateIBR() {
     var corpsid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom()%>").value ;
     var gradeid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom()%>").value ;
     var echelonid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom()%>").value ;
     var chevronid = document.getElementById("id_<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom()%>").value ;
     var inputIbrDetenu = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>") ;
     var inputIbrSup = document.getElementById("<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>") ;

     inputIbrSup.value = "" ;
     
     if(corpsid != null && corpsid != "" &&
    	gradeid != null && gradeid != "" &&
    	echelonid != null && echelonid != "" &&
    	chevronid != null && chevronid != "" )
       {
    	var agi = arrayGrilleIndiciaire[gradeid] ;
        if(agi != null)
          {
           var ibrd = false ; 
           for(cc in agi)
             {
              if( agi[cc].echelon == echelonid && agi[cc].chevron == chevronid )
                {
            	 inputIbrDetenu.value = agi[cc].indiceBrut ;
                 ibrd = true ;
                 break ;  
                }
             }
           if(ibrd)
             {
              var echsup = ""+(parseInt(echelonid)+1) ;
              var eEchSup = null ;
        	  for(dd in agi)
                {
                 if( agi[dd].echelon == echsup && (eEchSup==null||(eEchSup!=null&&eEchSup.chevron>agi[dd].chevron)) )
                   {
                    eEchSup = agi[dd] ;
                   }
                }
              if(eEchSup != null)
        	    inputIbrSup.value = eEchSup.indiceBrut ;
             }
          }
       }
     
  }
   

  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Saisie libre - Fonctions
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
   function majFreeDivFonction(inputid,hiddenid,tab,artid) {

       var data = arrayFA ;          
       var input = document.getElementById(inputid) ;
       var hidden = document.getElementById(hiddenid) ;
       if( input == null ) return ;
       //if( hidden.value == null || hidden.value == "")
       //    input.value = "" ;

       var xy = getAbsolutePos(input) ;
       var w = input.clientWidth ;
       var h = input.clientHeight ;
       
       if(fdiv[inputid] == null)
         {
          fdiv[inputid] = document.createElement("div") ;
          fdiv[inputid].style.position = "absolute" ;
          fdiv[inputid].style.left = xy.x+"px" ;
          fdiv[inputid].style.top = (xy.y+h+4)+"px";
          fdiv[inputid].style.width = ((w+2)*3)+"px" ;
          fdiv[inputid].style.height = "200px" ;
          fdiv[inputid].style.border = "1px solid #000000" ;
          fdiv[inputid].style.background = "#FFFFFF" ;
          fdiv[inputid].style.overflow = "auto" ;
          document.body.appendChild(fdiv[inputid]);
         }
       fdiv[inputid].innerHTML = "" ;
       var row = null ;
       var s = input.value.toLowerCase() ;

       //reset de l'id hidden pour tout faire apparaitre dans la liste des articles
       document.getElementById(hiddenid).value = "" ;
       
       for(cc in data)
         {
          var ccc = data[cc] ;
          var chaine = "" ;

          if( ccc.libc.toLowerCase().indexOf(s) >= 0 || ccc.libl.toLowerCase().indexOf(s) >= 0 )
            {
             var ic = ccc.libc.toLowerCase().indexOf(s) ;
             var il = ccc.libl.toLowerCase().indexOf(s) ;
             var chaine = "" ;
             if(ic>=0)
               chaine += ccc.libc.substring(0,ic)+"<b>"+ccc.libc.substring(ic,ic+s.length)+"</b>"+ccc.libc.substring(ic+s.length) ;
             else 
               chaine += ccc.libc ;
             chaine += "  -  " ;
             if(il>=0)
               chaine += ccc.libl.substring(0,il)+"<b>"+ccc.libl.substring(il,il+s.length)+"</b>"+ccc.libl.substring(il+s.length) ;
             else
               chaine += ccc.libl ;
            }
          else
            {
             chaine = ccc.libc + " - " + ccc.libl ;
            }
          
          row = document.createElement("div") ;
          row.style.width = "100%" ;
          row.style.borderBottom = "1px solid #C0C0C0" ;
          row.innerHTML = chaine ;//ccc.libc+"  -  "+ccc.libl ;
          row.style.cursor = "pointer" ;
          row.id = ccc.libc ;
          row.chaine = chaine ;
          row.onmouseover = function(){this.style.background = "#316AC5";this.style.color = "#FFFFFF";};
          row.onmouseout = function(){this.style.background = "#FFFFFF";this.style.color = "#000000";};
          row.onmousedown = function(){selectFreeDivFonction(inputid,hiddenid,tab,this.id,artid);};
          fdiv[inputid].appendChild( row ) ;
         }
       
       fdiv[inputid].style.display = "block" ;
     }

  function selectFreeDivFonction(inputid,hiddenid,tabid,valueid,artid) {
    //alert("inputid : '"+inputid+"'\nhiddenid : '"+hiddenid+"'\ntabid : '"+tabid+"'\nvalueid : '"+valueid+"'");
    var data = arrayFA ;          
    var input = document.getElementById(inputid) ;
    var hidden = document.getElementById(hiddenid) ;
    if( input == null || hidden == null ) return ;
    if( data[valueid] == null )
      return ;
    hidden.value = valueid ;
    input.value = data[valueid].libc+"  -  "+data[valueid].libl ;
    filtreFA(inputid,hiddenid,artid);
    closeFreeDiv(inputid);
  }

  function clicFreeDivFonction(inputid,hiddenid,tab,artid) {
	if(fdiv[inputid] != null)
      {
	   if( fdiv[inputid].style.display == "block" )
	     fdiv[inputid].style.display = "none" ;
	   else
		 fdiv[inputid].style.display = "block" ;
      }
	else
	  majFreeDivFonction(inputid,hiddenid,tab,artid) ; 
	      
  }


  function filtreFA(fId , hId , aId) {
	    var eF = document.getElementById(hId) ;
	    var eA = document.getElementById(aId) ;
	    var eL = document.getElementById(fId) ;
	    
	    if(eF != null && eA != null)
	      {
	       eA.innerHTML = "" ;
	       var val = eF.value ;
	       if(val != null && val != "")
	         {
	          var efid = val ;
	          if(efid != "" && arrayFA[efid] != null )
	            for(var ii=0;ii<arrayFA[efid].arts.length;ii++)
	              {
	               var ar = arrayFA[val].arts[ii] ;
	               var op = document.createElement("option") ;
	                 op.value = arrayA[ar].libc ;
	                 op.innerHTML = arrayA[ar].libc+"  -  "+arrayA[ar].libl ;
	               eA.appendChild(op);
	              }
	          else
	            for(var key in arrayA)
	              {
	               var op = document.createElement("option") ;
	                 op.value = arrayA[key].libc ;
	                 op.innerHTML = arrayA[key].libc+"  -  "+arrayA[key].libl ;
	               eA.appendChild(op);
	              } 
	         }
	       else
	         {
	          for(var key in arrayA)
	            {
	             var op = document.createElement("option") ;
	               op.value = arrayA[key].libc ;
	               op.innerHTML = arrayA[key].libc+"  -  "+arrayA[key].libl ;
	             eA.appendChild(op);
	            }
	         }
	      }
	    else
	      {
	       alert("input '"+fId+"' or input '"+aId+"' not found");
	      }
	  }


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Observation Services non retenus
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  var iconObs = [] ;

  function checkSelectForObs(select) {
	if( iconObs[select.id] != null )
	  {
	   iconObs[select.id]["obsA"].parentNode.removeChild( iconObs[select.id]["obsA"] ) ;
	   iconObs[select.id]["obsDiv"].parentNode.removeChild( iconObs[select.id]["obsDiv"] ) ;
	   iconObs[select.id] = null ;
	  }
    if(select.value == "Non retenue")
      {
       var divid = "obsdiv_"+select.id ;
    	
       var a = document.createElement("a") ;
         a.innerHTML = "..." ;
         a.obsid = select.id ;
         a.divid = divid ;
         a.onclick = function(e){ showObs(this.divid,true); } ;
         a.style.border = "none" ;
         a.style.paddingLeft = "16px" ;
         a.style.background = "url('<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif') no-repeat left center" ;
         a.style.cursor = "pointer" ;
       select.parentNode.appendChild( a ) ;
       
       var div = document.createElement("div") ;
         div.style.position = "absolute" ;
         div.style.width = "300px" ;
         div.style.height = "155px" ;
         div.style.border = "1px solid #000000" ;
         div.style.backgroundColor = "#d3d4d8" ;
         div.style.display = "none" ;
         div.id = divid ;
         var divSpan = document.createElement("span") ;
           divSpan.innerHTML = "Observations : " ;
         div.appendChild(divSpan);
         div.appendChild( document.createElement("br") );
         var divTextarea = document.createElement("textarea") ;
           divTextarea.rows = "6" ;
           divTextarea.style.width = "100%" ;
           divTextarea.name = "obs_" + select.name ;
         div.appendChild(divTextarea);
         var divDiv = document.createElement("div") ;
           divDiv.style.width = "100%" ;
           divDiv.style.textAlign = "right" ;
           var divDivA = document.createElement("a") ;
             divDivA.innerHTML = "Fermer" ;
             divDivA.style.cursor = "pointer" ;
             divDivA.style.textDecoration = "underline" ;
             divDivA.divid = divid ;
             divDivA.onclick = function(e){ showObs(this.divid,false); } ;
           divDiv.appendChild(divDivA) ;
         div.appendChild(divDiv);
       select.parentNode.appendChild( div ) ;

       iconObs[select.id] = {"obsA":a,"obsDiv":div} ;
      }
    
  }

  function showObs( obsDivId ,show ) {
    for(var s in iconObs)
       if( iconObs[s]["obsDiv"] != null )
    	   iconObs[s]["obsDiv"].style.display = "none" ;
    if( document.getElementById(obsDivId) != null && show )
    	document.getElementById(obsDivId).style.display = "" ;
  }
  
  function majObsBloc(selectId , obsAId , obsDivId ) {
    iconObs[selectId] = {"obsA":document.getElementById(obsAId),"obsDiv":document.getElementById(obsDivId) } ;
  }

  
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Utils
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
var __isIE =  navigator.appVersion.match(/MSIE/);  
var __userAgent = navigator.userAgent;  
var __isFireFox = __userAgent.match(/firefox/i);  
var __isFireFoxOld = __isFireFox && (__userAgent.match(/firefox\/2./i) || __userAgent.match(/firefox\/1./i));  
var __isFireFoxNew = __isFireFox && !__isFireFoxOld;  

function __parseBorderWidth(width) {  
    var res = 0;  
    if (typeof(width) == "string" && width != null && width != "" ) {  
        var p = width.indexOf("px");  
        if (p >= 0) 
          res = parseInt(width.substring(0, p));  
        else  
          res = 1;   
    }  
    return res;  
}  
  
function __getBorderWidth(element) {  
    var res = new Object();  
    res.left = 0; res.top = 0; res.right = 0; res.bottom = 0;  
    if (window.getComputedStyle) {  
        //for Firefox  
        var elStyle = window.getComputedStyle(element, null);  
        res.left = parseInt(elStyle.borderLeftWidth.slice(0, -2));    
        res.top = parseInt(elStyle.borderTopWidth.slice(0, -2));    
        res.right = parseInt(elStyle.borderRightWidth.slice(0, -2));    
        res.bottom = parseInt(elStyle.borderBottomWidth.slice(0, -2));    
    }  
    else {  
        //for other browsers  
        res.left = __parseBorderWidth(element.style.borderLeftWidth);  
        res.top = __parseBorderWidth(element.style.borderTopWidth);  
        res.right = __parseBorderWidth(element.style.borderRightWidth);  
        res.bottom = __parseBorderWidth(element.style.borderBottomWidth);  
    }  
    return res;  
}  

function getAbsolutePos(element) {  
    var res = new Object();  
    res.x = 0; res.y = 0;  
    if (element !== null) {  
        res.x = element.offsetLeft;  
        res.y = element.offsetTop;  
          
        var offsetParent = element.offsetParent;  
        var parentNode = element.parentNode;  
        var borderWidth = null;  
  
        while (offsetParent != null) {  
            res.x += offsetParent.offsetLeft;  
            res.y += offsetParent.offsetTop;  
              
            var parentTagName = offsetParent.tagName.toLowerCase();   
  
            if ((__isIE && parentTagName != "table") || (__isFireFoxNew && parentTagName == "td")) {              
                borderWidth = __getBorderWidth(offsetParent);  
                res.x += borderWidth.left;  
                res.y += borderWidth.top;  
            }  
              
            if (offsetParent != document.body && offsetParent != document.documentElement) {  
                res.x -= offsetParent.scrollLeft;  
                res.y -= offsetParent.scrollTop;  
            }  
  
            //next lines are necessary to support FireFox problem with offsetParent  
            if (!__isIE) {  
                while (offsetParent != parentNode && parentNode !== null) {  
                    res.x -= parentNode.scrollLeft;  
                    res.y -= parentNode.scrollTop;  
                    /* T430
                    if (__isFireFoxOld) {  
                        borderWidth = kGetBorderWidth(parentNode);  
                        res.x += borderWidth.left;  
                        res.y += borderWidth.top;  
                    }
                    */ 
                    parentNode = parentNode.parentNode;  
                }      
            }  
  
            parentNode = offsetParent.parentNode;  
            offsetParent = offsetParent.offsetParent;  
        }  
    }  
    return res;  
} 



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Controle Data
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * traitement appele a chaque submit
 *  => mise a jour du champ cache pour avoir le nombre de fonctions
 */
function doSubmit(elt){
                                   
  var oRows = document.getElementById('tabfonrows').getElementsByTagName('tr');
  var iRowCount = oRows.length ;

  hiddenElt = document.getElementsByName('nbFonctions')[0];
  hiddenElt.value=iRowCount;              
     /*var inputform = document.getElementById("calculette");
     inputform.submit();*/
     return true;  
}

 
/**
 *  fonction pour controle de surface de la saisie des champs
 *  pour l'outil de calcul de la duree
 */
function doCtrlDateCalcul(){
  /*elt = document.getElementsByName("calDateDeb")[0];    
  name = elt.name;
  val=elt.value;     

  if((val == undefined) || (val == ""))
    {
     alert("La date de d�but du calcul est obligatoire");
     return false;
    }
     
  if(!verif_date(elt,"date de d�but"))
    {
     return false;
    }

  elt = document.getElementsByName("calDateFin")[0];    
  name = elt.name;
  val=elt.value;     

  if((val == undefined) || (val == ""))
    {
     alert("La date de fin du calcul est obligatoire");
     return false;
    }
     
  if(!verif_date(elt,"date de fin"))
    {
     return false;
    }*/
}


/**
 * action appelee lorsque l'user modifie une valeur au niveau de la calculette
 * controle de surface
 */
function ctrlData(){  
 /* elt = document.getElementsByName("nom")[0];
  name = elt.name;
  val=elt.value;
      
  if((val == undefined) || (val == ""))
    {
     alert('Le champ nom est obligatoire');
     return false;
    }
 
  if(typeof val != "string")
    {
     alert('Le champ nom doit �tre une cha�ne de caract�re');
     return false; 
    }
  elt = document.getElementsByName("prenom")[0];
  name = elt.name;
  val=elt.value;
          
  if((val == undefined) || (val == ""))
    {
     alert('Le champ pr�nom est obligatoire');
     return false;
    }
 
  if(typeof val != "string")
    {
     alert('Le champ prenom doit �tre une cha�ne de caract�re');
    }

  elt = document.getElementsByName("datenais")[0];      
  name = elt.name;
  val=elt.value;     

  if((val == undefined) || (val == ""))
    {
     alert("La date de naissance est obligatoire");
     return false;
    }

  elt = document.getElementsByName("datenomin")[0];      
  name = elt.name;
  val=elt.value;     

  if((val == undefined) || (val == ""))
    {
     alert("La date de nomination est obligatoire");
     return false;
    }
 
  if(!verif_date(elt,"date de nomination"))
    {
     return false;
    }
          
  elt = document.getElementsByName("corpsacc")[0];
  name = elt.name;
  val=elt.value;

  if((val == undefined) || (val == ""))
    {
	    
	  alert("Le champ corps d'acceuil est obligatoire");
     return false;
    }

  elt = document.getElementsByName("gradeacc")[0];
  name = elt.name;
  val=elt.value;

  if((val == undefined) || (val == ""))
    {
     alert("Le champ grade d'acceuil est obligatoire");
     return false;
    }

  var oRows = document.getElementById("tabfonrows").getElementsByTagName('tr');
  var iRowCount = oRows.length  ;

  for(var ii = 0; ii <iRowCount; ii++)
    {
     elt = document.getElementsByName("dateDeb" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if(!verif_date(elt,'date de d�but'))
             {
              return false;
             }
          }else{
        	   alert("Le champ date d�but est de format JJ/MM/AAAA");
      	       return false;
          }
       }
     elt = document.getElementsByName("dateFin" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;       
        if((val != undefined) && (val != ""))
          {
           if(!verif_date(elt,'date de fin'))
             {
              return false;
             }
          }else{
        	   alert("Le champ date fin est de format JJ/MM/AAAA");
       	      return false;
          }
       }
     elt = document.getElementsByName("quotitereelle" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if( isNaN(val)|| val<0 || val >100)
             {
              alert("Les champs quotit� sont des entiers compris entre 0 et 100");
              return false;
             }
          }
       }
     elt = document.getElementsByName("quotiteretenue" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if( isNaN(val) || val<0 || val >100)
             {
              alert("Les champs quotit� sont des entiers compris entre 0 et 100");
              return false;
             }
          }
       }
     elt = document.getElementsByName("dureeRetenueAA" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if( isNaN(val) || val<0 || val >100)
             {
              alert("Le champ dur�e retenue (AA) est un entier");
              return false;
             }
          }
       }
     elt = document.getElementsByName("dureeRetenueMM" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if(isNaN(val) || val<0 || val >12)
             {
              alert("Le champ dur�e retenue (MM) est un entier compris entre 0 et 11");
              return false;
             }
          }
       }
     elt = document.getElementsByName("dureeRetenueJJ" + ii)[0];
     if(elt != undefined)
       {
        val=elt.value;
        if((val != undefined) && (val != ""))
          {
           if( isNaN(val) || val<0 || val >30)
             {
              alert("Le champ dur�e retenue (JJ) est un entier compris entre 0 et 29");
              return false;
             }
          }
       }
    }
  */
 return true;   
}


/**
 * controle de format pour les dates
 */
function verif_date(elt, nomChamp) {
   
  var reg=new RegExp("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$","g");
  value = elt.value;
  if ( (elt !="") && (elt.value != null))
    {
     if (!reg.test(value))
       {
       
     alert ("Le format du champ " + nomChamp + " doit �tre une date valide (format jj/mm/aaaa).");
        return false;
       }                   
    }
 return true;         
}


function hideResult(){
  tmpElt = document.getElementById("resultat");
  tmpElt.style.display = 'none';
  tmpElt = document.getElementById("libDetailCalcul");
  tmpElt.style.display = 'none';
  tmpElt = document.getElementById("detailCalcul");
  tmpElt.style.display = 'none';
  tmpElt = document.getElementById("btEdit");
  tmpElt.style.display = 'none'; 
}


  function quotiteFonction(idReel,idRet){
    var tmp = document.getElementById(idReel).value;
	document.getElementById(idRet).value = tmp;
  }



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Upload
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function recupInfos() {
  var div = document.getElementById("divRecupInfos") ;
  if(div != null)
    div.style.display = "block" ;
}



      
</script>
</head>

   <body bgcolor="#FFFFFF" text="#000000" onload="doOnLoad();">
   <!-- table all -->
   <!--   table width="100%" border="0" cellspacing="0" cellpadding="0"-->
   <table width="100%" border="0" >
      <tr>
          <!--  Logo -->
          <td width="20" valign="top" background="<%= Constantes.CONTEXT_URI %>/Images/fond_1bleu_bis.gif" height="389">            
             <table width="100%" border="0">
                 <tr>
                    <td height="56" valign="top">
                    <img src="<%=Constantes.CONTEXT_URI%>/Images/logo.gif" width="115" height="46" border="0" alt="logo_antares">
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                    <img src="<%=Constantes.CONTEXT_URI%>/Images/bandeau_vertical.gif" width="115"   border="0">
                    </td>
                </tr>
            </table>          
          </td>
          <td width="1" height="489">&nbsp;</td>
          <!-- corps de l'ecran -->
          <td valign="top">
            <!-- table du corps -->
            
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top">
                       <span class="titrePage">
                           <b><%=nomEcran%> </b> 
                       </span>
                   </td>
               </tr>
               <tr>
                  <td colspan="2" >
                   <img src="<%=Constantes.CONTEXT_URI%>/Images/barre2.gif" width="444" height="6"/>
                  </td> 
               </tr> 
              <tr>
                  <td colspan="2" align="right">
                    <a target="_blank" href="<%=getSessionURL("/eta/calc/gcalc_ctrl.jsp?a=reinit", response)%>" >R&eacute;initialiser</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=getSessionURL("/eta/calc/gcalc_ctrl.jsp?a=quit", response)%>">Quitter</a>    
                  </td>
               </tr>
               
               <tr>
                  <td colspan="2">&nbsp;</td>
               </tr>
               <tr>
                  <td colspan="2">Vous pouvez consulter le guide d'utilisation en <a href="https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/guidanta/guide_calculette.pdf">cliquant ici</a>.</td>
               </tr>
               <tr>
                  <td colspan="2" >&nbsp;</td>
               </tr>
               <!--  zone message d'erreur utilisateur -->
<%
               String theStyle = "";
               boolean isMessage = false;
               LEVEL messageLevel = cal.getUserMessage().getLevel();  
               if(messageLevel.isUserErrorLevel() )
                 { 
                  theStyle = "erreur";
                  isMessage = true;
                 }
               else if(messageLevel.isWarningLevel() )
                 {
                  theStyle = "warning";
                  isMessage = true;
                 }
                      
               if(isMessage) {
%>
               <tr> 
                 <td colspan="2" align="left">
                   <span class="<%=theStyle%>" >
                     <%= cal.getUserMessage().getLabel()%>
                   </span>
                 </td>
               </tr>
<%             } %>  
                    <tr>
                <td colspan="2" align="left">
                            <% if (cal.message()!=null && cal.message().equals(Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_4)) {%> 
                         <span  class="erreur"><%=Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_4 %></span> 
                         
                         </td>
                         </tr>
                         
                         <%}%>
                          <tr>
                         <td colspan="2" align="left">
                            <%  if (cal.message()!=null && cal.message().equals(Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_8)) {%> 
                         <span  class="erreur"><%=Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_8 %></span> 
                         
                         </td>
                         </tr>
                         <%}%>
                          <tr>
                         <td colspan="2" align="left">
                            <%  if (cal.message()!=null && cal.message().equals(Messages.DUREE_RETENUE_MANDATORY_FEV_WITH_3)) {%> 
                         <span  class="erreur"><%=Messages.DUREE_RETENUE_MANDATORY_FEV_WITH_3 %></span> 
                         
                         </td>
                         </tr>
                         <%}%>
               <tr>
               <!-- infos etab -->
               <tr>
                  <td colspan="2" align="left" height="14">
                     <img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" width="14" height="14"/>
                     <b>Informations de l'&eacute;tablissement</b>
                  </td>
               </tr>
               <tr>
                  <td class="libelle">N� UAI (ex RNE) :</td>
                  <td bgcolor="#dfdfdf"><%=cal.getNumEtablissement()%></td>
               </tr>
               <tr>
                  <td class="libelle" nowrap="nowrap" >Nom de l'&eacute;tablissement :</td>
                  <td bgcolor="#dfdfdf"><%=cal.getNomEtablissement()%></td>
               </tr>
               <tr>
                  <td colspan="2" >&nbsp;</td>
               </tr>
               <tr>
                  <td align="left" height="14" colspan="2">
                   <img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" width="14" height="14"/>
                   <b>Informations g&eacute;n&eacute;rales sur l'enseignant-chercheur</b>
                  </td>
               </tr>
               
<!--  RECUP INFOS -->

               <tr>
                 <td colspan="2">  
                   <form id="calculette_upload" 
                         name="calculette"
                         method="POST"
                         enctype="multipart/form-data"
                         action="<%=getSessionURL(CALCULETTE_FORWARD.CALCULETTE_CTRL.getForward()+"?a=recuperer", response)%>" >      
                     <input type="button" onclick="recupInfos();" value="R&eacute;cup&eacute;rer les informations" />
                     <br />
                     <div id="divRecupInfos" style="display:none;">
                       <input type="file" name="recupinfo" value="Parcourir"/>
                       <br />
                       <input type="submit" name="<%=FORM_CALCULETTE_ACTION.RECUPERER.getNom() %>" value="Valider" />
                     </div>
                   </form>  
                 </td>
               </tr>


<!--  NOM   PRENOM   NAISSANCE-->
          
               <tr>
               <td colspan="2">
               <form id="calculette" 
                     name="calculette"
                     method="POST"
                     action="<%=getSessionURL(CALCULETTE_FORWARD.CALCULETTE_CTRL.getForward(), response)%>"
                     onreset="hideResult();" 
                     onSubmit="return doSubmit(this);" >   
               <table width="100%" border="0" height="100%">
                
               <tr>
                 <td class="libelle" >Nom * :</td>
                 <td bgcolor="#DFDFDF" >                  
                   <input maxlength="25" size="25" name="<%=FORM_CALCULETTE_NAME.NOM.getNom() %>" value="<%=cal.getNom()%>"  />
                 </td>
               </tr>
               <tr>
                 <td class="libelle" >Pr&eacute;nom * :</td>
                 <td bgcolor="#DFDFDF">
                   <input maxlength="25" size="25" name="<%=FORM_CALCULETTE_NAME.PRENOM.getNom() %>" value="<%=cal.getPrenom()%>" />
                 </td>
               </tr>
               <tr>
                 <td class="libelle" nowrap="nowrap" >Date de naissance * :</td>
                 <td bgcolor="#DFDFDF">
                   <input maxlength="10" size="10" name="<%=FORM_CALCULETTE_NAME.DT_NAISS.getNom() %>" value="<%=Conversion.formatDate(cal.getDateNaissance(), Constantes.DATE_FORMAT_STRING)%>" />
                 </td>                     
               </tr>
               <tr><td colspan="2">&nbsp;</td></tr>
               
               
<!--  BLOC ACCUEIL -->

				<tr>
                 <td class="libelle" nowrap="nowrap" >Date de nomination * :</td>
                 <td bgcolor="#DFDFDF">
                   <input maxlength="10" size="10" name="<%=FORM_CALCULETTE_NAME.DT_NOMIN.getNom() %>" value="<%=Conversion.formatDate(cal.getDateNomination(), Constantes.DATE_FORMAT_STRING)%>" />
                 </td>                     
               </tr>

               <tr>
                 <td class="libelle" nowrap="nowrap" >Corps d'accueil * :</td>
                  <td bgcolor="#DFDFDF">
                    <select name="<%=FORM_CALCULETTE_NAME.CORPS_ACCEUIL.getNom() %>"  
                            onchange="hideResult();updateGrade(this.value,arrayGradeAcceuil,'<%=FORM_CALCULETTE_NAME.GRADE_ACCEUIL.getNom()%>');"
                            style="width:600px;" >
                      <option></option>                        
<%
                      List<CorpsRef> lstCorps = TableRefHelper.getCorpsAcceuilRef();
                      for (CorpsRef tmpCorp : lstCorps)
                        {    
                         if( cal.getCorspAcceuilId() != null &&  cal.getCorspAcceuilId().longValue() == tmpCorp.getCorpsId().longValue() )
                           {
                       	   out.println("<option value=\"" + tmpCorp.getCorpsId()  + "\" selected > " + tmpCorp.getLibelleCourt()+ " - "+ tmpCorp.getLibelleLong() + "</option>");
                           } 
                         else 
                           {
                            out.println("<option value=\"" + tmpCorp.getCorpsId()  + "\"> " + tmpCorp.getLibelleCourt()+ " - "+ tmpCorp.getLibelleLong()  + "</option>");
                           }                             
                        }
%>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="libelle" nowrap="nowrap" >Grade d'accueil * :</td>
                  <td bgcolor="#DFDFDF">
                    <select name="<%=FORM_CALCULETTE_NAME.GRADE_ACCEUIL.getNom() %>"  
                            onchange="hideResult();" 
                            style="width:600px;" >
                        <option></option>
<%
                          List<GradeRef> lstGrade = TableRefHelper.getGradeAcceuilRef();
                          for (GradeRef tmpGrade : lstGrade)
                            {
                             if( cal.getGradeAcceuilId() != null &&  cal.getGradeAcceuilId().longValue() == tmpGrade.getGradeId().longValue() )
                               {
                            	out.println("<option value=\"" + tmpGrade.getGradeId() + "\" selected> " + tmpGrade.getLibelleCourt()+ " - "+ tmpGrade.getLibelleLong() + "</option>");
                               }
                             else 
                               {
                            	out.println("<option value=\"" + tmpGrade.getGradeId() + "\"> " + tmpGrade.getLibelleCourt()+ " - "+ tmpGrade.getLibelleLong() + "</option>");
                               }
                            }                              
%>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                </tr>
                
<!--  BLOC ORIGINE -->


                <tr>
                  <td class="libelle" nowrap="nowrap" >Corps d'origine :</td>
                  <td bgcolor="#DFDFDF">
                  
<%
                  String corpsOriId      = "" ;
                  String corpsOriLibelle = cal.getCorpsOrigineLibelleOrId() ;

                  lstCorps = (List<CorpsRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.CORPS);
                  for (CorpsRef tmpCorp : lstCorps)
                     {
                      if( corpsOriLibelle != null && corpsOriLibelle.equals(Conversion.longToString(tmpCorp.getCorpsId())) )                             
                         {
                          corpsOriId = Conversion.longToString(tmpCorp.getCorpsId()) ;
                          corpsOriLibelle = tmpCorp.getLibelleCourt() + " - " + tmpCorp.getLibelleLong() ;
                         }                          
                     }
                  
%>
                  <input type="hidden"
                         name="id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                         id="id_<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                         value="<%=corpsOriId %>" />
                         
                  <input name="<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                         id="<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                         value="<%=corpsOriLibelle %>"
                         onkeyup="keyFreeDivCorpsori();"
                         onmousedown="clicFreeDivCorpsori();"
                         onblur="closeFreeDiv('<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>');"
                         style="width:500px;" />
                  <!--
                  <select name="<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                          id="<%=FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom() %>" 
                          onchange="hideResult();updateGradeOri();updateEchelonOri();updateChevronOri();updateIBRDetenu();"
                          style="width:600px;" >
                    <option></option>
                        <%
                          // lstCorps = (List<CorpsRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.CORPS);
                          // for (CorpsRef tmpCorp : lstCorps)
                          //   {
                          //    if( cal.getCorpsOrigineLibelleOrId() != null  && cal.getCorpsOrigineLibelleOrId().equals(Conversion.longToString(tmpCorp.getCorpsId())) )                             
                          //       out.println("<option value=\"" + tmpCorp.getCorpsId()  + "\" selected> " + tmpCorp.getLibelleCourt() + " - " + tmpCorp.getLibelleLong() + "</option>");
                          //    else 
                          //       out.println("<option value=\"" + tmpCorp.getCorpsId()  + "\"> " + tmpCorp.getLibelleCourt() + " - " + tmpCorp.getLibelleLong() + "</option>");                          
                          // }
                        %>
                    </select>
                   -->
                  </td>
                </tr>
                <tr>
                  <td class="libelle" nowrap="nowrap" >Grade d'origine :</td>
                  <td bgcolor="#DFDFDF">
                  
<%
                  String gradeOriId      = "" ;
                  String gradeOriLibelle = cal.getGradeOrigineLibelleOrId() ;

                  lstGrade = (List<GradeRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.GRADE);
                  for (GradeRef tmpGrade : lstGrade)
                    {
                     if( gradeOriLibelle != null && gradeOriLibelle.equals(Conversion.longToString(tmpGrade.getGradeId())) )                             
                       {
                        gradeOriId = Conversion.longToString(tmpGrade.getGradeId()) ;
                        gradeOriLibelle = tmpGrade.getLibelleCourt() + " - " + tmpGrade.getLibelleLong() ;
                       }                          
                    }              
%>
                  <input type="hidden"
                         name="id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>" 
                         id="id_<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>" 
                         value="<%=gradeOriId %>" />
                  
                  <input name="<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>" 
                         id="<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>" 
                         value="<%=gradeOriLibelle %>"
                         onkeyup="keyFreeDivGradeori();"
                         onmousedown="clicFreeDivGradeori();"
                         onblur="closeFreeDiv('<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>');"
                         style="width:500px;" />       
                         
                  <!--  
                    <select name="<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>"
                            id="<%=FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom() %>"  
                            onchange="hideResult();updateEchelonOri();updateChevronOri();updateIBRDetenu();"
                            style="width:600px;" >
                      <option></option>
<%
                      //lstGrade = (List<GradeRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.GRADE);
                      //for (GradeRef tmpGrade : lstGrade)
                      // {
                      //  if( cal.getGradeOrigineLibelleOrId() != null  &&  cal.getGradeOrigineLibelleOrId().equals(Conversion.longToString(tmpGrade.getGradeId())) )
                      //    {                              
                      //	 out.println("<option value=\"" + tmpGrade.getGradeId() + "\" selected > " + tmpGrade.getLibelleCourt() +" - "+ tmpGrade.getLibelleLong() + "</option>");
                      //    }
                      //  else if( cal.getCorpsOrigineLibelleOrId() == null || cal.getCorpsOrigineLibelleOrId().equals(tmpGrade.getCorpsId()) )
                      //    {
                      //	 out.println("<option value=\"" + tmpGrade.getGradeId() + "\"> " + tmpGrade.getLibelleCourt() + " - "+ tmpGrade.getLibelleLong() + "</option>");
                      //    }                             
                      //  }
%>
                    </select>
                    -->
                  </td>
                </tr>
                <tr>
                  <td class="libelle" nowrap="nowrap">Echelon d'origine :</td>
                  <td bgcolor="#DFDFDF">
<%                              
                  String tmpStr = Conversion.integerToString(cal.getEchelonOrigine());                             
%>
                  <input type="hidden"
                         name="id_<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                         id="id_<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                         value="<%=tmpStr%>" />
                  
                  <input name="<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                         id="<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                         value="<%=tmpStr %>"
                         onkeyup="keyFreeDivEchelonori();"
                         onmousedown="clicFreeDivEchelonori();"
                         onblur="closeFreeDiv('<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>');"
                         style="width:50px;" />
 
                    <!--    
                    <select name="<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                            id="<%=FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom() %>" 
                            onchange="hideResult();updateChevronOri();updateIBRDetenu();">
<%
                          //if(cal.getGradeOrigineLibelleOrId() != null && mapGrille.get(cal.getGradeOrigineLibelleOrId())!=null )
                          //  {
                          //   for(GrilleIndiciaireRef g : mapGrille.get(cal.getGradeOrigineLibelleOrId())) 
                          //     {
                          //      if( tmpStr != null && !tmpStr.equals("") && tmpStr.equals(Conversion.integerToString(g.getEchelon())) )
                          //        out.println("<option value=\"" + Conversion.integerToString(g.getEchelon()) + "\" selected > " + Conversion.integerToString(g.getEchelon()) + "</option>");
                          //      else
                          //        out.println("<option value=\"" + Conversion.integerToString(g.getEchelon()) + "\"> " + Conversion.integerToString(g.getEchelon()) + "</option>");
                          //     }
                          //  }
%>
                    </select>
                    -->
                  </td>
                </tr>
                <tr>
                     <td class="libelle" nowrap="nowrap" >Chevron d'origine :</td>
                     <td bgcolor="#DFDFDF">
<% 
                         tmpStr = Conversion.integerToString(cal.getChevronOrigine());                               
%>

                  <input type="hidden"
                         name="id_<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>" 
                         id="id_<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>" 
                         value="<%=tmpStr%>" />
                  
                  <input name="<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>" 
                         id="<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>" 
                         value="<%=tmpStr %>"
                         onkeyup="keyFreeDivChevronori();"
                         onmousedown="clicFreeDivChevronori();"
                         onblur="closeFreeDiv('<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>');"
                         style="width:50px;" />
                      
                      <!--  
                     <select name="<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>"
                             id="<%=FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom() %>"
                             onchange="hideResult();updateIBRDetenu();">
<% 
                        //if(cal.getGradeOrigineLibelleOrId() != null && mapGrille.get(cal.getGradeOrigineLibelleOrId())!=null )
                        //  {
                        //   for(GrilleIndiciaireRef g : mapGrille.get(cal.getGradeOrigineLibelleOrId())) 
                        //     {
                        //      if( tmpStr != null && !tmpStr.equals("") && tmpStr.equals(Conversion.integerToString(g.getChevron())) )
                        //        out.println("<option value=\"" + Conversion.integerToString(g.getChevron()) + "\" selected > " + Conversion.integerToString(g.getChevron()) + "</option>");
                        //      else
                        //        out.println("<option value=\"" + Conversion.integerToString(g.getChevron()) + "\"> " + Conversion.integerToString(g.getChevron()) + "</option>");
                        //     }
                        //  }
%>
                     </select>
                     --> 
                     </td>
                </tr>
                
                <tr>
                     <td class="libelle"  nowrap="nowrap" >Indice brut d&eacute;tenu :</td>
                     <td bgcolor="#DFDFDF">
<% 
                           tmpStr = Conversion.integerToString(cal.getIndiceBrutDetenue());
%>
                          <input maxlength="5" size="5" 
                                 name="<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>" 
                                 id="<%=FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom()%>" 
                                 value="<%=tmpStr %>" 
                                 onchange="hideResult();"/>
                        </td>
                </tr>
                <tr>
                     <td class="libelle"  nowrap="nowrap" >Indice brut &eacute;chelon sup&eacute;rieur :</td>
                     <td bgcolor="#DFDFDF">
<% 
                              tmpStr = Conversion.integerToString(cal.getIndiceBrutEchelonSup());
%>
                          <input maxlength="5" size="5" 
                                 name="<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>" 
                                 id="<%=FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom()%>" 
                                 value="<%=tmpStr %>" 
                                 onchange="hideResult();"/> 
                        </td>
                </tr>
                <tr> <td colspan="2" >&nbsp;</td> </tr>
                  
                <tr>
                     <td colspan="2" align="left" height="14">
                        <img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" width="14" height="14">
                        <b>Fonctions exerc&eacute;es</b>
                      </td>
                </tr>
                  
                <!-- Tableau des fonctions exercees -->
                <tr>
                  <td colspan="2" >
                    <table id="tabfonrows" border="0">
                      <thead>
                            <tr class="tabheader" bgcolor="#C0C0C0" >
                              <td>&nbsp;</td>
                              <td  colspan="2" class="entete_tableau">P&eacute;riode <br/> Du&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Au</td>
                              <td  class="entete_tableau">Fonction</td>
                              <td  class="entete_tableau">Article</td>
                              <td  colspan="2" class="entete_tableau">Quotit&eacute; <br/>R&eacute;elle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Retenue</td>
                              <td  colspan="11" class="entete_tableau">Dur&eacute;e (aa,mm,jj) <br/> Prise&nbsp;en&nbsp;compte&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Article&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Retenue</td>
                              <td  colspan="3" class="entete_tableau">R&eacute;sultat (aa,mm,jj)</td>                                          
                           </tr>
                         </thead>
                         <tbody id="tabfonrows_body" >
<%
                           int indx = 0;
                           for(Fonction fonc : cal.getLstFonction() ) {
                             indx=indx+1;                     
%>
                            <tr id="fonction_<%=indx%>">
                               <td>
                                  <input type="button" class="deleteButton" title="Supprimer cette fonction" onClick="deleteRow('fonction_<%=indx %>');" />&nbsp;
                                  <input type="hidden" name="f_<%=indx %>" value="f_<%=indx %>" /> 
                              </td>
                              <td>
<%                            tmpStr =  (fonc.getPeriode()!=null)?fonc.getPeriode().getStrDateDebut():"";    
%>
                                <input maxlength="10" style="width:70px;" name="<%= FORM_CALCULETTE_NAME.FONC_DT_DEB_PREFIX.getNom() + indx%>" value="<%=tmpStr%>" onchange="hideResult();"/>                            
                              </td>
                              <td>
<%                            tmpStr = (fonc.getPeriode()!=null)?fonc.getPeriode().getStrDateFin():"";
%>
                                <input maxlength="10" style="width:70px;" name="<%=  FORM_CALCULETTE_NAME.FONC_DT_FIN_PREFIX.getNom()+indx%>" value="<%=tmpStr%>" onchange="hideResult();"/>
                              </td>                          
                               <td>
                               <%
                                 String ttc = (fonc.getCodeFonction()!=null&&!fonc.getCodeFonction().equals(""))?fonc.getCodeFonction():"" ;
                                 String ttl = "" ;
                                 if( !ttc.equals("") )
                                   {
                                    List<FonctionRef> lstFonc = (List<FonctionRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.FONCTION);
                                    for (FonctionRef tmpFonctionRef : lstFonc)
                                      if( fonc.getCodeFonction() != null  && fonc.getCodeFonction().equals(tmpFonctionRef.getLibelleCourt()) )
                                        ttl = tmpFonctionRef.getLibelleCourt() + " - " + tmpFonctionRef.getLibelleLong() ; 
                                   }
                                 else
                                   ttl = fonc.getLibelleFonction() ;
                               %>
                               <input type="hidden" 
                                      id="id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>" 
                                      name="id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>" 
                                      value="<%=ttc%>" />
                               
                               <input name="<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>"  
                                      onkeyup="majFreeDivFonction('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','fonction_article','sa_<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>');filtreFA('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','sa_<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>');hideResult();"
                                      onblur="closeFreeDiv('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>');"
                                      onmousedown="clicFreeDivFonction('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','id_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','fonction_article','sa_<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>');hideResult();"
                                      id="sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>"
                                      style="width:300px;"
                                      value="<%=ttl %>" >
                               <!-- 
                                 <select name="<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>"  
                                         onchange="majfreediv('sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','fonction_article');filtreFA('sf_<%=FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>','sa_<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>');hideResult();"
                                         id="sf_<%= FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+indx%>"
                                         style="width:300px;">
                                    <option value=""></option>
<%                                  //List<FonctionRef> lstFonc = (List<FonctionRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.FONCTION);
                                    //for (FonctionRef tmpFonctionRef : lstFonc)
                                    //  {
                                    //   if( fonc.getCodeFonction() != null  && fonc.getCodeFonction().equals(tmpFonctionRef.getLibelleCourt()) )
                                    //     out.println("<option value=\"" + tmpFonctionRef.getLibelleCourt() + "\" selected >" + tmpFonctionRef.getLibelleCourt() +"&nbsp;&nbsp;-&nbsp;&nbsp;"+tmpFonctionRef.getLibelleLong() + "</option>");
                                    //   else 
                                    //     out.println("<option value=\"" + tmpFonctionRef.getLibelleCourt()  + "\" >" + tmpFonctionRef.getLibelleCourt() +"&nbsp;&nbsp;-&nbsp;&nbsp;"+tmpFonctionRef.getLibelleLong() + "</option>");                            
                                    //  }
%>
                                 </select>
                               -->
                               </td> 
                               <td>
                                   <select name="<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>"   
                                           onchange="hideResult();"
                                           id="sa_<%= FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>"
                                           style="width:100px;">
                                      <option></option>
<%                                    List<ArticleRef> lstArt = (List<ArticleRef>) TableRefHelper.getAllRef(ConstantesRef.TAB_REF.ARTICLE);
                                      for(ArticleRef tmpArticle : lstArt)
                                        {    
                                         if( fonc.getCodeArticle() != null && fonc.getCodeArticle().equals(tmpArticle.getLibelleCourt()) )
                                           out.println("<option value=\"" + tmpArticle.getLibelleCourt() + "\" selected >" + tmpArticle.getLibelleCourt()+"&nbsp;&nbsp;-&nbsp;&nbsp;"+tmpArticle.getLibelleLong() + "</option>");
                                         else
                                           {
                                            if(fonc.getCodeFonction() != null && !fonc.getCodeFonction().equals("") )
                                              {
                                               boolean isok = false ;
                                               List<FonctionArticleRef> lstfid = TableRefHelper.getFonctionArticleByFonctionId(fonc.getCodeFonction());
                                               for(FonctionArticleRef ref :lstfid)
                                                  isok |= ref.getLibelleCourtArticle().equals(tmpArticle.getLibelleCourt());
                                               if(isok)
                                                  out.println("<option value=\"" + tmpArticle.getLibelleCourt()  + "\">" +  tmpArticle.getLibelleCourt()+"&nbsp;&nbsp;-&nbsp;&nbsp;"+tmpArticle.getLibelleLong() + "</option>");  
                                              }
                                            else
                                              {
                                               out.println("<option value=\"" + tmpArticle.getLibelleCourt()  + "\">" +  tmpArticle.getLibelleCourt()+"&nbsp;&nbsp;-&nbsp;&nbsp;"+tmpArticle.getLibelleLong() + "</option>");
                                              }
                                           }
                                        }                                    
%>
                                   </select>
<%
                                   if( fonc.getCodeArticle() != null && fonc.getCodeArticle().equals("Non retenue") )
                                     {
                                      String divid = "obsdiv_"+FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx ;
                                      String aid = "obsa_"+FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx ;
                                      String obsValue = fonc.getObservations()!=null?fonc.getObservations():"" ;
%>                                    <a id="<%=aid%>" style="border:none;padding-left:16px;background:url(&quot;/antares/Images/yellowba.gif&quot;) no-repeat left center transparent;cursor:pointer;" 
                                               onclick="showObs('<%=divid%>',true);" >...</a>
                                      <div style="position: absolute; width: 300px; height: 155px; border: 1px solid rgb(0, 0, 0); background-color: rgb(211, 212, 216); display: none;" 
                                           id="<%=divid%>" >
                                        <span>Observations : </span><br>
                                        <textarea rows="6" style="width: 100%;" name="obs_<%=FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>"><%=obsValue%></textarea>
                                        <div style="width: 100%; text-align: right;">
                                          <a style="text-decoration:underline;cursor:pointer;" 
                                             onclick="showObs('<%=divid%>',false);"  >Fermer</a>
                                        </div>
                                      </div>
                                      <script type="text/javascript">
                                      majObsBloc('<%=FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+indx%>','<%=aid%>','<%=divid%>');
                                      </script>
<%                                   }
%>                                   
                              </td> 
                              <td nowrap="nowrap">
<%                            tmpStr= Conversion.integerToString(fonc.getQuotiteReelle());                                   
%>
                                <input maxlength="3" 
                                       style="width:30px;" 
                                       id="<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()+indx%>"
                                       name="<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()+indx%>" value="<%=tmpStr %>" 
                                       onkeyup="quotiteFonction('<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()+indx%>','<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_RET_PREFIX.getNom()+indx%>');hideResult();"/>% 
                              </td>
                              <td nowrap="nowrap">
<%                            tmpStr =  Conversion.integerToString(fonc.getQuotiteRetenue());
%>
                                <input maxlength="3" 
                                       style="width:30px"; 
                                       id="<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_RET_PREFIX.getNom()+indx%>"
                                       name="<%=FORM_CALCULETTE_NAME.FONC_QUOTITE_RET_PREFIX.getNom()+indx%>" 
                                       value="<%=tmpStr %>" 
                                       onchange="hideResult();"/>%  
                              </td>
<%                            tmpDureeA30Jours = fonc.getDisplayedDureePriseEnCompte();
                                                                            
                              if(  tmpDureeA30Jours != null)                              
                                {                                    	  
                                 tmpNbAnne = ""+tmpDureeA30Jours.getNbAnnees(); 
                                 tmpNbMois = ""+tmpDureeA30Jours.getNbMois();
                                 tmpNbJours= ""+tmpDureeA30Jours.getNbJours();
                                }
                              else 
                                {
                                 tmpNbAnne="";
                                 tmpNbMois="";
                                 tmpNbJours="";
                                }
%>
                              <td>
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureePriseEnCompteAA"+indx%>" value="<%=tmpNbAnne %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureePriseEnCompteAA"+indx%>" value="<%=tmpNbAnne %>" />  
                              </td>           
                              <td>                       
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureePriseEnCompteMM"+indx%>" value="<%=tmpNbMois %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureePriseEnCompteMM"+indx%>" value="<%=tmpNbMois %>" />
                              </td>           
                              <td>                                 
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureePriseEnCompteJJ"+indx%>" value="<%=tmpNbJours %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureePriseEnCompteJJ"+indx%>" value="<%=tmpNbJours %>" />
                              </td>
                              <td>&nbsp;</td> 
<%                            tmpDureeA30Jours = fonc.getDisplayedDureeArticle();
                                 
                                      if(tmpDureeA30Jours != null) {                                          
                                          tmpNbAnne = ""+tmpDureeA30Jours.getNbAnnees(); 
                                          tmpNbMois = ""+tmpDureeA30Jours.getNbMois();
                                          tmpNbJours= ""+tmpDureeA30Jours.getNbJours();
                                       }else {
                                           tmpNbAnne="";
                                           tmpNbMois="";
                                           tmpNbJours="";
                                       }
%>
                              <td>
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeArticleAA"+indx%>" value=<%=tmpNbAnne %> readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeArticleAA"+indx%>" value="<%=tmpNbAnne %>" />
                              </td>           
                              <td>                                 
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeArticleMM"+indx%>" value=<%=tmpNbMois %> readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeArticleMM"+indx%>" value="<%=tmpNbMois %>" />
                              </td>           
                              <td>                                 
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeArticleJJ"+indx%>" value=<%=tmpNbJours %> readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeArticleJJ"+indx%>" value="<%=tmpNbJours %>" />
                              </td>
                              <td>&nbsp;</td>
<%                            tmpDureeA30Jours = fonc.getDureeRetenueA30Jours();
							  String tmpCodeArticle = fonc.getCodeArticle();


					             if( tmpDureeA30Jours.toString().equals(new DureeA30Jours(-1, -1, -1).toString()))                                                                                 
						             {                                          
   								        tmpNbAnne = ""+0; 
   								        tmpNbMois = ""+0;
   								        tmpNbJours= ""+0;
                                     }
                                         if( !tmpDureeA30Jours.isEmpty())                                                                                  
                                         {                                          
                                        	 if(tmpDureeA30Jours.getNbAnnees()==-1){
                                        		 tmpNbAnne = ""+0; 
                                        	 }
                                        	 else{
                                             tmpNbAnne = ""+tmpDureeA30Jours.getNbAnnees();
                                        	 }
                                        	 if(tmpDureeA30Jours.getNbMois()==-1){
                                        		 tmpNbMois = ""+0; 
                                        	 }
                                        	 else{
                                        		 tmpNbMois = ""+tmpDureeA30Jours.getNbMois();
                                        	 }
                                        	 if(tmpDureeA30Jours.getNbJours()==-1){
                                        		 tmpNbJours = ""+0; 
                                        	 }
                                        	 else{
                                        		 tmpNbJours = ""+tmpDureeA30Jours.getNbJours();
                                        	 }
                                        	 
                                         }
                                         else 
                                        	 if (!ConstantesCalculette.ARTICLE.ARTICLE_3
                                        				.isArticleOK(tmpCodeArticle)){
                                        		 tmpNbAnne = ""; 
            								     tmpNbMois = "";
            								     tmpNbJours= "";
                                        	 }
%>
                              <td nowrap="nowrap">
                                <input maxlength="2" style="width:30px;" name="<%= FORM_CALCULETTE_NAME.FONC_DUREE_RET_AA_PREFIX.getNom()+indx%>" value="<%=tmpNbAnne %>" onchange="hideResult();"/>a
                              </td>           
                              <td nowrap="nowrap">                                    
                                <input maxlength="2" style="width:30px;" name="<%= FORM_CALCULETTE_NAME.FONC_DUREE_RET_MM_PREFIX.getNom()+indx%>" value="<%=tmpNbMois %>" onchange="hideResult();"/>m
                              </td>           
                              <td nowrap="nowrap">                                    
                                <input maxlength="2" style="width:30px;" name="<%= FORM_CALCULETTE_NAME.FONC_DUREE_RET_JJ_PREFIX.getNom()+indx%>" value="<%=tmpNbJours %>" onchange="hideResult();"/>j
                              </td>
<% 
                                      tmpDureeA30Jours = fonc.getDisplayedDureeResultat();
                                    
                                      if( (tmpDureeA30Jours != null)
                                      && (!tmpDureeA30Jours.isEmpty()))                                      
                                      {                                          
                                          tmpNbAnne = ""+tmpDureeA30Jours.getNbAnnees(); 
                                          tmpNbMois = ""+tmpDureeA30Jours.getNbMois();
                                          tmpNbJours= ""+tmpDureeA30Jours.getNbJours();
                                       }else {
                                           tmpNbAnne="";
                                           tmpNbMois="";
                                           tmpNbJours="";
                                       }
%>
                              <td>
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeResultatAA"+indx%>" value="<%=tmpNbAnne %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeResultatAA"+indx%>" value="<%=tmpNbAnne %>" />
                              </td>
                              <td>                                 
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeResultatMM"+indx%>" value="<%=tmpNbMois %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeResultatMM"+indx%>" value="<%=tmpNbMois %>" />
                              </td>
                              <td>                                 
                                 <input maxlength="2" style="width:30px;background-color:#EBEBE4;" name="<%= "dureeResultatJJ"+indx%>" value="<%=tmpNbJours %>" readonly ="readonly"/>
                                 <input type="hidden" name="<%= "dureeResultatJJ"+indx%>" value="<%=tmpNbJours %>" />
                              </td>   
                            </tr>                           
<%                        } // for fonction
%>                    </tbody>

                    </table> <!-- fin table des fonctions -->
                  </td>
                </tr>
                
                
                <tr id="tabfonlast" >
                  <td colspan="2" style="text-align:left;">
                    <input type="button" value="Ajouter une fonction" onClick="tabfonAddRow();hideResult();" />
                    <input type="hidden" id="nbFonctions" name="nbFonctions" value="<%=indx %>" > 
                  </td>
                </tr>
                          
                          
                <tr> <td colspan="2">&nbsp;</td> </tr>
                <tr>
                  <td colspan="2">
                    <input  type="submit" name="<%=FORM_CALCULETTE_ACTION.CALCULER.getNom() %>" value="Calculer le classement" onclick="return ctrlData();" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name=<%=FORM_CALCULETTE_ACTION.ENREG_INFO.getNom() %> value="Enregistrer les informations" onclick="return ctrlData();" />
                  </td>
                </tr>
                          
                <!-- Classement calcul�, affichage conditionn� --> 
                <tr id="resultat" >
                  <td width="100%" colspan="2">
                    <table width="100%" >
                      <tr>
                        <td align="left" width="100%" height="14" colspan="2"><img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" height="14">
                          <b>Classement calcul&eacute;</b>
                        </td>
                      </tr>  
                              
                      <tr>
                        <td class="libelle" >Echelon :</td>
                        <td bgcolor="#DFDFDF" width="200px"> <input name="res_echelon" style="background-color:#EBEBE4;" value="<%=cal.getEchelonCalcule()%>" readonly ="readonly"/> </td>
                      </tr>
                      <tr>
                        <td class="libelle" >Chevron :</td>
                        <td bgcolor="#DFDFDF" > <input name="res_chevron" style="background-color:#EBEBE4;" value="<%=cal.getChevronCalcule()%>" readonly ="readonly"/> </td>
                      </tr>
                      <tr>
                        <td class="libelle" >Indice brut :</td>
                        <td bgcolor="#DFDFDF" > <input name="res_indbrut" style="background-color:#EBEBE4;" value="<%=cal.getIndiceBrutCalcule()%>" readonly ="readonly"/> </td>
                      </tr>
                      <tr>
                        <td class="libelle" >Anciennet&eacute; conserv&eacute;e (aa,mm,jj) :</td>
                        <td bgcolor="#DFDFDF" > 
<% 
                        tmpDureeA30Jours = cal.getDureeAncienneteCalcule();
                                    
                        if(tmpDureeA30Jours != null)
                          {                                          
                           tmpNbAnne = ""+tmpDureeA30Jours.getNbAnnees(); 
                           tmpNbMois = ""+tmpDureeA30Jours.getNbMois();
                           tmpNbJours= ""+tmpDureeA30Jours.getNbJours();
                          }
                        else 
                          {
                           tmpNbAnne="";
                           tmpNbMois="";
                           tmpNbJours="";
                          }
%>                                        
                          <input name="ancienneteCalculeAA" style="background-color:#EBEBE4;" value="<%=tmpNbAnne %>" readonly ="readonly" maxlength="2" size="2" />a&nbsp;
                          <input name="ancienneteCalculeMM" style="background-color:#EBEBE4;" value="<%=tmpNbMois %>" readonly ="readonly" maxlength="2" size="2" />m&nbsp;
                          <input name="ancienneteCalculeJJ" style="background-color:#EBEBE4;" value="<%=tmpNbJours %>" readonly ="readonly" maxlength="2" size="2" />j 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr> 
                
                <!-- detail calcul -->
                <tr id="libDetailCalcul">
                  <td colspan="2" align="left" width="100%" height="14"><img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" height="14">
                    <b>D&eacute;tail des calculs</b>
                  </td>
                </tr>                       
                <tr id="detailCalcul">                      
                  <td colspan="2" bgcolor="#DFDFDF">
                    <textarea name="res_detail" id="res_detail" rows="6" style="background-color:#EBEBE4;" readonly="readonly" cols="100">
                      <%=cal.getDetailCalcul() %>
                    </textarea>
                  </td>
                </tr>  
                      
                <tr id="btEdit">
                  <td colspan="2">
                    <input type="submit" name="<%=FORM_CALCULETTE_ACTION.EDIT_DECOMPTE.getNom() %>" value="Editer le d�compte"  />                                
                  </td>
                </tr>
                        
                <tr>
                  <td align="right" colspan="2" >
                    <table style="border:1px solid #000000" >
                      <tr>
                        <td colspan="3" align="left" width="100%" ><img src="<%=Constantes.CONTEXT_URI%>/Images/yellowba.gif" width="14" height="14">
                          <b>Calcul de dur&eacute;e entre deux dates</b>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" width="100%" >
                          <table>                                      
                            <tr class="tabheader" bgcolor="#C0C0C0">
                              <td class="entete_tableau">Du</td>
                              <td  class="entete_tableau">Au</td>
                              <td  class="entete_tableau">Dur&eacute;e (aa,mm,jj)</td>
                            </tr>       
                            <tr>
<%                          tmpPeriode = cal.getPeriodePourCalcul();
                                            String dtDeb = "";
                                            String dtFin = "";
                                            if(tmpPeriode.getDateDebut() != null){
                                                dtDeb = Conversion.formatDate(tmpPeriode.getDateDebut(),Constantes.DATE_FORMAT_STRING);
                                            }
                                            
                                            if(tmpPeriode.getDateFin() != null){
                                               dtFin = Conversion.formatDate(tmpPeriode.getDateFin(),Constantes.DATE_FORMAT_STRING);
                                            }
%>
                              <td bgcolor="#DFDFDF" align="center"> <input maxlength="10" size="10" name="<%=FORM_CALCULETTE_NAME.CALCUL_DUREE_DATE_DEB.getNom() %>"  value="<%=dtDeb %>" /> </td>
                              <td bgcolor="#DFDFDF" align="center"> <input maxlength="10" size="10" name="<%=FORM_CALCULETTE_NAME.CALCUL_DUREE_DATE_FIN.getNom() %>"  value="<%=dtFin %>" /> </td>
                              <td bgcolor="#DFDFDF" align="center"> 
<%
                              float nbJours = tmpPeriode.getNbJoursA30Jours();
                              tmpDureeA30Jours = new DureeA30Jours(nbJours);
%>
                                <input maxlength="2" size="2" value="<%=tmpDureeA30Jours.getNbAnnees() %>" readonly ="readonly" />a&nbsp;
                                <input maxlength="2"  size="2" value="<%=tmpDureeA30Jours.getNbMois() %>" readonly ="readonly" />m&nbsp;
                                <input maxlength="2" size="2"  value="<%=tmpDureeA30Jours.getNbJours() %>" readonly ="readonly"/>j
                              </td>
                            </tr>
                          </table>
                          <input type="submit" name="<%=FORM_CALCULETTE_ACTION.CALCULER_DUREE.getNom() %>"  value="Calculer la dur&eacute;e" />                                    
                        </td>
                      </tr>
                    </table>
                             
                  </td>
                </tr>
                          
                <tr>
                  <td align="right" colspan="2">
                    <a href="#hautpage">Haut de page <img src="<%=Constantes.CONTEXT_URI%>/Images/top.gif" width="13" height="13" border="0"> </a>
                  </td>
                </tr>
                <tr>
                  <td align="center" colspan="2">
                    <table>                              
                      <%@include file="/includes/copyright.jsp"%>
                    </table>     
                  </td>
                </tr>
                
                </table>
                </form>
                </td>
                </tr>

              </table> <!-- table du corps -->
                              
        </td> <!-- td corps ecran -->
     </tr>
  </table> <!-- table  all -->         
</body>
</html>
</wl:summary>

