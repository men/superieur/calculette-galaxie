package fr.gouv.education.antares;

public class simpleLog 
{
    public static boolean m_shouldLog = false;
    
    static
    {
        m_shouldLog = "true".equals( System.getProperty( "antares.debug" ) );
        System.out.println( "Logging is set to " + ( m_shouldLog ? "true" : "false" ) );
    }
    
    public static void println( String i_sMsg )
    {
        if ( m_shouldLog )
        {
            System.out.println( i_sMsg );
            System.out.flush();
        }
    }

    public static void print( String i_sMsg )
    {
        if ( m_shouldLog )
        {
            System.out.print( i_sMsg );
            System.out.flush();
        }
    }
}
