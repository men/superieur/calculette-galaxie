package fr.gouv.education.antares;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import fr.gouv.education.antares.outils.AntaresLogger;

/**
 * Titre : constantes du projet Description : Definition des constantes utilises
 * par le projet Copyright : Copyright (c) 2001 Société : airial
 * 
 * @author christian schiele
 * @version 1.0
 */

public class Constantes {

	// Mécanisme d'upload.
	// Type de fichier uploadé.
	public static final String TYPE_FICH_UPLD_ASCII = "ASCII";
	public static final String TYPE_FICH_UPLD_BIN = "BINARY";

	// Constantes d'édition

	// Edition relecture
	public static final int NUM_CNU_NON_MEMBRE_BUR = 5;

	// Text paramètrable pour les courriers établissements
	public static final String TEXT_CODSECETAB = "T_CSE";
	// Timbres et textes
	public static final String CDSECRET_TEXT   = "S_TXT";
	public static final String CDSECRET_ADM    = "S_ADM";
	//Eo 1461
	public static final String LIB_QMIN         = "L_QMN";
	public static final String LIB_MEN         = "L_MEN";
	public static final String LIB_DIR_ENS     = "L_DPE";
	public static final String LIB_SDIR_REC    = "L_SDR";
	public static final String LIB_BUR_ORPES   = "L_BOR";
	public static final String COD_BUR_ORPES   = "C_BOR";
	public static final String NUM_BUR_ORPES   = "N_BOR";
	public static final String TEL_BUR_ORPES   = "T_BOR";
	public static final String ADR_MEN         = "A_MEN";
	public static final String TEL_MEN         = "T_MEN";
	public static final String FAX_MEN         = "F_MEN";
	public static final String INT_MEN         = "I_MEN";
	public static final String LIB_MIN         = "L_MIN";
	public static final String DAT_AFF         = "D_AFF";
	public static final String DAT_CON         = "D_CON";
	public static final String DAT_SOUT        = "D_SOU";
	public static final String BUR_MFT         = "B_MFT";
	public static final String BUR_MRC         = "B_MRC";
	public static final String SIG_MEN         = "S_MEN";
	//
	public static final String ART_AQ_MDC      = "A_AQM";
	public static final String ART_AQ_PR       = "A_AQP";
	public static final String ART_AQ_PRM      = "A_AQ3";
	public static final String ART_AQ_MDCM     = "A_AQ4";
	// V4
	public static final String DAT_DEC_STATUT    = "D_DEC";
	public static final String DAT_JOF           = "D_JOF";
	public static final String BUR_SOURCE_LEP    = "L_BSO";
	public static final String COD_BUR_GEST_PREV = "C_BGP";
	public static final String DATE_INIT = "--/--/----";
	public static final String ART_26_I_3 = "26-I-3";
	public static final String ART_46_4 = "46-4";
	public static final String ART_46_3 = "46-3";
	public static final String EMPV01_JUR = "EMPV01JUR";
	//
	public static final int NBCOPY_AQ = 1;
	
	// date observation
	public static final String DAT_OBSR = "D_OBS";

	// Corps au pluriel (éditionLettre de désignation des rapporteurs)
	public static final String PLUR_PR = "Professeurs des universités";
	public static final String PLUR_PRM = "Professeur du Muséum national d'histoire naturelle";
	public static final String PLUR_MCF = "Maîtres de conférences";
	public static final String PLUR_MCM = "Maîtres de conférences du Muséum national d'histoire naturelle";
	// mails
	public static final String MAIL_FT = "MAIL_FT";
	public static final String MAIL_FV = "MAIL_FV";
	public static final String MAIL_EXP = "M_EXP";
	public static final String MAIL_RCQ = "MAIL_RCQ";
	public static final String MAIL_RCS = "MAIL_RCS";
	public static final String MAIL_RVO = "MAIL_RVO";
	public static final String MAIL_EXP_Q = "M_EXQ";
	public static final String MAIL_EXP_R = "M_EXR";
	public static final String MAIL_C_A22 = "C_A22";
	public static final String MAIL_SIG = "M_SIG";
	
	
	
	public static final String MAIL_RAP = "MAIL_RAP";
	public static final String MAIL_RES = "MAIL_RES";
	public static final String MAIL_RGR = "MAIL_RGR";
	public static final String MAIL_EXV = "M_EXV";
	public static final String MAIL_EXP_N = "M_EXN";
	public static final String MAIL_EXP_E = "M_EXE";
	public static final String MAIL_EXP_D = "M_EXD";
	public static final String MAIL_OBJET_MC2_O = "MC2_O";
	public static final String MAIL_CORPS_MC2_C = "MC2_C";
	

	public static final String TRAITES = "Traites";
	public static final String ENCOURS = "EnCours";
	
	
	// 161632 Vega : Documments communs à tous les établissements à apporter obligatoirement par le candidat à chaque candidature
	public static final String DOC_COMMUN_VEGA_1 = "1"; //"piece_identite";
	public static final String DOC_COMMUN_VEGA_2 = "2"; //"arr_class";
	public static final String DOC_COMMUN_VEGA_3 = "3"; //"arr_der_aff_pos";
	public static final String DOC_COMMUN_VEGA_4 = "4"; //"cv";
	public static final String DOC_COMMUN_VEGA_5 = "5"; //"lettre_motivation";
	public static final String DOC_COMMUN_VEGA_6 = "6"; //"justif_rqth";
	
	//Paramètres : Limites sur les pdf à joindre à candidature VEGA
	public static final String LT_JC = "LT_JC";
	public static final String LP_JC = "LP_JC";	
	public static final String LT_AP = "LT_AP";
	public static final String LP_AP = "LP_AP";	
	public static final String LT_CV = "LT_CV";
	public static final String LP_CV = "LP_CV";	
	public static final String LT_LM = "LT_LM";
	public static final String LP_LM = "LP_LM";
	
	// Signataires
	public static final String NOM_SIG = "N_SIG";
	// Pour la phase recrutement
	public static final String ADR_MEN_RECRUT = "A_REC";
	public static final String LIB_SDIT_RECRUT = "L_REC";
	public static final String COD_SDIT_RECRUT = "C_REC";
	public static final String LIB_RESP_CHEF_SERV = "L_RES";
	public static final String NOM_RESP_CHEF_SERV = "N_RES";
	public static final String LIB_BUR_DPE = "L_DBU";
	public static final String LIB_SDIR_PREV = "L_SDP";
	public static final String LIB_BUR_DPE_B3 = "L_BB3";
	public static final String LIB_RESP_CHEF_SERV1 = "L_RE1";
	public static final String LIB_RESP_CHEF_SERV2 = "L_RE2";
	public static final String LIB_RESP_CHEF_SERV3 = "L_RE3";
	public static final String NUM_BUR_SERV3 = "n_RE3";
	public static final String NOM_CHEF_SERV3 = "N_RE3";
	public static final String MEL_RESP_CHEF_SERV3 = "M_RE3";
	public static final String LIB_RESP_CHEF_SERV4 = "L_RE4";
	public static final String LIB_ART_NOTIF_RAPP = "A_NRP";
	public static final String LIB_ART_NOTIF_RAPP_DERO = "A_NRD";
	public static final String NOM_RESP_CHEF_BURE3 = "N_CE3";
	public static final String LIB_RESP_CHEF_BURE3 = "L_CE3";
	public static final String LIB_DEC_REC_BURE3 = "L_DE3";
	public static final String TEL_DPE = "T_DPE";
	public static final String FAX_DPE = "F_DPE";
	public static final String LIB_SERVICE_MCFPR = "L_SMP";
	public static final String LIB_VOEUX = "L_V00";
	public static final String LIB_VOEUX_01 = "L_V01";
	public static final String TXT_SPE_ACC_CAND = "T_SAC";
	public static final String TXT_SPE_ACC_ECH = "T_SAE";
	public static final String TXT_SPE_ACC_ECH2 = "T_SAY";
	public static final String TXT_SPE_ACC_CRCT = "T_SAN";
	public static final String TXT_SPE_ACC_PEDR = "T_SAP";
	public static final String TXT_SPE_ACC_PARAM_CRCT = "T_SNG";
	public static final String TXT_SPE_ACC_PARAM_ELECTRA = "T_SEG";
	public static final String TXT_SPE_ACC_PARAM_ELARA = "T_SPG";
	public static final String TXT_SPE_ACC_PARAM_ADEL = "T_AH1";
	public static final String TXT_SPE_ACC_PARAM_ADEL_ETA = "T_AH2";

	// Pour phase derogation
	public static final String ART_FRAIS_DEP_PUB = "A_FDP";
	public static final String ART_FRAIS_DEP_VEH = "A_FDV";

	// Pour l'agrégation
	public static final String ART_NOTFRAPA = "A_NRA";
	public static final String DAT_ARRETE_NOTFRAPA = "D_NRA";
	public static final String SIG_SDR_REC = "S_SDR";
	public static final String LIB_REP = "L_REP";
	public static final String SIG_DIR = "S_DIR";
	public static final String TEXTCONC1_NOTFRAPA = "TC1NR";
	public static final String TEXTCONC2_NOTFRAPA = "TC2NR";

	// URLs pour le reporting
	public static final String URL_DPE = "U_DPE";
	public static final String URL_MEN = "U_MEN";
	public static final String URL_ANT = "U_ANT";
	public static final String URL_IMG = "U_IMG";

	// URL pour les fiches de postes publiés (evo no 298)
	public static final String URL_LPO = "U_LPO";

	// URL pour la fiche
	// "procedure de recrutement des ecs-questionnaire introduction pour galaxie"
	public static String URL_ENQ = "U_ENQ";
	
	//URL pour pièce complémentaire
	public static final String URL_PC ="U_PC";
	
	//Valeur de la date limite de soutenance
	public static final String DL_ST ="DL_ST";
	
	//Valeur du titre de la page newsletter
	public static final String L_NEW ="L_NEW";
	//Valeur du corps de la page newsletter
	public static final String C_NEW ="C_NEW";
		
	// ATRIA dates deb et fin
	public static final String ATRO_D = "ATR1D";
	public static final String ATRO_F = "ATR1F";
	public static final String ATRRC_D = "ATR2D";
	public static final String ATRRC_F = "ATR2F";
	public static final String ATRRE_D = "ATR3D";
	public static final String ATRRE_F = "ATR3F";
	public static final String ATRRE1_D = "ATR4D";
	public static final String ATRRE1_F = "ATR4F";

	//EVO 239129
	public static final String ATR5D = "ATR5D";
	public static final String ATR5F = "ATR5F";

	// ATRIA dates CALENDRIER de RECRUTEMENT
	public static final String IGR_PESAIS = "PESAIS01";
	public static final String IGR_DAFON = "DAFON01";
	public static final String IGE_PESAIS = "PESAIS02";
	public static final String IGE_DAFON = "DAFON02";
	public static final String ASI_PESAIS = "PESAIS03";
	public static final String ASI_DAFON = "DAFON03";
	public static final String TCH_PESAIS = "PESAIS04";
	public static final String TCH_DAFON = "DAFON04";
	public static final String ATRF_PESAIS = "PESAIS05";
	public static final String ATRF_DAFON = "DAFON05";
	public static final String ADAENES_PESAIS = "PESAIS06";
	public static final String ADAENES_DAFON = "DAFON06";
	public static final String SAENES_PESAIS = "PESAIS07";
	public static final String SAENES_DAFON = "DAFON07";
	public static final String ADJENES_PESAIS = "PESAIS08";
	public static final String ADJENES_DAFON = "DAFON08";
	public static final String CBIB_PESAIS = "PESAIS09";
	public static final String CBIB_DAFON = "DAFON09";
	public static final String BIB_PESAIS = "PESAIS10";
	public static final String BIB_DAFON = "DAFON10";
	public static final String BAS_PESAIS = "PESAIS11";
	public static final String BAS_DAFON = "DAFON11";
	public static final String ABIB_PESAIS = "PESAIS12";
	public static final String ABIB_DAFON = "DAFON12";
	public static final String MAG_PESAIS = "PESAIS13";
	public static final String MAG_DAFON = "DAFON13";
	public static final String ANTEE_PESAIS = "PESAIS14";
	public static final String ANTEE_DAFON = "DAFON14";
	public static final String FIDIS_PESAIS = "PESAIS15";
	public static final String FIDIS_DAFON = "DAFON15";

	// ATRIA MESSAGE CALENDRIER

	public static final String CVAL_MS_AC = "MS_AC";

	// Pour le lanceur
	public static final String EDITION_MUT = "PR_DEC_MUT";
	public static final String EDITION_DET = "PR_DEC_DET";
	public static final String EDITION_CON = "PR_NOM_CON";
	public static final String EDITION_AFF = "AVI_AFFECT";
	public static final String EDITION_ARQ = "ARQ";

	// evo adm: batch edition de la liste des qualifiés
	public static final String EDITION_LISTE_QUALIF = "LST_QUALIF";
	// evo adm: batch edition le la liste des propositions de promotion
	public static final String EDITION_LISTE_PROMO = "LST_PROMO";
	// code batch d'affectation VEGA 
	public static final String CDBTCH_AFF_VEGA = "V_AFF";

	// mails
	// mail de fin de travaux
	public static final String MFT_CORPS = "MFT_C";
	public static final String MFT_SUJET = "MFT_S";
	// mail de fin de voeux
	public static final String MFV_CORPS = "MFV_C";
	public static final String MFV_SUJET = "MFV_S";
	// mail relance nouveau candidat
	public static final String MRC_SUJET = "MRC_S";
	// mail relance nouveau candidat sans qualif
	public static final String MRC_CORPS1 = "MRC_1";
	// mail relance nouveau candidat sans candidature antee
	public static final String MRC_CORPS2 = "MRC_2";
	// mail relance nouveau candidat sans qualif,sans candidature antee
	public static final String MRC_CORPS3 = "MRC_3";
	// mail aux candidats pour indiquer publication des rapporteurs
	public static final String MRAP_CCNU = "MRC_C"; // coprs pour les membres
													// CNU
	public static final String MRAP_CORPS = "MRA_C";
	public static final String MRAP_SUJET = "MRA_S";
	public static final String MRAP_P_CORPS = "RAP_C";
	public static final String MRAP_P_SUJET = "RAP_S";
	
	public static final String MRAP_P_CORPS_GROUPE = "RP_GC";
	public static final String MRAP_P_SUJET_GROUPE = "RP_SG";
	// mail aux candidats pour indiquer publication des résultats
	public static final String MRES_CORPS = "MRE_C";
	public static final String MRES_SUJET = "MRE_S";
	// mail aux candidats pour indiquer publication des résultats
	public static final String M_TRA_CORPS = "M_TRA";
	public static final String O_TMA_SUJET = "O_TMA";
	// mail rapport envoi masse dossiers SIRAH
	public static final String O_MTA_SUJET = "O_MTA";
	// mail aux candidats antee pour qu'ils saisissent leus voeux
	public static final String MRV_CORPS1 = "MRV_1";
	public static final String MRV_SUJET = "MRV_S";
	// mail aux candidats antee Newsletter
	public static final String MNL_CORPS1 = "MNL_1";
	public static final String MNL_CORPS2 = "MNL_2";
	public static final String MNL_CORPS3 = "MNL_3";
	public static final String MNL_CORPS4 = "MNL_4";
	public static final String MNL_CORPS5 = "MNL_5";
	public static final String MNL_CORPS6 = "MNL_6";
	public static final String MNL_CORPS7 = "MNL_7";
	public static final String MNL_SUJET = "MNL_S";

	// evo mail au enseignants chercheurs (Electra: Procedure contradictoire
	// etablissement)
	public static final String MA_CORPS = "MA_TE";
	public static final String MA_SUJET = "MA_OE";
	public static final String MA_CORPS_T = "MA_TU";
	public static final String MA_SUJET_T = "MA_OU";
	public static final String MY_CORPS = "MY_TE";
	public static final String MY_SUJET = "MY_OE";
	public static final String MY_CORPS_T = "MY_TU";
	public static final String MY_SUJET_T = "MY_OU";

	// Parametre Astree
	public static final String ASTREE = "astree"; // permet de savoir si l'écran
													// sur lequel on est est un
													// écran ASTREE
	public static final String ASTREE_CNX = "astree_cnx"; // permet de définir
															// si on vient de la
															// connexion ASTREE
	
	// Mantis 181222 - Affichage numéro de version
	public static final String Version = "1.227.0"; // Version de l'application Galaxie (Mettre à jour à chaque nouvelle livraison client)
	public static final String DateVersion = "27/10/2022"; // Date de la version de l'application Galaxie (Mettre à jour à chaque nouvelle livraison client)

	public static String HeureVersion = getHeureVersion(); // heure de la version de l'application Galaxie
	
	public static final String ASTREE_COPYRIGHT = "[ GALAXIE Version "+Version+" - "+DateVersion+" | Copyright© 2010 MEN - MESR. Tous droits réservés ]";
	public static final String GALAXIE_COPYRIGHT = ASTREE_COPYRIGHT;

	public static final String DS_01 = "DS_01";
	public static final String DS_02 = "DS_02";
	public static final String DS_03 = "DS_03";
	public static final String DS_04 = "DS_04";
	public static final String TS_01 = "TS_01";
	public static final String TS_02 = "TS_02";
	public static final String TS_03 = "TS_03";
	public static final String TS_04 = "TS_04";
	public static final String TS_05 = "TS_05";
	public static final String NS_EX = "NS_EX";
	public static final String N_MAX = "N_MAX";
	public static final String MS_OC = "MS_OC";
	public static final String MS_TC = "MS_TC";
	public static final String MS_O6 = "MS_O6";
	public static final String MS_T6 = "MS_T6";
	public static final String MS_O5 = "MS_O5";
	public static final String MS_T5 = "MS_T5";
	public static final String MS_O4 = "MS_O4";
	public static final String MS_T4 = "MS_T4";
	public static final String MS_OA = "MS_Oa";
	public static final String MS_OB = "MS_Ob";
	public static final String MS_TA = "MS_Ta";
	public static final String MS_TB = "MS_Tb";
	public static final String LI_AO = "LI_AO";
	public static final String A_SEV = "A_SEV";
	public static final String MS_TD = "MS_TD";
	public static final String MS_TX = "MS_TX";
	// mail elara validation demande PEDR
	public static final String MS_OP = "MS_OP";
	public static final String MS_TP = "MS_TP";
	public static final String MS_OV = "MS_OV";
	public static final String MS_TV = "MS_TV";
	public static final String MS_OR = "MS_OR";
	public static final String MS_TR = "MS_TR";

	public static final String MA_OC = "MA_OC";
	public static final String MA_TC = "MA_TC";
	public static final String M_DAM = "M_DAM";
	public static final String M_OAI = "M_OAI";
	public static final String M_TAI = "M_TAI";

	public static final String MY_OC = "MY_OC";
	public static final String MY_TC = "MY_TC";
	// MAL ajout
	public static final String MY_OA = "MY_OA";
	public static final String MY_TA = "MY_TA";
	
	public static final String MY_OP = "MY_OP";
	public static final String MY_OV = "MY_OV";
	public static final String MY_TP = "MY_TP";
	public static final String MY_TV = "MY_TV";

	public static final String M_A2C = "M_A2C";
	public static final String M_A2S = "M_A2S";
	public static final String H_CLO = "H_CLO";
	public static final String TI_AC = "TI_AC";
	public static final String TI_CP = "TI_CP";
	public static final String L_DPE = "L_DPE";
	public static final String L_MEN = "L_MEN";
	public static final String MS_QU = "MS_QU";
	public static final String LT_RA = "LT_RA";
	public static final String L_DBU = "L_DBU";
	public static final String C_BOR = "C_BOR";
	public static final String T_RES = "T_RES";
	public static final String T_REC = "T_REC";
	// MANTIS 208581 - JES - Ajout paramètre heure cloture CANOPUS
	public static final String H_CLO_CANOPUS = "H_CCC";
	// 313987 : Paramètre mention RGPD obligatoire
	public static final String MRGPD = "MRGPD";
	
	// 313987 : Paramètre RGA + URL
	public static final String MRGA = "MRGA";
	public static final String U_RGA = "U_RGA";
	
	// 222289
	public static final String DCPE1 = "DCPE1";
	public static final String DCPE2 = "DCPE2";
	
	// 222289
	public static final String DCRC1 = "DCRC1";
	public static final String DCRC2 = "DCRC2";
		
	// Parametre du libelle court
	public static final String PRIP1 = "PRIP1";
	// Parametre du libelle long
	public static final String PRIP2 = "PRIP2";
	
	public static final String PRIME1 = "PRIM1";
	public static final String PRIME2 = "PRIM2";	
	
	public static final String SUPPRESSION = "SUPPRESSION";
	public static final String MODIFICATION = "MODIFICATION";
	public static final String CREATION = "CREATION";

	/* Mapping DB Mode de recrutement ( libelle court inexistant ) */
	public static final String MDR_INTERRUPT_PROC = "0";
	public static final String MDR_REJET_CA = "1";
	public static final String MDR_DIVERGENCES_LOCALES = "2";
	public static final String MDR_REFUS_DIRECTEUR = "3";
	public static final String MDR_MUTATION = "4";
	public static final String MDR_DETACHEMENT = "5";
	public static final String MDR_CONCOURS = "6";
	public static final String MDR_NON_VACANT = "7";
	public static final String MDR_REJET_CSE = "8";
	public static final String MDR_PAS_CANDIDATURE = "9";
	public static final String MDR_RECRUTEMENT = "A";
	public static final String MDR_AVIS_DEFAV_PRES = "B";
	// public static final String MDR_ABS_PROPO_CA = "C";
	public static final String MDR_PROC_463 = "C";
	public static final String MDR_ETRANGER = "D";
	public static final String MDR_BOE = "E";
	public static final String MDR_ABS_LAUREAT_COM = "H";
	public static final String MDR_CPJ = "J";

	/* Mapping DB Detail ( TRS_DET ) */
	public static final String CDDET_NON_RETENU_EN_MUTATION = "NM";
	public static final String CDDET_NON_RETENU_EN_DETACHEMENT = "ND";
	public static final String CDDET_CLASSEMENT_NON_PROPOSE = "NC";
	public static final String CDDET_RECRUTEMENT_NON_PROPOSE = "NR";
	public static final String CDDET_RECRUTEMENT_PROPOSE = "RE";
	public static final String CDDET_PROPOSE_EN_DETACHEMENT = "DT";
	public static final String CDDET_PROPOSE_EN_MUTATION = "MU";
	public static final String CDDET_CLASSEMENT_PROPOSE = "CL";

	/* Mapping DB INDI_CS_LRU ( libelle court inexistant ) */
	public static final String INDI_CS_LRU_COMITE_DE_SELECTION = "O";
	public static final String INDI_CS_LRU_COMMISSION_DE_SPECIALISTE = "N";
	public static final String INDI_CONTEXT_ALYA_SAISI = "O";
	public static final String INDI_CONTEXT_ALYA_NON_SAISI = "N";

	public static final String Avis_Ca_Masse_Servlet = "avisCaMassServlet";
	public static final String Avis_Ca_Servlet = "avisCaServlet";

	/* Key de rediection VEU */

	/* Classe sur un seul emploi en rang 1 */
	public static final int EMPLOI_RANG_1 = 1;
	/* Classe sur un seul emploi en rang différent de 1 */
	public static final int EMPLOI_RANG_N = 2;
	/* Classe sur plusieurs emplois toujours en rang 1 */
	public static final int N_EMPLOI_RANG_1 = 3;
	/* Classe sur plusieurs emplois dont au moins une fois en rang 1 */
	public static final int N_EMPLOI_RANG_1N = 4;
	/* Classe sur plusieurs emplois mais jamais en rang 1 */
	public static final int N_EMPLOI_RANG_N = 5;
	/* Non Classe */
	public static final int NON_CLASSE = 6;

	/* MODE DE SAISI DES VOEUX */
	public static final String MOD_SAISI_VOEUX_P = "P"; // Quand uniquement ADM
														// qui saisit les voeux
	public static final String MOD_SAISI_VOEUX_M = "M"; // Quand ADM modifie les
														// voeux du CAN
	public static final String MOD_SAISI_VOEUX_W = "W"; // Quand CAN saisit les
														// veoux
	
	/* Classé à aucun emploi VEGA*/
	public static final String ECRAN_VEGA_VOEUX_TYPE1 = "voeu01a.jsp";
	/* Classé sur un seul emploi VEGA*/
	public static final String ECRAN_VEGA_VOEUX_TYPE2 = "voeu01b.jsp";
	/* Classé sur plusieurs emplois VEGA et toujours en rang 1 */
	public static final String ECRAN_VEGA_VOEUX_TYPE3 = "voeu01c.jsp";
	/* Classe sur plusieurs emplois VEGA mais jamais en rang 1 */
	public static final String ECRAN_VEGA_VOEUX_TYPE4 = "voeu01d.jsp";
	/* Classe sur plusieurs emplois VEGA mais pas toujours en rang 1 */
	public static final String ECRAN_VEGA_VOEUX_TYPE5 = "voeu01e.jsp";
	/* Voeux déjà effectué*/
	public static final String ECRAN_VEGA_VOEUX_RECAP = "voeu02.jsp?recapVoeux=ok";
	
	/* CLEF SESSION */
	public static final String SESSION_MENU_ON = "menu_on";
	public static final String SESSION_AUTH_FLAG = "authFlag";
	public static final String SESSION_AUTH_TOKEN = "authToken";
	public static final String IS_LIE = "lstEmpLies";
	public static final String CAMPAGNE = "campagne";
	public static final String CAMPAGNE_RECRUT = "campagneR";
	public static final String CAMPAGNE_QUALIF = "campagneQ";
	public static final String CAMPAGNE_ELECTRA = "campagneA";
	public static final String CAMPAGNE_SIRIUS = "campagneI";
	public static final String CAMPAGNE_CANOPUS = "campagneP";
	public static final String CAMPAGNE_ALYA = "campagneY";
	public static final String CAMPAGNE_ELARA = "campagneE";
	public static final String CAMPAGNE_NAOS = "campagneN";
	public static final String CAMPAGNE_SIRAH = "campagneH";
	public static final String NUM_CANDIDAT = "numecan";
	public static final String NUM_ECH = "numen";
	public static final String NUM_CNAP = "numcnap";
	public static final String SESSION_LOGIN_CNAP = "loginCnap";
	public static final String CNAP_USER = "user";
	public static final String NUM_SESSION = "num_session";
	public static final String NUM_SESSION_SIRIUS = "num_session_i";
	public static final String NUM_SESSION_CANOPUS = "num_session_p";
	public static final String ACCES = "acs";
	public static final String SESSION_LOGIN_ADM = "login";
	public static final String SESSION_NUMIDE = "numide";
	public static final String SESSION_TYPE_CTU = "typeCTU";
	public static final String SESSION_NUME_CTU = "numeCTU";
	public static final String SESSION_NUME_CNU = "numeCNU";
	public static final String SESSION_NUME_CAN = "numeCAN";
	public static final String SESSION_ANCAMP = "anCAMP";
	public static final String SESSION_NUME_ETA = "numeta";
	public static final String SESSION_NUME_ETA2 = "numetab_as";// numetab
																// controlé par
																// un etab ratt
	public static final String SESSION_LISTE_ETA = "listeEnseignants";
	public static final String SESSION_NUME_ETAR = "numetar";
	public static final String SESSION_TYPE_ETA = "typeta"; // 08/2009 fme :
															// type etab
															// (rce,nrce,rect)
	public static final String SESSION_LIB_ETA = "libeta";
	public static final String SESSION_LIB_SEC = "libsec";
	public static final String SESSION_INDI_CS_LRU_ETA = "indiCsLru";
	public static final String NUM_JUR = "numejur";
	public static final String ECH_COURANT = "ech_courant";
	public static final String SESSION_CD_STA = "cdsta";

	public static final String SESSION_NUM_SEC = "numsec";

	public static final String SESSION_NUM_EMPLOI = "numemploi";
	public static final String SESSION_NUM_SESSION = "num_session";
	public static final String NUMSESSION_EMPLOI = "numsession_emploi";

	public static final String SESSION_NUMEMP = "numemp";
	public static final String SESSION_NOM = "nom";
	public static final String SESSION_PRENOM = "prenom";

	public static final String SESSION_LOGIN_CNU = "loginCnu";
	public static final String SESSION_NOM_CNU = "nomCNU";
	public static final String SESSION_PRENOM_CNU = "prenomCNU";
	public static final String SESSION_CIVILITE_CNU = "civiliteCNU";
	public static final String SESSION_SECTION_CNU = "sectionCNU";
	public static final String SESSION_SOUS_SECTION_CNU = "sousSectionCNU";	
	public static final String SESSION_QUA_CNU = "qualiteCNU";
	public static final String SESSION_QUA_GROUPE_CNU = "qualiteGroupeCNU";	
	public static final String SESSION_EMPLOI_BEAN = "remoteEmploi";
	public static final String SESSION_VOEUX_DATA = "voeux_data";
	public static final String SESSION_IDENTITE_CAN = "identiteCAN";
	public static final String SESSION_CORPS_ETA = "corps";
	public static final String SESSION_CLASSEMENTS = "lstClasses";
	public static final String SESSION_CLASSEMENTS_VEGA = "lstClassesVega";
	public static final String SESSION_CLASSEMENTS_ANTEE = "lstClassesAntee";
	public static final String SESSION_CLASSEMENTS_FIDIS = "lstClassesFidis";
	public static final String SESSION_QUALIF = "lstQualif";
	public static final String SESSION_DB_CAN_MUT = "dBCanMut";
	public static final String SESSION_IDENTITE_JUR = "identiteJUR";
	public static final String SESSION_VALIDATOR = "validator";
	public static final String SESSION_VALIDATOR_VALUE_OK = "ok";
	public static final String SESSION_VALIDATOR_VALUE_KO = "erreur";
	public static final String SESSION_CODREC = "codrec";
	public static final String SESSION_CODREC_LIBELLE = "codrecLibelle";

	//MSU 1463 début
	public static final String SESSION_MIPH_NUMGES = "numGesMIPH";
	public static final String SESSION_LOGIN_MIPH = "loginMiph";
	public static final String SESSION_NOM_MIPH = "nomMiph";
	//MSU 1463 fin
	public static final String MIPH_DATE_DEBUT_RECENS = "BOED1";
	public static final String MIPH_DATE_FIN_RECENS = "BOEF1";
	public static final String MIPH_DATE_DEBUT_RECRU = "BOED2";
	public static final String MIPH_DATE_FIN_RECRU = "BOEF2";
	public static final String ENQUETE_BOE_RECENS_BOE= "RECENS_BOE";
	public static final String ENQUETE_BOE_RECENS_AI = "RECENS_AI";
	public static final String ENQUETE_BOE_RECRUT = "RECRUT";
	
	//MSU Evo 1309
	public static final String NL_DUREE_VAL_ABN = "LI_NL";
	public static final String NL_DELAI_VAL_ABN = "D_ANL";
	public static final String MAIL_OR = "ML_OR";
	public static final String MAIL_TR = "ML_TR";

	
	
	public static final int NB_LIGNE_TAB_CLASSEMENT = 8;
	public static final int NB_LIGNE_TAB_CLASSEMENT_FOR_DUPLICATION_BOUTONS = 10;

	public static final String SESSION_APPLI = "appli";
	public static final String SESSION_ELECTRA = "session_electra";
	public static final String SESSION_ALYA = "session_alya";
	public static final String SESSION_NAOS = "session_naos";
	public static final String SESSION_ELARA = "session_elara";

	public static final String CAMPAGNE_ASTREE = "campagneS";
	public static final String NUM_SESSION_ASTREE = "num_session_astree";
	public static final String SESSION_NUM_SESSION_ASTREE = "num_session_astree";

	// confirmation emplois
	public static final String SESSION_SAV_MDR = "savmdr";
	public static final String SESSION_LST_ERREUR_NUMECAN = "lstErreurNumecan";
	public static final String SESSION_LST_ERREUR_NUMECAN_R10 = "lstErreurNumQualifR10";
	public static final String SESSION_LST_ERREUR_NUMECAN_R12 = "lstErreurNumQualifR12";
	public static final String PARAM_ACTION = "Action";
	public static final String PARAM_AJOUTER_LIGNE = "ajoutLigne";
	public static final String PARAM_SUPPRIMER_LIGNE = "suppressionLigne";
	public static final int PARAM_ACTION_VALUE_ABANDONNER = 0;
	public static final int PARAM_ACTION_VALUE_VALIDER = 1;
	public static final int PARAM_ACTION_VALUE_VERIFIER = 2;
	public static final int PARAM_ACTION_VALUE_AJOUTER_LIGNE = 3;
	public static final int PARAM_ACTION_VALUE_SUPPRIMER_LIGNE = 4;
	public static final int PARAM_ACTION_VALUE_CONSULTER = 5;
	public static final int PARAM_ACTION_VALUE_CONSULTER_POSTE = 6;
	public static final int PARAM_ACTION_VALUE_SAISIE_RAPPORTEURS_POSTE = 7;
	public static final int PARAM_ACTION_VALUE_ABSENCE_CLASSEMENT = 8;
	public static final int EMPLOI_ETAT_PROPOSE = 1;

	// message de controle entre jsp si pas utilisation des validators
	public static final String SESSION_CTRL_MESSAGE = "message";

	// pour rendre prioritaire le message relatif a un probleme de securite sur
	// la page
	// de login (obiger a reposter la page) - c'est la clef d'un attribut de
	// requete
	public static final String REQ_ATTR_MUST_POST = "mustPostLogin";

	// Cles de session pour report
	public static final String SESSION_REPORT_SECTION = "reportsec";
	public static final String SESSION_REPORT_COLLEGE = "reportcol";
	public static final String SESSION_REPORT_SSSECTION = "reportssc";
	public static final String SESSION_REPORT_GROUPE = "reportgrp";
	public static final String SESSION_REPORT_QUALITE = "reportqua";
	public static final String SESSION_REPORT_ADRADM = "reportadradm";
	public static final String SESSION_REPORT_ADMTEL = "reportadmtel";
	public static final String SESSION_REPORT_ADRPERS = "reportadrpers";
	public static final String SESSION_REPORT_PERSTEL = "reportperstel";

	// Type d'affichage du bandeau pour les jsp de recrutement */
	public static final int SESSION_BANDEAU_ETAB = 1;
	public static final int SESSION_BANDEAU_EMPLOI = 2;
	public static final int SESSION_BANDEAU_EMPLOIS_LIES = 3;
	// Profile de l'utilisateur
	public static final String SESSION_PROFILE = "profile";

	public static final String BTCH_RAP_DESIGN = "RAP_DESIGN";
	
	public static final String BTCH_PEDR_DESIGN = "PES_DESIGN";
	// MAL
	public static final String BTCH_RAP_DESIGN_ALYA = "RAP_DESIG2";

	public static final String BTCH_CNU_AVIMAS = "CNU_AVIMAS";
	public static final String BTCH_CNU_AVIMAS_ALYA = "CNU_AVIMA2";
	public static final String BTCH_CA_AVIMAS = "CA_AVIMAS";
	public static final String BTCH_CA_AVIMAS_ALYA = "CA_AVIMAS2";
	public static final String BTCH_ETA_MACC_MAS_ALYA = "ETA_MACC";
	public static final String BTCH_ADM_AVIRAT = "ADM_AVIRAT";
	public static final String DATE_NON_VALIDE = "Date non valide";
	public static final String DATE_FCT_PAR_PERIOD = "La date de d&eacute;but doit &ecirc;tre inférieure &agrave; la date de fin";
	public static final String DATE_FCT_PAR_RENSEIGNE = "Si la date de fin est renseign&eacute;e, la date de d&eacute;but doit l&acute;&ecirc;tre aussi";

	// MAL Modif
	public static final String BTCH_ADM_AVIRAT_ALYA = "ADM_AVIRA2";

	public static final String BTCH_RECUP_ACNU = "RECUP_ACNU";
	// MAL
	public static final String BTCH_RECUP_ACNUALYA = "RECUP_ALYA";

	public static final String BTCH_INJ_PROMO = "INJ_PROMO";
	public static final String BTCH_INJ_PROMO_INT = "INJ_PROINT";
	public static final String BTCH_INJ_PRCNU = "INJ_PRCNU";
	public static final String BTCH_INJ_PROMO_HU = "INJ_PRO_HU";
	public static final String BTCH_INJ_CRCT = "INJ_CRCT";
	public static final String BTCH_INJ_PEDR = "INJ_PEDR";
	public static final String BTCH_EXP_GESUP2 = "EXP_GESUP";
	public static final String BTCH_SF_ETATS = "SF_ETATS";
	public static final String BTCH_ARCHIV_ELECTRA = "ARCHIVELEC";
	public static final String BTCH_GENPDF_ELECTRA = "GENPDFELEC";
	public static final String BTCH_AS_ELE_INI = "AS_ELE_INI";
	public static final String BTCH_AS_ELE_FIN = "AS_ELE_FIN";
	public static final String BTCH_AS_ELE_EDI = "AS_ELE_EDI";
	public static final String BTCH_CRCT = "CRCT";
	public static final String BTCH_ARCHIV_CRCT = "ARCHIVCRCT";
	public static final String BTCH_ARCHIV_ALYA = "ARCHIVALYA";
	public static final String BTCH_ARCHIV_PEDR = "ARCHIVPEDR";	
	public static final String BTCH_ARCHIV_CPP = "ARCHIVCPP";	

	public static final String BTCH_INFA_ELECT = "INFA_ELECT";
	public static final String BTCH_EDIT_DIFF = "EDIT_DIFF";
	
	public static final String BTCH_ELARA_MAS = "ELARA_MAS";
	
	public static final String BTCH_QUALIF_MAS = "QUALIF_MAS";
	
	/*Lancement Batch relance établissement décision ELARA*/
	public static final String BTCH_PES_RELDEC = "PES_RELDEC";
	
	
	public static final String BTCH_ARCHIV_PRIME = "ARCH_PRIME";

	/* === Structure des pages WEB === */
	public static final int MAX_LIGNES = 10; /*
											 * nombre de lignes max. par page de
											 * resultat de recherche
											 */
	public static final int MAX_LIGNES_EV7 = 1500; /*
													 * nombre de lignes max. par
													 * page de resultat de
													 * recherche
													 */
	public static final int MAX_RESULT = 1500;
	public static final int MAX_RESULT2 = 5000;
	public static final int MAX_RESULT_DOUBLONS = 100;
	public static final int MAX_RESULT_GESUP = 200;

	// public static final int MAX_RESULT_DISPLAYED ; /* nombre de lignes max
	// pour l'affichage des résultats */
	public static final int MAX_CONV_PHARMA = 2; /*
												 * Maximum de date de
												 * convocation en pharmacie
												 */
	public static final int PAGE_SIZE = 10;
	public static final int PAGE_SIZE_EV7 = 1500;
	public static final String DATE_FORMAT_STRING = "dd/MM/yyyy";

	public static final String US_DATE_PATTERN = "yyyy/MM/dd";

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_FORMAT_STRING);
	public static final String HOUR_FORMAT_STRING = "HH:mm";
	public static final SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat(
			HOUR_FORMAT_STRING);
	public static final String DATEHOUR_FORMAT_STRING = "dd/MM/yyyy HH:mm:ss";
	public static final SimpleDateFormat DATEHOUR_FORMAT = new SimpleDateFormat(
			DATEHOUR_FORMAT_STRING);

	/* NOM DES CHAMPS EN BASE */
	public static final int AGE_MINI = 16;
	public static final String FRANCE = "FR";
	public static final String MONSIEUR = "M.";
	public static final String MADAME = "Mme";
	public static final String MADEMOISELLE = "Mlle";
	public static final String NAT_FR = "F";
	// code labo non référencé
	public static final String IDUR_NON_REF = "A";
	// DataSource en mise a jour
	public static final String DATASOURCE = "DataSource";
	// synonyme
	public static final String TXDATASOURCE = "DataSource";
	// DataSource pour consult uniquement
	public static final String CONSULT_DATASOURCE = "ConsultDataSource";
	//
	public static final String CANDIDAT_QUALIF = "Q";
	public static final String CANDIDAT_RECRUT = "R";
	//
	public static final String TYP_PHA_QUALIF = "Q";
	public static final String TYP_PHA_RECRUT = "R";
	public static final String TYP_PHA_ASTREE = "S";
	public static final String TYP_PHA_ELECTRA = "A";
	public static final String TYP_PHA_SIRIUS = "I";
	//
	public static final String PHA_QUALIF_CANDI = "QC";
	public static final String PHA_QUALIF_CANDI_GROUPE = "GC";
	//Mantis 230538 - JES ajout nouvelles phases
	public static final String PHA_QUALIF_CANDI_SOUT_TARDIVE = "QF";
	public static final String PHA_QUALIF_CANDI_DEPOT_DOSS = "QE";
	public static final String PHA_QUALIF_CANDI_GROUPE_DEPOT_DOSS = "GE";
	public static final String PHA_QUALIF_CANDI_ASTREE = "SC";
	public static final String PHA_QUALIF_CANDI_VEU = "RV";
	public static final String PHA_QUALIF_RAPP = "QR";
	// publication des résultats
	public static final String PHA_QUALIF_PUB = "QP";

	// Procedure contradictoire electra
	public static final String PHA_ELECTRA_PROC_CONTRADICTOIRE = "AP"; 
	// Qualification par le groupe-rapporteurs
	public static final String PHA_QUALIF_GROUPE_RAPP = "GR";
	
	// Qualification par le groupe-publication résultats
	public static final String PHA_QUALIF_GROUPE_PUB_RES = "GP";
	
	// Qualification par le groupe-candidature
	public static final String PHA_QUALIF_GROUPE_CTU = "GC";
	
	// Qualification par le groupe-hors période active
	public static final String PHA_QUALIF_GROUPE_HORS_PERIODE = "GH";
	
	// Recrutement - Publication des offres de postes
	public static final String PHA_PUB_OFFRE_POST = "RP";
	

	public static final String PHA_ASTREE_INITIALISAITON = "SI"; // phase
																	// d'initialisation
																	// ANTEE
	public static final String PHA_ASTREE_DEPOT_CTU = "SC"; // phase depot des
															// candidatures
															// ANTEE
	public static final String PHA_VEGA_SAISIE_VOEUX = "IV"; // Phase de saisie de voeux VEGA
	
	public static final String PHA_SIRAH_C1 = "C1"; // Phase de SIRAH C1
	public static final String PHA_SIRAH_C2 = "C2"; // Phase de SIRAH C2
	public static final String PHA_SIRAH_C3 = "C3"; // Phase de SIRAH C3
	public static final String PHA_SIRAH_C4 = "C4"; // Phase de SIRAH C4
	public static final String PHA_SIRAH_C5 = "C5"; // Phase de SIRAH C5
	
	public static final String PHA_ELARA_DS = "DS"; 
	
	
	/**
	 * EDI - 10/02/2016 - Mantis 170309 et 170287
     * Pour gérer les privilèges qui ne sont pas liés à des campagnes ou des phases
     * de campagnes, une phase a été créée : "Toutes phases", (CDTPH = "TP"). Celle-ci n'est liée
     * à aucune période ou campagne.
	 */
	public static final String PHA_TOUTES_PHASES = "TP";

	// type de candidature
	public static final String CDTU_GROUPE = "G";
	public static final String CDTU_NORMAL = "N";
	// La candidature à t'elle des rapporteurs désignés
	public static final String RAP_DESIGNES = "Désignés";
	public static final String RAP_SAISIS = "Saisis";
	public static final String RAP_NON_SAISIS = "Non Saisis";

	// Résultat qualification par le groupe
	public static final String GRP_RES_QG = "QG"; // Qualifié par le groupe

	/* NUMERO DE COLLEGE */
	public static final int COL_PROF = 1;
	public static final int COL_MAIT = 2;

	/* NUMERO DE CORPS */
	public static final int COR_PR = 1;
	public static final int COR_MDC = 2;
	public static final int COR_PRM = 3;
	public static final int COR_MDCM = 4;

	/* PROFILE UTILISATEURS - Ref : TR_PRO */
	public static final int PRO_ADM = 1; // Administrateur
	public static final int PRO_ACG = 2; // AC - Gestionaire
	public static final int PRO_ACSG = 3; // AC - Super Gestionnaire Super
											// DPE/A10
	public static final int PRO_ACSG_A6 = 10; // AC - Super Gestionnaire Super
												// DPE/A6
	public static final int PRO_CNUM = 4; // CNU - MEMBRE
	public static final int PRO_CNUP = 5; // CNU - PRESIDENT DE SECTION
	public static final int PRO_CAN = 6; // CANDIDAT
	public static final int PRO_CHEF_SDGCPES  = 20; // CHEF_SDGCPES
	public static final int PRO_ECH = 25; // ENSEIGNANT CHERCHEUR
	public static final int PRO_ECH2 = 32; // ENSEIGNANT CHERCHEUR ALYA2
	public static final int PRO_REC = 27; // Recteur
	public static final int PRO_CNAP = 31; // Section cnap
	public static final int PRO_DGRH = 37; // DGRH-C2
	public static final int PRO_DGRH_B2 = 38; // DGRH-B2
	public static final int PRO_JURY_CHIRON = 36; // JURY CHIRON
	public static final int PRO_UFR = 39; // Unité de Formation et de Recherche (UFR)

	public static final int PRO_JUR = 13; // JURY
	public static final int PRO_ETAG = 8; // Gestionnaire etablissement
	public static final int PRO_INST = 23; // Gestionnaire etablissement
	
	public static final int PRO_MIPH = 33; //MSU 1463 Gestionnaire MIPH

	/* QUALITES DES MEMBRES DU CNU */
	public static final int QUA_PDT = 1; // Président
	public static final int QUA_1VICE_PDT = 2; // 1er vice président
	public static final int QUA_2VICE_PDT = 3; // 2ème vice président
	public static final int QUA_ASS = 4; // Assesseur
	public static final int QUA_NON_MBRE = 5; // Pas membre du bureau
	public static final int QUA_PDT_SSEC = 6; // Président de sous section -
												// Q005 - Versailles -
												// 02/06/2003
	public static final int QUA_PDT_SEC_SSC = 7; // Président de section et de
													// sous section - Q60 -
													// Versailles - 13/11/2003

	/* INDICATEUR DES ORIGINES PROFESSIONNELLES DES DETACHES */
	public static final String INDIORIGDET_P = "P"; // PR
	public static final String INDIORIGDET_M = "M"; // MCF
	public static final String INDIORIGDET_C = "C"; // Commun

	/* APPLICATION */
	public static final String ANTEE = "antee";
	public static final String FIDIS = "fidis";
	public static final String GALAXIE = "galaxie";
	public static final String ELECTRA = "electra";
	public static final String ANTARES = "antares";
	public static final String SIRIUS = "sirius";
	public static final String VEGA = "vega";
	public static final String ALYA = "alya";
	public static final String NAOS = "naos";
	public static final String ELARA = "elara";
	public static final String BOE = "boe";
	public static final String SIRAH = "sirah";
	public static final String ATRIA = "atria";
	public static final String ALTAIR = "altair";

	/* PROFIL */
	public static final String PROFIL_CAN = "profil_can";
	public static final String PROFIL_CNU = "profil_cnu";
	public static final String PROFIL_DPE = "profil_dpe";
	public static final String PROFIL_ETA = "profil_eta";

	/* ACCES */
	public static final String ACS_ADM = "adm";
	public static final String ACS_ECH = "ech";
	public static final String ACS_ECH2 = "ech2";
	public static final String ACS_CAN = "can";
	public static final String ACS_CNU = "cnu";
	public static final String ACS_DPE = "dpe";
	public static final String ACS_ETA = "eta";
	public static final String ACS_JUR = "jury";
	public static final String ACS_REC = "rec";
	public static final String ACS_CNAP = "cnap";	
	//MSU 1463
	public static final String ACS_MIPH = "miph";
	public static final String ACS_DGRH = "DGRH";

	// public static final String ACS_CAN_ASTREE="canAstree"; // Pour la phase
	// ASTREE

	/* Type etab */
	public static final String RCE = "rce";
	public static final String NRCE = "nrce";
	public static final String RECT = "rect";
	public static final String UFR = "ufr";

	/* Etats des liens menu */
	public static final int ETAT_LIEN_AUCUN = 0;
	public static final int ETAT_LIEN_ACTIF = 1;
	public static final int ETAT_LIEN_SELECT = 2;
	public static final int ETAT_LIEN_INACTIF = 3;
	public static final int ETAT_LIEN_CACHE = 4;
	public static final int ETAT_LIEN_TITRE = 5;
	public static final int ETAT_MENU_LIEN = 6;

	/* PAGES DE RECHERCHE */
	// type recherche
	public static final String TYPE_RECH_RAP = "rechrap";
	public static final String TYPE_RECH_CAN = "rechcan";
	public static final String TYPE_RECH_RES = "rechres";
	public static final String MODE_RECH_NOUVELLE = "nouv_rech";
	public static final String MODE_RECH_AFFINER = "affiner_rech";
	public static final String MODE_RECH_NULLE = "rech_nulle";
	// attributs de Session
	public static final String SESSION_RECHERCHE = "rechercheCourante";
	public static final String SESSION_RECHERCHE_RES = "rechercheCouranteResultat";
	public static final String SESSION_RECHERCHE_QUAL = "temp"; // CETTE
																// UTILISATION
																// EST
																// TEMPORAIRE !!
	public static final String SESSION_RECHERCHE_POST = "temp_post"; // CETTE
																		// UTILISATION
																		// EST
																		// TEMPORAIRE
																		// !!
	// Paramètres de recherche
	public static final String RECH_NOM = "nom"; // Nom de naissance ou mariage
	public static final String RECH_PRENOM = "prenom";
	public static final String RECH_NUMEQUALIF = "numqualif";
	public static final String RECH_NUMESEC = "numesec";
	public static final String RECH_NUMECOR = "numecor";
	public static final String RECH_TYPECTU = "typectu";
	public static final String RECH_LIGNE = "ligne";
	public static final String RECH_NUMECAN = "numecan";
	public static final String RECH_CODREC = "codrec";
	public static final String RECH_POSTE = "numemp";
	public static final String RECH_DATESUPP = "datesupp";
	public static final String RECH_NUMETAB = "numetab";
	public static final String RECH_UAIMPLANT = "uaimplant";
	// Colonne des listes de résultats
	public static final String RESULT_NOM = "NOM"; // Nom de naissance
	public static final String RESULT_NOMMAR = "NOMMAR"; // Nom de marriage
	public static final String RESULT_PRENOM = "PRENOM"; // Prenom
	public static final String RESULT_DDN = "DDN"; // Date de naissance
	public static final String RESULT_DQUALIF = "DRESULT"; // Date de
															// qualification
	public static final String RESULT_NUMECAN = "NUMECAN"; // index candidat
	public static final String RESULT_NUMECTU = "NUMECTU"; // index candidature
	public static final String RESULT_NUMESEC = "NUMESEC"; // index section
	public static final String RESULT_NUMESEC2 = "NUMESEC2"; // index section 2
	public static final String RESULT_NUMESEC3 = "NUMESEC3"; // index section 3
	public static final String RESULT_NUMEMP = "NUMEMP"; // index emploi
	public static final String RESULT_NUMOFR = "NUMOFR"; // index emploi
	public static final String RESULT_IDGREMP = "IDGREMP"; // index emploi
	public static final String RESULT_NUMQUALIF = "NUMQUALIF"; // index
																// qualification
	public static final String RESULT_NB_EMP_LIES = "NBEMPLIES"; // nb d'emploie
																	// liés pour
																	// un poste
	public static final String RESULT_MAX_RG_CAND = "MAXRGCAND"; // Classement
																	// max d'un
																	// candidat
	public static final String RESULT_PREFIX = "LIGNE_PREFIX"; // Info avant une
																// ligne
	public static final String RESULT_SUFFIX = "LIGNE_SUFFIX"; // Info apres une
																// ligne
	public static final String RESULT_LIBELLE = "LIBELLE"; // index candidature
	public static final String RESULT_LIBSEC = "LIBSEC"; // libelle section
	public static final String RESULT_LIBSEC2 = "LIBSEC2"; // libelle section 2
	public static final String RESULT_LIBSEC3 = "LIBSEC3"; // libelle section 3
	public static final String RESULT_LIBCOR = "LIBCOR"; // libelle corps
	public static final String RESULT_VOEUX = "VOEUX"; // voeux de candidat
	public static final String RESULT_RANG = "RANG"; // classement pour un poste
	public static final String RESULT_RANG_EVENTUEL = "RANG_EVENTUEL"; // classement
																		// effectif
																		// pour
																		// un
																		// poste
	public static final String RESULT_DATCHOIX = "DATCHOIX"; // classement pour
																// un poste
	public static final String RESULT_LIBDET = "LIBDET"; // libelle code detail
	public static final String RESULT_LIBSUIVI = "LIBSUIVI"; // libelle code
																// suivi
	public static final String RESULT_EMAILCAN = "LIBSUIVI"; // libelle code
																// suivi
	// Comite de selection Versailles 19/10/2009
	public static final String RESULT_NUMECDS = "NUMECDS"; // nÂ°comite de
															// selection
	public static final String RESULT_DATESUPP = "DATESUPP"; // Date de
																// suppression
																// de
																// candidature

	// GALAXIE 05/09/2008 RLI RESULT_LIBCCOR
	public static final String RESULT_NUMETAB = "NUMETAB"; // numéro
															// d'etablissement
	public static final String RESULT_ANCAMP = "ANCAMP"; // annee de campagne
	public static final String RESULT_NUMSESSION = "NULMSESSION"; // numéro de
																	// session
	public static final String RESULT_NUMECOR = "NUMECOR"; // article pour un
															// poste
	public static final String RESULT_LIBCCOR = "LIBCCOR"; // article pour un
															// poste
	public static final String RESULT_ARTICLE = "CDART"; // article pour un
															// poste
	public static final String RESULT_LIBMC = "LIBMC"; // libelle mot-cle
	public static final String RESULT_IDMC = "IDMC"; // id mot-cle
	public static final String RESULT_PROFIL = "PROFIL"; // profil
	public static final String RESULT_LOCAL = "LOCAL"; // localisation
	public static final String RESULT_CDPLOCAL = "CDPLOCAL"; // code postal
																// localisation
	public static final String RESULT_INDIVAC = "INDIVAC"; //
	public static final String RESULT_INDIREP = "INDIREP"; //
	public static final String RESULT_INDIPUB = "INDIPUB"; //
	public static final String RESULT_NUMADR = "NUMADR"; // 1ere ligne d'adresse
	public static final String RESULT_LIGNE1ADR = "ADR1"; // 1ere ligne
															// d'adresse
	public static final String RESULT_LIGNE2ADR = "ADR2"; // 2eme ligne
															// d'adresse
	public static final String RESULT_LIGNE3ADR = "ADR3"; // 3eme ligne
															// d'adresse
	public static final String RESULT_CDPOST = "CDPOST"; // code postal
	public static final String RESULT_VILLE = "VILLE"; // ville
	public static final String RESULT_IDRESP = "IDRESP"; // identite du
															// responsable
	public static final String RESULT_FCTRESP = "FCTRESP"; // fonction du
															// responsable
	public static final String RESULT_TELRESP = "TELRESP"; // telephone du
															// responsable
	public static final String RESULT_TEL2RESP = "TEL2RESP"; // Autre telephone
																// du
																// responsable
	public static final String RESULT_FAXRESP = "FAXRESP"; // fax du responsable
	public static final String RESULT_EMAILRESP = "EMAILRESP"; // email du du
																// responsable
	public static final String RESULT_PJR = "PJ_R"; // pieces jointes
													// supplementaires
	public static final String RESULT_DATSAISIE = "DATSAISIE"; // date de saisie
																// du poste
	public static final String RESULT_DATFON = "DATFON"; // date de prise de
															// fonction du poste
	public static final String RESULT_DATMAJ = "DATMAJ"; // date de maj du poste
	public static final String RESULT_DATPUB = "DATPUB"; // date de publication
															// du poste
	public static final String RESULT_DATDEC = "DATDEC"; // date de saisie du
															// poste
	public static final String RESULT_DATTRA = "DATTRA";
	public static final String RESULT_DATCLO = "DATCLO"; // date de prise de
															// fonction du poste
	public static final String RESULT_DATOUV = "DATOUV"; // date de maj du poste
	public static final String RESULT_UFR = "UFR"; // Composante ou UFR
	public static final String RESULT_REFUFR = "REFUFR"; // reference UFR
	public static final String RESULT_IDUR1 = "IDUR1"; // id labo 1
	public static final String RESULT_IDUR2 = "IDUR2"; // id labo 2
	public static final String RESULT_IDUR3 = "IDUR3"; // id labo 3
	public static final String RESULT_IDUR4 = "IDUR4"; // id labo 4
	public static final String RESULT_IDUR5 = "IDUR5"; // id labo 5
	public static final String RESULT_IDUR = "IDUR"; // id labo
	public static final String RESULT_NUMETABRECH = "NUMETABRECH"; // id etablissement de recherche

	public static final String RESULT_LIBUR = "LIBUR"; // lib labo
	public static final String RESULT_CPTREPUB = "CPTREPUB"; // icompteur de
																// repub
	public static final String RESULT_CODREC = "CODREC"; // codrec
	public static final String RESULT_CIVIL = "CIVIL"; // civilite
	public static final String RESULT_INDICHAIRE = "INDICHAIRE"; // chaire
	public static final String RESULT_PJSUPP = "PJ_ETA";// pieces jointes
	public static final String RESULT_POURVOIEMENT = "POURVOIEMENT"; // chaire
	public static final String RESULT_CHOIX = "CHOIX"; // chaire
	// Versailles 16/11/2009 rajout pour Comite de selection
	public static final String RESULT_DATPUBCDS = "DATPUBCDS"; // date
																// publication
																// CDS
	public static final String RESULT_CDNTP = "CDNTP"; // nature de poste
	
	public static final String RESULT_LIB_PST_CPJ = "LIB_PST_CPJ";
	public static final String RESULT_MONT_FIN_CPJ = "MONT_FIN_CPJ";
	public static final String RESULT_DUREE_PRJ_CPJ = "DUREE_PRJ_CPJ";
	// TYPE DE MOT-CLE
	// GALAXIE 05/09/2008 RLI
	public static final String MC_ENS = "E"; // type de mot clé "enseignement"
	public static final String MC_RECH = "R"; // type de mot clé "recherche"
	public static final String MC_AUTRE = "A"; // autres type de mot clé autres

	//VEGA
	public static final String CLASSEMENT_MODIFICATION = "modification";
	public static final String CLASSEMENT_CREATION = "creation";
	
	
	/* REFERENCES JNDI */
	//MSU Evo 1396 début
	public static final String JNDI_GESTIONNAIRE = "fr.gouv.education.antares.ejb.listeGestionnaireSL.ListeGestionnaire";
	//MSU Evo 1396 fin
	public static final String JNDI_GESTION_INJECTION = "fr.gouv.education.antares.recrutement.admin.ejb.gestionInjectionSL.GestionInjection";
	public static final String JNDI_ADMIN_ID = "fr.gouv.education.antares.qualification.admin.ejb.gestionAdminSL.gestionAdmin";
	public static final String JNDI_IDE_EN = "fr.gouv.education.antares.ejb.ideEN.Ide";
	public static final String JNDI_ADRESSE = "fr.gouv.education.antares.ejb.adresseEN.Adresse";
	public static final String JNDI_BUREAU = "fr.gouv.education.antares.ejb.bureauEN.Bureau";
	public static final String JNDI_LISTE_TYPEDIPLOME = "fr.gouv.education.antares.ejb.listeTypDiplomeSL.ListeTypDiplome";
	public static final String JNDI_TYPDPL_EN = "fr.gouv.education.antares.ejb.typdplEN.Typdpl";
	public static final String JNDI_LISTE_ARTICLE = "fr.gouv.education.antares.ejb.listeArticleSL.ListeArticle";
	public static final String JNDI_LISTE_ARTICLE_DERO = "fr.gouv.education.antares.ejb.listeArticleDeroSL.ListeArticleDero";
	public static final String JNDI_AUTHENTIFICATION = "fr.gouv.education.antares.ejb.authentificationEN.Authentification";
	public static final String JNDI_CAMPAGNE = "fr.gouv.education.antares.ejb.campagneSL.Campagne";
	public static final String JNDI_CAMPAGNE_EN = "fr.gouv.education.antares.ejb.campagneEN.Campagne";
	public static final String JNDI_SESSION_EN = "fr.gouv.education.antares.ejb.sessionEN.Session";
	public static final String JNDI_CANDIDAT = "fr.gouv.education.antares.ejb.candidatEN.Candidat";
	public static final String JNDI_CANDIDAT_DERO = "fr.gouv.education.antares.ejb.candidatDeroEN.CandidatDero";
	public static final String JNDI_CIES = "fr.gouv.education.antares.ejb.ciesSL.Cies";
	public static final String JNDI_CIES_EN = "fr.gouv.education.antares.ejb.ciesEN.Cies";
	public static final String JNDI_GRD = "fr.gouv.education.antares.ejb.grdSL.Grd";
	public static final String JNDI_GRD_EN = "fr.gouv.education.antares.ejb.grdEN.Grd";
	public static final String JNDI_TET = "fr.gouv.education.antares.ejb.tetSL.Tet";
	public static final String JNDI_TET_EN = "fr.gouv.education.antares.ejb.tetEN.Tet";
	public static final String JNDI_FPR = "fr.gouv.education.antares.ejb.fprSL.Fpr";
	public static final String JNDI_FPR_EN = "fr.gouv.education.antares.ejb.fprEN.Fpr";
	public static final String JNDI_LISTE_FAMILLE = "fr.gouv.education.antares.ejb.listeFamilleSL.ListeFamille";
	public static final String JNDI_MFT_EN = "fr.gouv.education.antares.ejb.mftEN.Mft";
	public static final String JNDI_MC_EN = "fr.gouv.education.antares.ejb.mcEN.Mc";
	public static final String JNDI_MCSEC_EN = "fr.gouv.education.antares.ejb.mcsecEN.McSec";
	public static final String JNDI_MCC_EN = "fr.gouv.education.antares.ejb.mccEN.Mcc";
	public static final String JNDI_MCCSEC_EN = "fr.gouv.education.antares.ejb.mccsecEN.MccSec";
	public static final String JNDI_CND_EN = "fr.gouv.education.antares.ejb.cndEN.Cnd";
	public static final String JNDI_ARTCND_EN = "fr.gouv.education.antares.ejb.artcndEN.ArtCnd";
	public static final String JNDI_LISTE_CND = "fr.gouv.education.antares.ejb.listeConditionSL.ListeCondition";
	public static final String JNDI_LISTE_CND_COURT = "fr.gouv.education.antares.ejb.listeCndCourtSL.ListeCndCourt";

	public static final String JNDI_POSADM_ELECTRA = "fr.gouv.education.antares.ejb.listePosAdmElectraSL.ListePosAdmElectra";
	public static final String JNDI_POSADM = "fr.gouv.education.antares.ejb.listePosAdmSL.ListePosAdm";
	public static final String JNDI_CORP = "fr.gouv.education.antares.ejb.listeCorpSL.ListeCorp";
	public static final String JNDI_MOT_CESS_FONCTION = "fr.gouv.education.antares.ejb.listeMotifCessFonctionSL.ListeMotifCessFonction";
	public static final String JNDI_MOD_SERVICE = "fr.gouv.education.antares.ejb.listeModServiceSL.ListeModService";
	public static final String JNDI_STATUT = "fr.gouv.education.antares.ejb.listeStatutSL.ListeStatut";
	public static final String JNDI_STATUT_QUA = "fr.gouv.education.antares.ejb.listeStatutQuaSL.ListeStatutQua";
	public static final String JNDI_STATUT_QUA_GROUPE = "fr.gouv.education.antares.ejb.listeStatutQuaGroupeSL.ListeStatutQuaGroupe";
	public static final String JNDI_STATUT_QUAL_CNU = "fr.gouv.education.antares.ejb.listeStatutQualCnuSL.ListeStatutQualCnu";
	public static final String JNDI_STATUT_QUAL_CNU_CAN03 = "fr.gouv.education.antares.ejb.listeStatutQualCnuCan03SL.ListeStatutQualCnuCan03";
	public static final String JNDI_STATUT_QUAL_CNU_GROUPE = "fr.gouv.education.antares.ejb.listeStatutQualCnuAppelGroupeSL.ListeStatutQualCnuAppelGroupe";
	public static final String JNDI_MOT_CESS_CORP = "fr.gouv.education.antares.ejb.listeModAcceCorpSL.ListeModAcceCorp";
	public static final String JNDI_STATUT_RAPPORT_QUAL_CNU = "fr.gouv.education.antares.ejb.listeStatutRapportQualCnuSL.ListeStatutRapportQualCnu";
	public static final String JNDI_CONGE = "fr.gouv.education.antares.ejb.listeCongeSL.ListeConge";
	public static final String JNDI_DISCIPLINE = "fr.gouv.education.antares.ejb.listeDisciplineSL.ListeDiscipline";
	public static final String JNDI_DISCIPLINE_VEGA = "fr.gouv.education.antares.ejb.listeDisciplineVegaSL.ListeDisciplineVega";
	public static final String JNDI_DISCIPLINE_NON_EN = "fr.gouv.education.antares.ejb.listeSpecialiteNonEnSL.ListeSpecialiteNonEn";
	public static final String JNDI_DISCIPLINE_ENS = "fr.gouv.education.antares.ejb.listeSpecialiteEnsSL.ListeSpecialiteEns";
	public static final String JNDI_TYPE_BOE = "fr.gouv.education.antares.ejb.listeTypBoeSL.ListeTypBoe";
	public static final String JNDI_DIPLOMES = "fr.gouv.education.antares.ejb.listeDiplomesSL.ListeDiplomes";

	public static final String JNDI_CND_ATER = "fr.gouv.education.antares.ejb.cnd_aterSL.Cnd_ater";
	public static final String JNDI_CND_ATER_EN = "fr.gouv.education.antares.ejb.cnd_aterEN.Cnd_ater";

	public static final String JNDI_ENSEIGNANT = "fr.gouv.education.antares.ejb.enseignantChercheurEN.EnseignantChercheur";
	public static final String JNDI_ENSEIGNANT_ALYA = "fr.gouv.education.antares.ejb.enseignantChercheurAlyaEN.EnseignantChercheurAlya";
	public static final String JNDI_RAPPORTEUR_CNUSL = "fr.gouv.education.antares.promotion.cnu.ejb.rapporteurCnuSL.RapporteurCnu";
	public static final String JNDI_RAPPORTEUR_CRCT_CNUSL = "fr.gouv.education.antares.promotion.cnu.ejb.rapporteurCrctCnuSL.RapporteurCrctCnu";
	public static final String JNDI_RAPPORTEUR_CNUSL_ALYA = "fr.gouv.education.antares.evaluation.cnu.ejb.rapporteurCnuSL.RapporteurCnu";
	public static final String JNDI_RAPPORTEUR_CNUSL_ELECTRA = "fr.gouv.education.antares.evaluation.cnu.ejb.rapporteurCnuPromoInterneSL.RapporteurCnuPromoInterne";
	public static final String JNDI_RAPPORT = "fr.gouv.education.antares.ejb.rapportEN.Rapport";
	public static final String JNDI_RAPPORT_INTERNE = "fr.gouv.education.antares.ejb.rapportIntEN.RapportInt";
	public static final String JNDI_CRCT = "fr.gouv.education.antares.ejb.crctEN.Crct";
	public static final String JNDI_CPE = "fr.gouv.education.antares.ejb.cpeEN.Cpe";
	public static final String JNDI_PEDR = "fr.gouv.education.antares.ejb.pedrEN.Pedr";
	public static final String JNDI_PRIME = "fr.gouv.education.antares.ejb.primeEN.Prime";
	public static final String JNDI_ADEL = "fr.gouv.education.antares.ejb.adelEN.Adel";
	public static final String JNDI_UR_CNRS = "fr.gouv.education.antares.ejb.urCnrsEN.UrCnrs";
	public static final String JNDI_RAPPORT_ALYA = "fr.gouv.education.antares.ejb.rapportAlyaEN.RapportAlya";
	public static final String JNDI_SUPINFO = "fr.gouv.education.antares.ejb.supInfoEN.SupInfo";
	public static final String JNDI_INFOHU = "fr.gouv.education.antares.ejb.InfoHuEN.InfoHu";
	public static final String JNDI_INFOANT = "fr.gouv.education.antares.ejb.InfoAntEN.InfoAnt";
	public static final String JNDI_HARMONISATION = "fr.gouv.education.antares.ejb.harmonisationEN.Harmonisation";

	public static final String JNDI_RAPPORTEUR_PEDR_CNUSL = "fr.gouv.education.antares.pedr.cnu.ejb.rapporteurPedrCnuSL.RapporteurPedrCnu";
	public static final String JNDI_RAPPORTEUR_PRIME_CNUSL = "fr.gouv.education.antares.prime.cnu.ejb.rapporteurPrimeCnuSL.RapporteurPrimeCnu";
	
	public static final String JNDI_GRADE_HUSL = "fr.gouv.education.antares.ejb.gradeHuSL.GradeHu";
	
	public static final String JNDI_STATUT_RAP = "fr.gouv.education.antares.ejb.statutRapportEN.StatutRapport";
	public static final String JNDI_STATUT_RAP_INTERNE = "fr.gouv.education.antares.ejb.statutRapportIntEN.StatutRapportInt";
	public static final String JNDI_STATUT_CRCT = "fr.gouv.education.antares.ejb.statutCrctEN.StatutCrct";
	public static final String JNDI_STATUT_CPE = "fr.gouv.education.antares.ejb.statutCpeEN.StatutCpe";
	public static final String JNDI_STATUT_PEDR = "fr.gouv.education.antares.ejb.statutPedrEN.StatutPedr";
	public static final String JNDI_STATUT_QUAL = "fr.gouv.education.antares.ejb.statutQualifEN.StatutQualif";
	public static final String JNDI_STATUT_QUAL_RAPPORT = "fr.gouv.education.antares.ejb.statutQualifRapportEN.StatutQualifRapport";
	public static final String JNDI_STATUT_RAP_HU = "fr.gouv.education.antares.ejb.statutRapHUEN.StatutRapHU";
	public static final String JNDI_DIPLOME = "fr.gouv.education.antares.ejb.diplomeEN.Diplome";
	public static final String JNDI_UNITE_RECH_ETAB = "fr.gouv.education.antares.ejb.uniteRechercheEtablissementEN.UniteRechercheEtablissement";
	// public static final String JNDI_PUBLICATION_EC =
	// "fr.gouv.education.antares.ejb.publicationEnseignantChercheurEN.PublicationEnseignantChercheur";
	public static final String JNDI_MOT_CLE_RAP = "fr.gouv.education.antares.ejb.motCleRapportEN.MotCleRapport";
	public static final String JNDI_MOT_CLE_RAP_ALYA = "fr.gouv.education.antares.ejb.motCleRapportAlyaEN.MotCleRapportAlya";
	public static final String JNDI_PAY_EN = "fr.gouv.education.antares.ejb.payEN.Pay";
	public static final String JNDI_PAYLOCAL_EN = "fr.gouv.education.antares.ejb.payEN.PayLocal";
	public static final String JNDI_ORP_EN = "fr.gouv.education.antares.ejb.orpEN.Orp";
	public static final String JNDI_SSC_EN = "fr.gouv.education.antares.ejb.sscEN.Ssc";
	public static final String JNDI_GSP_EN = "fr.gouv.education.antares.ejb.gspEN.Gsp";
	public static final String JNDI_CIVILITE = "fr.gouv.education.antares.ejb.civiliteSL.Civilite";
	public static final String JNDI_CIVILITE_CODE = "fr.gouv.education.antares.ejb.civiliteLibCodeSL.CiviliteLibCode";
	public static final String JNDI_CNU = "fr.gouv.education.antares.ejb.cnuEN.Cnu";
	public static final String JNDI_CNU_HIST = "fr.gouv.education.antares.ejb.cnuHistEN.CnuHist";
	public static final String JNDI_CONSULT_DPL = "fr.gouv.education.antares.qualification.can.ejb.consultationDiplomeSL.ConsultationDiplome";
	public static final String JNDI_CONSULT_COND_ASTREE = "fr.gouv.education.antares.astree.can.ejb.consultationConditionsAstreeSL.ConsultationConditionsAstree";
	public static final String JNDI_CONSULT_COND_FIDIS = "fr.gouv.education.antares.astree.can.ejb.consultationConditionsFidisSL.ConsultationConditionsFidis";
	public static final String JNDI_CONSULT_DIPLOME_ASTREE = "fr.gouv.education.antares.astree.can.ejb.consultationDiplomeAstreeSL.ConsultationDiplomeAstree";
	public static final String JNDI_CORPS = "fr.gouv.education.antares.ejb.listecorpsSL.listeCorps";
	public static final String JNDI_ETAB_DEVAL_CPP = "fr.gouv.education.antares.ejb.listeEtatCPPSL.ListeEtat";
	public static final String JNDI_SUI = "fr.gouv.education.antares.ejb.listesuiviSL.listeSuivi";
	public static final String JNDI_CORPS_LIB = "fr.gouv.education.antares.ejb.listeCorpsLibSL.ListeCorpsLib";
	public static final String JNDI_CORPS_LIB_COURT = "fr.gouv.education.antares.ejb.listeCorpsLibCourtSL.ListeCorpsLibCourt";
	public static final String JNDI_CORPS_ID = "fr.gouv.education.antares.ejb.listeCorpsIdSL.ListeCorpsId";
	public static final String JNDI_CORPS_HM = "fr.gouv.education.antares.ejb.listeCorpsHmSL.ListeCorpsHm";
	public static final String JNDI_CORPS_HMF = "fr.gouv.education.antares.ejb.listeCorpsHmFSL.ListeCorpsHmF";
	public static final String JNDI_CTU = "fr.gouv.education.antares.ejb.candidatureEN.Candidature";
	public static final String JNDI_DOC_CTU = "fr.gouv.education.antares.ejb.candidaturePieceEN.CandidaturePiece";
	public static final String JNDI_TYP_DOC_CTU = "fr.gouv.education.antares.ejb.typeCandidaturePieceEN.TypeCandidaturePiece";
	public static final String JNDI_CTU_ASTREE = "fr.gouv.education.antares.ejb.candidatureAstreeEN.CandidatureAstree";
	public static final String JNDI_SEC_ASTREE = "fr.gouv.education.antares.ejb.listeSectionAstreeSL.listeSectionAstree";
	public static final String JNDI_HIST_CTU = "fr.gouv.education.antares.ejb.histCtuEN.HistCtu";
	public static final String JNDI_CTU_DERO = "fr.gouv.education.antares.ejb.candidatureDeroEN.CandidatureDero";
	public static final String JNDI_CTU_EMPLOI = "fr.gouv.education.antares.ejb.candidatureEmploiEN.CandidatureEmploi";
	public static final String JNDI_CTU_EMPLOI_DERO = "fr.gouv.education.antares.ejb.candidatureEmploiDeroEN.CandidatureEmploiDero";
	public static final String JNDI_CONV_GROUPE = "fr.gouv.education.antares.ejb.convocationGroupeEN.ConvocationGroupe";
	public static final String JNDI_CONV_PHARMA = "fr.gouv.education.antares.ejb.convocationPharmacieEN.ConvocationPharmacie";
	public static final String JNDI_DATE_CONV_PHARMA = "fr.gouv.education.antares.ejb.dateConvocationPharmacieEN.DateConvocationPharmacie";
	public static final String JNDI_FIND_ETAB_EMPLOI = "fr.gouv.education.antares.recrutement.admin.ejb.findEtabEmploiSL.FindEtabEmploi";
	public static final String JNDI_DIPLOME_CANDIDAT = "fr.gouv.education.antares.ejb.diplomeCandidatEN.DiplomeCandidat";
	public static final String JNDI_CONDITIONS_CTU_ASTREE = "fr.gouv.education.antares.ejb.conditionsCandidatureAstreeEN.ConditionsCandidatureAstree";
	public static final String JNDI_CONDITIONS_CTU_FIDIS = "fr.gouv.education.antares.ejb.conditionsCandidatureFidisEN.ConditionsCandidatureFidis";
	public static final String JNDI_DIPLOME_CTU_ASTREE = "fr.gouv.education.antares.ejb.diplomeCandidatureAstreeEN.DiplomeCandidatureAstree";
	public static final String JNDI_DIPLOME_CTU_FIDIS = "fr.gouv.education.antares.ejb.diplomeCandidatureFidisEN.DiplomeCandidatureFidis";
	public static final String JNDI_SUIVI_DOSSIER_ASTREE = "fr.gouv.education.antares.ejb.suiviDossierAstreeEN.SuiviDossierAstree";
	public static final String JNDI_DETAIL_DOSSIER_ASTREE = "fr.gouv.education.antares.ejb.detailDossierAstreeEN.DetailDossierAstree";
	public static final String JNDI_GESTION_AIDE = "fr.gouv.education.antares.ejb.gestionAideSL.GestionAide";
	public static final String JNDI_GESTION_TAB_REF = "fr.gouv.education.antares.ejb.gestionTabRefSL.GestionTabRef";
	public static final String JNDI_GESTION_TAB_REF_GALAXIE = "fr.gouv.education.antares.ejb.gestionTabRefGalaxieSL.GestionTabRefGalaxie";
	public static final String JNDI_CTU_VEGA = "fr.gouv.education.antares.ejb.candidatureVegaEN.CandidatureVega";
	public static final String JNDI_DIPLOME_CTU_VEGA = "fr.gouv.education.antares.ejb.diplomeCandidatureVegaEN.DiplomeCandidatureVega";
	public static final String JNDI_LAUREAT_VEGA = "fr.gouv.education.antares.ejb.laureatVegaEN.LaureatVega";
	public static final String JNDI_SAISIE_MOTIF_REFUS = "fr.gouv.education.antares.ejb.saisieMotifRefusQualifEN.SaisieMotifRefusQualif";
	public static final String JNDI_GESTION_UR_CNRS = "fr.gouv.education.antares.ejb.gestionUrCnrsSL.GestionUrCnrs";
	
	// Q59 - 24112033
	public static final String JNDI_LISTE_BUREAU = "fr.gouv.education.antares.ejb.listeBurSL.ListeBur";
	// T03
	public static final String JNDI_REGION = "fr.gouv.education.antares.ejb.regionEN.Region";
	public static final String JNDI_LISTE_REGION = "fr.gouv.education.antares.ejb.listeRegionSL.ListeRegion";
	public static final String JNDI_ACADEMIE = "fr.gouv.education.antares.ejb.acaEN.Aca";
	public static final String JNDI_GESTION_SECTION = "fr.gouv.education.antares.ejb.sectionEN.Section";
	public static final String JNDI_GESTION_CORPS = "fr.gouv.education.antares.ejb.corpsEN.Corps";
	public static final String JNDI_GESTION_GROUPE = "fr.gouv.education.antares.ejb.groupeEN.Groupe";
	public static final String JNDI_GESTION_SUI = "fr.gouv.education.antares.ejb.suiEN.Sui";
	public static final String JNDI_GESTION_DET = "fr.gouv.education.antares.ejb.detEN.Det";
	public static final String JNDI_GESTION_MDR = "fr.gouv.education.antares.ejb.mdrEN.Mdr";
	public static final String JNDI_GESTION_UR = "fr.gouv.education.antares.ejb.urEN.Ur";
	public static final String JNDI_UR_ETA = "fr.gouv.education.antares.ejb.urEtaEN.UrEta";
	public static final String JNDI_GESTION_DSC = "fr.gouv.education.antares.ejb.dscEN.Dsc";
	public static final String JNDI_GESTION_TAB_REF_SEC = "fr.gouv.education.antares.ejb.listeSectionRefSL.listeSectionRef";
	public static final String JNDI_GESTION_TAB_REF_SUI = "fr.gouv.education.antares.ejb.listeSuiviRefSL.ListeSuiviRef";
	public static final String JNDI_GESTION_TAB_REF_DET = "fr.gouv.education.antares.ejb.listeDetRefSL.ListeDetRef";
	public static final String JNDI_GESTION_ARTMDP = "fr.gouv.education.antares.ejb.listeArtMdpRefSL.ListeArtMdpRef";
	public static final String JNDI_GESTION_TAB_REF_ART = "fr.gouv.education.antares.ejb.listeArticleRefSL.ListeArticleRef";
	public static final String JNDI_GESTION_TAB_REF_BUR = "fr.gouv.education.antares.ejb.listeBurRefSL.listeBurRef";
	public static final String JNDI_GESTION_BUR = "fr.gouv.education.antares.ejb.burEN.Bur";
	public static final String JNDI_ARTICLE = "fr.gouv.education.antares.ejb.articleEN.Article";
	public static final String JNDI_ARTMDP = "fr.gouv.education.antares.ejb.artmdpEN.ArtMdp";
	public static final String JNDI_GESTION_CNU = "fr.gouv.education.antares.qualification.cnu.ejb.gestionCnuSL.GestionCnu";
	public static final String JNDI_GESTION_CONVOC_GROUPE_CNU = "fr.gouv.education.antares.qualification.cnu.ejb.gestionConvocGroupeSL.GestionConvocGroupe";
	public static final String JNDI_GESTION_RQU = "fr.gouv.education.antares.ejb.listeresultatSL.listeResultat";
	public static final String JNDI_GESTION_CTU = "fr.gouv.education.antares.qualification.can.ejb.gestionCandidatureSL.GestionCandidature";
	public static final String JNDI_GESTION_DOC_CTU = "fr.gouv.education.antares.qualification.can.ejb.gestionCandidaturePieceSL.GestionCandidaturePiece";
	public static final String JNDI_GESTION_CTU_ASTREE = "fr.gouv.education.antares.astree.can.ejb.gestionCandidatureAstreeSL.GestionCandidatureAstree";
	public static final String JNDI_GESTION_CTU_FIDIS = "fr.gouv.education.antares.astree.can.ejb.gestionCandidatureFidisSL.GestionCandidatureFidis";
	public static final String JNDI_GESTION_CTU_ATER = "fr.gouv.education.antares.astree.can.ejb.gestionCandidatureAterSL.GestionCandidatureAter";
	public static final String JNDI_GESTION_SUIVI_CTU_ASTREE = "fr.gouv.education.antares.astree.eta.ejb.gestionSuiviCtuAstreeSL.GestionSuiviCtuAstree";
	public static final String JNDI_GESTION_SUIVI_CTU_FIDIS = "fr.gouv.education.antares.astree.eta.ejb.gestionSuiviCtuFidisSL.GestionSuiviCtuFidis";
	public static final String JNDI_GESTION_COMITE_SELECTION = "fr.gouv.education.antares.ejb.gestionComiteSelectionSL.GestionComiteSelection";
	public static final String JNDI_RECHERCHE_CANDIDATS_ASTREE = "fr.gouv.education.antares.astree.eta.ejb.rechercheCandidatsAstreeSL.RechercheCandidatsAstree";
	public static final String JNDI_GESTION_ETA = "fr.gouv.education.antares.recrutement.eta.ejb.gestionEtabSL.GestionEtab";
	public static final String JNDI_GESTION_HABILITATION = "fr.gouv.education.antares.ejb.gestionHabilitationSL.GestionHabilitation";
	public static final String JNDI_GESTIONNAIRE_IDE = "fr.gouv.education.antares.ejb.gestionnaireIdeSL.GestionnaireIde";
	public static final String JNDI_GESTIONNAIRE_EN = "fr.gouv.education.antares.ejb.gestionnaireEN.Gestionnaire";
	public static final String JNDI_GESTION_VOEUX = "fr.gouv.education.antares.recrutement.can.ejb.gestionVoeuxSL.GestionVoeux";
	public static final String JNDI_GESTION_VOEUX_VEGA = "fr.gouv.education.antares.vega.can.ejb.gestionVoeuxVegaSL.GestionVoeuxVega";
	public static final String JNDI_GESTION_ATRIA = "fr.gouv.education.antares.recrutement.eta.ejb.gestionAtriaSL.GestionAtria";
	public static final String JNDI_GESTION_ADM_CPCNU = "fr.gouv.education.antares.sfcnu.adm.ejb.gestionCnuSL.GestionSFCnu";
	public static final String JNDI_GESTION_CALCUL_INDEMNITES = "fr.gouv.education.antares.ejb.gestionCalculIndemnitesSL.GestionCalculIndemnites";
	public static final String JNDI_GESTION_ENQUETE = "fr.gouv.education.antares.recrutement.eta.ejb.gestionEnqueteSL.GestionEnquete";
	public static final String JNDI_GESTION_PURGE = "fr.gouv.education.antares.recrutement.admin.ejb.gestionPurgePostesSL.GestionPurgePostes";
	public static final String JNDI_GESTION_ETA_VOL_RAP = "fr.gouv.education.antares.promotion.eta.ejb.gestionEtaSL.GestionEtablissement";
	public static final String JNDI_GESTION_ETA_VOL_RAP_ALYA = "fr.gouv.education.antares.evaluation.eta.ejb.gestionEtaSL.GestionEtablissement";
	public static final String JNDI_GESTION_ETA_ELARA = "fr.gouv.education.antares.pedr.eta.ejb.gestionEtaSL.GestionEtablissement";
	public static final String JNDI_GESTION_CTU_VEGA = "fr.gouv.education.antares.vega.can.ejb.gestionCandidatureVegaSL.GestionCandidatureVega";
	public static final String JNDI_LISTE_CANDIDATURE = "fr.gouv.education.antares.ejb.listeCandidatureSL.ListeCandidature";
	public static final String JNDI_LISTE_POSITION_CHIRON = "fr.gouv.education.antares.ejb.listePositionChironSL.ListePositionChiron";
	public static final String JNDI_REPARTITION_DOSIER_UNITAIRE = "fr.gouv.education.antares.ejb.repartition.unitaire.dossierSL.RepartionDossierUnitaire";
	public static final String JNDI_REPARTITION_LETTRE = "fr.gouv.education.antares.ejb.repartitionlettreSL.RepartitionLettre";
	public static final String JNDI_LISTE_DOSSIER_CONFIE = "fr.gouv.education.antares.ejb.listeDossierConfieSL.ListeDossierConfie";
	public static final String JNDI_CONSULT_MOTIF_TRANSFERT = "fr.gouv.education.antares.ejb.consultationMotifTransfertSL.ConsultationMotifTransfert";
	//MSU 1323
	public static final String JNDI_GESTION_ETA_NAOS = "fr.gouv.education.antares.naos.eta.ejb.gestionEtaSL.GestionEtablissement";

	public static final String JNDI_GESTION_SECOND_DEGRE = "fr.gouv.education.antares.ejb.gestionSecondDegreSL.GestionSecondDegre";
	public static final String JNDI_GESTION_ALERTE = "fr.gouv.education.antares.ejb.gestionAlerteSL.GestionAlerte";
	public static final String JNDI_GESTION_MULTILANG = "fr.gouv.education.antares.ejb.gestionMultilangSL.GestionMultilang";
	public static final String JNDI_GESTION_MODE_TRANSMISSION = "fr.gouv.education.antares.ejb.gestionModeTransmissionSL.GestionModeTransmission";
	public static final String JNDI_GESTION_DISCIPLINE = "fr.gouv.education.antares.ejb.gestionDisciplineSL.GestionDiscipline";
	public static final String JNDI_PROMO_RECHCNU = "fr.gouv.education.antares.promotion.cnu.ejb.rechercheCnuSL.RechercheCnu";
	public static final String JNDI_PROMO_CRCT_RECHCNU = "fr.gouv.education.antares.promotion.cnu.ejb.rechercheCrctCnuSL.RechercheCrctCnu";
	public static final String JNDI_PROMO_RECHCNU_ALYA = "fr.gouv.education.antares.evaluation.cnu.ejb.rechercheCnuSL.RechercheCnu";
	public static final String JNDI_PROMO_RECHCNU_ELECTRA = "fr.gouv.education.antares.evaluation.cnu.ejb.rechercheCnuPromoInterneSL.RechercheCnuPromoInterne";
	public static final String JNDI_GESTION_AVIS_RAP = "fr.gouv.education.antares.ejb.avisRapportSL.AvisRapport";
	public static final String JNDI_GESTION_AVIS_RAP_INTERNE = "fr.gouv.education.antares.ejb.avisRapportInterneSL.AvisRapportInterne";
	public static final String JNDI_GESTION_AVIS_RAP_ALYA = "fr.gouv.education.antares.ejb.avisRapportAlyaSL.AvisRapportAlya";
	public static final String JNDI_GESTION_ATRIA_ADM = "fr.gouv.education.antares.recrutement.admin.ejb.gestionAtriaAdmSL.GestionAtriaAdm";
	public static final String JNDI_EMPLOI = "fr.gouv.education.antares.ejb.emploiEN.Emploi";
	public static final String JNDI_EMPLOI_FIDIS = "fr.gouv.education.antares.ejb.emploiFidisEN.EmploiFidis";
	public static final String JNDI_GESTION_AVIS_PRIME = "fr.gouv.education.antares.ejb.avisPrimeSL.AvisPrime";
	//Evo 58
	public static final String JNDI_EMPLOI_VEGA = "fr.gouv.education.antares.ejb.emploiVegaEN.EmploiVega";
	public static final String JNDI_CONCOURS_CANOPUS = "fr.gouv.education.antares.ejb.concoursCanopusEN.ConcoursCanopus";
	public static final String JNDI_VEGA_CONDITION = "fr.gouv.education.antares.ejb.cndVegaEN.CndVega";
	public static final String JNDI_VEGA_DIPLOME = "fr.gouv.education.antares.ejb.diplomeVegaEN.DiplomeVega";

	public static final String JNDI_ETABLISSEMENT = "fr.gouv.education.antares.ejb.etablissementEN.Etablissement";
	public static final String JNDI_GESTION_ETABLISSEMENT = "fr.gouv.education.antares.ejb.gestionEtablissementSL.GestionEtablissement";
	public static final String JNDI_GESTION_ID = "fr.gouv.education.antares.qualification.can.ejb.gestionIdentiteSL.gestionIdentite";
	public static final String JNDI_GESTION_EC = "fr.gouv.education.antares.promotion.ech.ejb.gestionEchSL.gestionIdentite";
	public static final String JNDI_GESTION_EC_ALYA = "fr.gouv.education.antares.evaluation.ech.ejb.gestionEchSL.GestionIdentite";
	public static final String JNDI_GESTION_RAP = "fr.gouv.education.antares.promotion.ech.ejb.gestionRapportSL.gestionRapport";
	public static final String JNDI_GESTION_RAP_PROMOTION_INTERNE = "fr.gouv.education.antares.promotion.ech.ejb.gestionRapportPromoInterneSL.GestionRapportPromoInterne";
	public static final String JNDI_GESTION_RAP_HU = "fr.gouv.education.antares.promotion.ech.ejb.gestionRapportHUSL.GestionRapportHU";
	public static final String JNDI_GESTION_HARMONISATION = "fr.gouv.education.antares.ejb.gestionHarmonisationSL.GestionHarmonisation";
	public static final String JNDI_GESTION_ADEL = "fr.gouv.education.antares.ejb.gestionAdelSL.GestionAdel";
	public static final String JNDI_GESTION_RAP_ALYA = "fr.gouv.education.antares.evaluation.ech.ejb.gestionRapportSL.GestionRapport";
	public static final String JNDI_GESTION_PEDR = "fr.gouv.education.antares.pedr.ech.ejb.gestionPedrSL.GestionPedr";
	public static final String JNDI_GESTION_TAB_REF_ANOREM = "fr.gouv.education.antares.ejb.listeAnoremRefSL.ListeAnoremRef";
	public static final String JNDI_GESTION_TAB_REF_ANOREM_ANT = "fr.gouv.education.antares.ejb.listeAnoremAntRefSL.ListeAnoremAntRef";
	public static final String JNDI_ANOREM = "fr.gouv.education.antares.ejb.anoremEN.Anorem";
	public static final String JNDI_ANOREM_ANT = "fr.gouv.education.antares.ejb.anoremAntEN.AnoremAnt";
	public static final String JNDI_GESTION_PRIME = "fr.gouv.education.antares.prime.ech.ejb.gestionPrimeSL.GestionPrime";
	
	public static final String JNDI_ELARA_PROMO_RECHCNU = "fr.gouv.education.antares.pedr.cnu.ejb.recherchePedrCnuSL.RecherchePedrCnu";
	
	public static final String JNDI_ELARA_PRIME_RECHCNU = "fr.gouv.education.antares.prime.cnu.ejb.recherchePrimeCnuSL.RecherchePrimeCnu";
	
	public static final String JNDI_GESTION_CRCT = "fr.gouv.education.antares.promotion.ech.ejb.gestionCrctSL.gestionCrct";
	public static final String JNDI_GESTION_CPE = "fr.gouv.education.antares.promotion.ech.ejb.gestionCpeSL.gestionCpe";
	public static final String JNDI_GESTION_EVAL_ALYA = "fr.gouv.education.antares.evaluation.ech.ejb.gestionAlyaSL.GestionAlya";
	
	public static final String JNDI_GESTION_CNAP = "fr.gouv.education.antares.ejb.gestionCnapSL.GestionCnap";
	public static final String JNDI_CNAP = "fr.gouv.education.antares.ejb.cnapEN.Cnap";
	public static final String JNDI_SEC_CNAP = "fr.gouv.education.antares.ejb.secCnapEN.SecCnap";

	public static final String JNDI_AVIS_RAPPORT_SL = "fr.gouv.education.antares.ejb.avisRapportSL.AvisRapport";
	public static final String JNDI_AVIS_PEDR_SL = "fr.gouv.education.antares.ejb.avisPedrSL.AvisPedr";
	public static final String JNDI_AVIS_PRIME_SL = "fr.gouv.education.antares.ejb.avisPrimeSL.AvisPrime";
	public static final String JNDI_AVIS_RAPPORT_SL_ALYA = "fr.gouv.education.antares.ejb.avisRapportAlyaSL.AvisRapportAlya";
	public static final String JNDI_AVIS_RAPPORT_EN = "fr.gouv.education.antares.ejb.avisRapportEN.AvisRapport";
	public static final String JNDI_PARAM_ARRETES_CHIRON = "fr.gouv.education.antares.ejb.paramArretesChironEN.ParamArretesChiron";
	public static final String JNDI_AVIS_PEDR_EN = "fr.gouv.education.antares.ejb.avisPedrEN.AvisPedr";
	public static final String JNDI_AVIS_PRIME_EN = "fr.gouv.education.antares.ejb.avisPrimeEN.AvisPrime";
	public static final String JNDI_AVIS_RAPPORT_EN_ALYA = "fr.gouv.education.antares.ejb.avisRapportAlyaEN.AvisRapportAlya";
	public static final String JNDI_JUR = "fr.gouv.education.antares.ejb.juryEN.Jury";
	public static final String JNDI_JUR_ASTREE = "fr.gouv.education.antares.ejb.juryAstreeEN.JuryAstree";
	public static final String JNDI_JUR_FIDIS = "fr.gouv.education.antares.ejb.juryFidisEN.JuryFidis";
	public static final String JNDI_NATIONALITE = "fr.gouv.education.antares.ejb.nationaliteSL.Nationalite";
	public static final String JNDI_NUM_SESSION = "fr.gouv.education.antares.ejb.numSessionSL.NumSession";
	public static final String JNDI_PARAM = "fr.gouv.education.antares.ejb.parametresEN.Parametres";
	public static final String JNDI_PARAM_USERS = "fr.gouv.education.antares.ejb.parametresUsersEN.ParametresUsers";
	public static final String JNDI_PAYS = "fr.gouv.education.antares.ejb.paysSL.Pays";
	public static final String JNDI_PAYS_RAPPORT = "fr.gouv.education.antares.ejb.paysRapportSL.Pays";
	public static final String JNDI_PERIODE_PHASE_EN = "fr.gouv.education.antares.ejb.periodephaseEN.periodePhase";
	public static final String JNDI_PERIODE_PHASE_EN_RO = "fr.gouv.education.antares.ejb.periodephaseEN.periodePhaseRO";
	public static final String JNDI_PERIODE_PHASE_SL = "fr.gouv.education.antares.ejb.periodephaseSL.PeriodePhase";
	public static final String JNDI_SEC = "fr.gouv.education.antares.ejb.listesectionSL.listeSection";
	public static final String JNDI_SEC_MCC = "fr.gouv.education.antares.ejb.listesectionMccSL.listeSectionMcc";
	public static final String JNDI_ETAT_PEDR = "fr.gouv.education.antares.ejb.listeEtatPedrSL.ListeEtat";
	public static final String JNDI_ETAT_PRIP = "fr.gouv.education.antares.ejb.listeEtatPripSL.ListeEtatPrip";
	public static final String JNDI_ETAB = "fr.gouv.education.antares.ejb.listeEtabSL.ListeEtab";
	public static final String JNDI_ETAB_RATT_ACTI = "fr.gouv.education.antares.ejb.listeEtabRattActiSL.ListeEtabRattActi";
	public static final String JNDI_REG = "fr.gouv.education.antares.ejb.listeRegionSL.ListeRegion";
	public static final String JNDI_SEC_MEDI = "fr.gouv.education.antares.ejb.listeSectionMediSL.ListeSectionMedi";
	public static final String JNDI_SEC_GRP = "fr.gouv.education.antares.ejb.listeSectionGroupeSL.ListeSectionGroupe";
	public static final String JNDI_SSSEC = "fr.gouv.education.antares.ejb.listeSousSectionSL.ListeSousSection";
	public static final String JNDI_SSC_DISC = "fr.gouv.education.antares.ejb.listeSousSectionDiscSL.ListeSousSectionDisc";
	public static final String JNDI_SEC_PHARMA = "fr.gouv.education.antares.ejb.listeSectionPharmacieSL.ListeSectionPharmacie";
	public static final String JNDI_MODE_FIN_TZ = "fr.gouv.education.antares.ejb.listeModeFinancementTzSL.ListeModeFinancementTz";
	public static final String JNDI_SITUPROF = "fr.gouv.education.antares.ejb.situprofSL.Situprof";
	public static final String JNDI_SITUPROFDETAC = "fr.gouv.education.antares.ejb.situProfDetacSL.SituProfDetac";
	// 20/10/2009 rajout pour Vivier Versailles
	public static final String JNDI_SITUPROFVIV = "fr.gouv.education.antares.ejb.situProfVivierSL.SituProfVivier";

	public static final String JNDI_SITUPROFDETACPR = "fr.gouv.education.antares.ejb.situProfDetacPrSL.SituProfDetacPr";
	public static final String JNDI_ORIG_PROF = "fr.gouv.education.antares.ejb.listeSituProfSL.ListeSituProf";
	public static final String JNDI_ORIG_PROF_TZ = "fr.gouv.education.antares.ejb.listeSituProfTzSL.ListeSituProfTz";
	public static final String JNDI_PRIVILEGE_SL = "fr.gouv.education.antares.ejb.privilegeSL.Privilege";
	public static final String JNDI_PUBLICATION = "fr.gouv.education.antares.ejb.publicationEN.Publication";
	public static final String JNDI_RECHERCHE_CNU = "fr.gouv.education.antares.qualification.cnu.ejb.rechercheCnuSL.RechercheCnu";
	public static final String JNDI_RECHERCHE_CANDIDAT_SL = "fr.gouv.education.antares.qualification.admin.ejb.rechercheCandidatSL.RechercheCandidat";
	public static final String JNDI_RECHERCHE_CANDIDAT_DERO_SL = "fr.gouv.education.antares.qualification.admin.ejb.rechercheCandidatDeroSL.RechercheCandidatDero";
	public static final String JNDI_RECHERCHE_CANDIDAT = "fr.gouv.education.antares.qualification.admin.ejb.rechercheCandidatSF.RechercheCandidat";
	public static final String JNDI_RECHERCHE_CANDIDAT_GROUPE = "fr.gouv.education.antares.qualification.admin.ejb.rechercheCandGroupeSL.RechercheCandGroupe";
	public static final String JNDI_RECHERCHE_CANDIDATURE = "fr.gouv.education.antares.qualification.can.ejb.rechercheCandidatureSL.RechercheCandidature";
	public static final String JNDI_RECHERCHE_CLASSES = "fr.gouv.education.antares.recrutement.eta.ejb.rechercheClassesSL.RechercheClasses";
	public static final String JNDI_RECHERCHE_QUALIFIE = "fr.gouv.education.antares.recrutement.eta.ejb.rechercheQualifiesSF.RechercheQualifies";
	public static final String JNDI_RECH_EMPLOIS = "fr.gouv.education.antares.recrutement.admin.ejb.rechercheEmploisSL.RechercheEmplois";
	public static final String JNDI_PROFIL = "fr.gouv.education.antares.ejb.profilEN.Profil";
	public static final String JNDI_PROFIL_ADMIN = "fr.gouv.education.antares.ejb.profilEN.ProfilAdmin";
	public static final String JNDI_MDR = "fr.gouv.education.antares.ejb.listeModeRecrutementSL.ListeModeRecrutement";
	public static final String JNDI_MDR_ASTREE = "fr.gouv.education.antares.ejb.listeModeRecrutAstreeSL.ListeModeRecrutAstree";
	public static final String JNDI_MDR_ART = "fr.gouv.education.antares.ejb.listeModeRecrutArtSL.ListeModeRecrutArt";
	public static final String JNDI_MDER_ASTREE = "fr.gouv.education.antares.ejb.listeModeRecrutementAstreeSL.ListeModeRecrutementAstree";

	public static final String JNDI_LISTE_NATEMPCORPS = "fr.gouv.education.antares.ejb.listeNatureEmploiCorpsSL.ListeNatureEmploiCorps";
	public static final String JNDI_LISTE_PARAM = "fr.gouv.education.antares.ejb.listeParametresSL.ListeParametres";
	public static final String JNDI_LISTE_PROFIL = "fr.gouv.education.antares.ejb.listeProfilSL.ListeProfil";
	public static final String JNDI_LISTE_PHASE = "fr.gouv.education.antares.ejb.listePhaseSL.ListePhase";
	public static final String JNDI_LISTE_BATCH = "fr.gouv.education.antares.ejb.listeBatchSL.ListeBatch";
	public static final String JNDI_LISTE_PHASE_QUALIF = "fr.gouv.education.antares.ejb.listePhaseQualifSL.ListePhaseQualif";
	public static final String JNDI_LISTE_PHASE_RECRUT = "fr.gouv.education.antares.ejb.listePhaseRecrutSL.ListePhaseRecrut";
	public static final String JNDI_LISTE_PHASE_ASTREE = "fr.gouv.education.antares.ejb.listePhaseAstreeSL.ListePhaseAstree";
	public static final String JNDI_LISTE_PHASE_SIRIUS = "fr.gouv.education.antares.ejb.listePhaseSiriusSL.ListePhaseSirius";
	public static final String JNDI_LISTE_PHASE_ACTIVITE = "fr.gouv.education.antares.ejb.listePhaseActiviteSL.ListePhaseActivite";
	public static final String JNDI_LISTE_PHASE_ELARA = "fr.gouv.education.antares.ejb.listePhaseElaraSL.ListePhaseElara";
	public static final String JNDI_LISTE_PHASE_ALYA = "fr.gouv.education.antares.ejb.listePhaseAlyaSL.ListePhaseAlya";
	public static final String JNDI_LISTE_SUIVI_AUTORISE_ASTREE = "fr.gouv.education.antares.ejb.listeSuiviAutoriseAstreeSL.ListeSuiviAutoriseAstree";
	public static final String JNDI_LISTE_POSTE_ASTREE = "fr.gouv.education.antares.ejb.listePostesAstreeSL.ListePostesAstree";
	public static final String JNDI_LISTE_PHASE_NAOS = "fr.gouv.education.antares.ejb.listePhaseNaosSL.ListePhaseNaos";
	public static final String JNDI_LISTE_PHASE_SIRAH = "fr.gouv.education.antares.ejb.listePhaseSirahSL.ListePhaseSirah";
	public static final String JNDI_LISTE_POSTE_FIDIS = "fr.gouv.education.antares.ejb.listePostesFidisSL.ListePostesFidis";
	public static final String JNDI_LISTE_DSC = "fr.gouv.education.antares.ejb.listeDomScientSL.ListeDomScient";
	public static final String JNDI_TYPE_BATCH = "fr.gouv.education.antares.ejb.typeBatchsEN.TypeBatchs";
	public static final String JNDI_GESTION_BATCH = "fr.gouv.education.antares.ejb.gestionBatchsSL.GestionBatchs";

	public static final String JNDI_SECTIONALL = "fr.gouv.education.antares.ejb.listesectionallSL.ListeSectionAll";

	public static final String JNDI_LISTE_TYPE_REM = "fr.gouv.education.antares.ejb.listeTypDeRemonteSL.ListeTypDeRemonte";
	public static final String JNDI_LISTE_TYPE_REM_DET = "fr.gouv.education.antares.ejb.listeTypDeRemonteDetSL.ListeTypDeRemonteDet";

	public static final String JNDI_GESTION_TRAITEMENTS = "fr.gouv.education.antares.ejb.gestionTraitementsSL.GestionTraitements";
	public static final String JNDI_TRAITEMENT = "fr.gouv.education.antares.ejb.traitementEN.Traitement";
	public static final String JNDI_LISTE_BUR = "fr.gouv.education.antares.ejb.listeBurSL.ListeBur";
	public static final String JNDI_LISTE_BUR_GEST_RECRUT = "fr.gouv.education.antares.ejb.listeBurGestSL.ListeBurGest";
	public static final String JNDI_GROUPE_EMPLOI_LIE = "fr.gouv.education.antares.ejb.groupeEmploiLieEN.GroupeEmploiLie";
	public static final String JNDI_PAGE = "fr.gouv.education.antares.ejb.pageEN.Page";
	public static final String JNDI_GESTION_PUB = "fr.gouv.education.antares.qualification.admin.ejb.gestionPublicationSL.GestionPub";
	public static final String JNDI_LISTE_RESULTAT = "fr.gouv.education.antares.ejb.listeresultatSL.listeResultat";
	public static final String JNDI_LISTE_RESULTAT_ACTIF = "fr.gouv.education.antares.ejb.listeresultatActifSL.listeResultatActif";
	public static final String JNDI_LISTE_RESULTAT_DERO = "fr.gouv.education.antares.ejb.listeResultatDeroSL.ListeResultatDero";
	public static final String JNDI_LISTE_RESULTAT_GROUPE = "fr.gouv.education.antares.ejb.listeResultatGroupeSL.ListeResultatGroupe";
	public static final String JNDI_RECHERCHE_DOUBLON = "fr.gouv.education.antares.qualification.admin.ejb.rechercheDoublonSL.RechercheDoublon";
	public static final String JNDI_GESTION_PHARMACIE = "fr.gouv.education.antares.qualification.admin.ejb.gestionPharmacieSL.GestionPharmacie";
	public static final String JNDI_CONSULTATION_CONVOC = "fr.gouv.education.antares.qualification.can.ejb.consultationConvocSL.ConsultationConvoc";
	public static final String JNDI_GESTION_CONVOC_GROUPE = "fr.gouv.education.antares.qualification.admin.ejb.gestionConvocGroupeSL.GestionConvocGroupe";
	public static final String JNDI_GESTION_RESULTAT_GROUPE = "fr.gouv.education.antares.qualification.admin.ejb.gestionResultatGroupeSL.GestionResultatGroupe";
	public static final String JNDI_BILAN_AFFECTATION = "fr.gouv.education.antares.recrutement.admin.ejb.bilanAffectationSL.BilanAffectation";
	public static final String JNDI_CONCOURS = "fr.gouv.education.antares.ejb.listeConcoursSL.listeConcours";
	public static final String JNDI_LISTE_ACADEMIE = "fr.gouv.education.antares.ejb.listeAcademieSL.ListeAcademie";
	public static final String JNDI_LISTE_QUALIF_ASTREE = "fr.gouv.education.antares.ejb.listeQualificationAstreeSL.ListeQualificationAstree";
	public static final String JNDI_EXTRACTION_ASTREE = "fr.gouv.education.antares.ejb.extractionEtabAstreeEN.ExtractionEtabAstree";
	public static final String JNDI_GESTION_EXTRACTION_ASTREE = "fr.gouv.education.antares.astree.eta.ejb.gestionExtractionAstreeSL.GestionExtractionAstree";
	public static final String JNDI_GESTION_EXTRACTION_DGRH = "fr.gouv.education.antares.astree.eta.ejb.gestionExtractionDgrhSL.GestionExtrationDGRH";
	public static final String JNDI_GESTION_EXTRACTION_FIDIS = "fr.gouv.education.antares.astree.eta.ejb.gestionExtractionFidisSL.GestionExtractionFidis";
	public static final String JNDI_GESTION_RESULTATS_ANTARES = "fr.gouv.education.antares.astree.admin.ejb.gestionResultatsAntaresSL.GestionResultatsAntares";
	public static final String JNDI_GESTION_AFFECTATION_CAN = "fr.gouv.education.antares.recrutement.can.ejb.gestionAffectationSL.GestionAffectation";
	public static final String JNDI_RECHERCHE_DOUBLON_FIDIS = "fr.gouv.education.antares.qualification.admin.ejb.rechercheDoublonFidisSL.FidisRechercheDoublon";
	public static final String JNDI_RECHERCHE_DOUBLON_CANOPUS = "fr.gouv.education.antares.qualification.admin.ejb.rechercheDoublonCanopusSL.CanopusRechercheDoublon";
	public static final String JNDI_RECHERCHE_DOUBLON_ALTAIR = "fr.gouv.education.antares.qualification.admin.ejb.rechercheDoublonAltairSL.AltairRechercheDoublon";

	// evo Galaxie : Etablissement - informations sur les agents de
	// l'etablissement
	public static final String JNDI_GESTION_INFO_AGENTS = "fr.gouv.education.antares.ejb.gestionInfoAgentSL.GestionInfoAgents";

	public static final String JNDI_GESTION_EMPLOI = "fr.gouv.education.antares.recrutement.admin.ejb.gestionEmploiSL.GestionEmploi";

	public static final String JNDI_LISTE_TYPETAB = "fr.gouv.education.antares.ejb.listeTypEtabSL.ListeTypEtab";
	public static final String JNDI_GESTION_EDITIONS_ASTREE = "fr.gouv.education.antares.astree.editions.ejb.gestionEditionsAstreeSL.GestionEditionsAstree";
	public static final String JNDI_GESTION_SAISIE_EMPLOI_ASTREE = "fr.gouv.education.antares.astree.eta.ejb.gestionSaisieEmploisAstreeSL.GestionSaisieEmploisAstree";

	// Liste crée pour CNU
	public static final String JNDI_LISTE_QUALITE = "fr.gouv.education.antares.ejb.listequaliteSL.ListeQualite";
	//EVO appel au groupe 221128 - ajout liste qualité de groupe
	public static final String JNDI_LISTE_QUALITE_GROUPE = "fr.gouv.education.antares.ejb.listeQualiteGroupeSL.ListeQualiteGroupe";
	public static final String JNDI_LISTE_QUALITE_CPCNU = "fr.gouv.education.antares.ejb.listeQualiteCPCNUSL.ListeQualiteCPCNU";
	public static final String JNDI_LISTE_GROUPE = "fr.gouv.education.antares.ejb.listegroupeSL.ListeGroupe";
	public static final String JNDI_LISTE_COLLEGE = "fr.gouv.education.antares.ejb.listeCollegeSL.ListeCollege";
	public static final String JNDI_LISTE_ETAB = "fr.gouv.education.antares.ejb.listeEtabSL.ListeEtab";
	public static final String JNDI_LISTE_ETABCNU = "fr.gouv.education.antares.ejb.listeEtabCNUSL.ListeEtabCNU";
	public static final String JNDI_LISTE_ETAB_NUMETAB = "fr.gouv.education.antares.ejb.listeEtabNumetabSL.ListeEtabNumetab";
	public static final String JNDI_LISTE_ETABR = "fr.gouv.education.antares.ejb.listeEtabRSL.ListeEtabR";
	public static final String JNDI_LISTE_SITUATION = "fr.gouv.education.antares.ejb.listeSituationSL.ListeSituation";

	public static final String JNDI_LISTE_GRADE = "fr.gouv.education.antares.ejb.listeGradeSL.ListeGrade";
	public static final String JNDI_AUTHENTIFICATION_EN = "fr.gouv.education.antares.ejb.authentificationEN.Authentification";
	// Liste crée pour Odm
	public static final String JNDI_ORDRE_MISSION = "fr.gouv.education.antares.ejb.ordreMissionEN.OrdreMission";
	public static final String JNDI_ORDRE_MISSION_DERO = "fr.gouv.education.antares.ejb.ordreMissionDeroEN.OrdreMissionDero";
	public static final String JNDI_ORDRE_MISSION_GROUPE = "fr.gouv.education.antares.ejb.ordreMissionGroupeEN.OrdreMissionGroupe";
	// Q32 ordre de mission sante
	public static final String JNDI_ORDRE_MISSION_SANTE = "fr.gouv.education.antares.ejb.ordreMissionSanteEN.OrdreMissionSante";
	public static final String JNDI_SEANCE = "fr.gouv.education.antares.ejb.seanceEN.Seance";
	public static final String JNDI_SEANCE_GROUPE = "fr.gouv.education.antares.ejb.seanceGroupeEN.SeanceGroupe";
	public static final String JNDI_SEANCE_DERO = "fr.gouv.education.antares.ejb.seanceDeroEN.SeanceDero";
	// Q32 ordre de mission sante
	public static final String JNDI_SEANCE_SANTE = "fr.gouv.education.antares.ejb.seanceSanteEN.SeanceSante";
	public static final String JNDI_GESTION_ORDMIS = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisSL.GestionOrdMis";
	public static final String JNDI_GESTION_ORDMIS_DERO = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisDeroSL.GestionOrdMisDero";
	public static final String JNDI_GESTION_ORDMIS_GROUPE = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisGroupeSL.GestionOrdMisGroupe";
	// Q32 ordre de mission sante
	public static final String JNDI_GESTION_ORDMIS_SANTE = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisSanteSL.GestionOrdMisSante";
	// evo ordre mission pharmacie
	public static final String JNDI_GESTION_ORDMIS_PHARMA = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisPharmaSL.GestionOrdMisPharma";
	public static final String JNDI_ORDRE_MISSION_PHARMA = "fr.gouv.education.antares.ejb.ordreMissionPharmaEN.OrdreMissionPharma";
	public static final String JNDI_SEANCE_PHARMA = "fr.gouv.education.antares.ejb.seancePharmaEN.SeancePharma";

	// Q33 ordre de mission sdpes
	public static final String JNDI_GESTION_ORDMIS_SDPES = "fr.gouv.education.antares.qualification.admin.ejb.gestionOrdMisSDPESSL.GestionOrdMisSDPES";
	public static final String JNDI_TYPE_ORDMIS = "fr.gouv.education.antares.ejb.typOrdMisEN.TypOrdMis";
	public static final String JNDI_LISTE_TYPE_ORDMIS = "fr.gouv.education.antares.ejb.listeTypOrdMisSL.ListeTypOrdMis";
	public static final String JNDI_LISTE_TYPE_ODM_DERO = "fr.gouv.education.antares.ejb.listeTypOrdMisDeroSL.ListeTypOrdMisDero";
	public static final String JNDI_LISTE_TYPE_ODM_GRS = "fr.gouv.education.antares.ejb.listeTypOrdMisGroupeSL.ListeTypOrdMisGroupe";
	public static final String JNDI_LISTE_TYPE_ODM_SDPES = "fr.gouv.education.antares.ejb.listeTypOrdMisSDPESSL.ListeTypOrdMisSDPES";
	public static final String JNDI_LISTE_TYPE_ODM_SANTE = "fr.gouv.education.antares.ejb.listeTypOrdMisSanteSL.ListeTypOrdMisSante";
	public static final String JNDI_LISTE_TYPE_ODM_SFCNU = "fr.gouv.education.antares.ejb.listeTypOrdMisSfCnuSL.ListeTypOrdMisSfCnu";
	public static final String JNDI_LISTE_TYPE_ODM_PHARMA = "fr.gouv.education.antares.ejb.listeTypOrdMisPharmaSL.ListeTypOrdMisPharma";
	public static final String JNDI_GESTION_NOMINATION_ANTEE_FIDIS = "fr.gouv.education.antares.ejb.gestionNominationAnteeFidisSL.GestionNominationAnteeFidis";
	// ordre de mission évaluation
	public static final String JNDI_ORDRE_MISSION_EVAL = "fr.gouv.education.antares.ejb.ordreMissionEvalEN.OrdreMissionEval";
	public static final String JNDI_SEANCE_EVAL = "fr.gouv.education.antares.ejb.seanceEvalEN.SeanceEval";

	// evo
	public static final String JNDI_GESTION_ORDMIS_SFCNU = "fr.gouv.education.antares.sfcnu.adm.ejb.gestionOrdMisSfCnuSL.GestionOrdMisSfCnu";
	// liste rajouté pour newsletter
	public static final String JNDI_NWL_EN = "fr.gouv.education.antares.ejb.nwlEN.Nwl";
	public static final String JNDI_NEWSLETTER = "fr.gouv.education.antares.ejb.gestionNewsletterSL.GestionNewsletter";

	// evo Génération automatique des NUMEN
	public static final String JNDI_GESTION_NUMEN = "fr.gouv.education.antares.ejb.gestionNumenSL.GestionNumen";
	public static final String JNDI_NUMEN = "fr.gouv.education.antares.ejb.numenEN.Numen";
	// liste rajouté pour vivier comite selection
	public static final String JNDI_VIVIER = "fr.gouv.education.antares.ejb.vivEN.Viv";
	public static final String JNDI_GESTION_VIVIER = "fr.gouv.education.antares.ejb.gestionVivierSL/GestionVivier";
	public static final String CHOIXI_TYPORDMIS = "1";
	// Q27 repeter adresse si existe
	// Versailles - le 2/07/2003
	public static final String ODM_TYP_COL1 = "2";
	public static final String ODM_TYP_COL2 = "3";
	public static final String ODM_TYP_GRP_COL1 = "4";
	public static final String ODM_TYP_GRP_COL2 = "5";
	// Q27-FIN
	// Q272
	public static final String ODM_TYP_DERO_COL1 = "13";
	public static final String ODM_TYP_DERO_COL2 = "14";

	// Q50 prise en compte de "tous coprs confondus"
	public static final int TOUS_CORPS_CONFONDUS = 0;

	// prise en compte de tous les jures de l'épreuve pour les ordres de mission
	// agrégation (écran ODM-L)
	public static final int TOUS_JURES_EPG = 0;

	// Validateurs
	public static final String JNDI_VLD_TAB_DPL = "fr.gouv.education.antares.qualification.can.taglibs.TagValidatorCheckbox";
	public static final String JNDI_VLD_TAB_CLA = "fr.gouv.education.antares.recrutement.eta.taglibs.TagValidatorClassement";
	public static final String JNDI_VLD_TAB_CLA_FIDIS = "fr.gouv.education.antares.recrutement.eta.taglibs.TagValidatorClassementFidis";
	public static final String JNDI_VLD_TAB_CLA_ANTFID = "fr.gouv.education.antares.outils.servlets.jur.antfid.taglib.TagValidatorClassementAntFid";
	public static final String JNDI_VLD_TAB_CLA_VEGA = "fr.gouv.education.antares.vega.eta.taglibs.TagValidatorClassementVega";	
	public static final String JNDI_VLD_VOEUX = "fr.gouv.education.antares.recrutement.can.taglibs.TagValidatorVoeux";
	public static final String JNDI_VLD_DATE = "fr.gouv.education.antares.qualification.can.taglibs.TagValidationDate";
	public static final String JNDI_VLD_DATE_ASTREE = "fr.gouv.education.antares.astree.can.taglibs.TagValidationDate";
	public static final String JNDI_VLD_NUMECAN_NUMQUALIF = "fr.gouv.education.antares.recrutement.admin.taglibs.TagValidatorNumecanNumqualif";
	public static final String JNDI_VLD_NUMEN = "fr.gouv.education.antares.recrutement.eta.taglibs.TagValidatorNumen";
	public static final String JNDI_VLD_SAISI_EMPLOIS = "fr.gouv.education.antares.recrutement.admin.taglibs.TagValidatorSaisiEmplois";
	// EvoR1
	public static final String JNDI_VLD_SAISI_EMPLOIS_ETA = "fr.gouv.education.antares.recrutement.eta.taglibs.TagValidatorSaisiEmploisEta";
	public static final String JNDI_VLD_CANDIDATURE = "fr.gouv.education.antares.qualification.can.taglibs.TagValidatorCandidature";
	public static final String JNDI_VLD_CANDIDATURE_ASTREE = "fr.gouv.education.antares.astree.can.taglibs.TagValidatorCandidatureAstree";

	// Reporting
	public static final String JNDI_REPORT = "fr.gouv.education.antares.reporting.ejb.reportSL.Report";
	public static final String JNDI_REPORT_ADRESSE_CNU = "fr.gouv.education.antares.reporting.ejb.reportAdresseCnuSL.ReportAdresseCnu";
	public static final String JNDI_REPORT_ADRESSE_CANDIDAT_CNU = "fr.gouv.education.antares.reporting.ejb.reportAdresseCandidatCnuSL.ReportAdresseCnu";
	public static final String JNDI_REPORT_SANTE = "fr.gouv.education.antares.reporting.ejb.reportSanteSL.reportSante";
	public static final String JNDI_REPORT_RELECT = "fr.gouv.education.antares.reporting.ejb.reportRelectSL.Relect";
	public static final String JNDI_REPORT_SECTION = "fr.gouv.education.antares.ejb.sectionEN.Section";

	// GALAXIE 02/09/2008 RLI
	public static final String JNDI_RECHERCHE_POSTE = "fr.gouv.education.antares.ejb.recherchePosteSF.RecherchePoste";
	public static final String JNDI_GESTION_POSTE_ANTEE = "fr.gouv.education.antares.recrutement.eta.ejb.gestionPosteAnteeSL.GestionPosteAntee";
	public static final String JNDI_GESTION_POSTE_FIDIS = "fr.gouv.education.antares.recrutement.eta.ejb.gestionPosteFidisSL.GestionPosteFidis";
	public static final String JNDI_LISTE_MC = "fr.gouv.education.antares.ejb.listeMotCleSL.ListeMotCle";
	public static final String JNDI_LISTE_MCC = "fr.gouv.education.antares.ejb.listeMotCleMccSL.ListeMotCleMcc";
	public static final String JNDI_LISTE_MC_ALYA = "fr.gouv.education.antares.ejb.listeMotCleAlyaSL.ListeMotCleAlya";
	public static final String JNDI_LISTE_LABO = "fr.gouv.education.antares.ejb.listeLaboPosteSL.ListeLaboPoste";
	public static final String JNDI_LISTE_RESEARCH_FIELDS = "fr.gouv.education.antares.ejb.listeResearchFieldsSL.ListeResearchFields";
	public static final String JNDI_GESTION_EMPLOI_FIDIS = "fr.gouv.education.antares.recrutement.eta.ejb.gestionEmploiFidisSL.GestionEmploiFidis";
	//evo 212639
	public static final String JNDI_GESTION_EMPLOI_VEGA = "fr.gouv.education.antares.vega.eta.ejb.gestionEmploiVegaSL.GestionEmploiVega";
	
	public static final String JNDI_GESTION_EMPLOI_ANTEE = "fr.gouv.education.antares.recrutement.admin.ejb.gestionEmploiSL.GestionEmploi";
	
	public static final String JNDI_GESTION_CHOIX = "fr.gouv.education.antares.recrutement.can.ejb.gestionChoixSL.GestionChoix";

	// GALAXIE 19/09/2008 FME
	public static final String JNDI_CTU_FIDIS = "fr.gouv.education.antares.ejb.candidatureFidisEN.CandidatureFidis";

	// GALAXIE 17/11/2009 Comite de selection
	public static final String JNDI_GESTION_CDS_FIDIS = "fr.gouv.education.antares.recrutement.eta.ejb.gestionComiteFidisSL.GestionComiteFidis";
	public static final String JNDI_GESTION_CDS_ANTEE = "fr.gouv.education.antares.recrutement.eta.ejb.gestionComiteAnteeSL.GestionComiteAntee";

	// ELECTRA
	public static final String JNDI_GESTION_ELECTRA = "fr.gouv.education.antares.astree.admin.ejb.gestionElectraSL.GestionElectra";
	public static final String JNDI_GESTION_ALYA = "fr.gouv.education.antares.astree.admin.ejb.gestionAlyaSL.GestionAlya";
	public static final String JNDI_DEMANDES_CRCT = "fr.gouv.education.antares.ejb.demandesCrctSL.DemandesCrct";
	
	// Gestion purge #1363 FME
	public static final String JNDI_GESTION_PURGEDIT = "fr.gouv.education.antares.ejb.gestionPurgeSL.GestionPurge";
	
	//MSU Evo 1007 
	public static final String JNDI_GESTION_NOTE="fr.gouv.education.antares.ejb.gestionNoteSL.GestionNote";
	//MSU Evo 1463
	public static final String JNDI_GESTION_ENQUETE_BOE="fr.gouv.education.antares.recrutement.eta.ejb.gestionEnqueteBoeSL.GestionEnqueteBoe";
	
	public static final String JNDI_GESTION_VIVIER_CHIRON ="fr.gouv.education.antares.ejb.gestionVivierChironSL.GestionVivierChiron";
	public static final String JNDI_GESTION_IDENT_JURY_CHIRON ="fr.gouv.education.antares.ejb.gestionIdentiteJuryChironSL.GestionIdentiteJuryChiron";
	public static final String JNDI_JURY_CHIRON = "fr.gouv.education.antares.ejb.juryChironEN.JuryChiron";
	
	public static final String JNDI_COMMISSION_CHIRON = "fr.gouv.education.antares.ejb.gestionJuryChironSL.GestionCommission";
	
	public static final String JNDI_GESTION_ASTREE_DOC_ETA = "fr.gouv.education.antares.astree.eta.ejb.gestionDocEtabAstreeSL.GestionDocEtabAstree";
	
	public static final String JNDI_NOTES_RAPPORT_INTERNE = "fr.gouv.education.antares.ejb.notesRapportSL.Notes";
	public static final String JNDI_NOTES_PRIME = "fr.gouv.education.antares.ejb.notesPrimeSL.NotesPrime";
	
	public static final String JNDI_NOTES_RAPPORT_HU = "fr.gouv.education.antares.ejb.notesRapportHUSL.NotesHU";
	
	public static final String JNDI_STATUT_PRIME = "fr.gouv.education.antares.ejb.statutPrimeEN.StatutPrime";
	
	public static final String JNDI_LISTE_DOSSIER_ELECTRAHU = "fr.gouv.education.antares.ejb.listeDossierElectraHUSL.ListeDossierElectraHU";
	
	public static final String JNDI_FLUX_ODYSSEE = "fr.gouv.education.antares.ejb.gestionFluxOdysseeSL.GestionFluxOdyssee";
	// systemes de fichier t3
	public static final String T3FS_RESULTS = "results";
	public static final String T3FS_TRANSMISSIONS = "transmissions";
	public static final String T3FS_UPLOAD = "injection";
	public static final String T3FS_FOND_PAGE_ASTREE = "fondPage";

	public static final String ODMQUALIF = "ODMQUALIF";
	public static final String ODMGROUPE = "ODMGROUPE";
	public static final String ODMPROMO = "ODMPROMO";

	// Liens emplois
	public static final String LIEN_NON_POURVU = "1";
	public static final String LIEN_DIVERGENCE_LOCALE = "2";

	public static final int TAG_CAN_CLASSEMENT_AUCUN = 0;
	public static final int TAG_CAN_CLASSEMENT_DETAC = 1;
	public static final int TAG_CAN_CLASSEMENT_NONDETAC = 2;
	public static final int TAG_CAN_CLASSEMENT_QUALIF = 3;

	// Coordonnées du bureau
	public static final String BUREAU_DPE = "DPE B3";
	public static final String BUREAU_DPE_TEL = "01 55 55 62 72";
	public static final String BUREAU_DPE_FAX = "01 55 55 61 67";

	// Longueur des champs maximum a afficher avec complement a 0
	public static final int NB_DIGIT_NUMESEC = 2;
	public static final int NB_DIGIT_NUMSSEC = 2;
	public static final int NB_DIGIT_NUMEMP = 4;
	public static final int NB_DIGIT_NUMQUALIF = 11;
	public static final int NB_DIGIT_MILLESIME = 2;

	// type des candidatures
	public static final String TYPE_CTU_NORMAL = "N";
	public static final String TYPE_CTU_GROUPE = "G";
	// Nature de résultat des candidatures
	public static final String RESULT_TJRS_PAS_COMMUNIQUE = "T";
	public static final String RESULT_QUALIFIE = "Q";
	public static final String RESULT_NON_QUALIFIE = "N";
	public static final String RESULT_DISPENSE_REFUSEE = "DR";
    public static final String RESULT_IRRECEVABLE = "I";
	
	// type de pages
	public static final String TYPE_PAGE_RECRUT = "R";
	public static final String TYPE_PAGE_QUALIF = "Q";

	// type de campagne
	public static final String TYPE_CAMP_RECRUT = "R";
	public static final String TYPE_CAMP_QUALIF = "Q";
	public static final String TYPE_CAMP_ASTREE = "S";
	public static final String TYPE_CAMP_RAP_ACTIVITE = "A";
	public static final String TYPE_CAMP_SIRIUS = "I";
	public static final String TYPE_CAMP_ALTAIR = "T";
	public static final String TYPE_CAMP_CANOPUS = "P";
	public static final String TYPE_CAMP_ALYA = "Y";
	public static final String TYPE_CAMP_ELARA = "E";
	public static final String TYPE_CAMP_NAOS = "N";
	public static final String TYPE_CAMP_SIRAH = "H";

	// code suivi ASTREE
	public static final String CDSUI_ENREGISTRE_NON_VEIRFIE = "EN";
	public static final String CDSUI_ATTENTE_QUALIF = "AQ";
	public static final String CDSUI_QUALIF_NONOBTENUE = "NQ";

	// Situation de l'emploi
	public static final String EMPLOI_ISOLE = "isole";
	public static final String EMPLOI_LIABLE = "liable";
	public static final String EMPLOI_LIE = "lie";

	// 11/06/2008 rlitteaut evo galaxie 3
	// affichage tableau de bord tbo1
	public static final String ART_LIBELLE = "LIBELLE";
	public static final String ART_NUMECOR = "NUMECOR";
	public static final String ART_PUBLIES = "N_PUBLIES";
	public static final String ART_SAISIS = "N_SAISIS";
	public static final String ART_MUTATION = "N_MUTATION";
	public static final String ART_DETACH = "N_DETACHMNT";
	public static final String ART_CONCOURS = "N_CONCOURS";
	public static final String ART_RECRUTEMENT = "N_RECRUTEMENT";
	public static final String ART_NON_VACANT = "N_NON_VACANT";
	public static final String ART_ABS_CAND = "N_ABS_CAND";
	public static final String ART_REJET_CS = "N_REJET_CS";
	public static final String ART_REJET_CA = "N_REJET_CA";
	public static final String ART_REFUS_DIR = "N_REFUS_DIR";
	public static final String ART_DIV_LOCAL = "N_DIV_LOCAL";

	/* === base pour l'URI === */
	public static final String CONTEXT_URI = "/antares";

	/* === Context pour les reports sur le serveur de reports === */
	public static final String REPORTS_URI = "/antaresReports/report";

	// Nom du fichier des emplois uplpoadé.
	public static final String NOM_UPLD_FIC_EMPLOIS = "recrut.txt";
	// Nom du fichier des nommés uploadé.
	public static final String NOM_UPLD_FIC_NOMMES = "nommes.txt";
	// Nom du fichier GESUP uploadé.
	public static final String NOM_UPLD_FIC_GESUP = "gesup.txt";
	// Nom du fichier Etab uploadé.
	public static final String NOM_UPLD_FIC_ETAB = "etab.txt";
	// Nom du fichier des emplois uplpoadé.
	public static final String NOM_UPLD_FIC_EMPLOIS_DERO = "recrutDero.txt";

	/* === Type de document uploadé === */
	public static final String DOCUMENT_CV = "CV";
	public static final String DOCUMENT_REDAC = "REDAC";
	public static final String DOCUMENT_LETTRE_MOTIV = "LETTRE_MOTIVATION";
	public static final String DOCUMENT_DIPLOME = "DIPLOME";
	public static final String DOCUMENT_PUBLI = "PUBLI";
	public static final String DOCUMENT_COMPLET = "DossierComplet";
	public static final String DEMANDE_COMPLETE = "DemandeComplete";
	public static final String AVIS_CNU = "";
	public static final String SAISIE_RAPPORTEUR_VERT = "V";
	public static final String SAISIE_RAPPORTEUR_ORANGE = "O";
	public static final String SAISIE_RAPPORTEUR_ROUGE = "R";
	

	/**
	 * Gets the String from the property file
	 * 
	 *@param key
	 *            key
	 *@return The key value
	 */
	public static String getPropsValue(String key) {
		if (key != null) {
			return properties.getProperty(key);
		}
		return null;
	}

	/**
	 * Fournit une enumeration sur les cles de proprietes.
	 * 
	 * @return Une enumeration de cles.
	 */
	public static java.util.Enumeration getProperties() {
		return properties.keys();
	}

	private static final Properties properties = new Properties();

	/*
	 * ================ Les constantes suivantes sont initialisees dans un
	 * initialisateur statique ================
	 */
	/* === URL JNDI SERVEUR APPLICATIF=== */
	public static final String JNDI_HOME;

	/* === URL Site du Ministere=== */
	public static final String URL_MIN;

	/* === URL du fichier de retour candidat === */
	public static final String URL_QUITTER_ANTARES;

	/* === URL du fichier de retour établissement === */
	public static final String URL_QUITTER_ANTARES_ETA;

	/* === URL JNDI SERVEUR REFERENTIEL FICHIERS T3 === */
	public static final String JNDI_FILE_T3;

	/* === REP DE BASE DES BATCHS (sur le serveur d'application) === */
	public static final String BATCH_HOME;

	/* === REP DE BASE DES IMAGES (sur le serveur d'application) === */
	public static final String IMAGES_HOME;
	
	/* === REP DES LIBRAIRIES (sur le serveur d'application) === */
	public static final String LIB_HOME;

	/* === URL serveur de reports StyleReport === */
	public static final String REPORTS_BASE_URL;

	/* === URL Site */
	public static final String DOMAIN_NAME;

	/* === URL de base fichier d'aide === */
	public static final String URL_AIDE;
	/* === URL de base fichier des question frequentes === */
	public static final String URL_FAQ;
	public static final String URL_CANDIDATS; // portail
	public static final String URL_LOGIN_CANDIDATS; // application
	public static final String EMAIL_DO_NOT_REPLY; // dans les messages

	/* === Mot de passe system pour l'acces aux fonctionnalites securisees === */
	public static final String SYS_PWD;

	/* === Section Upload de fichier === */
	// Répertoire temporaire où sont stockés les fichiers lors de l'upload.
	public static final String UPLOAD_TMP_DIR;

	public static final String UPLD_DIR_PATCHS;

	/*
	 * === URL de base tableau de Classement (evo mail Mme Brun 12/07/04 valide
	 * par Mr Bui Khac ) ===
	 */
	public static final String URL_TAB_CLASSEMENT;

	public static final String JDBC_DRIVER;
	public static final String USER;
	public static final String URL;
	public static final String PWD;

	public static final String FS_REP_PARENT;
	public static final String FS_FOND_PAGE_ASTREE;
	public static final String FS_FOND_PAGE_ADM;
	public static final String FS_ORDRE_DU_JOUR_ADM;
	public static final String FS_FICHE_POSTE;
	public static final String FS_RESULTS;
	public static final String FS_TRANSMISSIONS;
	public static final String FS_UPLOAD;
	public static final String FS_BATCH_EXTRACTION;
	public static final String FS_ECHANGE_PORTAIL;
	public static final String FS_ECHANGE_PORTAIL_EURAXESS;
	public static final String FS_VIVIER_CV;
	public static final String FS_RAPPORT_ENSEIGNANT;
	public static final String FS_RAPPORT_PROMO_INTERNE;
	public static final String FS_CRCT_ENSEIGNANT;
	public static final String FS_CPE_ENSEIGNANT;
	public static final String FS_PEDR_ENSEIGNANT;
	public static final String FS_PRIP_ENSEIGNANT;
	public static final String FS_SIRAH_ENSEIGNANT;
	public static final String FS_RAPPORT_EDITION;
	public static final String FS_RAPPORT_EDITION_CNU;
	public static final String FS_RAPPORT_EDITION_DESIGNES;
	public static final String FS_PEDR_EDITION_DESIGNES;
	public static final String FS_PROMO_INTERNE;
	public static final String FS_PRIME_ENSEIGNANT;
	
	public static final String FS_RAPPORT_EDITION_DESIGNES_ALYA;

	public static final String FS_AVISSEC_MASSE;
	public static final String FS_AVISCA_MASSE;
	public static final String FS_DOSSIERS_NON_PROPOSE_CNU;
	public static final String FS_EXPORT_PROMOUVABLES;
	public static final String FS_ATRIA;
	public static final String FS_ENV_MAIL;
	public static final String FS_LISTE_PROMOUVABLES;
	public static final String FS_CRCT;
	public static final String FS_RAPPORT_EDITION_NAOS;
	public static final String FS_RAPPORT_EDITION_NAOS_CPP;
	public static final String FS_RAPPORT_EDITION_NAOS_CNU;
	public static final String FS_RAPPORT_EDITION_ELARA;
	public static final String FS_RAPPORT_EDITION_ELARA_PRIP; //evo 273320
	public static final String FS_PEDR_EDITION_ELARA_INST;
	public static final String FS_PRIME_EDITION_ELARA_INST;
	public static final String FS_PEDR_EDITION_ELARA_CNU;
	public static final String FS_PEDR_EDITION_ELARA_HU;
	public static final String FS_PEDR_EDITION_ELARA_CNAP;
	public static final String FS_CDS;
	public static final String FS_CANOPUS;
	public static final String FS_RAPPORT_ALYA;
	public static final String FS_RAPPORT_EDITION_ALYA;
	public static final String FS_RAPPORT_EDITION_CNU_ALYA;
	public static final String FS_AVISSEC_MASSE_ALYA;
	public static final String FS_AVISCA_MASSE_ALYA;
	public static final String FS_MACC_MASSE_ALYA;
	public static final String FS_ELARA_AVISCNU_MASSE ;
	public static final String FS_ODM_INDIV;
	public static final String FS_ODM_TMP;

	public static final String FS_INFO_AGENTS;

	public static final String FS_MAJ_UR;// evo 438
	public static final String  FS_MAJ_UR_XML;
	public static final String FS_MAJ_URCNRS;
	public static final String LOG_MAJ_URCNRS;
	
	public static final String FS_FLUX_ODYSSEE_EC;
	
	//EVO 232079
	public static final String LOG_TRANS_MASSE_ADEL;
	
	public static final String FS_QUALIF_CTU_EDITION_CNU;
	public static final String FS_ELECTRA_EDITION_CNU;
	

	public static final String NBR_MAX_PUBLI;

	public static final int LIMITED_RTF_SIZE;
	public static final int FUSION_OUT_CHUNK_SIZE;
	public static final int FUSION_OUT_SLEEP_TIME;

	public static final String ARTICLE_REF_FILE;
	public static final String CORPS_REF_FILE;
	public static final String FONCTION_ARTICLE_REF_FILE;
	public static final String FONCTION_REF_FILE;
	public static final String GRADE_REF_FILE;
	public static final String GRILLE_INDICIAIRE_REF_FILE;
	public static final String CALCUL_CLASSEMENT_XSD_FILE;
	public static final String DATA_CLASSEMENT_XSD_FILE;
	
	public static final String ALTAIR_ETAB_URL ;
	public static final String ALTAIR_CAND_URL ;
	
	public static final String FS_QUALIF_RST_SAISIE_CNU;
	public static final String FS_VEGA_ETAB_MODELESDOC;
	
	public static final String FS_VEGA_DEPOT_DOC;
	
	public static final String FS_VEGA_CANDIDAT;
	public static final String URL_SIRA;
	public static final List<String> POINT_MONTAGES;
	public static final String SEUIL_POINT_MONTAGES;
	public static final String FS_ASTREE_ETAB_MODELESDOC;
	public static final String  URL_RESEDA;
	public static final String  LOGIN_WS_RESEDA;
	public static final String  PASSWORD_WS_RESEDA;

	public static final String  URL_ODYSSEE_CREATION_EC;
	public static final String  URL_ODYSSEE_MISE_A_JOUR_EC;
	public static final String  URL_ODYSSEE_SUPPRESSION_EC;
	public static final String	URL_ODYSSEE_TOKEN;
	public static final String	CLIENT_ID_ODYSSEE;
	public static final String	CLIENT_SECRET_ODYSSEE;
	public static final String	GRANT_TYPE_ODYSSEE;
	
	public static final String PATH_POINT_MONTAGE_1;
	public static final String PATH_POINT_MONTAGE_2;
	public static final String PATH_POINT_MONTAGE_3;
	public static final String PATH_POINT_MONTAGE_4;
	public static final String FS_LOGS_FICHIERS_PROTEGES_PLAN_CLASSEMENT;
	public static final String FS_LOGS_FICHIERS_PROTEGES;
	public static final String FS_ANTEE_DEPOT_DOC;
	public static final String FS_FIDIS_DEPOT_DOC;
	public static final String FS_LOGS_PURGE_COMMISSIONS_VEGA;
	public static final String FS_LOGS_BATCH_ROLLBACK_FICHIERS_BASCULE;
	public static final String FS_LOGS_MAJ_ABONNEMENTS_OFFRES_POSTES;
	public static final String FS_QUALIF_EDITION_MASSE_ADM;
	public static final String FS_LOGS_BATCH_QUALIF_EDITION_MASSE_ADM;	
	public static final String FS_LOGS_BASCULE_ANTEE_FIDIS;
	public static final String FS_LOGS_PURGE_SIRAH;
	public static final String FS_LOGS_PURGE_ANTEE;
	public static final String FS_LOGS_PURGE_FIDIS;
	public static final String FS_LOGS_PURGE_CHIRON;
	public static final String FS_LOGS_PURGE_CANOPUS;
	public static final String FS_LOGS_SUPP_DOCS_REPRISE;
	public static final String FS_CHIRON_ETAB_MODELESDOC;
	public static final String FS_RAPPORT_HU_ENSEIGNANT;
	
	static {
		// legacy code for BEA WAR location path deployment
		try {

			// System.out.println( "########### ENV : " + System.getenv() );
			// System.out.println( "########### Properties : " +
			// System.getProperties() );

			String ap = System.getProperty("antares.properties", "");
			StringBuffer buffIn = new StringBuffer();
			if ("".equals(ap)) {
				// find this in the root domain directory of the server,
				buffIn.append("config");
				buffIn.append(java.io.File.separator);
				// GHO 04/06/02: pas en dur, vous serez gentil
				// buffIn.append( "mydomain" );
				buffIn
						.append(System.getProperty("weblogic.Domain",
								"mydomain"));
				buffIn.append(java.io.File.separator);
				buffIn.append("antares.properties");
			} else {
				buffIn.append(ap);
			}
			// init IO Stream
			File f = new File(buffIn.toString());
			FileInputStream in = new FileInputStream(f);

			// load it
			properties.load(in);
			// GHO 18/03/02: si les variables de classe sont declarees finales,
			// le compilateur doit etre certain qu'elles seront initialisees
			// dans l'initialisateur statique (sinon, on a une erreur de
			// compilation).
			// On doit donc les placer hors
			// d'un contexte ou une exception peut etre jetee
			// CONTEXT_URI = properties.getProperty( "contextUri" );
			// JNDI_HOME = properties.getProperty( "T3AppServer" );
			in.close();
		} catch (java.io.FileNotFoundException ex) {

			AntaresLogger.error(
					"Fichier de propriete antares.properties introuvable ", ex);

		} catch (java.io.IOException ex) {
			AntaresLogger.error("Problème de connexion à la base des données", ex);
		}

		DOMAIN_NAME = properties.getProperty("domainName",
				"http://localhost:7001");
		ALTAIR_ETAB_URL = properties.getProperty("altairEtabUrl", "http://localhost/altairetab");
		ALTAIR_CAND_URL = properties.getProperty("altairCandUrl", "http://localhost/altaircand");
		
		boolean hasPointMontage = Boolean.TRUE;
		int numPointMontage = 1;
		POINT_MONTAGES = new ArrayList<String>();
		while (hasPointMontage) {
			String pointMontage = properties.getProperty("point.montage.plan.classement.path."+numPointMontage,
					"");
			//Les batchs ne trouvent pas le jar
			if(pointMontage != null && (!"".equals(pointMontage))){				
//			if(StringUtils.isNotBlank(pointMontage)) {
				POINT_MONTAGES.add(pointMontage);
				numPointMontage++;
			} else {
				hasPointMontage = false;
			}
		}
		SEUIL_POINT_MONTAGES = properties.getProperty("point.montage.seuil",
				"3");
		
		/**
		 * todo supprimer cette propriete et remplacer son utilisation par une
		 * reference a DOMAIN_NAME
		 */
		properties.setProperty("contextPager", DOMAIN_NAME);

		URL_MIN = properties.getProperty("urlMinistere",
				"http://www.enseignementsup-recherche.gouv.fr");
		JNDI_HOME = properties
				.getProperty("T3AppServer", "t3://localhost:7001");
		JNDI_FILE_T3 = properties.getProperty("T3FileServer", JNDI_HOME);
		BATCH_HOME = properties.getProperty("BATCHS_HOME",
				"/home/antares/batchs/");
		LIB_HOME = properties.getProperty("LIB_HOME", "/home/antares/batchs/");
		REPORTS_BASE_URL = properties.getProperty("reportsUrl",
				"http://localhost:7001");
		URL_AIDE = properties.getProperty("urlAide", "http://localhost:7001"
				+ CONTEXT_URI + "/aide/");
		URL_FAQ = properties
				.getProperty(
						"urlFaq",
						"https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_questions_reponses.htm");
		IMAGES_HOME = properties.getProperty("IMAGES_HOME",
				"/home/antares/images/");
		URL_CANDIDATS = properties
				.getProperty("urlCandidats",
						"https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/candidats.html");
		URL_LOGIN_CANDIDATS = properties
				.getProperty("urlLoginCandidats",
						"https://galaxie.enseignementsup-recherche.gouv.fr/antares/can/index.jsp");
		EMAIL_DO_NOT_REPLY = properties.getProperty("emailNoReply",
				"galaxie@education.gouv.fr");

		SYS_PWD = properties.getProperty("sysPwd", "weblogic");
		// URL QUITTER ANTARES pour CAN
		URL_QUITTER_ANTARES = properties
				.getProperty(
						"urlQuitterAntares",
						"http://www.enseignementsup-recherche.gouv.fr/pid20610/les-enseignants-chercheurs.html");
		// URL QUITTER ANTARES pour ETA
		URL_QUITTER_ANTARES_ETA = properties
				.getProperty(
						"urlQuitterAntaresEta",
						"http://www.enseignementsup-recherche.gouv.fr/pid20610/les-enseignants-chercheurs.html");

		UPLOAD_TMP_DIR = properties.getProperty("UPLOAD_TMP_DIR", "/tmp");
		UPLD_DIR_PATCHS = properties.getProperty("UPLOAD_TMP_DIR",
				"livraisons/");
		URL_TAB_CLASSEMENT = properties
				.getProperty("URL_TAB_CLASSEMENT",
						"http://i-dgrh.adc.education.fr/direction/avancement/index.htm");

		JDBC_DRIVER = properties.getProperty("JDBC_DRIVER");
		USER = properties.getProperty("USER");
		URL = properties.getProperty("URL");
		PWD = properties.getProperty("PWD");
		FS_FOND_PAGE_ASTREE = properties.getProperty("FS_FOND_PAGE_ASTREE");
		FS_VEGA_ETAB_MODELESDOC = properties.getProperty("FS_VEGA_ETAB_MODELESDOC");
		FS_VEGA_CANDIDAT = properties.getProperty("FS_VEGA_CANDIDAT");
	
		FS_FOND_PAGE_ADM = properties.getProperty("FS_FOND_PAGE_ADM");
		FS_ORDRE_DU_JOUR_ADM = properties.getProperty("FS_ORDRE_DU_JOUR_ADM");		
		FS_FICHE_POSTE = properties.getProperty("FS_FICHE_POSTE");
		FS_CANOPUS = properties.getProperty("FS_CANOPUS");
		FS_RESULTS = properties.getProperty("FS_RESULTS");
		FS_TRANSMISSIONS = properties.getProperty("FS_TRANSMISSIONS");
		FS_UPLOAD = properties.getProperty("FS_UPLOAD");
		FS_BATCH_EXTRACTION = properties.getProperty("FS_BATCH_EXTRACTION");
		FS_ECHANGE_PORTAIL = properties.getProperty("FS_ECHANGE_PORTAIL");
		FS_ECHANGE_PORTAIL_EURAXESS = properties
				.getProperty("FS_ECHANGE_PORTAIL_EURAXESS");
		FS_VIVIER_CV = properties.getProperty("FS_VIVIER_CV");
		FS_RAPPORT_ENSEIGNANT = properties.getProperty("FS_RAPPORT_ENSEIGNANT");
		FS_RAPPORT_PROMO_INTERNE = properties.getProperty("FS_RAPPORT_PROMO_INTERNE");
		FS_CRCT_ENSEIGNANT = properties.getProperty("FS_CRCT_ENSEIGNANT");
		FS_CPE_ENSEIGNANT = properties.getProperty("FS_CPE_ENSEIGNANT");
		FS_PEDR_ENSEIGNANT = properties.getProperty("FS_PEDR_ENSEIGNANT");
		FS_PRIP_ENSEIGNANT = properties.getProperty("FS_PRIP_ENSEIGNANT");
		FS_SIRAH_ENSEIGNANT = properties.getProperty("FS_SIRAH_ENSEIGNANT");
		URL_SIRA = properties.getProperty("URL_SIRA");;
		FS_AVISSEC_MASSE = properties.getProperty("FS_AVISSEC_MASSE");
		FS_AVISCA_MASSE = properties.getProperty("FS_AVISCA_MASSE");
		FS_RAPPORT_EDITION = properties.getProperty("FS_RAPPORT_EDITION");
		FS_RAPPORT_EDITION_CNU = properties
				.getProperty("FS_RAPPORT_EDITION_CNU");
		FS_RAPPORT_EDITION_DESIGNES = properties
				.getProperty("FS_RAPPORT_EDITION_DESIGNES");
		FS_PEDR_EDITION_DESIGNES = properties
		.getProperty("FS_PEDR_EDITION_DESIGNES");
		
		FS_PROMO_INTERNE = properties
				.getProperty("FS_PROMO_INTERNE");
		
		FS_PRIME_ENSEIGNANT = properties.getProperty("FS_PRIME_ENSEIGNANT");
		
		FS_RAPPORT_EDITION_DESIGNES_ALYA = properties
				.getProperty("FS_RAPPORT_EDITION_DESIGNES_ALYA");

		FS_DOSSIERS_NON_PROPOSE_CNU = properties
				.getProperty("FS_DOSSIERS_NON_PROPOSE_CNU");
		FS_EXPORT_PROMOUVABLES = properties
				.getProperty("FS_EXPORT_PROMOUVABLES");
		FS_ATRIA = properties.getProperty("FS_ATRIA");
		FS_ENV_MAIL = properties.getProperty("FS_ENV_MAIL");
		FS_LISTE_PROMOUVABLES = properties.getProperty("FS_PROMOUVABLES");
		FS_CRCT = properties.getProperty("FS_CRCT");
		FS_RAPPORT_EDITION_NAOS = properties.getProperty("FS_RAPPORT_EDITION_NAOS");
		FS_RAPPORT_EDITION_NAOS_CPP = properties.getProperty("FS_RAPPORT_EDITION_NAOS_CPP");
		FS_RAPPORT_EDITION_NAOS_CNU = properties.getProperty("FS_RAPPORT_EDITION_NAOS_CNU");
		FS_RAPPORT_EDITION_ELARA = properties.getProperty("FS_RAPPORT_EDITION_ELARA");
		FS_RAPPORT_EDITION_ELARA_PRIP = properties.getProperty("FS_RAPPORT_EDITION_ELARA_PRIP");
		FS_PEDR_EDITION_ELARA_INST = properties.getProperty("FS_PEDR_EDITION_ELARA_INST");
		FS_PRIME_EDITION_ELARA_INST = properties.getProperty("FS_PRIME_EDITION_ELARA_INST");
		FS_PEDR_EDITION_ELARA_CNU = properties.getProperty("FS_PEDR_EDITION_ELARA_CNU");
		FS_PEDR_EDITION_ELARA_HU = properties.getProperty("FS_PEDR_EDITION_ELARA_HU");
		FS_PEDR_EDITION_ELARA_CNAP = properties.getProperty("FS_PEDR_EDITION_ELARA_CNAP");
		FS_CDS = properties.getProperty("FS_CDS");
		FS_RAPPORT_ALYA = properties.getProperty("FS_RAPPORT_ALYA");
		FS_RAPPORT_EDITION_ALYA = properties
				.getProperty("FS_RAPPORT_EDITION_ALYA");
		FS_RAPPORT_EDITION_CNU_ALYA = properties
				.getProperty("FS_RAPPORT_EDITION_CNU_ALYA");
		FS_AVISSEC_MASSE_ALYA = properties.getProperty("FS_AVISSEC_MASSE_ALYA");
		FS_AVISCA_MASSE_ALYA = properties.getProperty("FS_AVISCA_MASSE_ALYA");
		FS_MACC_MASSE_ALYA = properties.getProperty("FS_MACC_MASSE_ALYA");
		FS_ELARA_AVISCNU_MASSE = properties.getProperty("FS_ELARA_AVISCNU_MASSE");
		FS_ODM_INDIV = properties.getProperty("FS_ODM_INDIV");
		FS_ODM_TMP = properties.getProperty("FS_ODM_TMP");

		FS_INFO_AGENTS = properties.getProperty("FS_INFO_AGENTS");

		FS_MAJ_UR = properties.getProperty("FS_MAJ_UR");// evo 438
		FS_MAJ_UR_XML = properties.getProperty("FS_MAJ_UR_XML");
		FS_MAJ_URCNRS = properties.getProperty("FS_MAJ_URCNRS");
		LOG_MAJ_URCNRS = properties.getProperty("LOG_MAJ_URCNRS");
		LOG_TRANS_MASSE_ADEL = properties.getProperty("LOG_TRANS_MASSE_ADEL");
		
		FS_FLUX_ODYSSEE_EC = properties.getProperty("FS_FLUX_ODYSSEE_EC");

		FS_QUALIF_CTU_EDITION_CNU = properties.getProperty("FS_QUALIF_CTU_EDITION_CNU");
		
		FS_ELECTRA_EDITION_CNU = properties.getProperty("FS_ELECTRA_EDITION_CNU");
		
		NBR_MAX_PUBLI = properties.getProperty("NBR_MAX_PUBLI");

		LIMITED_RTF_SIZE = Math.max(Integer.parseInt(properties.getProperty(
				"LIMITED_RTF_SIZE", "0")), Constantes.MAX_SIZE_FILE);
		FUSION_OUT_CHUNK_SIZE = Integer.parseInt(properties.getProperty(
				"FUSION_OUT_CHUNK_SIZE", "0"));
		FUSION_OUT_SLEEP_TIME = Integer.parseInt(properties.getProperty(
				"FUSION_OUT_SLEEP_TIME", "0"));

		ARTICLE_REF_FILE = properties.getProperty("ARTICLE_REF_FILE");
		CORPS_REF_FILE = properties.getProperty("CORPS_REF_FILE");
		FONCTION_ARTICLE_REF_FILE = properties
				.getProperty("FONCTION_ARTICLE_REF_FILE");
		FONCTION_REF_FILE = properties.getProperty("FONCTION_REF_FILE");
		GRADE_REF_FILE = properties.getProperty("GRADE_REF_FILE");
		GRILLE_INDICIAIRE_REF_FILE = properties
				.getProperty("GRILLE_INDICIAIRE_REF_FILE");
		CALCUL_CLASSEMENT_XSD_FILE = properties
				.getProperty("CALCUL_CLASSEMENT_XSD_FILE");
		DATA_CLASSEMENT_XSD_FILE = properties
				.getProperty("DATA_CLASSEMENT_XSD_FILE");
		FS_QUALIF_RST_SAISIE_CNU = properties.getProperty("FS_QUALIF_RST_SAISIE_CNU","/appli/galaxie/GALAXIE/QUALIF-resultatSaisieCSV");
		FS_VEGA_DEPOT_DOC = properties.getProperty("FS_VEGA_DEPOT_DOC");
		FS_ASTREE_ETAB_MODELESDOC = properties.getProperty("FS_ASTREE_ETAB_MODELESDOC","/appli/galaxie/GALAXIE/ASTREE/ETABLISSEMENT");
		URL_RESEDA = properties.getProperty("URL_RESEDA");
		LOGIN_WS_RESEDA = properties.getProperty("LOGIN_WS_RESEDA");
		PASSWORD_WS_RESEDA = properties.getProperty("PASSWORD_WS_RESEDA");		
		URL_ODYSSEE_CREATION_EC = properties.getProperty("URL_ODYSSEE_CREATION_EC");
		URL_ODYSSEE_MISE_A_JOUR_EC = properties.getProperty("URL_ODYSSEE_MISE_A_JOUR_EC");
		URL_ODYSSEE_SUPPRESSION_EC = properties.getProperty("URL_ODYSSEE_SUPPRESSION_EC");
		URL_ODYSSEE_TOKEN = properties.getProperty("URL_ODYSSEE_TOKEN");
		CLIENT_ID_ODYSSEE = properties.getProperty("CLIENT_ID_ODYSSEE");
		CLIENT_SECRET_ODYSSEE = properties.getProperty("CLIENT_SECRET_ODYSSEE");
		GRANT_TYPE_ODYSSEE =  properties.getProperty("GRANT_TYPE_ODYSSEE");
		
		FS_CHIRON_ETAB_MODELESDOC = properties.getProperty("FS_CHIRON_ETAB_MODELESDOC");
		// try {
		// MAX_RESULT_DISPLAYED =
		// Integer.parseInt(properties.getProperty("MAX_RESULT_DISPLAYED"));
		// } catch (NumberFormatException e) {
		// e.printStackTrace(); //To change body of catch statement use Options
		// | File Templates.
		// }
		
		FS_REP_PARENT = properties.getProperty("FS_REP_PARENT");		
		
		PATH_POINT_MONTAGE_1 = properties.getProperty("point.montage.plan.classement.path.1");
		PATH_POINT_MONTAGE_2 = properties.getProperty("point.montage.plan.classement.path.2");	
		PATH_POINT_MONTAGE_3 = properties.getProperty("point.montage.plan.classement.path.3");	
		PATH_POINT_MONTAGE_4 = properties.getProperty("point.montage.plan.classement.path.4");
		FS_LOGS_FICHIERS_PROTEGES_PLAN_CLASSEMENT = properties.getProperty("FS_LOGS_FICHIERS_PROTEGES_PLAN_CLASSEMENT");
		FS_LOGS_FICHIERS_PROTEGES = properties.getProperty("FS_LOGS_FICHIERS_PROTEGES");
		FS_ANTEE_DEPOT_DOC = properties.getProperty("FS_ANTEE_DEPOT_DOC");
		FS_FIDIS_DEPOT_DOC = properties.getProperty("FS_FIDIS_DEPOT_DOC");
		FS_LOGS_PURGE_COMMISSIONS_VEGA = properties.getProperty("FS_LOGS_PURGE_COMMISSIONS_VEGA");
		FS_LOGS_BATCH_ROLLBACK_FICHIERS_BASCULE = properties.getProperty("FS_LOGS_BATCH_ROLLBACK_FICHIERS_BASCULE");
		FS_ANTARES_ARCH_PURG = properties.getProperty("FS_ANTARES_ARCH_PURG");
		FS_LOGS_MAJ_ABONNEMENTS_OFFRES_POSTES = properties.getProperty("FS_LOGS_MAJ_ABONNEMENTS_OFFRES_POSTES");
		FS_QUALIF_EDITION_MASSE_ADM = properties.getProperty("FS_QUALIF_EDITION_MASSE_ADM");
		FS_LOGS_BATCH_QUALIF_EDITION_MASSE_ADM = properties.getProperty("FS_LOGS_BATCH_QUALIF_EDITION_MASSE_ADM");
		FS_LOGS_BASCULE_ANTEE_FIDIS = properties.getProperty("FS_LOGS_BASCULE_ANTEE_FIDIS");
		FS_LOGS_PURGE_SIRAH = properties.getProperty("FS_LOGS_PURGE_SIRAH");
		FS_LOGS_PURGE_ANTEE = properties.getProperty("FS_LOGS_PURGE_ANTEE");
		FS_LOGS_PURGE_FIDIS = properties.getProperty("FS_LOGS_PURGE_FIDIS");
		FS_LOGS_PURGE_CHIRON = properties.getProperty("FS_LOGS_PURGE_CHIRON");
		FS_LOGS_PURGE_CANOPUS = properties.getProperty("FS_LOGS_PURGE_CANOPUS");
		FS_LOGS_SUPP_DOCS_REPRISE = properties.getProperty("FS_LOGS_SUPP_DOCS_REPRISE");
		FS_RAPPORT_HU_ENSEIGNANT = properties.getProperty("FS_RAPPORT_HU_ENSEIGNANT");
		
	}

	public static final String REPORTS_URL = REPORTS_BASE_URL + REPORTS_URI;

	/* === pour les batchs === */
	public static final String JMS_BATCH_QUEUE_FACTORY = "fr.gouv.education.antares.jms.batchsQueueFactory";
	public static final String JMS_BATCH_QUEUE = "fr.gouv.education.antares.jms.batchsQueue";

	//
	private static ResourceBundle bundle = getBundle();
	
	// ATRIA SYNTHESE
	public static final String ATRIA_SYN_R = "R";
	public static final String ATRIA_SYN_NR = "NR";
	public static final String ATRIA_SYN_RE = "RE";
	public static final String ATRIA_SYN_CG = "CG";
	public static final String ATRIA_SYN_NR_REC = "NR-REC";
	public static final String ATRIA_SYN_TE = "TE";

	private static ResourceBundle getBundle() {
		ResourceBundle bundle = null;
		try {
			bundle = ResourceBundle
					.getBundle("fr.gouv.education.antares.CompilationInfo");
		} catch (Exception ex) {
		}
		return bundle;
	}

	private static String getDateVersion() {
		String rtn;
		if (bundle == null) {
			rtn = "";
		} else {
			rtn = bundle.getString("VERSION_D");
		}
		return rtn;
	}

	private static String getHeureVersion() {
		String rtn;
		if (bundle == null) {
			rtn = "";
		} else {
			rtn = bundle.getString("VERSION_H");
		}
		return rtn;
	}

	// ATRIA CODES CHAMPS

	// Enseignants-chercheurs et assimilés
	public static final String MCF = "MCF"; // Maîtres de conférences
	public static final String PR = "PR"; // Professeurs des universités
	public static final String MCM = "MCM"; // Maitres de conferences du Museum
											// national d'histoire naturelle
	public static final String PRM = "PRM"; // Professeurs du Muséum national
											// d'histoire naturelle
	public static final String EC_MCF = "EC_MCF"; // Maîtres de conférences
	public static final String EC_MCM = "EC_MCM"; // Maitres de conferences du
													// Museum national
													// d'histoire naturelle
	public static final String EC_MCF_MNHN = "EC_MCF_MNHN";// Maîtres de
															// conférences du
															// Muséum national
															// d'histoire
															// naturelle
	public static final String EC_MCF_EHESS = "EC_MCF_EHESS"; // Maîtres de
																// conférences
																// de l'École
																// des hautes
																// études en
																// sciences
																// sociales
	public static final String EC_PR_EEE = "EC_PR_EEE"; // Maîtres de
														// conférences de
														// l'EPHE, de l'ENC et
														// de l'EFEO
	public static final String EC_PR = "EC_PR"; // Professeurs des universités
	public static final String EC_PRM = "EC_PRM"; // Professeurs du Muséum
													// national d'histoire
													// naturelle
	public static final String EC_PR_MNHN = "EC_PR_MNHN"; // Professeurs du
															// Muséum national
															// d'histoire
															// naturelle
	public static final String EC_DIR_EHESS = "EC_DIR_EHESS"; // Directeurs
																// d'études de
																// l'École des
																// hautes études
																// en sciences
																// sociales
	public static final String EC_DIR_EEE = "EC_DIR_EEE"; // Directeurs d'études
															// de l'EPHE, de
															// l'ENC et de
															// l'EFEO
	public static final String EC_PR_CNAM = "EC_PR_CNAM"; // Professeurs du
															// Conservatoire
															// national des arts
															// et métiers
	public static final String EC_AST_APA = "EC_AST_APA"; // Astronomes adjoints
															// et Physiciens
															// adjoints
	public static final String EC_AST_PHY = "EC_AST_PHY"; // Astronomes et
															// Physiciens
	public static final String EC_MCF_PHM = "EC_MCF_PHM"; // Maîtres de
															// conférences
															// praticiens
															// hospitaliers de
															// médecine
	public static final String EC_PR_UPH = "EC_PR_UPH"; // Professeurs des
														// universités
														// praticiens
														// hospitaliers
	public static final String EC_MCF_PHO = "EC_MCF_PHO"; // Maîtres de
															// conférences
															// praticiens
															// hospitaliers
															// d'odontologie
	public static final String EC_PR_CDO1 = "EC_PR_CDO1"; // Professeurs de
															// chirurgie
															// dentaire
															// odontologistes de
															// 1er grade
	public static final String EC_PR_UO = "EC_PR_UO"; // Professeurs des
														// universités
														// odontologistes

	// Personnels d'encadrement
	public static final String PE_EAEN_ESR_I = "PE_EAEN_ESR_I"; // (Interne)
																// Besoins
																// d'implantations
																// d'emplois
																// d'administrateurs
																// de
																// l'éducation
																// nationale, de
																// l'enseignement
																// supérieur et
																// de la
																// recherche
	public static final String PE_EAEN_ESR_E = "PE_EAEN_ESR_E"; // (Externe)
																// Besoins
																// d'implantations
																// d'emplois
																// d'administrateurs
																// de
																// l'éducation
																// nationale, de
																// l'enseignement
																// supérieur et
																// de la
																// recherche

	// Filière ATOSS
	public static final String AT_AAENES_Y1 = "AT_AAENES_Y1"; // Attachés
																// d'administration
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur-
																// AAAA+1
	public static final String AT_AAENES_Y1_I = "AT_AAENES_Y1_I"; // (Interne)
																	// Attachés
																	// d'administration
																	// de
																	// l'éducation
																	// nationale
																	// et de
																	// l'enseignement
																	// supérieur-
																	// AAAA+1
	public static final String AT_AAENES_Y1_E = "AT_AAENES_Y1_E"; // (externe)
																	// Attachés
																	// d'administration
																	// de
																	// l'éducation
																	// nationale
																	// et de
																	// l'enseignement
																	// supérieur-
																	// AAAA+1
	public static final String AT_AAENES_Y2 = "AT_AAENES_Y2"; // Attachés
																// d'administration
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur –
																// AAAA+2
	public static final String AT_AAENES_Y2_E = "AT_AAENES_Y2_E"; // (externe)
																	// Attachés
																	// d'administration
																	// de
																	// l'éducation
																	// nationale
																	// et de
																	// l'enseignement
																	// supérieur
																	// – AAAA+2
	public static final String AT_SAENES = "AT_SAENES"; // Secrétaires
														// administratifs de
														// l'éducation nationale
														// et de l'enseignement
														// supérieur
	public static final String AT_SAENES_I = "AT_SAENES_I"; // (Interne)
															// Secrétaires
															// administratifs de
															// l'éducation
															// nationale et de
															// l'enseignement
															// supérieur
	public static final String AT_SAENES_E = "AT_SAENES_E"; // (externe)
															// Secrétaires
															// administratifs de
															// l'éducation
															// nationale et de
															// l'enseignement
															// supérieur
	public static final String AT_AAENES4 = "AT_AAENES4"; // Adjoints
															// administratifs de
															// l'éducation
															// nationale et de
															// l'enseignement
															// supérieur -
															// Échelle 4
	public static final String AT_AAENES4_I = "AT_AAENES4_I"; // (Interne)
																// Adjoints
																// administratifs
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur -
																// Échelle 4
	public static final String AT_AAENES4_E = "AT_AAENES4_E"; // (externe)
																// Adjoints
																// administratifs
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur -
																// Échelle 4
	public static final String AT_AAENES3_HP = "AT_AAENES3_HP"; // Adjoints
																// administratifs
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur -
																// Échelle 3
																// (recrutements
																// sans concours
																// - hors PACTE)
	public static final String AT_AAENES3_P = "AT_AAENES3_P"; // Adjoints
																// administratifs
																// de
																// l'éducation
																// nationale et
																// de
																// l'enseignement
																// supérieur -
																// Échelle 3
																// (recrutements
																// PACTE)
	public static final String AT_CTSS = "AT_CTSS"; // Conseillers techniques de
													// service social
	public static final String AT_CTSS_I = "AT_CTSS_I"; // (Interne) Conseillers
														// techniques de service
														// social
	public static final String AT_CTSS_E = "AT_CTSS_E"; // (externe) Conseillers
														// techniques de service
														// social
	public static final String AT_INF = "AT_INF"; // Infirmières
	public static final String AT_INF_I = "AT_INF_I"; // (Interne) Infirmières
	public static final String AT_INF_E = "AT_INF_I"; // (externe) Infirmières
	public static final String AT_ASS = "AT_ASS"; // (Interne) Assistants de
													// service social
	public static final String AT_ASS_E = "AT_ASS_E"; // (externe) Assistants de
														// service social
	public static final String AT_ASS_I = "AT_ASS_I"; // (Interne) Assistants de
														// service social

	// Filière ITRF
	public static final String IT_IRHC_E = "IT_IRHC_E"; // //(externe)
														// Ingénieurs de
														// recherche hors classe
	public static final String IT_IRC1 = "IT_IRC1"; // // (externe) Ingénieurs
													// de recherche 1ère classe
	public static final String IT_IRC1_E = "IT_IRC1_E"; // // (externe)
														// Ingénieurs de
														// recherche 1ère classe
	public static final String IT_IRC2_I = "IT_IRC2_I"; // // (Interne)
														// Ingénieurs de
														// recherche 2ème classe
	public static final String IT_IRC2_E = "IT_IRC2_E"; // //(externe)
														// Ingénieurs de
														// recherche 2ème classe
	public static final String IT_ING_ETUDE_I = "IT_ING_ETUDE_I"; // //(Interne)
																	// Ingénieurs
																	// d'études
	public static final String IT_ING_ETUDE_E = "IT_ING_ETUDE_E"; // //(externe)
																	// Ingénieurs
																	// d'études
	public static final String IT_ASS_ING_I = "IT_ASS_ING_I"; // //(Interne)
																// Assistants
																// ingénieurs
	public static final String IT_ASS_ING_E = "IT_ASS_ING_E"; // //(externe)
																// Assistants
																// ingénieurs
	public static final String IT_TRF_I = "IT_TRF_I"; // //(Interne) Techniciens
														// de recherche et de
														// formation
	public static final String IT_TRF_E = "IT_TRF_E"; // //(externe) Techniciens
														// de recherche et de
														// formation
	public static final String IT_ATRF5_I = "IT_ATRF5_I"; // //(Interne)
															// Adjoints
															// Techniques de
															// recherche et de
															// formation -
															// Échelle 5
	public static final String IT_ATRF5_E = "IT_ATRF5_E"; // //(externe)
															// Adjoints
															// Techniques de
															// recherche et de
															// formation -
															// Échelle 5
	public static final String IT_ATRF3_HP = "IT_ATRF3_HP"; // //Adjoints
															// Techniques de
															// recherche et de
															// formation -
															// Échelle 3
															// (recrutements
															// sans concours –
															// hors PACTE)
	public static final String IT_ATRF3_P = "IT_ATRF3_P"; // //Adjoints
															// Techniques de
															// recherche et de
															// formation -
															// Échelle 3
															// (recrutements
															// PACTE)

	// Filière Bibliothèque
	public static final String BI_CGCB = "BI_CGCB"; // //Conservateurs généraux
													// et conservateurs des
													// bibliothèques
	public static final String BI_CGCB_I = "BI_CGCB_I"; // //(Interne)
														// Conservateurs
														// généraux et
														// conservateurs des
														// bibliothèques
	public static final String BI_CGCB_E = "BI_CGCB_E"; // //(externe)
														// Conservateurs
														// généraux et
														// conservateurs des
														// bibliothèques
	public static final String BI_BIB = "BI_BIB"; // //Bibliothécaires
	public static final String BI_BIB_I = "BI_BIB_I"; // //(Interne)
														// Bibliothécaires
	public static final String BI_BIB_E = "BI_BIB_E"; // //(externe)
														// Bibliothécaires
	public static final String BI_ASS_BIB = "BI_ASS_BIB"; // //Assistants des
															// bibliothèques
	public static final String BI_ASS_BIB_I = "BI_ASS_BIB_I"; // //(Interne)
																// Assistants
																// des
																// bibliothèques
	public static final String BI_ASS_BIB_E = "BI_ASS_BIB_E"; // //(externe)
																// Assistants
																// des
																// bibliothèques
	public static final String BI_BAS = "BI_BAS"; // //Bibliothécaires adjoints
													// spécialisés
	public static final String BI_BAS_I = "BI_BAS_I"; // //(Interne)
														// Bibliothécaires
														// adjoints spécialisés
	public static final String BI_BAS_E = "BI_BAS_E"; // //(externe)
														// Bibliothécaires
														// adjoints spécialisés
	public static final String BI_MBIB5 = "BI_MBIB5"; // //Magasiniers des
														// bibliothèques -
														// Echelle 5
	public static final String BI_MBIB5_I = "BI_MBIB5_I"; // //(Interne)
															// Magasiniers des
															// bibliothèques -
															// Echelle 5
	public static final String BI_MBIB5_E = "BI_MBIB5_E"; // //(externe)
															// Magasiniers des
															// bibliothèques -
															// Echelle 5
	public static final String BI_MBIB3 = "BI_MBIB3"; // //Magasiniers des
														// bibliothèques -
														// Echelle 3
														// (recrutements sans
														// concours)

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Codes champs ATRIA v2 * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * *
	 */

	// Types de valeurs
	public static final String ATRIA_TYPE_CON = "CON"; // Concours
	public static final String ATRIA_TYPE_INT = "INT"; // Concours interne
	public static final String ATRIA_TYPE_EXT = "EXT"; // Concours externe
	public static final String ATRIA_TYPE_TH = "TH"; // Travailleurs handicapes
	public static final String ATRIA_TYPE_TOT = "TOT"; // Total ligne

	// Enseignants-chercheurs et assimiles
	public static final String ATRIA_EC_MCF = "EC_MCF"; // Maitres de
														// conferences
	public static final String ATRIA_EC_MCF_MNHN = "EC_MCF_MNHN"; // Maitres de
																	// conferences
																	// du Museum
																	// national
																	// d'histoire
																	// naturelle
	public static final String ATRIA_EC_MCF_EHESS = "EC_MCF_EHESS"; // Maitres
																	// de
																	// conferences
																	// de
																	// l'Ecole
																	// des
																	// hautes
																	// etudes en
																	// sciences
																	// sociales
	public static final String ATRIA_EC_MCF_EEE = "EC_MCF_EEE"; // Maitres de
																// conferences
																// de l'EPHE, de
																// l'ENC et de
																// l'EFEO
	public static final String ATRIA_EC_PRCE = "EC_PRCE"; // Professeurs des
															// universités de
															// classe
															// exceptionnelle
	public static final String ATRIA_EC_PR1C = "EC_PR1C"; // Professeurs des
															// universités de
															// 1ère classe
	public static final String ATRIA_EC_PR2C = "EC_PR2C"; // Professeurs des
															// universités de
															// 2ème classe
	public static final String ATRIA_EC_PR_MNHN = "EC_PR_MNHN"; // Professeurs
																// du Museum
																// national
																// d'histoire
																// naturelle
	public static final String ATRIA_EC_DIR_EHESS = "EC_DIR_EHESS"; // Directeurs
																	// d'etudes
																	// de
																	// l'Ecole
																	// des
																	// hautes
																	// etudes en
																	// sciences
																	// sociales
	public static final String ATRIA_EC_DIR_EEE = "EC_DIR_EEE"; // Directeurs
																// d'etudes de
																// l'EPHE, de
																// l'ENC et de
																// l'EFEO
	public static final String ATRIA_EC_PR_CNAM = "EC_PR_CNAM"; // Professeurs
																// du
																// Conservatoire
																// national des
																// arts et
																// metiers
	public static final String ATRIA_EC_AST_APA = "EC_AST_APA"; // Astronomes
																// adjoints et
																// Physiciens
																// adjoints
	public static final String ATRIA_EC_AST_PHY = "EC_AST_PHY"; // Astronomes et
																// Physiciens
	public static final String ATRIA_EC_MCF_PHM = "EC_MCF_PHM"; // Maitres de
																// conferences
																// praticiens
																// hospitaliers
																// de medecine
	public static final String ATRIA_EC_PR_UPH = "EC_PR_UPH"; // Professeurs des
																// universites
																// praticiens
																// hospitaliers
	public static final String ATRIA_EC_MCF_PHO = "EC_MCF_PHO"; // Maitres de
																// conferences
																// praticiens
																// hospitaliers
																// d'odontologie
	public static final String ATRIA_EC_PR_CDO1 = "EC_PR_CDO1"; // Professeurs
																// de chirurgie
																// dentaire
																// odontologistes
																// de 1er grade
	public static final String ATRIA_EC_PR_UO = "EC_PR_UO"; // Professeurs des
															// universites
															// odontologistes

	// Filiere ITRF
	public static final String ATRIA_IT_IRHC = "IT_IRHC"; // Ingenieurs de
															// recherche hors
															// classe
	public static final String ATRIA_IT_IRC1 = "IT_IRC1"; // Ingenieurs de
															// recherche 1ere
															// classe
	public static final String ATRIA_IT_IRC2 = "IT_IRC2"; // Ingenieurs de
															// recherche 2eme
															// classe
	public static final String ATRIA_IT_ING_ETUDE = "IT_ING_ETUDE"; // Ingenieurs
																	// d'etudes
	public static final String ATRIA_IT_ASS_ING = "IT_ASS_ING"; // Assistants
																// ingenieurs
	public static final String ATRIA_IT_TRF_CS = "IT_TRF_CS"; // Techniciens de
																// recherche et
																// de formation
																// de classe
																// superieure
	public static final String ATRIA_IT_TRF_CN = "IT_TRF_CN"; // Techniciens de
																// recherche et
																// de formation
																// de classe
																// normale
	public static final String ATRIA_IT_ATRF5 = "IT_ATRF5"; // Adjoints
															// Techniques de
															// recherche et de
															// formation -
															// Echelle 5
	public static final String ATRIA_IT_ATRF3_HP = "IT_ATRF3_HP"; // Adjoints
																	// Techniques
																	// de
																	// recherche
																	// et de
																	// formation
																	// - Echelle
																	// 3
																	// (recrutements
																	// sans
																	// concours
																	// – hors
																	// PACTE)
	public static final String ATRIA_IT_ATRF3_P = "IT_ATRF3_P"; // Adjoints
																// Techniques de
																// recherche et
																// de formation
																// - Echelle 3
																// (recrutements
																// PACTE)

	// Filiere ATOSS
	public static final String ATRIA_AT_AAENES = "AT_AAENES"; // Attaches
																// d'administration
																// de
																// l'education
																// nationale et
																// de
																// l'enseignement
																// superieur
	public static final String ATRIA_AT_SAENES_CS = "AT_SAENES_CS"; // Secretaires
																	// administratifs
																	// de
																	// l'education
																	// nationale
																	// et de
																	// l'enseignement
																	// superieur
																	// de classe
																	// superieure
	public static final String ATRIA_AT_SAENES_CN = "AT_SAENES_CN"; // Secretaires
																	// administratifs
																	// de
																	// l'education
																	// nationale
																	// et de
																	// l'enseignement
																	// superieur
																	// de classe
																	// normale
	public static final String ATRIA_AT_AAENES4 = "AT_AAENES4"; // Adjoints
																// administratifs
																// de
																// l'education
																// nationale et
																// de
																// l'enseignement
																// superieur -
																// Echelle 4
	public static final String ATRIA_AT_AAENES3_HP = "AT_AAENES3_HP"; // Adjoints
																		// administratifs
																		// de
																		// l'education
																		// nationale
																		// et de
																		// l'enseignement
																		// superieur
																		// -
																		// Echelle
																		// 3
																		// (recrutements
																		// sans
																		// concours
																		// -
																		// hors
																		// PACTE)
	public static final String ATRIA_AT_AAENES3_P = "AT_AAENES3_P"; // Adjoints
																	// administratifs
																	// de
																	// l'education
																	// nationale
																	// et de
																	// l'enseignement
																	// superieur
																	// - Echelle
																	// 3
																	// (recrutements
																	// PACTE)
	public static final String ATRIA_AT_CTSS = "AT_CTSS"; // Conseillers
															// techniques de
															// service social
	public static final String ATRIA_AT_INF = "AT_INF"; // Infirmieres
	public static final String ATRIA_AT_ASS = "AT_ASS"; // Assistants de service
														// social

	// Filiere Bibliotheque
	public static final String ATRIA_BI_CGCB = "BI_CGCB"; // Conservateurs
															// generaux des
															// bibliotheques
	public static final String ATRIA_BI_CB = "BI_CB"; // Conservateurs des
														// bibliotheques
	public static final String ATRIA_BI_BIB = "BI_BIB"; // Bibliothecaires
	public static final String ATRIA_BI_ASS_BIB = "BI_ASS_BIB"; // Assistants
																// des
																// bibliotheques
	public static final String ATRIA_BI_BAS = "BI_BAS"; // Bibliothecaires
														// adjoints spécialises
	public static final String ATRIA_BI_MBIB5 = "BI_MBIB5"; // Magasiniers des
															// bibliotheques -
															// Echelle 5
	public static final String ATRIA_BI_MBIB3 = "BI_MBIB3"; // Magasiniers des
															// bibliotheques -
															// Echelle 3
															// (recrutements
															// sans concours)

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Fin Codes champs ATRIA v2 * * * * * * * * * * * * * * * * * * * * * * * *
	 * * * * * * * * * * * * * * *
	 */
	
	//MSU 1340 début
	public static final String TYP_DIP_AUCUN = "A";
	//MSU 1340 fin

	// Enquete
	public static final String SAISIE_TERMINEE = "Saisie terminée";
	public static final String SAISIE_PARTIELLE = "Saisie partielle";
	public static final String En_ATTENTE_DE_SAISIE = "En attente de saisie";

	// Purgage des postes
	public static final String TB_TD_PST = "TD_PST";
	public static final String TB_TDF_PST = "TDF_PST";
	public static final String TB_TD_GEL = "TD_GEL";
	public static final String TB_TD_ADR = "TD_ADR";

	// Status du rapport
	public static final String STATUS_NON_VISE_CA = "NVE";
	public static final String STATUS_NON_SAISI = "NSA";
	public static final String STATUS_NON_VISE_CNU = "NVU";
	public static final String STATUS_VISE_CNU = "VIU";
	public static final String STATUS_TRANSMIS_CNU = "TRU";
	public static final String STATUS_A_VALIDER = "AVA";
	public static final String STATUS_VALIDE = "VAL";
	public static final String STATUS_VISE_CA = "VCA";
	public static final String STATUS_INTERROMPU = "IEC";
	public static final String STATUS_INTERROMPU_U = "IEU";
	public static final String STATUS_TRANSMIS_CNRS = "TCN";
	public static final String STATUS_TRANSMIS_ENS = "CRH";
	public static final String STATUS_TRANSMIS_LOCAL = "TRL";
	public static final String STATUS_ETAT_FINAL = "FIN";
	public static final String STATUS_VISE_CA_AVT_SPECIF = "VAS";
	public static final String STATUS_A_TRANSMETTRE = "ATE";
	public static final String STATUS_A_CORRIGER = "ACO";
	public static final String STATUS_ENREGISTRE = "ENR";
	public static final String STATUS_VALIDE_ENS = "VAE";
	
	public static final String STATUS_TRANSMISE_ETAB = "TRE";
	public static final String STATUS_A_TRANSMETTRE_INSTANCES_LOCALES = "VAN";
	public static final String STATUS_A_TRANSMETTRE_INSTANCES_NATIONALES = "VAL";
	
	public static final String STATUS_IRRECEVABLE = "IRE";
	public static final String STATUS_RECEVABLE = "REC";
	public static final String STATUS_ATTRIBUTIONS_ENREGISTREES = "ATT";

	// titre des ecrans pour la procedure contradictoire des ech
	public static final String TITRE_ECH_PROCEDURE = "Consultation de l'avis du CAc";
	public static final String TITRE_ECH_SECTION_PROCEDURE = "Consultation de l'avis de la section";

	public static final String MESS_ECH_PROCEDURE = "Votre établissement n'a pas encore saisi d'avis sur votre dossier";
	public static final String MESS_ECH_SECTION_PROCEDURE = "La section n'a pas encore saisi d'avis sur votre dossier";
	public static final String MESS_ECH_AVANC_SPECIF_PROCEDURE = "Vous avez déclaré opter pour la procédure spécifique d'avancement de "
			+ "grade des enseignants-chercheurs.<br/> Vous n'avez donc pas d'avis de section sur votre dossier.";

	// code pour les differents types d'avis du dossier electra
	public static final String CODE_AVIS_ETA = "AETAB";
	public static final String CODE_AVIS_COMPL_ETA = "CETAB";
	public static final String CODE_AVIS_CNU_RAPPORTEUR_1 = "ARAP1";
	public static final String CODE_AVIS_CNU_RAPPORTEUR_2 = "ARAP2";
	public static final String CODE_AVIS_CNU = "ACNU";
	public static final String CODE_AVIS_CNU2 = "ACNU2";
	public static final String CODE_AVIS_CNAP = "ACNAP";
	public static final String CODE_AVIS_COMPL_CNU = "CCNU";
	public static final String AVIS_PAR_DEFAUT_ETAB = "Dossier vu et visé par le CAc qui n'a pas saisi d'avis";
	public static final String AVIS_PAR_DEFAUT_ETAB_V2 = "** Dossier vu mais non validé par l''établissement **";
	//renommage ticket 1077
	//public static final String AVIS_PAR_DEFAUT_ETAB = "Dossier vu et visé par le CAc qui n'a pas saisi d'avis";
	public static final String AVIS_PAR_DEFAUT_SEC = "Dossier vu et visé par la section du CNU qui n'a pas saisi d'avis";
	public static final String AVIS_PAR_DEFAUT_ETAB_CRCT = "Demande vue par le chef d'établissement qui n'a pas saisi d'avis";
	public static final String AVIS_PAR_DEFAUT_ETAB_CPE = "Demande vue par le chef d'établissement qui n'a pas saisi d'avis";
	
	// code promotion
	public static final String CODE_PROMO = "cdpromo";

	public static final int MAX_VARCHAR_SIZE = 4000;
	public static final int MAX_SIZE = 8000;
	public static final int MAX_RAP_SIZE = 6000;
	public static final int MAX_CLOB_SIZE = 10000;
	
	public static final int MAX_SIZE_AVIS_CRCT = 2000;
	
	//MAX SIZE FILE 5MO
	public static final int MAX_SIZE_FILE = 5242000;
	
	// Mode de transmission
	public static final String CDMTD = "cdmdt";
	public static final String LIBELLE = "libelle";
	public static final String INDISELEC = "indiselec";
	public static final String CDINFOCOMPL = "cdinfocompl";
	public static final String LIBINFOCOMPL = "libinfocompl";
	public static final String VALINFOCOMPL = "valinfocompl";
	public static final String INDIQUALIF = "indiqualif";
	public static final String INDIQUALIFCOMPL = "indiqualifcompl";
	public static final String INDIQUALIF2 = "indiqualif2";
	public static final String INDIQUALIF2COMPL = "indiqualif2compl";
	public static final String INDIPOSTE = "indiposte";
	public static final String INDIVEGA = "indivega";
	public static final String INDIPOSTECOMPL = "indipostecompl";

	public static final String CDTYPEVOI = "cdtypevoi";

	public static final String DOSS = "DOSS"; /* dossiers de qualification */
	public static final String PCOM = "PCOM"; /* pièces complémentaires */

	public static final String REGEMAIL = "^[a-zA-Z]+[a-zA-Z0-9\\._-]*[a-zA-Z0-9]@[a-zA-Z]+[a-zA-Z0-9\\._-]*[a-zA-Z0-9]+\\.[a-zA-Z]{2,4}$";
	public static final String REGNUMBER = "[0-9]+";
	public static final String REGURL = "https?://.*.";
	public static final String REGURL_SANS_CARAC_SPEC = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:.]*[-a-zA-Z0-9+&@#/%=~_|]";

	// GALAXIE 22/01/2010 FGU
	public static final String JNDI_RAPPORT_GRADE = "fr.gouv.education.antares.ejb.gradeRapportSL.Grade";
	// GALAXIE 22/01/2010 FGU
	public static final String JNDI_RAPPORT_SECTION = "fr.gouv.education.antares.ejb.sectionRapportSL.Section";
	// images raport activité
	public static final String LOGO1 = "/appli/galaxie/GALAXIE/rapport/MINISTERE_EDUC.JPEG";
	public static final String LOGO2 = "/appli/galaxie/GALAXIE/rapport/REPUBLIQUE_FR.JPEG";

	// ETAT RAPPORT
	public static final String AVIS_RAP_AVISCA = "Visé CA";

	public static final String AVIS_RAP_VCA = "VCA";
	public static final String AVIS_RAP_VIU = "VIU";
	
	// ETAT CRCT
	public static final String AVIS_CRCT_VCE = "VCE";
	public static final String AVIS_CRCT_IRE = "IRE";
	
	// ETAT CPE	
	public static final String AVIS_CPE_VCE = "VCE";
	public static final String AVIS_CPE_AVE = "AVE";
	public static final String AVIS_CPE_ANE = "ANE";
	public static final String AVIS_CPE_FIN = "FIN";
	

	// ETAT CRCT ATTRIBUTIONS	
	public static final String AVIS_CRCT_VIU = "VIU";
	public static final String AVIS_CRCT_VAE = "VAE";
	public static final String AVIS_CRCT_ATT = "ATT";
	public static final String AVIS_CRCT_FIN = "FIN";

	public static final Integer PROFIL_CNU_MEMBRE = 4;
	public static final Integer PROFIL_CNU_PRESIDENT = 5;

	// type de population pour la remontée d'info
	public static final String RH_SUPINFO = "RHSUPINF";
	public static final String HUCSV = "HUCSV";
	public static final String CRCTCSV = "CRCTCSV";
	public static final String ANT = "ANT";

	//ano 224252
	public static final String PROMCSV ="PROMCSV";
		
	public static final String URL_ALTAIR_BASE = "http://localhost:8080";

	// EVO 323: Ajout de deux constantes pour champ indireq
	public static final String INDIREQ_N = "N";
	public static final String INDIREQ_O = "O";
	
	// EVO 202452
	public static final String INDI_A = "A";
	public static final String INDI_N = "N";
	public static final String INDI_O = "O";
	public static final String INDI_Y = "Y";
	
	public static final String CODE_DIPLOME_8 = "8";
	public static final String CODE_DIPLOME_6 = "6";
	
	public static final String CTU02_MSG_M6 = "CTU02_MSG_M6";
	public static final String CTU02_MSG_M7 = "CTU02_MSG_M7";
	

	public static final String CONFIRMATION_NOM_FIDIS = "La restauration des nominations FIDIS a &eacute;t&eacute; &eacute;ffectu&eacute;";
	public static final String LOG_MAJ_UR = "/appli/galaxie/GALAXIE/batchs/results/MAJ_UR/Generes";// ano 543
	public static final String LOG_MAJ_UR_XML = "/appli/galaxie/GALAXIE/batchs/results/MAJ_UR_XML/Generes";
	//public static final String LOG_MAJ_URCNRS = "/appli/galaxie/GALAXIE/batchs/results/MAJ_URCNRS/Generes";
	
	//pedr
	public static final String INF_NON_COMMUNIQUE = "Information non communiquée par l'établissement";
	public static final String INF_NON_COMMUNIQUE_SHORT = "Information non communiquée";
	
	public static final Integer TAILLE_TD_REMREJET_LIGNE = 225;
	
	public static final String JNDI_GESTION_ANO = "fr.gouv.education.antares.ejb.gestionAnoSL.GestionAno";
	
	public static final String JNDI_SO_EN = "fr.gouv.education.antares.ejb.soEN.So";
	
	public static final String JNDI_LISTE_SO = "fr.gouv.education.antares.ejb.listeSoSL.ListeSo";
	
	public static final String JNDI_SOSEC_EN = "fr.gouv.education.antares.ejb.sosecEN.SoSec";
	
	public static final String JNDI_GEST_SEC_MTD_ALT = "fr.gouv.education.antares.qualification.cnu.ejb.gestionSecMtdAltSL.GestionSecMtdAlt";
	
	//MSU Evo 1319
	public static final String TXT_SPE_CTUREC_PARAM = "A_CTU";
	
	public static final String TI_CV = "TI_CV";
	//MSU Evo 1468 
	public static final String EMAIL_FROM = "noreply@education.gouv.fr"; 
	public static final String EMAIL_BOUNCED_MAILS = "test1@test.test"; 
	
	//MSU Evo 1463 début	
	public static final String CAT_GRP_CORPS_A = "Catégorie A";
	public static final String CAT_GRP_CORPS_B = "Catégorie B";
	public static final String CAT_GRP_CORPS_C = "Catégorie C";

	public static final String BOE_DECRET_25081995 = "Décret du 25 août 1995";
	public static final String BOE_ARTICLE_46 = "Articles 4 et 6"; 
	public static final String BOE_DOCTORANTS = "Doctorants";
	public static final String BOE_CONCOURS = "Concours";
	
	public static final String GR_CORPS_INGREC = "Ingénieur de recherche";
	public static final String GR_CORPS_INGETU = "Ingénieur d'études";
	public static final String GR_CORPS_ASSING = "Assistant Ingénieur";
	public static final String GR_CORPS_BIB = "Bibliothécaire";
	public static final String GR_CORPS_CONBIB = "Conservateur des bibliothèques";
	public static final String GR_CORPS_CASU = "CASU";

	public static final String GR_CORPS_AAE = "Attaché d'administration de l'Etat";
	public static final String GR_CORPS_MED = "Médecin";
	public static final String GR_CORPS_INF = "Infirmier";
	public static final String GR_CORPS_ENSSUP = "Enseignant du supérieur";
	public static final String GR_CORPS_ESD = "Enseignant du second degré";
	public static final String GR_CORPS_ESP = "Enseignant du premier degré";
	public static final String GR_CORPS_CTSS = "CTSS";
	
	public static final String GR_CORPS_TRF = "Technicien de recherche et de formation";
	public static final String GR_CORPS_BAS = "Bibliothécaire assistant spécialisé";
	public static final String GR_CORPS_SAENES = "SAENES";
	public static final String GR_CORPS_ASS = "Assistant de service social";
	public static final String PRESIDENT = "Président";
	public static final String VICE_PRESIDENT = "Vice-Président";
	public static final String MEMBRE = "Membre";
	public static final String RAPPORTEUR = "Rapporteur";
	
	public static final String GR_CORPS_ATRF = "Adjoint technique de recherche et de formation";
	public static final String GR_CORPS_MAGBIB = "Magasinier de bibliothèque";
	public static final String GR_CORPS_ADJAENES = "ADJAENES";
	
	//Types de handicap
	public static final String TYPE_HANDI_MOTEUR = "moteur";
	public static final String TYPE_HANDI_VISUEL = "visuel";
	public static final String TYPE_HANDI_AUDI = "auditif";
	public static final String TYPE_HANDI_PSY = "psychique";
	public static final String TYPE_HANDI_MCI = "maladie chronique invalidante";
	public static final String TYPE_HANDI_AUTRE = "autre";
	public static final String TYPE_HANDI_NC = "non communiqué";

	
	//Tranches d'âge
	public static final String TR_AGE_M25 = "-25";
	public static final String TR_AGE_25A40 = "25 à 40";
	public static final String TR_AGE_41A55 = "41 à 55";
	public static final String TR_AGE_P55 = "+55";
	
	public static final String LIB_TR_AGE_M25 = "Moins de 25 ans";
	public static final String LIB_TR_AGE_25A39 = "De 25 à 39 ans";
	public static final String LIB_TR_AGE_40A55 = "De 40 à 55 ans";
	public static final String LIB_TR_AGE_P55 = "Plus de 55 ans";
	
	//Répartitions
	public static final String REP_CONTRAT_CONC = "Répartition par contrats et concours";
	public static final String REP_TRANCHES_AGE = "Répartition par tranches d'âge";
	public static final String REP_TYPES_BOE = "Répartition par types de bénéficiaires de l'obligation d'emploi";
	public static final String REP_TYPE_HANDI = "Répartition par types de handicap";
	public static final String REP_NIV_DIPL = "Répartition par niveaux de diplôme";
	//MSU Evo 1463 fin
	
	//MSU Evo sécu 1134 
	public static final String JNDI_GESTION_DEM_CHGT_MDP = "fr.gouv.education.antares.ejb.gestionDemandeChgtMdpSL.GestionDemandeChgtMdp";

	//Mantis 177769 - module SIRAH
	public static final String JNDI_LISTE_PAYS = "fr.gouv.education.antares.ejb.listePaysSL.ListePays";
	public static final String JNDI_LISTE_SECCNRS = "fr.gouv.education.antares.ejb.listeSecCnrsSL.ListeSecCnrs";
	public static final String JNDI_LISTE_TYPEDEM = "fr.gouv.education.antares.ejb.listeTypeDemSL.ListeTypeDem";
	public static final String JNDI_LISTE_URCNRS = "fr.gouv.education.antares.ejb.listeUrCnrsSL.ListeUrCnrs";
	public static final String JNDI_UR = "fr.gouv.education.antares.ejb.urEN.Ur";
	public static final String JNDI_MOTIF_DESIST = "fr.gouv.education.antares.ejb.motifDesistEN.MotifDesist";
	public static final String STATUS_DESISTEMENT = "DES";
	public static final String STATUS_TRANSMIS_CNRS_SIRAH = "TRC";
	public static final String STATUS_PROPOSITION_CNRS = "PRC";
	public static final String STATUS_AVIS_ETAB_A_VALIDER = "NVE";
	public static final String STATUS_AVIS_ETAB_VALIDE = "VAE";
	public static final String STATUS_REPONSE_ENSEIGNANT = "REC";
	public static final String STATUS_ARBITRAGE_FINAL = "FIN";
	public static final String STATUS_NON_TRANSMIS_CNRS = "NTR";
	public static final String OBJET_MAIL_RENVOI_COR = "M_ORS";
	public static final String TEXTE_MAIL_RENVOI_COR = "M_TRS";
	
	//Evo 202452
	public static final String STATUT_QUALIF_ENCOURS = "EC";
	public static final String STATUT_QUALIF_VALIDE = "VA";
	public static final String STATUT_QUALIF_TRANSMIS = "TR";
	public static final String STATUT_QUALIF_RECEVABLE = "RE";
	public static final String STATUT_QUALIF_RECE_COND = "RC";
	public static final String STATUT_QUALIF_IRRECEVABLE = "IR";
	public static final String STATUT_QUALIF_IRRECEVABLE_DISPENSE_REFUSEE = "ID";
	public static final String STATUT_QUALIF_DEJA_QUALIFIE = "DQ";
	// VMA ANO 223971 / 217967 : certains candidats n'apparaissent plus dans l'écran : Recherche de candidats (CAN03)
	public static final String STATUT_QUALIF_NON_TRANSMIS = "NT";
	//evo 230534
	public static final String STATUT_QUALIF_RENONCEMENT = "RN";
	//evo 308558
	public static final String STATUT_QUALIF_RECEVABILITE_EN_ATTENTE = "RA";
	public static final String STATUT_QUALIF_DEJA_PR = "PR";
	public static final String STATUT_QUALIF_MCF = "MC";
	
	//Mantis 163169 - 
	// constante définissant la valeur de la discipline "Sans discipline"
	public static final String CODEDISC_H0000 = "H0000";
	public static final String TYPE_DOC_CTU_DI = "DI";
	public static final String TYPE_DOC_CTU_RS = "RS";
	public static final String TYPE_DOC_CTU_ID = "ID";
	public static final String TYPE_DOC_CTU_T1 = "T1";
	public static final String TYPE_DOC_CTU_T2 = "T2";
	public static final String TYPE_DOC_CTU_T3 = "T3";
	public static final String TYPE_DOC_CTU_T4 = "T4";
	public static final String TYPE_DOC_CTU_T5 = "T5";
	public static final String TYPE_DOC_CTU_AE = "AE";
	public static final String TYPE_DOC_CTU_CT = "CT";
	public static final String TYPE_DOC_CTU_BP = "BP";
	public static final String TYPE_DOC_CTU_BB = "BB";
	public static final String TYPE_DOC_CTU_JF = "JF";
	public static final String TYPE_DOC_CTU_AN = "AN";
	public static final String TYPE_DOC_CTU_AD = "AD";
	public static final String TYPE_DOC_CTU_EX = "EX";
	public static final String TYPE_DOC_CTU_PC = "PC";
	public static final String TYPE_DOC_CTU_PC2 = "P2";
	public static final String TYPE_DOC_CTU_PC3 = "P3";
	public static final String TYPE_DOC_CTU_PC4 = "P4";
	public static final String TYPE_DOC_CTU_PC5 = "P5";
	public static final String TYPE_DOC_CTU_PC6 = "P6";
	public static final String TYPE_DOC_CTU_AC = "AC";
	
	public static final String TYPE_DOC_ALL= "ALL";
	
	public static final String TYPE_DOC_ACT_PRO_A = "A";
	public static final String TYPE_DOC_ACT_PRO_B = "B";
	public static final String TYPE_DOC_ACT_PRO_C = "C";
	public static final String TYPE_DOC_ACT_PRO_J = "J";
	
	public static final String CTU04_ENREGISTREMENT_OK = "CTU04_ENREGISTREMENT_OK";
	public static final String CTU04_DEPOT_OK = "CTU04_DEPOT_OK";
	public static final String CTU04_SUPPRESSION_OK = "CTU04_SUPPRESSION_OK";
	public static final String CTU04_TRANSMETTRE_OK = "CTU04_TRANSMETTRE_OK";
	public static final String CTU04_TRANSMETTRE_WARNING = "CTU04_TRANSMETTRE_WARNING";
	public static final String CTU04_TRANSMETTRE_KO = "CTU04_TRANSMETTRE_KO";
	public static final String CTU04_ERR_NON_ENREGISTRER = "CTU04_ERR_NON_ENREGISTRER";
	public static final String CTU04_DTO = "CTU04_DTO";
	public static final String CTU04_NOM_FICHIER_MODULE_Q = "Q";
	public static final String CTU04_NOM_FICHIER_UNDERSCORE = "_";
	public static final String CTU04_NOM_FICHIER_TRADUCTION = "2";
	public static final String CTU04_NOM_FICHIER_ORIGINAL = "1";
	public static final String CTU04_NOM_FICHIER_EXTENSION = ".pdf";
	public static final String CTU04_ERR_FICHIER_NON_PDF = "CTU04_ERR_FICHIER_NON_PDF";
	public static final String CTU04_ERR_MESSAGE_FICHIER_EXPLOIT = "MESSAGE_FICHIER_EXPLOIT";
	public static final String CTU04_ERR_FICHIER_NON_VALIDE = "CTU04_ERR_FICHIER_NON_VALIDE";
	public static final String CTU04_ERR_FICHIER_TRADUCTION = "TRA";
	public static final String CTU04_ERR_TAILLE_FICHIER = "CTU04_ERR_TAILLE_FICHIER";
	public static final String CTU04_ERR_TAILLE_FICHIER_BIS = "CTU04_ERR_TAILLE_FICHIER_BIS";
	public static final String CTU04_ERR_TAILLE_PAGE_FICHIER = "CTU04_ERR_TAILLE_PAGE_FICHIER";
	public static final String CTU04_ERR_PAGE_FICHIER = "CTU04_ERR_PAGE_FICHIER";	
	public static final String CTU04_TAILLE_MAX_FICHIER = "CTU04_TAILLE_MAX_FICHIER";
	public static final String CTU04_PAGE_MAX_FICHIER = "CTU04_PAGE_MAX_FICHIER";
	
	
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteAE 			="CTU04_ERR_OBLIGATOIRE_ActiviteAE";
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteCT 			= "CTU04_ERR_OBLIGATOIRE_ActiviteCT";
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteBP 		= "CTU04_ERR_OBLIGATOIRE_ActiviteBP";
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteBDP 		= "CTU04_ERR_OBLIGATOIRE_ActiviteBDP";
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteJF 			= "CTU04_ERR_OBLIGATOIRE_ActiviteJF";
	public static final String CTU04_ERR_OBLIGATOIRE_Nomination 		= "CTU04_ERR_OBLIGATOIRE_Nomination";
	public static final String CTU04_ERR_OBLIGATOIRE_Detachement 	= "CTU04_ERR_OBLIGATOIRE_Detachement";
	public static final String CTU04_ERR_OBLIGATOIRE_Expose 				= "CTU04_ERR_OBLIGATOIRE_Expose";
	public static final String CTU04_ERR_OBLIGATOIRE_Diplome 				= "CTU04_ERR_OBLIGATOIRE_Diplome";
	public static final String CTU04_ERR_OBLIGATOIRE_Rapport 				= "CTU04_ERR_OBLIGATOIRE_Rapport";
	public static final String CTU04_ERR_OBLIGATOIRE_PieceIdentite 	= "CTU04_ERR_OBLIGATOIRE_PieceIdentite";
	public static final String CTU04_ERR_OBLIGATOIRE_Doc1 					= "CTU04_ERR_OBLIGATOIRE_Doc1";
	public static final String CTU04_ERR_OBLIGATOIRE_Doc2 					= "CTU04_ERR_OBLIGATOIRE_Doc2";
	public static final String CTU04_ERR_OBLIGATOIRE_Doc3 					= "CTU04_ERR_OBLIGATOIRE_Doc3";
	public static final String CTU04_ERR_OBLIGATOIRE_Doc4 					= "CTU04_ERR_OBLIGATOIRE_Doc4";
	public static final String CTU04_ERR_OBLIGATOIRE_Doc5 					= "CTU04_ERR_OBLIGATOIRE_Doc5";
	public static final String CTU04_ERR_OBLIGATOIRE_ActiviteAP 			="CTU04_ERR_OBLIGATOIRE_ActiviteAP";
	
	
	
		
	public static final String CTU06_L_ERR_CRITERE_OBLIGATOIRE = "CTU06_L_ERR_CRITERE_OBLIGATOIRE";
	public static final String CTU06_L_ERR_AUCUNE_CANDIDATURE = "CTU06_L_ERR_AUCUNE_CANDIDATURE";
	public static final String CTU06_LISTE_CANDIDATURE = "CTU06_LISTE_CANDIDATURE";
	public static final String CTU06_CRITERES = "CTU06_CRITERES";
	public static final String CTU06_L_ERR_CRITERE_NOM_LENGTH = "CTU06_L_ERR_CRITERE_NOM_LENGTH";

	public static final String CTU07_M_RETOUR_CTU06 = "retour_ctu06";
	
	public static final String CTU09_M_ERR_LETTRE = "CTU09_M_ERR_LETTRE";
	
	public static final String CTU10_M_ERR_CRITERE_OBLIGATOIRE = "CTU10_M_ERR_CRITERE_OBLIGATOIRE";
	public static final String CTU10_M_ERR_SELECT_GESTIONNAIRE = "CTU10_M_ERR_SELECT_GESTIONNAIRE";
	public static final String CTU10_L_ERR_AUCUNE_CANDIDATURE = "CTU10_L_ERR_AUCUNE_CANDIDATURE";
	public static final String CTU10_LISTE_CANDIDATURE = "CTU10_LISTE_CANDIDATURE";
	public static final String CTU10_CRITERES = "CTU10_CRITERES";
	
	public static final String CAN03_ERR_AUCUNE_CANDIDATURE = "CAN03_ERR_AUCUNE_CANDIDATURE";
	
	public static final Integer NUMECOR_1 = 1;
	public static final Integer NUMECOR_3 = 3;
	public static final Integer NUMERO_IDENTIFIANT_PROFIL_6 = 6;
	
	public static final String CRITERE = "critere";
	public static final String ERRORS = "errors";
	public static final String ESPACE_TIRET_ESPACE = " - ";
	
	public static final String CTU11_L_ERR_CRITERE_OBLIGATOIRE = "CTU11_L_ERR_CRITERE_OBLIGATOIRE";
	public static final String CTU11_L_LISTE_DOSSIER_CONFIE = "listeDossierConfie";	
	
	public static final String CTU12_L_CONSULTER_MOTIF_TRANSFERT = "ConsulterMotifTransfert";
	public static final String CTU12_L_RETOUR_CTU11 = "submit_back_ctu11";
	
	public static final String CTU08_ERREUR_MOTIF_OBLIGATOIRE = "CTU08_ERREUR_MOTIF_OBLIGATOIRE";
	public static final String CTU08_ENREGISTREMENT_OK = "CTU08_ENREGISTREMENT_OK";
	
	public static final String BUREAU_ORP = "B_ORP";
	public static final String DPEE3 = "DPEE3";
	
	public static final String JNDI_LISTE_MODERECVEGA = "fr.gouv.education.antares.ejb.listeModesRecrutVegaSL.ListeModesRecrutVega";
	public static final String JNDI_RETARD_SIRAH = "fr.gouv.education.antares.ejb.retardSirahEN.RetardSirah";
	public static final String JNDI_LISTE_DISCIPLINEAGENTVEGA = "fr.gouv.education.antares.ejb.listeDisciplineAgentVegaSL.ListeDisciplineAgentVega";
	public static final String JNDI_LISTE_TYPEPOSTEVEGA = "fr.gouv.education.antares.ejb.listeTypePosteVegaSL.ListeTypePosteVega";
	
	//MANTIS 217519 - JES - paramètres pour envoi mail au candidat en cas de dossier irrecevable
	public static final String IRR_C = "IRR_C";
	public static final String IRR_S = "IRR_S";
	
	//EVO 233731 / 233734
	public static final String MTR_O = "MTR_O";
	public static final String MTR_C = "MTR_C";
		
	//evo 233825
	public static final String C_VAL = "C_VAL";
	public static final String S_VAL = "S_VAL";
	
	//JES - appel au groupe
	public static final String IR_CG = "IR_CG";
	public static final String IR_SG = "IR_SG";
	
	public static final String GRP_DEB01_ERR_CRITERE_OBLIGATOIRE = "GRP_DEB01_ERR_CRITERE_OBLIGATOIRE";
	public static final String GRP_DEB01_ERR_AUCUNE_CANDIDATURE = "GRP_DEB01_ERR_AUCUNE_CANDIDATURE";
	public static final String GRP_DEB01_LISTE_CANDIDATURE = "GRP_DEB01_LISTE_CANDIDATURE";
	public static final String GRP_DEB01_CRITERES = "GRP_DEB01_CRITERES";
	public static final String GRP_DEB01_SUCCES_DEBLOC_CTU = "GRP_DEB01_SUCCES_DEBLOC_CTU";
	
	public static final String JNDI_PARAM_CORRCONC = "fr.gouv.education.antares.ejb.correspondantConcEN.CorrespondantConc";
	public static final String JNDI_PARAM_COLORATION_ITRF = "fr.gouv.education.antares.ejb.colorationItrfEN.ColorationItrf";	
	
	public static final String EDIT_CAN_DISP_DIPL = "Demande de dispense de diplôme car titulaire de diplômes universitaires, qualifications et/ou titres de niveau équivalent";

	public static final String PARAM_FOR_FILE_GEST_CONC = "GEST_CONC";
	public static final String PARAM_FOR_FILE_ORG_CONC = "ORG_CONC";
	public static final String PARAM_FOR_FILE_ETAB_MERE_FILLE_COLOR = "ETAB_MERE_FILLE_COLOR";
	public static final String PARAM_FOR_FILE_ETAB_COLOR = "ETAB_COLOR";
	public static final String PARAM_FOR_FILE_COLOR_CAT_A_B = "COLOR_CAT_A_B";
	public static final String PARAM_FOR_FILE_COLOR_CAT_C = "COLOR_CAT_C";
	public static final String PARAM_FOR_FILE_COLOR_CAT_C_DOWNLOAD = "COLOR_CAT_A_B_DOWNLOAD";
	public static final String PARAM_FOR_FILE_COLOR_CAT_A_B_DOWNLOAD = "COLOR_CAT_C_DOWNLOAD";	
	public static final String PARAM_FOR__FILE_PDF_TDB_COLOR = "EDIT_PDF_TDB_COLOR";
	
	public static final Map<String, String> COLOR_ITRF_FILE_URI_MAP = new HashMap<String, String>();
    static {
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_GEST_CONC, "/AtriaGenerationFichierGestConc");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_ORG_CONC, "/AtriaGenerationFichierOrgConc");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_ETAB_MERE_FILLE_COLOR, "/AtriaGenerationFichierEtabMereFilleColor");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_ETAB_COLOR, "/AtriaGenerationFichierEtabColor");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_COLOR_CAT_A_B, "/AtriaGenerationFichierColorCatAB");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_COLOR_CAT_C, "/AtriaGenerationFichierColorCatC");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_COLOR_CAT_A_B_DOWNLOAD, "/AtriaGenerationFichierColorCatABDownload");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR_FILE_COLOR_CAT_C_DOWNLOAD, "/AtriaGenerationFichierColorCatCDownload");
    	COLOR_ITRF_FILE_URI_MAP.put(PARAM_FOR__FILE_PDF_TDB_COLOR, "/AtriaGenerationFichierEditTabBordColor");
    }

    public static final String REP_DDC_12_05_PDF_SEPT = "1 an à mi-temps avec prise de fonction au {0}";
	public static final String REP_DDC_6_1_PDF_SEPT = "6 mois à temps plein avec prise de fonction au {0}";
	public static final String REP_DDC_6_1_PDF_FEV = "6 mois à temps plein avec prise de fonction au {0}";

	
	public static final Integer CAS_NOMINAL_12_1_PDF_SEPT = 1;
	public static final Integer CAS_NOMINAL_12_05_PDF_SEPT = 2;
	public static final Integer CAS_NOMINAL_6_1_PDF_SEPT = 3;
	public static final Integer CAS_NOMINAL_6_1_PDF_FEV = 4;
	
	public static final String IDREPCHOIX_CAS_NOMINAL = "3";
	public static final String PDF_SEPT = "01/09";
	public static final String PDF_FEV = "01/02";
	
	public static final String TEMP_PARTIEL = "50";
	public static final String TEMP_PLEIN = "100";
	public static final String DUREE_12_MOIS = "12";
	public static final String DUREE_6_MOIS = "6";
	public static final int CHOIX_REP_DMDE_CHANGEMENT = 3;
    
    public static final String PARAM_CAT_A_B = "CAT_AB";
    public static final String PARAM_CAT_C = "CAT_C";   
    public static final String LABEL_CAT_A = "A";
    public static final String LABEL_CAT_B = "B";
    public static final String LABEL_CAT_C = "C";
    public static final Integer SAFRNO_INCREMENTATION_CAT_A_B = 10;
    public static final Integer SAFRNO_INCREMENTATION_CAT_C = 1;
    public static final String SAFRNO_SUFFIX_FOR_CAT_C = "W";
    public static final String SAFRNO_SUFFIX_FOR_CAT_AB = "9";
    public static final String SAFRNO_SUFFIX_TO_CONCAT_FOR_CAT_AB = "";
    public static final int FIRST_NBCHAR_TO_INCREMENT_FOR_COLOR_CAT_C = 5;
    public static final int SAFRNO_LENGHT = 6;   
    public static final String SAFNRO = "safnro";   
    
    public static final String TEXTE_CDJ_PST = "MS_PA";
    public static final String JNDI_GESTION_CHIRON = "fr.gouv.education.antares.ejb.gestionVivierChironSL.GestionVivierChiron";
    public static final String JNDI_GESTION_COMMISSION_VEGA = "fr.gouv.education.antares.ejb.gestionCommissionVegaSL.GestionCommissionVega";
    
    public static final String TXT_TIT_CHIRON = "Attention, le membre du vivier s&eacute;lectionn&eacute; n'est pas encore titulaire";
    //ano 259657
    public static final String TXT_INT_ETAB_CHIRON = "Attention, le membre du vivier s&eacute;lectionn&eacute; est interne &agrave; votre &eacute;tablissement";
    public static final String TXT_ACTIV_CHIRON = "Attention, le membre du vivier sélectionn&eacute; n'est pas actuellement en position d'activit&eacute;";
    public static final String CHIRON = "chiron";
    
    public static final String TXT_ABSENCE_QUALIFICATION = " En l'absence de qualification, demande de dispense par l'instance compétente de l'établissement";
    
    public static final String imageOn = "<image src=\""+Constantes.CONTEXT_URI+"/Images/cbON.gif\">";
    public static final String imageOff = "<image src=\""+Constantes.CONTEXT_URI+"/Images/cbOFF.gif\">";
    
    public static final String CODE_DEPT_ETR = "999";
	public static final String CAS_MULTIAFFECTATIONS = "Dans le cadre d'une multi-affectation, veuillez préciser votre demande de changement en commentaire ";
	public static final String CAS_PARTICULIER = "Dans le cadre d'un cas particulier, veuillez préciser votre demande de changement en commentaire ";
	
	public static final String RAPPORTEUR_NON_DESIGNE = "Rapporteur non d&eacute;sign&eacute;";
	public static final String COM_VEGA = "comVega";
	
	public static final String VEGA_DOC_TYPE_L1 = "L1";
	public static final String VEGA_DOC_TYPE_L2 = "L2";
	public static final String VEGA_DOC_TYPE_L3 = "L3";
	public static final String VEGA_DOC_TYPE_P1 = "P1";
	public static final String VEGA_DOC_TYPE_P2 = "P2";
	public static final String VEGA_DOC_TYPE_PV = "PV";
	
	public static final String VEGA_DOC_TYPE_CDC = "CDC";
	public static final String VEGA_DOC_TYPE_MDR = "MDR";
	public static final String VEGA_DOC_TYPE_DC1 = "DC1";
	public static final String VEGA_DOC_TYPE_DC2 = "DC2";
	public static final String VEGA_DOC_TYPE_DC3 = "DC3";
	public static final String VEGA_DOC_TYPE_DC4 = "DC4";
	public static final String VEGA_DOC_TYPE_DC5 = "DC5";
	public static final String VEGA_DOC_TYPE_DC6 = "DC6";

	
	public static final String ASTREE_DOC_TYPE_L1 = "L1";
	public static final String ASTREE_DOC_TYPE_L2 = "L2";
	public static final String ASTREE_DOC_TYPE_L3 = "L3";
	public static final String ASTREE_DOC_TYPE_P1 = "P1";
	public static final String ASTREE_DOC_TYPE_P2 = "P2";
	public static final String ASTREE_DOC_TYPE_PV = "PV";
	
	public static final Map<String, String> AVIS_DEPART_VEGA_CODE_LABEL = new LinkedHashMap <String, String>();
    static {
    	AVIS_DEPART_VEGA_CODE_LABEL.put("O", "Accord");
    	AVIS_DEPART_VEGA_CODE_LABEL.put("N", "Refus");
    	AVIS_DEPART_VEGA_CODE_LABEL.put("S", "Sans avis");   	
    }
    
    public static final Map<String, String> ACTION_TYPE_ENVOI_DOCUMENT = new HashMap <String, String>();
    static {
    	
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceIdentite", "Envoyer : Pièce d'identité");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDiplome", "Envoyer : Diplôme");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDipl", "Envoyer : Traduction diplôme");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerRapport", "Envoyer : Rapport de soutenance");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraRapport", "Envoyer : Traduction rapport de soutenance");    	
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDocument1", "Envoyer : Travaux-> Document1");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDocument2", "Envoyer : Travaux-> Document2");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDocument3", "Envoyer : Travaux-> Document3");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDocument4", "Envoyer : Travaux-> Document4");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerDocument5", "Envoyer : Travaux-> Document5");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDocument1", "Envoyer : Travaux-> traduction document1");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDocument2", "Envoyer : Travaux-> traduction document2");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDocument3", "Envoyer : Travaux-> traduction document3");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDocument4", "Envoyer : Travaux-> traduction document4");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraDocument5", "Envoyer : Travaux-> traduction document5");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerExpose", "Envoyer : CV du candidat");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire", "Envoyer : Pièces complémentaires");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire2", "Envoyer : Pièces complémentaires 2");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire3", "Envoyer : Pièces complémentaires 3");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire4", "Envoyer : Pièces complémentaires 4");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire5", "Envoyer : Pièces complémentaires 5");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerPieceComplementaire6", "Envoyer : Pièces complémentaires 6");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerActiviteAE", "Envoyer : Attestation employeur");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraActiviteAE", "Envoyer : Traduction attestation employeur");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerActiviteCT", "Envoyer : Contrat de travail");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraActiviteCT", "Envoyer : Traduction contrat de travail");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerActiviteBP", "Envoyer : Dernier bulletin de paie");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraActiviteBP", "Envoyer : Traduction dernier bulletin de paie");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerActiviteBDP", "Envoyer : Premier et dernier bulletin de paie");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraActiviteBDP", "Envoyer : Traduction premier et dernier bulletin de paie");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerActiviteJF", "Envoyer : Justificatifs fiscaux");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerTraActiviteJF", "Envoyer : Traduction justificatifs fiscaux");
    	ACTION_TYPE_ENVOI_DOCUMENT.put("EnvoyerNomination", "Envoyer : Arrêté de nomination");
    	
    }
    
    public static final String ID_GROUP_DOC_CTU_TRAVAUX = "2";
    
    // Lettre qui représente le module ANTEE et FIDIS
    public static final String MODULE_ANTEE = "S";
    public static final String MODULE_FIDIS = "F";
    
    public static final String LIST_DOC_ETAB_KEY_NAME = "Documents spécifiques demandés par l'établissement";
    
    public static final String TYPE_CONGE_CRCT = "1";
    public static final String TYPE_CONGE_CPE = "2";
    public static final String TYPE_SUITE_CONGE_YES = "O"; 
    public static final String TYPE_SUITE_CONGE_NO = "N";
    
    //264131 ARA
    public static final String[] LISTE_NUM_CORPS_CRCT_CPE = {"300","301"};
    
    public static final String[] LISTE_NUM_CORPS_CPP = {"300","301","330","331","332","333","334","335","338","339","341","342","343","344","346","347","350","351","353","354","355","356","360","361","362","531","532","551","553","559","563","575","600","615"};
    
    public static final String[] LISTE_NUM_CORPS_CNAP_CPE = {"330","331","332","333","334","335","338","339"};
    public static final String[] LISTE_NUM_CORPS_NON_ELIGIBLE_CRCT = {"600","615","531","532","551","553","559","563","575"};
    
    public static final String[] LISTE_NUM_CORPS_ELIGIBLE_CRCT = {"300","301"};
    
    public static final String[] LISTE_TYPER_LIBC_ELIGIBLE_CPP = {"1","2","G"};
    
    
    public static final String NUMCOR_PRM = "3";
    public static final String NUMCOR_MCM = "4";
    
    public static final String CODE_FCT_PART_AUTRES = "17";
    
    public static final String AVIS_CPE_NON_SAISIE = "Demande vue par le chef d'établissement qui n'a pas saisi d'avis";
    
    public final static String TYPE_EXCEPTION_NOM_FICHIER_NON_PDF = "Nom fichier n'est se termine pas par .pdf";
	
    public static final String JNDI_PRIP = "fr.gouv.education.antares.ejb.pripEN.Prip";
    
    public static final String JNDI_GESTION_PRIP = "fr.gouv.education.antares.prip.ech.ejb.gestionPripSL.GestionPrip";
    
    public static final String JNDI_STATUT_PRIP = "fr.gouv.education.antares.ejb.statutPripEN.StatutPrip";
    
    public static final String[] LISTE_NUM_CORPS_PRIP = {"300","301","310","311","315","316","317","320","321","322","323","330","331","332","333","334","335","338","339","341","342","343","344","346","347","350","351","353","354","355","356","360","361","362","531","532","551","553","559","563","575","600","615"};
    
    private static final String[] LISTE_NUM_CORPS_PRIME = {"300","350","360","353","355","330","331","301","354","356","351","332","333"};
    
    public static List<String> getListNumCorpsPrime() {
        return Collections.unmodifiableList(Arrays.asList(LISTE_NUM_CORPS_PRIME));
    }
    
    private static final String[] LISTE_CD_GRADE_PROMO_INT = {"3014", "3324", "3334", "3514", "3544", "3564"};
    
    public static List<String> getListCdGradePromoInt() {
        return Collections.unmodifiableList(Arrays.asList(LISTE_CD_GRADE_PROMO_INT));
    }
    
    public static final String TRESP = "TRESP";
    public static final String TRECP = "TRECP";    
    public static final String MRESP = "MRESP";
    //statut ELARA PRIP
    public static final String STATUT_PRIP_ENREGISTRE= "ENR";
    public static final String STATUT_PRIP_FINAL= "FIN";
    
    public static final Map<String, String> MAP_DECISION_LABEL_ELARA = new HashMap <String, String>();
    static {
    	MAP_DECISION_LABEL_ELARA.put("R", "Refusée");
    	MAP_DECISION_LABEL_ELARA.put("A", "Acceptée");    	
    }
    
    public static final String COM_ANTEE = "comANTEE";
    public static final String COM_FIDIS = "comFIDIS";
    public static final String T_AH3 = "T_AH3";
    public static final String ARR_CD4_ASTREE = "A_CD4";
    
    public static final String PHA_NAOS_CPP_ECH = "NC";
    
    public static final String PHA_NAOS_CRCT_ECH = "NV";
    
    public static final String CODE_MOTIF_DEMANDE_SIRAH_AUTRE = "5";
    
    public static final int NB_QUESTION_SECURITE = 4; 
    
    public static final String MS_ID = "MS_ID";
    
    // Répertoire du log batch archivage et purge antares
    public static final String BTCH_ANTARES_ARCH_PURG = "ANTARES_ARCH_PURG";
    // Répertoire du résultat batch archivage et purge antares
    public static final String FS_ANTARES_ARCH_PURG; 
    
    public static final String B_HLB = "B_HLB";
    
    public static final String PIECE_DOSSIER_ANTEE = "PIECE_DOSSIER_ANTEE";
    public static final String COMITE_ANTEE = "COMITE_ANTEE";
    public static final String NOMINATION_ANTEE = "NOMINATION_ANTEE";
    
    public static final String PIECE_DOSSIER_FIDIS = "PIECE_DOSSIER_FIDIS";
    public static final String COMITE_FIDIS = "COMITE_FIDIS";
    public static final String NOMINATION_FIDIS = "NOMINATION_FIDIS";
    public static final String O_TMA = "O_TMA";
    public static final String M_TRA = "M_TRA";
    public static final String CODE_ARTICLE_CPJ = "CPJ";
    public static final String LIST_DPL_CPJ_STR = "('1','8','10')";
    public static final String LIST_PIECES_CPJ_OBL_STR = "('ID','DI','RS')";
    public static final String LIST_TYPE_DPL_CPJ_STR = "('T','ET')";
    public static final String NUMNATIONAL_NON_REFERENCE = "NC";
    public static final String CODE_ARTICLE_46_1 = "46-1";    
    public static final String ARTICLE_46_1_EXCLUDED_IN_VOEUX_PROCEDURE = "O"; 
    public static final String PARAM_NAME_TO_EXCLUDE_ARTICLE_46_1_IN_VOEUX_PROCEDURE = "E_461";
    public static final String TYPE_DIPLOME_AGREGATION_12 = "12";    
    public static final String INSTPEDR_HU = "HU";    
    public static final String NS_PI = "NS_PI";
    public static final String PARAM_NB_MAX_EXTRACTIONS_ANTEE_FIDIS = "N_EXT";
}
