package fr.gouv.education.antares.outils.sort;



import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import fr.gouv.education.antares.simpleLog;


/**
 * <b>Description : </b>Accesseur dynamique à un bean<br>
 * <br>
 * <br>
 * 
 */
public class DynaBean {
	


	private static final String TYPE_INT = "int"; //$NON-NLS-1$

	private static final String TYPE_DOUBLE = "double"; //$NON-NLS-1$

	private static final String TYPE_BOOLEAN = "boolean"; //$NON-NLS-1$

	/** Liste des objets */
	private Object[] vo = null;

	/** Description de l'objet */
	private PropertyDescriptor[] propertyDescriptor = null;

	/** Liste des objets */
	private int VIEW_LOG = 0;

	/**
	 * Constructeur
	 */
	public DynaBean() {
	}

	/**
	 * Constructeur
	 * 
	 * @param vo
	 *            Collection de vo à accèder
	 */
	public DynaBean(Object vo) {
		this.setVo(vo);
	}

	/**
	 * Constructeur
	 * 
	 * @param vo
	 *            Collection de vo à accèder
	 */
	public DynaBean(Object[] vo) {
		this.setVo(vo);
	}

	/**
	 * Récupérer la class de la colonne
	 * 
	 * @param columnIndex
	 *            la colonne demandé
	 * @return la class trouvée
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return propertyDescriptor[columnIndex].getPropertyType();
	}

	/**
	 * Récupérer la class de la colonne
	 * 
	 * @param columnIndex
	 *            la colonne demandé
	 * @return la class trouvée
	 */
	public Class<?> getColumnClass(String columnName) throws Exception {
		int columnIndex = getColumnID(columnName);

		return propertyDescriptor[columnIndex].getPropertyType();
	}

	/**
	 * Récuperer le nombre d'attributs du VO
	 * 
	 * @return le nombre d'attributs
	 */
	public int getColumnCount() {
		return propertyDescriptor.length;
	}

	/**
	 * Récupérer le numéro de la colonne
	 * 
	 * @param columnName
	 *            la colonne demandée
	 * @return le numéro de la colonne
	 * @throws Exception
	 */
	public int getColumnID(String columnName) throws Exception {
		// balayage de la description
		for (int i = 0; i < propertyDescriptor.length; i++) {
			if (propertyDescriptor[i].getName().equals(columnName)) {
				return i;
			}
		}

		// Exception si colonne non trouvée
		throw new Exception("Colonne non trouvée:" + columnName); //$NON-NLS-1$
	}

	/**
	 * Récupérer le nom de la colonne
	 * 
	 * @param columnIndex
	 *            le n° de la colonne
	 * @return le nom de la colonne
	 */
	public String getColumnName(int columnIndex) {
		return propertyDescriptor[columnIndex].getName();
	}

	
	/**
	 * Mémoriser la description du bean
	 * 
	 * @param propertyDescriptor
	 *            la description du bean
	 */
	public void setPropertyDescriptor(PropertyDescriptor[] propertyDescriptor) {
		this.propertyDescriptor = propertyDescriptor;
	}

	/**
	 * Récupérer le nombre de VO
	 * 
	 * @return le nombre de VO
	 */
	public int getRowCount() {
		return vo.length;
	}

	/**
	 * Récupérer la valeur pour une ligne , une colonne
	 * 
	 * @param row
	 *            la ligne demandée
	 * @param column
	 *            la colonne demandée
	 * @return la valeur
	 * @throws Exception
	 */
	public Object getValueAt(int row, int column) throws Exception {
		String name = null;
		name = propertyDescriptor[column].getName();

		return getValueAt(row, name);
	}

	/**
	 * Récupérer la valeur pour une ligne , une colonne
	 * 
	 * @param row
	 *            la ligne demandée
	 * @param column
	 *            le nom de la colonne demandée
	 * @return la valeur
	 * @throws Exception
	 */
	public Object getValueAt(int row, String column) throws Exception {
		try {
			Object value = null;
			String classe = null;

			// Récupération de la valeur
			value = PropertyUtils.getNestedProperty(vo[row], column);

			// Récupération de la class
			classe = getColumnClass(column).getName();

			
			if (VIEW_LOG > 0) {
				simpleLog.println( 
						"\nrow:" + row + "\ncolumn:" + column + //$NON-NLS-1$//$NON-NLS-2$
								"\nname:" + column
								+ "\nvalue:" + value + "\nclass:" + //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
								classe);
			}
			

			return value;

			// Gestion des exceptions
		} catch (java.lang.NoSuchMethodException e) {
			
				if (VIEW_LOG > 0) {
					simpleLog.println(
							"java.lang.NoSuchMethodException" + //$NON-NLS-1$
									" - row : " + row
									+ " column : " + column + "/n VO:" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									vo[row]);
				}
			
		} catch (java.lang.IllegalAccessException e) {
			
				if (VIEW_LOG > 0) {
						simpleLog.println(
							"java.lang.IllegalAccessException" + //$NON-NLS-1$
									" - row : " + row
									+ " column : " + column + "/n VO:" + //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
									vo[row]);
				}
			
		} catch (java.lang.reflect.InvocationTargetException e) {
			
				if (VIEW_LOG > 0) {
					simpleLog.println(
							"java.lang.reflect.InvocationTargetException" + //$NON-NLS-1$
									" - row : " + row
									+ " column : " + column + "/n VO:" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									vo[row]);
				}
			
		}

		return null;
	}

	/**
	 * Récupérer la valeur pour une colonne
	 * 
	 * @param column
	 *            la colonne demandée
	 * @return la valeur
	 * @throws Exception
	 */
	public Object getValueAt(String columnName) throws Exception {
		return getValueAt(0, columnName);
	}

	/**
	 * Récupérer la valeur une colonne
	 * 
	 * @param column
	 *            la colonne demandée
	 * @return la valeur
	 * @throws Exception
	 */
	public Object getValueAt(int column) throws Exception {
		String name = null;
		name = propertyDescriptor[column].getName();

		return getValueAt(0, name);
	}

	/**
	 * Réinitialiser la liste des VO
	 */
	public void reset() {
		for (int i = 0; i < vo.length; i++) {
			for (int j = 0; j < getColumnCount(); j++) {
				try {
					if (PropertyUtils.isWriteable(vo[i], getColumnName(j))) {
						String cl = getColumnClass(j).getName();

						if (TYPE_INT.equals(cl)) {
							PropertyUtils.setNestedProperty(vo[i],
									getColumnName(j), new Integer(0));
						} else if (TYPE_DOUBLE.equals(cl)) {
							PropertyUtils.setNestedProperty(vo[i],
									getColumnName(j), new Double(0));
						} else if (TYPE_BOOLEAN.equals(cl)) {
							PropertyUtils.setNestedProperty(vo[i],
									getColumnName(j), new Boolean(false));
						} else {
							PropertyUtils.setNestedProperty(vo[i],
									getColumnName(j), null);
						}
					}
				} catch (IllegalAccessException e) {
					
						if (VIEW_LOG > 0) {
							simpleLog.println("java.lang.IllegalAccessException" + //$NON-NLS-1$
											" - row : " + i + " column : " + //$NON-NLS-1$//$NON-NLS-2$
											getColumnName(j) + "/n VO:" + vo[i]); //$NON-NLS-1$
						}
					
				} catch (InvocationTargetException e) {
					
						if (VIEW_LOG > 0) {
							simpleLog.println(
											"java.lang.reflect.InvocationTargetException" + //$NON-NLS-1$
													" - row : " + i
													+ " column : " + //$NON-NLS-1$//$NON-NLS-2$
													getColumnName(j)
													+ "/n VO:" + vo[i]); //$NON-NLS-1$
						}
					
				} catch (NoSuchMethodException e) {
					
						if (VIEW_LOG > 0) {
							simpleLog.println("java.lang.NoSuchMethodException" + //$NON-NLS-1$
											" - row : " + i + " column : " + //$NON-NLS-1$ //$NON-NLS-2$
											getColumnName(j) + "/n VO:" + vo[i]); //$NON-NLS-1$
						}
					
				}
			}
		}
	}

	/**
	 * Mémoriser la liste des VO
	 * 
	 * @param vo
	 *            la liste des VO
	 */
	public void setVo(Object[] vo) {
		this.vo = vo;

		if (vo.length > 0) {
			propertyDescriptor = PropertyUtils.getPropertyDescriptors(vo[0]);
		}
	}

	/**
	 * Mémoriser le VO
	 * 
	 * @param vo
	 *            le VO
	 */
	public void setVo(Object vo) {
		Object[] vos = null;
		vos = new Object[1];
		vos[0] = vo;
		setVo(vos);
	}
}
