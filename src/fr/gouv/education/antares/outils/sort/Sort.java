package fr.gouv.education.antares.outils.sort;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;

/**
 * <b>Description : </b>Trier une collection<br>
 * <br>
 * 
 * 
 */
public class Sort {
	// ~ Méthodes
	// ---------------------------------------------------------------

	public static final String FIELD_SEPARATOR = ","; //$NON-NLS-1$

	/**
	 * Trier une collection
	 * 
	 * @param pIn
	 *            La collection à trier
	 * @param pColumns
	 *            Les colonnes servant au tri
	 * @param pAsc
	 *            Tri ascendant ou descendant
	 */
	@SuppressWarnings("unchecked")
	public static void trier(Collection pIn, String pColumns, boolean pAsc) {
		Object[] in = null;

		if (pIn == null)
			return;
		if (pColumns == null)
			return;

		if (pIn.size() < 2) {
			return;
		}

		in = new Object[pIn.size()];
		pIn.toArray(in);

		trier(in, pColumns, pAsc);

		pIn.clear();

		pIn.addAll(Arrays.asList(in));
	}

	/**
	 * Trier une collection
	 * 
	 * @param pIn
	 *            La collection à trier
	 * @param pColumns
	 *            Les colonnes servant au tri
	 * @param pAsc
	 *            Tri ascendant ou descendant
	 */
	@SuppressWarnings("unchecked")
	public static void trier(ArrayList pIn, String pColumns, boolean pAsc) {
		Object[] in = null;

		if (pIn == null)
			return;
		if (pColumns == null)
			return;

		if (pIn.size() < 2) {
			return;
		}

		in = new Object[pIn.size()];
		pIn.toArray(in);

		trier(in, pColumns, pAsc);

		pIn.removeAll(pIn);

		pIn.addAll(Arrays.asList(in));
	}

	/**
	 * Trier une collection
	 * 
	 * @param pIn
	 *            La collection à trier
	 * @param pColumns
	 *            Les colonnes servant au tri
	 * @param pAsc
	 *            Tri ascendant ou descendant
	 */
	@SuppressWarnings("unchecked")
	public static void trier(Object[] pIn, String pColumns, boolean pAsc) {
		StringTokenizer st = null;
		ArrayList<Object> col = null;
		String[] column = null;

		if (pIn == null)
			return;
		if (pColumns == null)
			return;

		// Separe les differentes colonnes
		st = new StringTokenizer(pColumns, FIELD_SEPARATOR);
		col = new ArrayList<Object>();

		// Charge les colonnes dans un vecteur
		while (st.hasMoreTokens()) {
			col.add(st.nextToken());
		}

		// transforme le vecteur en tableau
		column = new String[col.size()];
		col.toArray(column);

		// Appel le tri
		Arrays.sort(pIn, new CompareBean(column, pAsc));
	}
}

