package fr.gouv.education.antares.outils.sort;



import java.sql.Date;
import java.util.Comparator;

import fr.gouv.education.antares.simpleLog;
import fr.gouv.education.antares.calc.objets.Fonction;


/**
 * <b>Description : </b>Comparateur de bean<br>
 * <br>
 * 
 */
@SuppressWarnings("unchecked")
public class CompareBean implements Comparator {
	
	
	/** Liste des colonnes à trier */
	private String[] columns = null;

	/** type de tri Ascendant/Descendant */
	private boolean asc = true;

	/** Dynabean du premier objet à lire */
	private DynaBean db0 = null;

	/** Dynabean du deuxième objet à lire */
	private DynaBean db1 = null;

	/** Ignore Time */
	private boolean ignoreTime = true;

	/**
	 * Constructeur du comparateur
	 */
	public CompareBean() {
	}

	/**
	 * Constructeur du comparateur
	 */
	public CompareBean(String[] columns, boolean asc) {
		// Mémorisation des infos
		this.columns = columns;
		this.asc = asc;

		// Création des DynaBean de comparaison
		this.db0 = new DynaBean();
		this.db1 = new DynaBean();
	}

	/**
	 * @see java.util.Comparator#compare(Object, Object)
	 */
	public int compare(Object arg0, Object arg1) {
		int rtn = 0;
		
		
		

		// Passage des arguments au dynaBean
		db0.setVo(arg0);
		db1.setVo(arg1);
		
		// Comparaison par appel récursif de la méthode
		try {
			rtn = compare(0);
		} catch (Exception e) {			
			
			simpleLog.println("error : " +  e.getMessage());
		}

		// On inverse le resultat en cas de tri decroissant
		if (asc == false) {
			rtn *= -1;
		}

		return rtn;
	}

	/**
	 * Comparer les champs
	 * 
	 * @param level
	 *            le niveau de comparaison
	 * @return int le résultat de la comparaison
	 * @throws Exception
	 */
	protected int compare(int level) throws Exception {
		Object value0 = null;
		Object value1 = null;
		int rtn = 0;

		// Si l'on a depassé la taille du tableau on arrete
		if (level >= columns.length) {
			return 0;
		}

		// Récupération des valeurs
		value0 = db0.getValueAt(columns[level]);
		value1 = db1.getValueAt(columns[level]);
		

		// Comparaison en fonction des types
		if ((value0 == null) && (value0 == null)) {
			rtn = 0;
		} else if (value0 == null) {
			rtn = -1;
		} else if (value1 == null) {
			rtn = +1;
		} else if (Integer.class.equals(value0.getClass())) {
			rtn = ((Integer) value0).compareTo(((Integer) value1));
		} else if (Double.class.equals(value0.getClass())) {
			rtn = ((Double) value0).compareTo(((Double) value1));
		} else if (String.class.equals(value0.getClass())) {
			rtn = ((String) value0).compareTo(((String) value1));
		} else if (java.sql.Date.class.equals(value0.getClass())) {
			rtn = ((java.sql.Date) value0).compareTo(((java.sql.Date) value1));
		} else if (java.util.Date.class.equals(value0.getClass())) {
			rtn = ((java.util.Date) value0).compareTo(((java.util.Date) value1));
		} else if (java.sql.Timestamp.class.equals(value0.getClass())) {
			rtn = ((java.sql.Timestamp) value0).compareTo(((java.sql.Timestamp) value1));
		} 
		// Si les valeurs sont différentes, on arrete la comparaison
		if (rtn != 0) {
			return rtn;
		}

		// On appelle la comparaison suivante
		return compare(level + 1);
	}

	/**
	 * Déterminer si le BO est identique
	 * 
	 * @param pBO :
	 *            l'objet à comparer
	 * @see java.util.Comparator#equals(Object)
	 */
	public boolean testerEgalite(Object pBO1, Object pBO2) {
		boolean equals = true;

		try {
			// Création des dynabean de comparaison
			DynaBean objDB1 = new DynaBean();
			DynaBean objDB2 = new DynaBean();

			// Valorisation des dynabean de comparaison
			objDB1.setVo(pBO1);
			objDB2.setVo(pBO2);

			// Comparaison attribut par attribut
			for (int i = 0; i < objDB1.getColumnCount(); i++) {
				// Récupération des valeurs
				Object field1 = objDB1.getValueAt(0, i);
				Object field2 = objDB2.getValueAt(0, i);

				if ((field1 == null) && (field2 == null)) {
				} else if (field1 == null) {
					return false;
				} else if (field2 == null) {
					return false;
				} else if (Date.class.equals(field1.getClass())
						&& isIgnoreTime()) {
					
					if ((field1 == null) || (field2 == null)) {
						return true;
					}
					return (((Date)field1).compareTo((Date)field2) !=0);
					
				} else if (!field1.equals(field2)) {
					return false;
				}
			}

			// Traitement des exceptions
		} catch (Exception e) {
			equals = false;
		}

		return equals;
	}

	/**
	 * Ignorer la partie time d'une date
	 * 
	 * @return vrai/faux
	 */
	public boolean isIgnoreTime() {
		return ignoreTime;
	}

	/**
	 * Ignorer la partie time d'une date
	 * 
	 * @param pIgnoreTime
	 *            ignore la partie time d'une date
	 */
	public void setIgnoreTime(boolean pIgnoreTime) {
		ignoreTime = pIgnoreTime;
	}

	
}
