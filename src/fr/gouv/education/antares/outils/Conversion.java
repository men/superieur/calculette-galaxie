package fr.gouv.education.antares.outils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.simpleLog;

/**
 * Titre : Conversion Description : permet de faire des conversions de type
 * Copyright : Copyright (c) 2001 Société : airial
 * 
 * @author christian schiele
 * @version 1.0
 */

public class Conversion {
	private final static char UCH = 127;
	private final static char[] tableConvAccent = { UCH, UCH, UCH, UCH, UCH,
			UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH,
			UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH,
			UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH,
			UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH, UCH,
			UCH, UCH, UCH, UCH, UCH, UCH, UCH, 'A', 'A', 'A', 'A', 'A', 'A',
			'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
			'O', 'O', 'O', 'O', UCH, '0', 'U', 'U', 'U', 'U', 'Y', UCH, UCH,
			'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i',
			'i', 'i', 'i', 'd', 'n', 'o', 'o', 'o', 'o', 'o', UCH, '0', 'u',
			'u', 'u', 'u', 'y', UCH, 'y' };

	final static String[] hex = { "%00", "%01", "%02", "%03", "%04", "%05",
			"%06", "%07", "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e",
			"%0f", "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
			"%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f", "%20",
			"%21", "%22", "%23", "%24", "%25", "%26", "%27", "%28", "%29",
			"%2a", "%2b", "%2c", "%2d", "%2e", "%2f", "%30", "%31", "%32",
			"%33", "%34", "%35", "%36", "%37", "%38", "%39", "%3a", "%3b",
			"%3c", "%3d", "%3e", "%3f", "%40", "%41", "%42", "%43", "%44",
			"%45", "%46", "%47", "%48", "%49", "%4a", "%4b", "%4c", "%4d",
			"%4e", "%4f", "%50", "%51", "%52", "%53", "%54", "%55", "%56",
			"%57", "%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f",
			"%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67", "%68",
			"%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f", "%70", "%71",
			"%72", "%73", "%74", "%75", "%76", "%77", "%78", "%79", "%7a",
			"%7b", "%7c", "%7d", "%7e", "%7f", "%80", "%81", "%82", "%83",
			"%84", "%85", "%86", "%87", "%88", "%89", "%8a", "%8b", "%8c",
			"%8d", "%8e", "%8f", "%90", "%91", "%92", "%93", "%94", "%95",
			"%96", "%97", "%98", "%99", "%9a", "%9b", "%9c", "%9d", "%9e",
			"%9f", "%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7",
			"%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af", "%b0",
			"%b1", "%b2", "%b3", "%b4", "%b5", "%b6", "%b7", "%b8", "%b9",
			"%ba", "%bb", "%bc", "%bd", "%be", "%bf", "%c0", "%c1", "%c2",
			"%c3", "%c4", "%c5", "%c6", "%c7", "%c8", "%c9", "%ca", "%cb",
			"%cc", "%cd", "%ce", "%cf", "%d0", "%d1", "%d2", "%d3", "%d4",
			"%d5", "%d6", "%d7", "%d8", "%d9", "%da", "%db", "%dc", "%dd",
			"%de", "%df", "%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6",
			"%e7", "%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef",
			"%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7", "%f8",
			"%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff" };

	private static final String LOCK_CONVERT_DATE = "LOCKED";

	/**
	 * Encode une chaine dans le format "x-www-form-urlencoded", comprenant
	 * l'option UTF-8-in-URL. La fonction opère ainsi :
	 * <ul>
	 * <li>
	 * <p>
	 * Les caractères ASCII de 'a' à 'z', de 'A' à 'Z' et '0' à '9' ne sont pas
	 * affectée.
	 * </p>
	 * <li>
	 * <p>
	 * Les caractères non réservées : - _ . ! ~ ' ( ), non plus.
	 * </p>
	 * <li>
	 * <p>
	 * Le caractère espace est changé en '+'
	 * </p>
	 * <li>
	 * <p>
	 * Tous les autres caractères ASCII sont remplacés par le code '%xx' avec
	 * 'xx' le code ASCII.
	 * </p>
	 * <li>
	 * <p>
	 * Tous caractères non-ASCII sont encodés en deux étapes : D'abord ils sont
	 * convertis en une séquence de 2 ou 3 bytes (algorithme UTF-8), ensuite
	 * chaque byte est encodé au format '%xx'.
	 * </ul>
	 * Code récupéré sur le site :
	 * http://www.w3.org/International/O-URL-code.html Javadoc traduite par mes
	 * soins (RAG).
	 * 
	 * @param s
	 *            chaine à encoder.
	 * @return la chaine encodée.
	 */
	public static String encode(String s) {
		if (s == null)
			return null;

		StringBuffer sbuf = new StringBuffer();
		int len = s.length();
		for (int i = 0; i < len; i++) {
			int ch = s.charAt(i);
			if ('A' <= ch && ch <= 'Z') { // 'A'..'Z'
				sbuf.append((char) ch);
			} else if ('a' <= ch && ch <= 'z') { // 'a'..'z'
				sbuf.append((char) ch);
			} else if ('0' <= ch && ch <= '9') { // '0'..'9'
				sbuf.append((char) ch);
			} else if (ch == ' ') { // space
				sbuf.append('+');
			} else if (ch == '-'
					|| ch == '_' // unreserved
					|| ch == '.' || ch == '!' || ch == '~' || ch == '*'
					|| ch == '\'' || ch == '(' || ch == ')') {
				sbuf.append((char) ch);
			} else if (ch <= 0x00ff) { // other ASCII
				// Note de RAG : Par rapport au code d'origine, j'ai étendu la
				// plage
				// ASCII de 0x7f à 0xff, car les anglo-saxons ne tiennent pas
				// compte des caractères accentués dans leur plage initiale.
				sbuf.append(hex[ch]);
			} else if (ch <= 0x07FF) { // non-ASCII <= 0x7FF
				sbuf.append(hex[0xc0 | (ch >> 6)]);
				sbuf.append(hex[0x80 | (ch & 0x3F)]);
			} else { // 0x7FF < ch <= 0xFFFF
				sbuf.append(hex[0xe0 | (ch >> 12)]);
				sbuf.append(hex[0x80 | ((ch >> 6) & 0x3F)]);
				sbuf.append(hex[0x80 | (ch & 0x3F)]);
			}
		}
		return sbuf.toString();
	}

	/**
	 * Encode une chaine dans le format "x-www-form-urlencoded", comprenant
	 * l'option UTF-8-in-URL. La fonction opère ainsi :
	 * <ul>
	 * <li>
	 * <p>
	 * Les caractères ASCII de 'a' à 'z', de 'A' à 'Z', '0' à '9' et '?', '&',
	 * '=' (délimiteurs des paramètres) ne sont pas affectés.
	 * </p>
	 * <li>
	 * <p>
	 * Les caractères non réservées : - _ . ! ~ ' ( ), non plus.
	 * </p>
	 * <li>
	 * <p>
	 * Le caractère espace est changé en '+'
	 * </p>
	 * <li>
	 * <p>
	 * Tous les autres caractères ASCII sont remplacés par le code '%xx' avec
	 * 'xx' le code ASCII.
	 * </p>
	 * <li>
	 * <p>
	 * Tous caractères non-ASCII sont encodés en deux étapes : D'abord ils sont
	 * convertis en une séquence de 2 ou 3 bytes (algorithme UTF-8), ensuite
	 * chaque byte est encodé au format '%xx'.
	 * </ul>
	 * Code récupéré sur le site :
	 * http://www.w3.org/International/O-URL-code.html Javadoc traduite par mes
	 * soins (RAG).
	 * 
	 * @param s
	 *            chaine à encoder. Peut être null;
	 * @return la chaine encodée.
	 */
	public static String encodeUrl(String s) {
		if ((s == null) || "".equals(s)) {
			return s;
		}

		StringBuffer sbuf = new StringBuffer(s.length());
		int len = s.length();
		for (int i = 0; i < len; i++) {
			int ch = s.charAt(i);
			if ('A' <= ch && ch <= 'Z') { // 'A'..'Z'
				sbuf.append((char) ch);
			} else if ('a' <= ch && ch <= 'z') { // 'a'..'z'
				sbuf.append((char) ch);
			} else if ('0' <= ch && ch <= '9') { // '0'..'9'
				sbuf.append((char) ch);
			} else if ('?' == ch || '&' == ch || '=' == ch) {
				sbuf.append((char) ch);
			} else if (ch == ' ') { // space
				sbuf.append('+');
			} else if (ch == '-'
					|| ch == '_' // unreserved
					|| ch == '.' || ch == '!' || ch == '~' || ch == '*'
					|| ch == '\'' || ch == '(' || ch == ')') {
				sbuf.append((char) ch);
			} else if (ch <= 0x00ff) { // other ASCII
				// Note de RAG : Par rapport au code d'origine, j'ai étendu la
				// plage
				// ASCII de 0x7f à 0xff, car les anglo-saxons ne tiennent pas
				// compte des caractères accentués dans leur plage initiale.
				sbuf.append(hex[ch]);
			} else if (ch <= 0x07FF) { // non-ASCII <= 0x7FF
				sbuf.append(hex[0xc0 | (ch >> 6)]);
				sbuf.append(hex[0x80 | (ch & 0x3F)]);
			} else { // 0x7FF < ch <= 0xFFFF
				sbuf.append(hex[0xe0 | (ch >> 12)]);
				sbuf.append(hex[0x80 | ((ch >> 6) & 0x3F)]);
				sbuf.append(hex[0x80 | (ch & 0x3F)]);
			}
		}
		return sbuf.toString();
	}

	/**
	 * Convertit une chaine ayant des caracteres de code > 128 en caracteres
	 * dans le code ASCII standard. On essaie de trouver un equivalent le plus
	 * fidele possible (ex: e avec accent devient e sans accent).
	 * 
	 * @param data
	 *            La chaine a convertir. Peut être nulle ou vide.
	 * @return La chaine convertie.
	 */
	public static String ConvAccent(String data) {
		if (data == null || "".equals(data)) {
			return data;
		}
		char[] depart = data.toCharArray();
		StringBuffer arrivee = new StringBuffer(data.length());
		for (int i = 0; i < data.length(); i++) {
			char c = depart[i];

			// GHO 23/10/2003 pour empecher le cross scripting
			if (c == 60) // <
			{
				arrivee.append("&lt;");
			} else if (c == 62) // >
			{
				arrivee.append("&gt;");
			} else if (c <= 127) {
				arrivee.append(c);
			} else if (c > 127 && c <= 255) {
				arrivee.append(tableConvAccent[c - 128]);
			} else {
				arrivee.append(UCH);
			}
		}
		return (arrivee.toString());
	}

	/**
	 * Convertit une chaine ayant des caracteres de code > 128 en caracteres
	 * dans le code ASCII standard. On essaie de trouver un equivalent le plus
	 * fidele possible (ex: e avec accent devient e sans accent). Ici, on ne
	 * convertit pas à > et <.
	 * 
	 * @param data
	 *            La chaine a convertir. Peut être nulle ou vide.
	 * @return La chaine convertie.
	 */
	public static String ConvAccentHTML(String data) {
		if (data == null || "".equals(data)) {
			return data;
		}
		char[] depart = data.toCharArray();
		StringBuffer arrivee = new StringBuffer(data.length());
		for (int i = 0; i < data.length(); i++) {
			char c = depart[i];

			if (c <= 127) {
				arrivee.append(c);
			} else if (c > 127 && c <= 255) {
				arrivee.append(tableConvAccent[c - 128]);
			} else {
				arrivee.append(UCH);
			}
		}
		return (arrivee.toString());
	}

	
	public static String quoteToUnicode(String s) {
	  if(s == null) s = "" ;
	  return s.replaceAll("\"", "/u0022" ) ;
	}
	
	public static String unicodeToQuote(String s) {
	  if(s == null) s = "" ;
      return s.replaceAll("/u0022","\"") ;
    }
	
	public static String convertEscapeQuote(String s) {
		  if(s == null) s = "" ;
	      return s.replace("\'","\\'");
	    }
	
	public static String convertEscapeDoubleQuote(String s) {
		  if(s == null) s = "" ;
		  s = s.replace("'","&lsquo;");
		  s = s.replace("\"","&quot;");
		  s = s.replace("&","&amp;");
		  return s;
	    }
	
	/**
	 * Convertit une chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code> en date de classe
	 * <code>java.sql.Timestamp</code>.
	 * 
	 * @param date
	 *            La chaine a convertir. Peut etre nulle.
	 * @return La date correspondante. Nulle si le parametre est null
	 */
	public static Timestamp stringToTimestamp(String date) {
		// Modif FRO : ajout de synchronized
		Date myDate = null;
		if (date == null || "".equals(date))
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATE_FORMAT;
			formatter.setLenient(false);
			ParsePosition pos = new ParsePosition(0);
			myDate = formatter.parse(date, pos);
		}
		if (myDate == null) {
			return null;
		} else {
			return new Timestamp(myDate.getTime());
		}
	}

	/**
	 * Convertit une chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code> en date de classe
	 * <code>java.util.Date</code>.
	 * 
	 * @param date
	 *            La chaine a convertir. Peut etre nulle.
	 * @return La date correspondante. Nulle si le parametre est null
	 */

	public static Date stringToDate(String date) {
		// Modif FRO : ajout de synchronized
		Date myDate = null;
		if (date == null || "".equals(date))
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATE_FORMAT;
			formatter.setLenient(false);
			ParsePosition pos = new ParsePosition(0);
			myDate = formatter.parse(date, pos);
		}
		return myDate;
	}
	
	/**
	 * Convertit une chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code> en date de classe
	 * <code>java.util.Date</code>.
	 * 
	 * @param date
	 *            La chaine a convertir. Peut etre nulle.
	 * @return La date correspondante. Nulle si le parametre est null
	 */

	public static Date stringToDateTime(String date) {
		// Modif FRO : ajout de synchronized
		Date myDate = null;
		if (date == null || "".equals(date))
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATEHOUR_FORMAT;
			formatter.setLenient(false);
			ParsePosition pos = new ParsePosition(0);
			myDate = formatter.parse(date, pos);
		}
		return myDate;
	}
	
	public static Date stringToDateTime(String date, String formaDate) {
		// Modif FRO : ajout de synchronized
		Date myDate = null;
		if (date == null || "".equals(date))
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = new SimpleDateFormat(formaDate);
			formatter.setLenient(false);
			ParsePosition pos = new ParsePosition(0);
			myDate = formatter.parse(date, pos);
		}
		return myDate;
	}

	public static Timestamp stringToTimestampHour(String date) {
		// Modif FRO : ajout de synchronized
		Date myDate = null;
		if (date == null || "".equals(date))
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATEHOUR_FORMAT;
			formatter.setLenient(false);
			ParsePosition pos = new ParsePosition(0);
			myDate = formatter.parse(date, pos);
		}
		if (myDate == null) {
			return null;
		} else {
			return new Timestamp(myDate.getTime());
		}
	}

	/**
	 * Convertit une date en chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code>.
	 * 
	 * @param date
	 *            La date a convertir.
	 * @return La chaine de caracteres correspondante.
	 */
	public static String formatDate(Timestamp date) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATE_FORMAT;
			rtn = formatter.format(date);
		}
		return rtn;
	}

	/**
	 * Convertit une date en chaine au format passé par la chaine 'format'.
	 * 
	 * @param date
	 *            La date a convertir.
	 * @return La chaine de caracteres correspondante.
	 */
	public static String formatDate(Timestamp date, String format) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format,
					Locale.FRANCE);
			rtn = dateFormat.format(date);
		}
		return rtn;
	}

	/**
	 * Calcule la date correspondant a la date passee en parametre plus une
	 * annee et la convertit en chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code>.
	 * 
	 * @param date
	 *            La date a incrementer
	 * @return La chaine de caractere correspondant a (date + 1 an)
	 */
	public static String getNextYear(Timestamp date) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			GregorianCalendar cal = new GregorianCalendar();

			cal.setTime(date);
			cal.roll(Calendar.YEAR, true);
			SimpleDateFormat formatter = Constantes.DATE_FORMAT;
			rtn = formatter.format(cal.getTime());
		}
		return rtn;
	}

	/**
	 * Calcule la date correspondant a la date passee en parametre plus une
	 * annee et la convertit en chaine au format
	 * <code> Constantes.DATE_FORMAT_STRING</code>.
	 * 
	 * @param date
	 *            La date a incrementer sous forme de chaine au format
	 *            <code> Constantes.DATE_FORMAT_STRING</code>
	 * @return La chaine de caractere correspondant a (date + 1 an)
	 */
	public static String getNextYear(String date) {
		return getNextYear(stringToTimestamp(date));
	}

	/**
	 * Conversion format Timestamp en string heure, minute (heure sur 24 h)
	 */
	public static String formatHeureFromTimestamp(Timestamp date) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.HOUR_FORMAT;
			rtn = formatter.format(date);

		}
		return rtn;
	}

	/**
	 * Convertit une date en chaine au format
	 * <code> Constantes.DATEHEURE_FORMAT_STRING</code>.
	 * 
	 * @param date
	 *            La date a convertir.
	 * @return La chaine de caracteres correspondante.
	 */
	public static String formatDateHeureFromTimestamp(Timestamp date) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat formatter = Constantes.DATEHOUR_FORMAT;
			rtn = formatter.format(date);
		}
		return rtn;
	}

	/**
	 * Methode de concatenation du champ datedeb et hrdeb afin de le transformer
	 * en Timestamp (le valueOf d'un Timestamp est de type yyyy-mm-dd
	 * hh:mm:ss.fffffffff)
	 * 
	 * @param date
	 *            La date.
	 * @param time
	 *            L'heure.
	 * @return Le TimeStamp correspondant.
	 */
	public static String concatDateTime(String date, String time) {
		if (date != null && time != null && !"".equals(time)) {
			String str = date.substring(6) + "-" + date.substring(3, 5) + "-"
					+ date.substring(0, 2);
			return str + " "
					+ (time.indexOf(':') == -1 ? time.concat(":00") : time)
					+ ":00.0";
		} else if (date != null) {
			String str = date.substring(6) + "-" + date.substring(3, 5) + "-"
					+ date.substring(0, 2);
			return str + " " + "00:00:00.0";
		}
		return "";

	}

	public static String integerToString(Integer nombre) {
		String result = "";
		if (nombre != null) {
			result = nombre.toString();
		}
		return result;
	}

	public static String longToString(Long nombre) {

		String result = "";
		if (nombre != null) {
			result = nombre.toString();
		}

		return result;
	}

	public static int stringToInt(String nombre) {
		int retour = 0;
		if ((nombre == null) || (nombre.equals(""))) {
			retour = 0;
		} else {
			try {
				retour = Integer.parseInt(nombre);
			} catch (NumberFormatException e) {
				simpleLog.println(nombre + "is not a valid integer");
			}
		}
		return retour;
	}

	public static Integer stringToInteger(String nombre) {
		Integer retour = null;

		if ((nombre == null) || (nombre.equals(""))) {
			retour = null;
		} else {
			try {
				retour = new Integer(nombre);
			} catch (NumberFormatException e) {
				//simpleLog.println(nombre + " n'est pas une bonne valeur");
				//e.printStackTrace();
			}
		}

		return retour;
	}

	public static Long stringToLong(String nombre) {
		Long retour = null;

		if ((nombre == null) || (nombre.equals(""))) {
			retour = null;
		} else {
			try {
				retour = Long.valueOf(nombre);
			} catch (NumberFormatException e) {
				simpleLog.println(nombre + "n'est pas une bonne valeur");
				e.printStackTrace();
			}
		}

		return retour;
	}

	/**
	 * Ramène la valeur d'une colonne d'un Resultset. Si la colonne est nulle
	 * alors la méthode retourne une chaine vide.
	 * 
	 * @param rs
	 *            L'objet Resultset qui est consulté.
	 * @param col
	 *            Nom de la colonne à consulter.
	 */
	public static String getColFromRS(ResultSet rs, String col, int type)
			throws Exception {
		String valS = "";
		Object val = rs.getObject(col);
		if (!rs.wasNull()) {
			if (type == Types.TIMESTAMP) {
				valS = formatDate((Timestamp) val);
				if (valS == null)
					valS = "";
			} else {
				valS = val.toString();
			}
		}

		return valS;
	}

	/**
	 * Retourne une chaine contenant l'entier formaté sur n digit Ex:
	 * nDigitNumber(new Integer(7), 3) = "007"
	 * 
	 * @param num
	 *            : nombre à formater sur nbDigit digits
	 * @param nbDigit
	 *            : nombre de digit
	 */
	public static String nDigitNumber(Integer num, int nbDigit) {
		return nDigitNumber("" + num.intValue(), nbDigit);
	}

	/**
	 * Retourne une chaine contenant l'entier formaté sur n digit Ex:
	 * nDigitNumber(7, 3) = "007"
	 * 
	 * @param num
	 *            : nombre à formater sur nbDigit digits
	 * @param nbDigit
	 *            : nombre de digit
	 */
	public static String nDigitNumber(int num, int nbDigit) {
		return nDigitNumber("" + num, nbDigit);
	}

	/**
	 * Retourne une chaine contenant l'entier formaté sur n digit Ex:
	 * nDigitNumber("7", 3) = "007"
	 * 
	 * @param num
	 *            : nombre à formater sur nbDigit digits
	 * @param nbDigit
	 *            : nombre de digit
	 */
	public static String nDigitNumber(String num, int nbDigit) {
		if (nbDigit <= num.length()) {
			return num;
		}

		StringBuffer buff = new StringBuffer(nbDigit);
		// la technique suivante evite une boucle
		char arr[] = new char[nbDigit - num.length()];
		Arrays.fill(arr, '0');

		buff.append(arr);
		buff.append(num);

		return buff.toString();
	}

	/**
	 * Remplit une chaine a droite avec un caractere donne de facon que la
	 * chaine resultante soit d'une certaine longueur.
	 * 
	 * @param str
	 *            La chaine a completer
	 * @param n
	 *            La longueur de la chaine resultante
	 * @param c
	 *            Le caractere de remplissage
	 * @return La chaine completee ou tronquee selon la valeur de n
	 */
	public static String rPad(String str, int n, char c) {
		if (str == null) {
			str = "";
		}
		if (n == 0) // RAG : Traitement du cas n=0
		{
			return "";
		}

		if (n < str.length()) {
			return str.substring(0, n);
		}

		StringBuffer buff = new StringBuffer(n);

		char arr[] = new char[n - str.length()];
		Arrays.fill(arr, c);

		buff.append(str);
		buff.append(arr);

		return buff.toString();

	}

	/**
	 * Remplit une chaine a droite avec un blanc de facon que la chaine
	 * resultante soit d'une certaine longueur.
	 * 
	 * @param str
	 *            La chaine a completer
	 * @param n
	 *            La longueur de la chaine resultante
	 * @return La chaine completee par des blancs ou tronquee selon la valeur de
	 *         n
	 */
	public static String rPad(String str, int n) {
		return rPad(str, n, ' ');
	}

	/**
	 * Remplit une chaine a gauche avec un caractere donne de facon que la
	 * chaine resultante soit d'une certaine longueur.
	 * 
	 * @param str
	 *            La chaine a completer
	 * @param n
	 *            La longueur de la chaine resultante
	 * @param c
	 *            Le caractere de remplissage
	 * @return La chaine completee ou tronquee selon la valeur de n
	 */
	public static String lPad(String str, int n, char c) {
		if (str == null) {
			str = "";
		}

		if (n == 0) // RAG : Traitement du cas n=0
		{
			return "";
		}

		if (n < str.length()) {
			return str.substring(0, n);
		}

		StringBuffer buff = new StringBuffer(n);

		char arr[] = new char[n - str.length()];
		Arrays.fill(arr, c);

		buff.append(arr);
		buff.append(str);

		return buff.toString();

	}

	/**
	 * Remplit une chaine a gauche avec un blanc de facon que la chaine
	 * resultante soit d'une certaine longueur.
	 * 
	 * @param str
	 *            La chaine a completer
	 * @param n
	 *            La longueur de la chaine resultante
	 * @return La chaine completee par des blancs ou tronquee selon la valeur de
	 *         n
	 */
	public static String lPad(String str, int n) {
		return rPad(str, n, ' ');
	}

	/**
	 * Verifie si une chaine donne represente une date au sens de
	 * Conversion.stringToTimestamp
	 * 
	 * @param date
	 *            La chaine a tester
	 * @return true ssi la chaine est une date.
	 */
	public static boolean verifDate(String date) {
		// Si la date est renseignée
		// On regarde si elle est correcte, sinon on retourne false
		boolean retour = true;
		if (date != null && !date.equals("")) {
			if (Conversion.stringToTimestamp(date) == null) {
				retour = false;
			}
		}
		return retour;
	}

	/**
	 * Renvoie le chaine avec le premier caractère en capitale.
	 * 
	 * @str chaine à modifier.
	 * @return la chaine 'str' avec le premier caractère en capitale.
	 */
	public static String capitalise(String str) {
		String capital;

		capital = "";

		if (str != null) {
			if (str.length() > 0) {
				capital = str.substring(0, 1).toUpperCase();
				if (str.length() > 1) {
					capital = capital.concat(str.substring(1));
				}
			}
		}
		return capital.toString();
	}

	/* Test de format de date */
	public static void main(String argv[]) {
		try {
			System.out.println(argv[0]);
			System.out.println(ConvAccent(argv[0]));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Verifie que le format de l'heure est de type HH:MM
	 * 
	 * @return true ssi le format de l'heure est correcte.
	 */
	public static boolean verifFormatHeure(String heure) {
		boolean retour = true;
		String heureDeb1 = null;
		String heureDeb2 = null;
		String sepDeb = null;
		String minDeb1 = null;
		String minDeb2 = null;

		try {
			heureDeb1 = heure.substring(0, 1);
			heureDeb2 = heure.substring(1, 2);
			sepDeb = heure.substring(2, 3);
			minDeb1 = heure.substring(3, 4);
			minDeb2 = heure.substring(4, 5);

			if ((!":".equals(sepDeb))
					|| (Integer.parseInt(minDeb1) < 0 || Integer
							.parseInt(minDeb1) > 5)
					|| (Integer.parseInt(minDeb2) < 0 || Integer
							.parseInt(minDeb2) > 9)
					|| ("0".equals(heureDeb1) && (Integer.parseInt(heureDeb2) < 0 || Integer
							.parseInt(heureDeb2) > 9))
					|| ("1".equals(heureDeb1) && (Integer.parseInt(heureDeb2) < 0 || Integer
							.parseInt(heureDeb2) > 9))
					|| ("2".equals(heureDeb1) && (Integer.parseInt(heureDeb2) < 0 || Integer
							.parseInt(heureDeb2) > 4))) {
				retour = false;
			}
		} catch (Exception e) {
			retour = false;
		}
		return retour;
	}

	/**
	 * avec import import java.security.*; permet de crypter une chaine de
	 * caractère Encode la chaine passé en paramètre avec lalgorithme
	 * authentification
	 * 
	 * @param key
	 *            : la chaine à authentifier
	 * @return la valeur (string) hexadécimale sur 32 bits
	 * 
	 * */
	public static String authentification(String chaine) {
		String key = "valmyr" + chaine + "guicuc";
		byte[] uniqueKey = key.getBytes();
		byte[] hash = null;
		try {
			// on récupère un objet qui permettra de crypter la chaine
			hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
		} catch (NoSuchAlgorithmException e) {
			throw new Error("no MD5 support in this VM");
		}
		StringBuffer hashString = new StringBuffer();
		for (int i = 0; i < hash.length; ++i) {
			String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append(0);
				hashString.append(hex.charAt(hex.length() - 1));
			} else {
				hashString.append(hex.substring(hex.length() - 2));
			}
		}
		return hashString.toString();
	}

	public static boolean isInteger(String string) {
		try {
			Integer.valueOf(string);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static boolean isDate(String date) {
		// Si la date est renseignée
		// On regarde si elle est correcte, sinon on retourne false
		boolean retour = true;
		if (date != null && !date.equals("")) {
			if (Conversion.stringToTimestamp(date) == null
					|| date.substring(6).length() < 4
					|| !isInteger(date.substring(6))) {
				retour = false;
			}
		}
		return retour;
	}

	/**
	 * parse une date en un tableau de String, chaque élément comportant
	 * respectivement le jour, le mois et l'année de l'année
	 * 
	 * @param date
	 *            Date
	 * @return String[]
	 */
	public static final String[] parseDate(final Date date) {
		String dateToString = formatDate(date, Constantes.DATE_FORMAT_STRING);
		String[] result;
		result = dateToString.split("/");

		return result;
	}

	public static String formatDate(Date date, String format) {
		// Modif FRO : ajout de synchronized
		String rtn = null;
		if (date == null)
			return null;
		synchronized (Conversion.LOCK_CONVERT_DATE) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format,
					Locale.FRANCE);
			rtn = dateFormat.format(date);
		}
		return rtn;
	}

	public static int getYears(Date date) {
		Calendar curr = Calendar.getInstance();
		Calendar birth = Calendar.getInstance();
		birth.setTime(date);
		int yeardiff = curr.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
		curr.add(Calendar.YEAR, -yeardiff);
		if (birth.after(curr)) {
			yeardiff = yeardiff - 1;
		}
		return yeardiff;
	}

	public static boolean dateFormatIsValid(String dateString) {
		if (dateString == null || "".equals(dateString))
			return false;

		switch (dateString.length()) {
		case 4:
			try {
				Integer.parseInt(dateString);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		case 7:
			try {
				Integer.parseInt(dateString.substring(0, 2));
				Integer.parseInt(dateString.substring(3));
				return "/".equals(dateString.substring(2, 3));
			} catch (NumberFormatException e) {
				return false;
			}
		case 10:
			try {
				Integer.parseInt(dateString.substring(0, 2));
				Integer.parseInt(dateString.substring(3, 5));
				Integer.parseInt(dateString.substring(6));
				return "/".equals(dateString.substring(2, 3))
						&& "/".equals(dateString.substring(5, 6));
			} catch (NumberFormatException e) {
				return false;
			}
		default:
			return false;
		}
	}

	public static String getHash(File file) {

		String sessionid = null;
		try {
			byte[] defaultBytes = getFileToByte(file);
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
			//System.out.println("sessionid " + sessionid + " md5 version is "+ hexString.toString());
			sessionid = hexString + "";
		} catch (Exception nsae) {

		}

		return sessionid;

	}

	private static byte[] getFileToByte(File file) {
		byte[] ab = null;
		try {
			FileInputStream fi = new FileInputStream(file);
			FileChannel ch = fi.getChannel();
			ByteBuffer bf = ByteBuffer.allocate((int) file.length());
			ch.read(bf);
			bf.flip();
			ab = new byte[(int) file.length()];
			bf.get(ab);
			bf.clear();
			ch.close();
			fi.close();
		} catch (Exception e) {
			e.printStackTrace();
			ab = new byte[0];
		}
		return ab;
	}
	

  
  public static String convHtmlToUtf8(String s) {
	  if(s==null || s.equals("")) return "" ;
	  String res = s.replaceAll("&#8211;","–")
	                .replaceAll("&#8212;","—")
	                .replaceAll("&#8216;","‘")
	                .replaceAll("&#8217;","’")
	                .replaceAll("&#8218;","‚")
	                .replaceAll("&#8220;","“")
	                .replaceAll("&#8221;","”")
	                .replaceAll("&#8222;","„")
	                .replaceAll("&#8224;","†")
	                .replaceAll("&#8225;","‡")
	                .replaceAll("&#8226;","•")
	                .replaceAll("&#8230;","…")
	                .replaceAll("&#8240;","‰")
	                .replaceAll("&#8364;","€")
	                .replaceAll("&#8482;","™") ;
	  return res ;
  }
  
  
  public static String implode(String separator, String[] data) {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < data.length - 1; i++) {
	    //data.length - 1 => to not add separator at the end
	        if (!data[i].matches(" *")) {//empty string are ""; " "; "  "; and so on
	            sb.append(data[i]);
	            sb.append(separator);
	        }
	    }
	    sb.append(data[data.length - 1].trim());
	    return sb.toString();
	}

  //evo 221131
  public static String getNumberInRomanNumber(int nombre) {
	    LinkedHashMap<String,Integer> nombreRomains = new LinkedHashMap<String,Integer>();
	    nombreRomains.put("M", 1000);
	    nombreRomains.put("CM", 900);
	    nombreRomains.put("D", 500);
	    nombreRomains.put("CD", 400);
	    nombreRomains.put("C", 100);
	    nombreRomains.put("XC", 90);
	    nombreRomains.put("L", 50);
	    nombreRomains.put("XL", 40);
	    nombreRomains.put("X", 10);
	    nombreRomains.put("IX", 9);
	    nombreRomains.put("V", 5);
	    nombreRomains.put("IV", 4);
	    nombreRomains.put("I", 1);
	    String res = "";
	    for(Map.Entry<String,Integer> entry : nombreRomains.entrySet()){
	      int matches = nombre/entry.getValue();
	      res += repeat(entry.getKey(), matches);
	      nombre = nombre % entry.getValue();
	    }
	    return res;
	  }
	  public static String repeat(String s, int n) {
	    if(s == null) {
	        return null;
	    }
	    final StringBuilder sb = new StringBuilder();
	    for(int i = 0; i < n; i++) {
	        sb.append(s);
	    }
	    return sb.toString();
	  }
	  
	  public static boolean stringToBoolean(String strBoolean) {
		  if(strBoolean != null && strBoolean.equals("O")){
			  return true;
		  }
		  return false;
	  }

   //evo 221131 - fin
}
