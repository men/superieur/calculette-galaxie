package fr.gouv.education.antares.outils.sql;

/**
 * Titre :
 * Description :
 * Copyright :    Copyright (c) 2002
 * Société :
 * @author
 * @version 1.0
 */


import weblogic.logging.NonCatalogLogger;

public class AntaresLogger
{

    private static final NonCatalogLogger logger=new NonCatalogLogger("Antares");

    private final static int debug;

    static
    {
        String debug_temp=System.getProperty("antares.debug","99");
        if("true".equals(debug_temp))
        {
           debug=0;
        }
        else if ("false".equals(debug_temp))
        {
           debug=99;
        }
        else
        {
            int temp;
            try
            {
                temp=Integer.parseInt(System.getProperty("antares.debug","99"));
            }
            catch(Exception e)
            {
                temp=99;
            }
            debug=temp;
        }
	System.out.println("AntaresLogger  debug_temp=" + debug_temp +
		    " debug=" + debug);
    }

    /**
     * Journalise une erreure.
     * @param msg Le message d'erreur
     */
    public static void error(String msg)
    {
        logger.error(msg);
    }

    /**
     * Journalise une erreur en donnant la pile d'execution d'une exception.
     * @param msg Le message d'erreur
     * @param t L'exception (Throwable) à l'origine de l'erreur.
     */
    public static void error(String msg,Throwable t)
    {
        logger.error(msg,t);
    }

    /**
     * Permet de journaliser un message de debug. Comme debug ci-dessous
     * mais dans aldebug-date 
     * @param msg Le message de debug
     */
    public static void aldbg(String msg) {
        if (debug>=0 && debug != 99)	// eo migration
        {
        	System.out.println(msg);
        }
    }

    /**
     * Permet de journaliser un message de debug. Celui-ci apparait dans la sortie
     * standard ssi l'option "debug to stdout" a ete coche. En outre, la propriete systeme
     * antares.debug doit etre positionnee a true.
     * @param msg Le message de debug
     */
    public static void debug(String msg)
    {
        if (debug>=0 && debug != 99)
        {
           //	System.out.println(msg);
            logger.debug(msg);
        }
    }

    /**
     * Permet de journaliser un message de debug. Celui-ci apparait dans la sortie
     * standard ssi l'option "debug to stdout" a ete coche. En outre, la propriete systeme
     * antares.debug doit etre positionnee a true.
     * @param msg Le message de debug
     */
    public static void debug(String msg,int niveau)
    {
        if(niveau>=debug)
        {
            logger.debug(msg);
        }
    }

    /**
     * Permet de journaliser un message d'info.
     * @param msg Le message d'info
     */
    public static void info(String msg)
    {
        logger.info(msg);
    }

    /**
     * Permet de journaliser un message de niveau warning.
     * @param msg Le message d'avertissement a journaliser
     */
    public static void warning(String msg)
    {
        logger.warning(msg);
    }

}
