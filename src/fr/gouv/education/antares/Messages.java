package fr.gouv.education.antares;

/**
 * Titre : Messages
 * Description : Messages de l'application
 * Copyright :    Copyright (c) 2001
 * Soci&eacute;t&eacute; : Airial
 * @author Christian Schi&eacute;l&eacute;
 * @version 1.0
 */

public interface Messages
{
    public static final String M001 = "Erreur: saisie obligatoire";
    public static final String M002 = "Erreur: mail incorrect";
    public static final String M003 = "Erreur: le pays et le code postal sont incoh&eacute;rents";
    public static final String M004 = "Erreur: date non valide";
    public static final String M005 = "Erreur : age inf&eacute;rieur &agrave; 16 ans ou date de naissance ant&eacute;rieure à 1900.";
    public static final String M006 = "Erreur: saisie incorrecte";
    public static final String M007 = "Erreur: confirmation incorrecte";
    public static final String M008 = "Erreur: numen incorrect";
    public static final String M009 = "Erreur: le nom marital ne peut &ecirc;tre saisi que si la civilit&eacute; est Mme";
    public static final String M010 = "Erreur: Num&eacute;ro et/ou mot de passe incorrect(s)";
    public static final String M011 = "Erreur: heure non valide";
    public static final String M012 = "Erreur: suppression impossible, le membre est d&eacute;j&agrave; d&eacute;sign&eacute; pour une candidature";
    public static final String M013 = "Erreur: le pays et le code postal de l'adresse sont incoh&eacute;rents";
    public static final String M014 = "Erreur: s&eacute;ance incorrecte";
    public static final String M015 = "Erreur: Num&eacute;ro de candidat incorrect!";
    public static final String M016 = "Erreur: Num&eacute;ro de qualification incorrect!";
    public static final String M017 = "La recherche n'a ramen&eacute; aucun r&eacute;sultat !";
    public static final String M018 = "Emploi inexistant pour l'&eacute;tablissement";
    public static final String M019 = "Un des 2 emails du responsable doit &ecirc;tre renseign&eacute;";
    public static final String M020 = "Erreur: s&eacute;lection d'une section obligatoire";
    public static final String M021 = "Une erreur est survenue, veuillez recommencer ou bien pr&eacute;venir l'administrateur";
    public static final String M022 = "Erreur: la selection d'un type de pv est obligatoire pour continuer";
    public static final String M023 = "Erreur: la selection d'un emploi est obligatoire pour continuer";
    public static final String M024 = "Le num&eacute;ro de candidat s&eacute;lectionn&eacute; ne poss&egrave;de aucun num&eacute;ro de qualification";
    public static final String M025 = "La confirmation de l'emploi se s'est pas correctement termin&eacute;e!";
    public static final String M026 = "Erreur: Identifiant et/ou code secret incorrect(s)";
    public static final String M027 = "Des criteres saisis sont incompatibles avec la famille de l edition selectionnee";
    public static final String M028 = "Erreur: la sous-section choise est incompatible avec la section selectionnee";
    public static final String M029 = "Erreur: une famille doit etre obligatoirement selectionnee pour continuer";
    public static final String M030 = "Erreur: le critere section est obligatoire";
    public static final String M031 = "Erreur: incompatilite de saisie dans les complements";
    public static final String M032 = "ATTENTION, la saisie de ce mode de recrutement va d&eacute;lier cet emploi du groupe.";
    public static final String M033 = "ATTENTION, Vous allez manipuler un groupe d'emplois.";
    public static final String M034 = "L'ordre de mission s&eacute;lectionn&eacute n'existe pas.";
    public static final String M035 = "Identification erron&eacute;e. Veuillez contacter votre correspondant au minist&egrave;re.";
    public static final String M036 = "Le login saisi existe d&eacute;j&agrave;";
    public static final String M037 = "Le couple corps/section existe d&eacute;j&agrave;";
    public static final String M038 = "Erreur : Code inconnu";
    public static final String M039 = "Votre Candidature n'est recevable que si votre situation rel&egrave;ve des paragraphes 2,3,4 ou 5 de l'article 44 du d&eacute;cret n°84-431 du 6 juin 1984 modifi&eacute;";
    public static final String M040 = "Erreur: Caract&egrave;re invalide ";
    public static final String M041 = "La taille du titre de la th&egrave;se est trop grande. Veuillez recommencer la saisie.";
    public static final String M042 = "Erreur: Format de la date non valide";
    public static final String M043 = "Erreur: Phase et Session incompatibles";
    public static final String M044 = "Erreur: Champs Titre et Directeur de Th&egrave;se obligatoires pour les candidatures non &agrave; titre professionnel ";
    public static final String M045 = "Erreur: Pour l'&eacute;dition Etiquette CNU, on ne peut cocher &agrave; la fois Adresse administrative et Adresse personnelle";
    public static final String M046 = "Pour des raisons de s&eacute;curit&eacute;, vous devez resaisir le mot de passe";
    public static final String M047 = "Erreur : Si la qualit&eacute; n'est pas remplie, le coll&egrave;ge et un seul des crit&egrave;res Groupe/Section/Sous-Section sont obligatoires";
    public static final String M048 = "Erreur : Ne choisissez qu'un seul des crit&egrave;res Groupe/Section/Sous-Section";
    public static final String M049 = "Erreur : Section Invalide";
    public static final String M050 = "Vous devez saisir le NUMEN d'un candidat dont le corps est le m&ecirc;me que celui de l'emploi.";
    public static final String M051 = "Le numen saisi n'existe pas.";
    public static final String M052 = "Saisie erronée - informations non enregistrées.";
    public static final String M053 = "Erreur: l'heure doit &ecirc;tre comprise entre 6 et 20";
    public static final String M054 = "Erreur: les minutes doivent &ecirc;tre comprises entre 00 et 59";
    public static final String M055 = "Erreur: la premi&egrave;re ligne de la zone adresse et la ville sont obligatoires";
    public static final String M056 = "Erreur: Champs Titre de Th&egrave;se obligatoire pour les candidatures non &agrave; titre professionnel ";
    public static final String M057 = "Erreur: Champs Directeur de Th&egrave;se obligatoire pour les candidatures non &agrave; titre professionnel ";
    public static final String M058 = "Erreur: Pour cette nature de r&eacute;sultat la date du r&eacute;sultat est obligatoire";
    public static final String M059 = "Erreur: Si la date du r&eacute;sultat est saisie la nature est obligatoire";
    public static final String M060 = "Erreur: La date du r&eacute;sultat doit &ecirc;tre inf&eacute;rieur ou &eacute;gale &agrave; la date du jour";
    public static final String M061 = "Vous devez saisir au moins une s&eacute;ance.";
    public static final String M062 = "Pour la 1&egrave;re s&eacute;ance, la date de fin doit &ecirc;tre sup&eacute;rieure ou &eacute;gale &agrave; la date de d&eacute;but.";
    public static final String M063 = "Pour la 2&egrave;me s&eacute;ance, la date de fin doit &ecirc;tre sup&eacute;rieure ou &eacute;gale &agrave; la date de d&eacute;but.";
    public static final String M064 = "La date de fin de la 1&egrave;re s&eacute;ance doit &ecirc;tre inf&eacute;rieure ou &eacute;gale &agrave; la date de d&eacute;but de la 2&egrave;me s&eacute;ance.";
    public static final String M065 = "Toute date saisie doit &ecirc;tre sup&eacute;rieure ou &eacute;gale &agrave; la date du jour.";
    public static final String M066 = "Vous devez remplir tous les champs de la 1&egrave;re s&eacute;ance.";
    public static final String M067 = "Vous devez remplir tous les champs de la 2&egrave;me s&eacute;ance.";
    public static final String M068 = "Erreur : tri obligatoire";
    public static final String M069 = "Vous devez remplir tous les champs d'une journ&eacute;e de convocation";
    public static final String M070 = "La date de la 2&egrave;me journ&eacute;e doit &ecirc;tre post&eacute;rieure &agrave; celle de la 1&egrave;re journ&eacute;e.";
    public static final String M071 = "Vous ne pouvez pas saisir un m&ecirc;me Etablissement-Emploi pour diff&eacute;rentes candidatures";
    public static final String M072 = "Erreur: La base doit &ecirc;tre sauvegard&eacute;e avant de pouvoir lancer ce traitement";
    public static final String M073 = "Erreur : Le couple login-mot de passe existe d&eacute;j&agrave;.";
	// R21 - MG 09/05/2003 - rajout d un message pour la saisie de la date
    //Q142 - Versailles - 09/09/2003 - modification du message
    public static final String M074 = "Erreur: Date de Soutenance de la Th&egrave;se obligatoire pour les candidatures non &agrave; titre professionnel ";
	//Q29 - Versailles 21/07/2003 - prise en compte d'une 3&egrave;me s&eacute;ance de r&eacute;union pour les ordres de mission
    public static final String M075 = "Pour la 3&egrave;me s&eacute;ance, la date de fin doit &ecirc;tre sup&eacute;rieure ou &eacute;gale &agrave; la date de d&eacute;but.";
    public static final String M076 = "La date de fin de la 2&egrave;me s&eacute;ance doit &ecirc;tre inf&eacute;rieure ou &eacute;gale &agrave; la date de d&eacute;but de la 3&egrave;me s&eacute;ance.";
    public static final String M077 = "Vous devez remplir tous les champs de la 3&egrave;me s&eacute;ance.";
	//Q56 - Versailles - 23/09/2003 - rajout message plus explicite qui remplace le message M055
    public static final String M080 = "Erreur: la premi&egrave;re ligne adresse et la ville sont obligatoires (et le code postal pour la France)";
    public static final String M081 = "Erreur: Champs Type de Dipl&ocirc;me obligatoire pour les candidatures non &agrave; titre professionnel ";
    //GALAXIE 03/09/2008 RLI
    public static final String M082 = "Aucun poste ne correspond aux crit&egrave;res que vous avez saisis";
    public static final String M083 = "Aucun poste ANTEE";
    public static final String M084 = "Aucun poste FIDIS";
    public static final String M085 = "Toutes les informations relatives  &agrave; l'offre de poste seront supprim&eacute;es. Confirmez-vous la suppression de cette offre de poste ?";
    public static final String M086 = "Autorisation de publication du poste impossible. Les champs suivants sont obligatoires :";
    public static final String M087 = "Re-publication interdite pour un poste en phase active de recrutement. Veuillez corriger la référence GALAXIE.";
    public static final String M088 = "En utilisant un numéro de poste existant, vous effectuez la re-publication de l'offre.";
    public static final String M089 = "Numéro de poste déjà utilisé pour un poste de la campagne et de la session courantes. Veuillez corriger le numéro de poste";
    public static final String M090 = "Saisie obligatoire du chemin complet du fichier contenant la fiche de poste &agrave; enregistrer sur le serveur.";
    public static final String M091 = "Le nom que vous avez affect&eacute; &agrave; la fiche de poste doit être de la forme 'FOP_numéro de l'établissement_numéro de poste.rtf'";
    public static final String M092 = "le fichier transmis est vide";
    public static final String M093 = "Erreur: couple corps-article non valide";
    public static final String M094 = "La date de publication doit être supérieure à la date du jour et inf&eacute;rieure ou &eacute;gale à la date d'ouverture des candidatures";
    public static final String M095 = "La date de publication doit être supérieure à la date du jour et inf&eacute;rieure ou égale &agrave; la date d'ouverture des candidatures";
    public static final String M096 = "Aucune candidature aux postes de la session synchronisée supprimée";
    public static final String M097 = "Aucune candidature aux postes au fil de l'eau supprimée";
    public static final String M097bis = "Aucune candidature aux postes appels à candidature ATER";
    public static final String M098 = "Aucune candidature supprimée ne correspond aux crit&egrave;res que vous avez saisis";
   
    //Evo Electra (Lot 7) : Controle de quotas de promotion CNU 	
    public static final String M099 ="Erreur: Le choix du type d'invalidation est obligatoire";
    public static final String M100 ="Votre invalidation a bien été enregistrée";
    public static final String M101 ="Votre validation a bien été enregistrée";
    
    //Evo Electra (Lot 8) : Saisie de proposition de promotion
    public static final String M102 = "Accès refusé: La saisie de masse n'est accessible qu'aux établissements de rattachement";
    public static final String M103 = "Opération refusée : vous avez dépassé le contingent de promotion prévu";
    
    public static final String M104 = "Opération refusée : il reste des dossiers non visés pour cette section";
    public static final String M105 = "Opération refusée : aucun dossier n'a encore été visé pour cette section";
    
    public static final String M106 ="Ce numen existe d&eacute;j&agrave;";
    public static final String M107 ="Erreur: couple article-section non valide";
    public static final String M109 = "Autorisation de publication de l'offre de poste refus&eacute;e car la date de publication est &eacute;chue. Pour pouvoir publier cette offre de poste, veuillez la modifier en repoussant la date de publication.";
    public static final String M110 = "Autorisation de publication de l'offre de poste refus&eacute;e car la date d'ouverture des candidatures est &eacute;chue. Vous &ecirc;tes invit&eacute; &agrave; publier cette offre de poste au fil de l'eau.";
    public static final String M111 = "Saisie obligatoire du chemin complet du fichier &agrave; enregistrer sur le serveur.";
    public static final String M112 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format zip : doit comporter l'extension .zip .";
    public static final String M113 = "Le fichier a bien &eacute;t&eacute; d&eacute;pos&eacute; pour mise &agrave; jour du portail.";
    public static final String M114 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre  sous la forme 'CV_numéro du vivier.pdf' .";
    public static final String M115 = "La notice biographique a bien &eacute;t&eacute; d&eacute;pos&eacute;e.";
    public static final String M116 = "Le fichier s&eacute;lectionn&eacute; est trop volumineux";
    public static final String M117 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format pdf.";
    public static final String M118 = "Le document que vous avez s&eacute;lectionn&eacute;e est vide.";
    public static final String M119 = "Vous ne pouvez pas avoir plus de 4 publications";
    public static final String M120 = "Le fichier s&eacute;lectionn&eacute; est trop volumineux, maximum 1 Mo";
    public static final String M121 = "Le fichier s&eacute;lectionn&eacute; est trop volumineux, maximum 2 Mo";
    public static final String M122=  "La partie r&eacute;dactionnelle a bien &eacute;t&eacute; d&eacute;pos&eacute;e.";
    public static final String M123 = "La notice biographique a bien &eacute;t&eacute; supprim&eacute;e.";
    public static final String M124=  "La partie r&eacute;dactionnelle a bien &eacute;t&eacute; supprim&eacute;e.";
    public static final String M125=  "Attention : une proposition de promotion est obligatoire pour continuer cette op&eacute;ration";
    public static final String M126=  "Erreur : Echec de t&eacute;l&eacute;chargement du document sélectionn&eacute; ";
    public static final String M127 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format PDF ou RTF.";
    public static final String M128 = "Opération refusée : Fichier non exploitable (Veuillez contacter la DGRH)";
    public static final String M129 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format PNG.";
    public static final String M130 = "Il est possible que vous ayez chargé deux fois le même fichier. Vérifier vos pièces jointes.";
    public static final String M131=  "La notice biographique est plus grande que la partie rédactionnelle, il peut s'agir d'une erreur. Vérifiez vos pièces jointes.";
    public static final String M132=  "Opération refusée : Fichier non exploitable";
    
    
    public static final String M133 = "Erreur: Si un champ est saisi, la saisie des autres champs (de la m&ecirc;me zone) devient obligatoire.";

    public static final String M134_1= "Identifiant inconnu, veuillez contacter votre correspondant à la DGRH";
    
    public static final String M134 = "veuillez contacter la DGRH A2-2 (tél: 01-55-55-62-44)";

    public static final String M135 = "Le fichier s&eacute;lectionn&eacute; est trop volumineux, maximum 5 Mo";
    
    // evo message PV ELARA
    public static final String M136 = "Op&eacute;ration refus&eacute;e : il reste des demandes non vis&eacute;es pour cette section";
    public static final String M137 = "Op&eacute;ration refus&eacute;e : aucune demande n'a encore &eacute;t&eacute; vis&eacute;e pour cette section";
    
    // CAMPAGNE
    public static final String M138 = "Erreur: La date de prise de fonction 1 doit &ecirc;tre inf&eacute;rieure &agrave; la date de prise de fonction 2";
    
    // MESSAGE ASTREE A PARTIR DE 200
    public static final String M200 = "Aucune candidature n'est enregistr&eacute;e.";
    public static final String M201 = "Pour cr&eacute;er une nouvelle candidature, cliquez sur le lien &quot;Nouvelle candidature&quot; dans le menu.";
    public static final String M202 = "Vous pouvez consulter, modifier voire supprimer chaque candidature en cliquant sur le lien \"D&eacute;tail\" de la candidature.";
    //MSU 1220 Evo début
    public static final String M203 = "Vous ne pouvez s&eacute;lectionner un n° de qualification qu'en cas de candidature au concours, au contrat BOE ou &agrave; la Proc&eacute;dure 46-3.";
    //MSU 1220 Evo fin
    public static final String M204 = "La s&eacute;lection de la qualification est obligatoire pour ce type de candidature.";
    public static final String M205 = "Le NUMEN de votre fiche d'identit&eacute; est invalide.";
    public static final String M205_bis = "Le NUMEN de votre fiche d'identit&eacute; correspond &agrave; un corps diff&eacute;rent de celui du poste pour lequel la mutation est sollicit&eacute;e.";
    public static final String M206 = "Pour ce type de candidature, le corps s&eacute;lectionn&eacute; ne correspond pas au corps de la qualification.";
    public static final String M207 = "Qualification en cours d'obtention : vous pouvez enregistrer une candidature au recrutement mais elle ne sera examin&eacute;e qu'apr&egrave;s obtention effective de votre qualification et à condition que celle-ci intervienne avant la date de clôture des candidatures pour le poste choisi.";
    public static final String M208 = "Aucun poste ne correspond &agrave; votre demande.";
    public static final String M209 = "La saisie du choix du poste est obligatoire.";
    public static final String M210 = "Attention, vous n'avez pas indiqu&eacute; de condition remplie.";
    public static final String M211 = "Erreur: saisie incorrecte. Veuillez entrer la date sous le format suivant : JJ/MM/AAAA.";
    public static final String M212 = "Erreur: saisie incorrecte. Veuillez entrer une date ant&eacute;rieure &agrave; celle indiqu&eacute;e ci apr&egrave;s.";
    public static final String M213 = "Votre dossier de candidature a bien &eacute;t&eacute; enregistr&eacute;";
    public static final String M214 = "Votre dossier de candidature a bien &eacute;t&eacute; modifi&eacute;";
    public static final String M215 = "Vous n'avez pas indiqu&eacute; de condition remplie.";
    public static final String M216 = "Confirmez-vous votre demande de modification ?";
    public static final String M217 = "La saisie du NUMEN est obligatoire.";
    public static final String M218 = "Le NUMEN saisi est incompatible avec au moins l'une de vos candidatures aux postes.";
    public static final String M219 = "Pas de candidature r&eacute;pondant aux crit&egrave;res de recherche demand&eacute;s.";
    public static final String M220 = "Cochez le poste pour lequel vous &ecirc;tes candidat puis cliquez sur le bouton &quot;Cr&eacute;er une candidature pour le poste&quot; en bas de page pour poursuivre la saisie de votre candidature.";
    public static final String M221 = "Aucun poste ne correspond &agrave; votre demande.";
    public static final String M222 = "Le cas &eacute;ch&eacute;ant, s&eacute;lectionnez les conditions que vous remplissez ou la situation particuli&egrave;re que vous souhaitez faire valoir, puis cliquez sur le bouton &quot;Cr&eacute;er la candidature&quot; en bas de page pour poursuivre la saisie de votre candidature. <br> " +
    								  "Si vous ne vous trouvez dans aucune des situations particuli&egrave;res propos&eacute;es, vous pouvez ne rien cocher dans cet &eacute;cran et cliquer sur le bouton &quot;Cr&eacute;er la candidature&quot; en bas de page pour poursuivre directement la saisie de votre candidature.";
    public static final String M223 = "Votre candidature a bien &eacute;t&eacute; supprim&eacute;e.";
    public static final String M224 = "Fin de la saisie du suivi des candidatures sur un poste.";
    public static final String M225 = "Le NUMEN saisi est inconnu";
    public static final String M226 = "Chaque classement doit &ecirc;tre unique.";
    public static final String M227 = "Le classement doit &ecirc;tre compris entre 1 et 7";
    public static final String M228 = "L'extraction demand&eacute;e porte sur un nombre de dossiers sup&eacute;rieur &agrave; la limite autoris&eacute;e. Veuillez affiner vos crit&egrave;res de s&eacute;lection.";
    public static final String M229 = "Aucun dossier de candidature n'est &agrave; extraire";
    public static final String M230 = "La saisie du fond de page est obligatoire si vous d&eacute;sirez Editer, Modifier ou Supprimer.";
    public static final String M231 = "Vous devez s&eacute;lectionner un et un seul fond de page parmi les fonds de pages &eacute;tablissement et commun.";
    public static final String M232 = "Vous ne pouvez pas supprimer un fond de page commun.";
    public static final String M233 = "L'&eacute;dition demand&eacute;e porte sur un nombre de dossiers sup&eacute;rieur &agrave; la limite autoris&eacute;e. Veuillez affiner vos crit&egrave;res de s&eacute;lection.";
    public static final String M234 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format RTF.";
//    public static final String M234_2 = "Le fichier s&eacute;lectionn&eacute; doit &ecirc;tre au format PDF.";
    public static final String M234_2 = "Fichier indiqu&eacute; incorrect : la fiche d'offre de poste attendue doit &ecirc;tre au format PDF.";
    public static final String M235 = "Saisie obligatoire du chemin complet du fichier contenant le fond de page &agrave; enregistrer sur le serveur.";
    public static final String M236 = "Il n'y a aucun fond de page &eacute;tablissement.";
    public static final String M237 = "V&eacute;rifier que l'emplacement du fichier choisi soit correct.";
    public static final String M238 = "Titre de la th&egrave;se ou du rapport.";
    public static final String M239 = "Date de soutenance.";
    public static final String M240 = "Directeur de th&egrave;se.";
    public static final String M241 = "Derni&egrave;re situation pendant la th&egrave;se.";
    public static final String M242 = "Avant toute &eacute;dition ou tous autres travaux, il est indispensable d'actualiser le suivi des candidatures bloqu&eacute;es sur les postes dont le n&deg; figure en rouge dans les listes. ";
    public static final String M243 = "Modification de la date de lancement automatique prise en compte.";
    public static final String M244 = "Vous ne pouvez lancer qu'un seul batch &agrave; la fois.";
    public static final String M245 = "Vous devez effectuer au moins une saisie.";
    public static final String M246 = "La saisie du choix est obligatoire pour le type de format du t&eacute;l&eacute;chargement.";
    public static final String M247 = "La suppression de ce candidat est impossible car il a au moins une candidature ANTEE. Si vous devez supprimer ce candidat, il vous faut au pr&eacute;alable supprimer toutes ses candidatures ANTEE.";
    public static final String M248 = "La case doit &ecirc;tre coch&eacute;e si le traitement demand&eacute; correspond &agrave; l'injection du fichier.";
    public static final String M249 = "L'&eacute;dition que vous avez s&eacute;lectionn&eacute;e est vide.";
    public static final String M250 = "Il n'y a aucune candidature &agrave; la mutation recevable sur l'emploi. ";
    public static final String M251 = "La saisie du choix candidat &agrave; la mutation sur le poste est obligatoire.";
    public static final String M252 = "Le choix du candidat &agrave; la mutation sur le poste doit &ecirc;tre diff&eacute;rent de celui pr&eacute;selectionn&eacute;.";
    public static final String M253 = "Il n'y a aucune candidature au  d&eacute;tachement recevable sur l'emploi.";
    public static final String M254 = "La saisie du choix candidat au d&eacute;tachement sur le poste est obligatoire.";
    public static final String M255 = "Au moins un des crit&egrave;res doit &ecirc;tre renseign&eacute;.";
    public static final String M256 = "Aucun candidat recevable sur le poste ne correspond aux crit&egrave;res saisis.";
    public static final String M257 = "La saisie des classements doit &ecirc;tre diff&eacute;rentes de celle pr&eacute;selectionn&eacute;e.";
    public static final String M258 = "La consultation porte sur un emploi qui appartient &agrave; un groupe d&#8217;emplois : parmi les r&eacute;f&eacute;rences des emplois du groupe, la r&eacute;f&eacute;rence de l&#8217;emploi sur lequel porte la consultation est signal&eacute;e en rouge.";
    public static final String M259 = "Le choix du candidat au d&eacute;tachement sur le poste doit &ecirc;tre diff&eacute;rent de celui pr&eacute;selectionn&eacute;.";
    public static final String M260 = "La saisie du fond de page est obligatoire si vous d&eacute;sirez Modifier ou Supprimer.";
    public static final String M261 = "V&eacute;rifier que vous n'avez pas d&eacute;j&agrave; saisie une candidature qui appartient au groupe d'emploi li&eacute;.";
    public static final String M262 = "Les candidatures indiqu&eacute;es en vert sont des candidatures recevables.";
    public static final String M263 = "Le nom que vous avez affect&eacute; au mod&egrave;le de fond de page ne doit pas contenir des caract&egrave;res accentu&eacute;s, des blancs ou autres caract&egrave;res interdits. ";
    public static final String M264 = "La saisie du fond de page est obligatoire pour &eacute;diter la liste des candidats.";
    public static final String M265 = "ATTENTION, vous avez saisi des caract&egrave;res non imprimables dans votre candidature, veuillez les supprimer.";
    public static final String M266 = "ATTENTION, vous avez saisi des caract&egrave;res non imprimables dans votre identit&eacute;, veuillez les supprimer.";
    public static final String M267 = "ATTENTION, l'extraction diff&eacute;rentielle que vous demandez ne ram&egrave;ne aucune candidature. Veuillez vous assurer que l'extraction que vous demander a d&eacute;j&agrave; fait l'objet d'une extraction totale ou bien modifiez les crit&egrave;res de s&eacute;lection.";
    public static final String M268 = "Si la candidature recherch&eacute;e n'est pas affich&eacute;e, veuillez affiner la recherche. " ;
    public static final String M269 = "Voici les " ;
    public static final String M270 = " premi&egrave;res candidatures " ;
    public static final String M271 = "Aucune candidature trouv&eacute;e r&eacute;pondant aux crit&egrave;res de recherches" ;
    public static final String M273 = "la 39ème section a évolué vers la 85ème section" ;
    public static final String M274 = "la 40ème section a évolué vers la 86ème section" ;
    public static final String M275 = "la 41ème section a évolué vers la 87ème section" ;
    public static final String M276 = "ATTENTION, vous devez soit sélectionner une section CNU soit un secteur Disciplinaire." ;
    public static final String M277 = "ATTENTION, vous devez soit sélectionner un établissement soit saisir un lieu d'exercice, une ville et une région ." ;
    
    public static final String M278	= "Erreur: saisie incorrecte. Veuillez entrer une valeur numérique";
    public static final String M279	= "Erreur: saisie incorrecte. Veuillez entrer une durée sous la forme : MM/JJ";
    public static final String M280	= "Erreur: saisie incorrecte. Veuillez entrer un pourcentage (2 chiffres au maximum après la virgule)";
    public static final String M281	= "Les données saisies ont été enregistrées";
    public static final String M282	= "Les données saisies dans l'écran précédent ont été enregistrées";
    
    public static final String M283 = "Vous devez s&eacute;lectionner un et un seul modèle d'avis parmi les modèles d'avis de la section et les modèles d'avis proposés par la CP-CNU.";
    public static final String M284 = "Il n'y a aucun modèle d'avis sélectionné.";
    public static final String M285 = "Saisie obligatoire du chemin complet du fichier contenant le modèle d'avis de la section &agrave; enregistrer sur le serveur.";
    public static final String M286 = "Vous ne pouvez pas supprimer un modèle d'avis proposé par la CP-CNU.";
    public static final String M287 = "Aucun dossier trouvé.";
    public static final String M288 = "Le nom que vous avez affect&eacute; au mod&egrave;le d'avis ne doit pas contenir des caract&egrave;res accentu&eacute;s, des blancs ou autres caract&egrave;res interdits.";
    public static final String M289 = "Vous devez s&eacute;lectionner un et un seul modèle d'avis";
    public static final String M290 = "Vous ne pouvez pas supprimer un modèle d'avis commun.";
    public static final String M291 = "Saisie obligatoire du chemin complet du fichier contenant le modèle d'avis &agrave; enregistrer sur le serveur.";
    // MESSAGE FIDIS A PARTIR DE 300
    public static final String M301 = "Erreur: la date doit être supérieure à la date du jour";
    public static final String M302 = "Erreur: La date de clôture doit être supérieure à la date d'ouverture plus 30 jours";
    public static final String M303 = "En utilisant un numéro de poste existant vous effectuez la re-publication de l'offre." +
                                      "Toutes les données de l'ancienne offre seront donc remplacées par les données saisies maintenant";
    public static final String M304 = "Erreur: La date de publication doit &ecirc;tre sup&eacute;rieure d'un jour au moins &agrave; la date de saisie.";
    public static final String M305 = "Erreur: La date d'ouverture des candidatures doit &ecirc;tre &eacute;gale ou sup&eacute;rieure &agrave; la date de publication.";
    public static final String M306 = "Erreur: La date de cl&ocirc;ture doit  &ecirc;tre sup&eacute;rieure &agrave; la date de publication plus 30 jours";
    public static final String M307 = "Erreur: La date de d&eacute;but des travaux doit &ecirc;tre sup&eacute;rieure &agrave; la date de cl&ocirc;ture des candidatures " +
            "et inf&eacute;rieure aux dates de d&eacute;cision de l'&eacute;tablissement et de prise de fonction";
    public static final String M308 = "Erreur: La date d&eacute;cision de l'&eacute;tablissement doit &ecirc;tre sup&eacute;rieure &agrave; la date de cl&ocirc;ture des candidatures ainsi que " +
            "&agrave; la date de d&eacute;but de travaux et inf&eacute;rieure &agrave; la date de prise de fonction";
    public static final String M309 = "Erreur: La date de prise de fonction doit &ecirc;tre sup&eacute;rieure aux dates de cl&ocirc;ture des candidatures " +
            "et de d&eacute;cision de l'&eacute;tablissement";
    public static final String M310 = "Erreur: Il existe un doublon de section";
    public static final String M311 = "Erreur: La saisie des sections est discontinue";
    public static final String M312 = "Erreur: La date de publication doit  &ecirc;tre sup&eacute;rieure d'un jour au moins &agrave; la date du jour.";
    public static final String M313 = "Erreur: Vous ne pouvez accepter plus d'un poste";
    public static final String M314 = "Erreur: La date d&eacute;cision de l'&eacute;tablissement doit &ecirc;tre sup&eacute;rieure &agrave; la date du jour";
    public static final String SELECTION_ARTICLE_15_II_ET_ARTICLE_8="L'article 15-II n'est pas compatible avec l'article 8.";
    public static final String SELECTION_ARTICLE_15_II_ET_ARTICLE_4="L'article 15-II n'est pas compatible avec l'article 4.";
    public static final String DUREE_RETENUE_MANDATORY_WITH_3="Pour les fonctions rattachées à l'article 3, vous devez indiquer dans la durée retenue l'ancienneté acquise dans l'échelon d'origine à la date de nomination de l'enseignant-chercheur.";
    public static final String DUREE_RETENUE_MANDATORY_FEV_WITH_3="Attention : vous avez saisi une durée retenue non nulle pour une période dont la date de début et la date de fin sont égales.";
    
    public static final String NOCHECKED = "Erreur: Veuillez s&eacute;lectionner au moins un mode de transmission du dossier.";
    public static final String EMAILMANQUANT = "Erreur: Vous devez spécifier un email ou décocher la case correspondante.";
    public static final String URLMANQUANT = "Erreur: Vous devez spécifier une URL ou décocher la case correspondante.";
    public static final String EMAILNONVALIDE = "Erreur: L'email saisi n'est pas valide.";
    public static final String URLNONVALIDE = "Erreur: L'URL saisie n'est pas valide.";
    
    public static final String NOQUALIFAFFICH = "Vous n'avez pas à ce jour de qualification valide référencée dans GALAXIE.<br/>"+ 
			"Attention, les qualifications obtenues au titre de la campagne de qualification en cours ne sont mentionnées<br/>"+
			"que si leur résultat a déjà été communiqué à l'administration par le CNU et publié sur GALAXIE.<br/>"+
			"En cas de doute, veuillez consulter la rubrique &quot;Résultats qualification&quot; pour vérifier si tous vos résultats de la campagne en cours sont connus. <br/><br/>";
    
    public static final String M315 = "Erreur: La date de fin doit être supérieure à celle du dernier paiement au titre de l'exercice ";

    public static final String M316 = "Erreur: Le nombre de paiements doit etre supérieur à zéro et supérieur au nombre " +
    "de paiement déjà effectués";
    public static final String M317 = "Erreur: La date de fin doit être supérieure d'au moins un jour à la date de début ";
    public static final String M318 = "Erreur: Le nombre de paiements doit etre supérieur à zéro";
    
    public static final String M319 = "Erreur: La durée renseigné ne doit être supérieure à la durée de l'ordre de mission";
    public static final String M320 = "Erreur: La date de fin doit &ecirc;tre sup&eacute;rieure ou &eacute;gale à la date de d&eacute;but ";
    public static final String M321 = "Erreur: Cette association phase/profil existe d&eacute;j&agrave; pour la campagne en cours ";
    public static final String M322 = "Vous ne pouvez choisir qu'une seule section: CNU ou CNAP.";
    public static final String M323 = "La date d'ouverture des candidatures saisie ne doit pas correspondre à un jour de week-end.";
    public static final String M324 = "La date de cl&ocirc;ture des candidatures saisie ne doit pas correspondre à un jour de week-end.";
    // CNU : Saisie en masse des résultats de qualification
    public static final String M325 = "T&eacute;l&eacute;chargement impossible, le fichier doit &ecirc;tre au format CSV.";
    public static final String M326 = "T&eacute;l&eacute;chargement impossible, le fichier ne doit pas d&eacute;passer 5 Mo.";
    public static final String M327 = "T&eacute;l&eacute;chargement impossible, un fichier de m&ecirc;me nom existe d&eacute;j&agrave;.";
    public static final String M328 = "T&eacute;l&eacute;chargement impossible, le fichier doit &ecirc;tre au bon format CSV.";
    public static final String M329 = "Lancement du traitement impossible : il n'y a aucun fichier CSV &agrave; traiter.";
    public static final String M330 = "Une erreur s'est produite pendant le t&eacute;l&eacute;chargement du fichier.";
    
    //EDI - 18/01/2017 - Evo 178348
    public static final String M004bis = "Erreur : la date doit être comprise entre les dates de campagne.";
    //EDI - 18/01/2017 - Evo 169029
    public static final String M331 = "Les donn&eacute;es ont bient &eacute;t&eacute; enregistr&eacute;es.";
    public static final String M332 = "Erreur : Le code de tri doit être un chiffre.";
    
    //ARA - 17/03/2017 - Evo 159243
    public static final String M333 = "La saisie d'un avis \"Insuffisamment renseign&eacute;\" entra&icirc;ne automatiquement la demande dans le groupe des 50% restants.<br/>"+
    								  "Le ou les cas d&eacute;tect&eacute;s ont &eacute;t&eacute; rectifi&eacute;s en cons&eacute;quence.";
    public static final String M334 ="Attention : Ne peuvent postuler que les personnes titulaires au moment de la prise de fonction dans l'enseignement sup&eacute;rieur.<br/>"+
    								 "S&eacute;lectionner les conditions que vous remplissez ou la situation particuli&egrave;re que vous souhaitez faire valoir puis cliquer sur le bouton &laquo; Cr&eacute;er la candidature &raquo; en bas de page pour poursuivre la saisie de votre candidature.";
    public static final String M335 ="Attention : Ne peuvent postuler que les personnes titulaires au moment de la prise de fonction dans l'enseignement sup&eacute;rieur.<br/>"+
    								 "S&eacute;lectionner les conditions que vous remplissez ou la situation particuli&egrave;re que vous souhaitez faire valoir puis cliquer sur le bouton &laquo; Modifier la candidature &raquo; en bas de page pour poursuivre la saisie de votre candidature.";
    //ECA 21/06/2017 - evo 175517 
    public static final String M336 = "Erreur, le champ Provenance est obligatoire";
    //ECA - fin 
    
    //SRG : 
    public static final String M337 = "Aucun poste trouv&eacute;";
    
    //evo 212767
    public static final String M338 = "Erreur: Le motif et la date de motif doivent être renseignés";
    //evo - fin
    
    //evo 221128 
    public static final String M339 = "Erreur: saisie obligatoire, vous devez s&eacute;lectionner un groupe et un collège avant de lancer l'&eacute;dition de la liste d'&eacute;margement CNU.";
    
    public static final String M340 = "Erreur: saisie obligatoire, vous devez s&eacute;lectionner un groupe et un corps avant de lancer l'&eacute;dition de la liste d'&eacute;margement candidat.";
    
    public static final String M341 = "Saisie obligatoire, vous devez s&eacute;lectionner un corps avant de lancer l'&eacute;dition.";
 
    //ANO 229412
    public static final String M342 = "Le fichier s&eacute;lectionn&eacute; est trop volumineux, maximum {0} Mo";

    
    public static final String M343 = "La taille de ce champ ne doit pas d&eacute;passer 50 caract&egrave;res";
    
    //ANO 167010
    public static final String M344 = "Le choix d'un diplôme est obligatoire. Au moins une case doit être coch&eacute;e dans la rubrique « Diplômes »";
    
    //evo 227225
    public static final String M345 = "ATTENTION ! Le candidat ne remplit pas les conditions requises pour faire appel au groupe.";
    
    public static final String M346 = "Erreur: Format du nombre non valide";
    
    public static final String M347 = "Le fichier est verrouill&eacute;. Veuillez le d&eacute;verrouiller avant de le joindre. ";
    public static final String M348 = "Votre candidature a bien &eacute;t&eacute; enregistr&eacute;e.";
    
    public static final String MSG_RGPD_RES = "La présente décision a donné lieu à la mise en oeuvre d'un traitement "
    		+ "algorithmique. En application de l'article L. 311-3-1 du Code des relations "
    		+ "entre le public et l'administration, vous avez le droit d'obtenir la "
    		+ "communication des règles définissant ce traitement et des principales "
    		+ "caractéristiques de sa mise en oeuvre, dans les conditions prévues par "
    		+ "l'article R.311-3-1-2 du même code. En cas d'absence de réponse de "
    		+ "l'administration après un mois suivant la réception de votre demande ou en "
    		+ "cas de refus de l'administration de vous communiquer ces éléments, vous "
    		+ "disposez de deux mois pour saisir la Commission d'accès aux documents administratifs.";
    /**
     * Le nom saisi doit contenir au moins 3 caract&egrave;res.
     */
    public static final String M349 = "Le nom saisi doit contenir au moins 3 caract&egrave;res.";
    public static final String M350 = "Erreur: cet email est incorrect ou il existe déjà";
    
    public static final String M351 = "Ce champ ne doit pas comporter '@gmail.fr'";
    public static final String M352 = "Ce champ doit comporter '@'";
    public static final String M353 = "Ce champ ne doit pas comporter ';'";
    public static final String M354 = "Ce champ ne doit pas comporter '@cnu.education.gouv.fr";
    
}
