package fr.gouv.education.antares.calc;

import fr.gouv.education.antares.outils.StringUtils;


public class ConstantesCalculette {
	
	public static String LINEFEED = System.getProperty("line.separator");
	
	public static String DateDebut;
	public static String DateFin;
	

	public static enum ARTICLE{
		ARTICLE_LIBRE("Libre"), 				
		ARTICLE_SNA("SNA"),
		ARTICLE_NON_RETENU("Non retenue"),
		ARTICLE_3("3"),
		ARTICLE_4("4"),
		ARTICLE_5("5"),
		ARTICLE_6("6"),
		ARTICLE_8("8"),
		ARTICLE_9_MCF("9 MCF"),
		ARTICLE_9_PR("9 PR"),
		ARTICLE_10("10"),
		ARTICLE_11("11"),
		ARTICLE_12("12"),
		ARTICLE_13("13"),
		ARTICLE_14("14"),
		ARTICLE_15_II("15-II"),
		ARTICLE_5_1("5-1"),
		;


		protected String label;

		/** Constructeur */
		private ARTICLE(String pLabel) {
			this.label = pLabel;
		}

		public String getLabel() {
			return this.label;
		}

		public boolean isArticleOK(String pStr){
			return label.equals(pStr);
		}
	}

	public static enum CALCULETTE_FORWARD{
		CALCULETTE_ECRAN("/eta/calc/gcalc.jsp"),
		CALCULETTE_CTRL("/eta/calc/gcalc_ctrl.jsp");		

		protected String forward;

		public String getForward(){
			return forward;
		}

		CALCULETTE_FORWARD(String val){
			forward = val;
		}


	}


	public enum FORM_CALCULETTE_ID{      
		TABLE_FONCTION("tabfonrows"),
		ADD_FONCTION("tabfonlast"),
		ZONE_RESULTAT("resultat"),
		ZONE_LIB_DETAIL_CAL("libDetailCalcul"),                 
		ZONE_DETAIL_CAL("detailCalcul"),
		BT_EDIT("btEdit");


		private String nom;
		FORM_CALCULETTE_ID(String pNom){
			nom = pNom;
		}

		public String getNom(){
			return nom;
		}
	}

	public static enum DETAIL_CALCUL_MESSAGE{
		ARTICLE_ALL_MESSAGE,
		ARTICLE_11_MESSAGE,
		ARTICLE_10_MESSAGE,
		ARTICLE_6_MESSAGE,
		ARTICLE_12_MESSAGE
		;
	}


	public static enum VALEUR_CALCUL{
		MAX_REST_ART_4(1080f),
		MAX_REST_ART_5(1440f),
		NB_JRS_12_ANS(4320f),
		TAUX_12_ANS_ART_10_ET_6(0.5f),
		TAUX_12_ANS_ART_12(0.5f),
		TAUX_PLUS_12_ANS_ART_10_ET_6(3/4f),
		TAUX_PLUS_12_ANS_ART_12(2/3f)		
		;

		private Float value;

		private VALEUR_CALCUL(Float pValue){
			this.value =  pValue;
		}

		public Float getValue(){
			return value;
		}		
	}
	
	
	 public static final String APPLICATION_ART_ALL = "Total des services accomplis utilisé pour la détermination du classement : %d an(s) %d mois %d jour(s)" + LINEFEED +
	"Attention, du fait des arrondis appliqués pour l'affichage des résultats dans la liste des fonctions exercées, le total ci-dessus peut ne pas correspondre exactement à la somme des résultats figurant au niveau de chaque fonction." + LINEFEED;

		
	
	public static final String APPLICATION_ART_11 = "Application de l'article 11 : " + LINEFEED + 
	"L'article 4  a été remplacé par l'article 11 pour les calculs de durées (2/3 des services effectifs au lieu de la totalité )"+ LINEFEED +
	"L'article 5 a été remplacé par l'article 11 pour les calculs de durées (2/3 des services effectifs au lieu de la totalité )" +LINEFEED;

	public static final String APPLICATION_ART_10_12 = "Article %s : " + LINEFEED +
	"Total des services accomplis : %d ans %d mois %d jours " + LINEFEED +
	"Durée pouvant être retenue : " + LINEFEED +
	" 1/2 des 12 premières années : %d ans %d mois %d jours " + LINEFEED +
	" 2/3 au-delà des 12 premières années : %d ans %d mois %d jours "+ LINEFEED +
	"Total général pouvant être retenu au titre de l'article  %s : %d ans %d mois %d jours " + LINEFEED;



	// constantes grade
	public static String MAITRE_CONF = "MCF";
	
	public static final String MAITRE_CONF_NORMALE = "MCF CN";

	public static final boolean isMaitreDeConf(String pStr){
		return MAITRE_CONF.equals(pStr);
	}

	public static String MAITRE_CONF_ASSOCIE = "MCF AS";

	public static final boolean isMaitreDeConfAssocie(String pStr){
		return MAITRE_CONF_ASSOCIE.equals(pStr);
	}

	public static String PR_ASSOCIE = "PR AS";

	public static final boolean isPrAssocie(String pStr){
		return PR_ASSOCIE.equals(pStr);
	}
	
	public static final boolean isMaitreDeConfNormal(String libelleCourt) {
		return MAITRE_CONF_NORMALE.equals(libelleCourt);
	}


	public static String PR = "PR";

	public static final boolean isPR(String pStr){
		return PR.equals(pStr);
	}


	public enum FORM_CALCULETTE_ACTION{
		RECUPERER("recuperer"),
		CALCULER("calculer"),
		CALCULER_DUREE("calculDuree"),
		ENREG_INFO("enregistrerInfos"),
		EDIT_DECOMPTE("editerDecompte"),

		;

		private String nom;

		public String getNom(){
			return nom;
		}
		FORM_CALCULETTE_ACTION(String pNom){
			nom = pNom;
		}
	}

	public enum FORM_CALCULETTE_NAME{
		NOM("nom"),
		PRENOM("prenom"),
		DT_NAISS("datenais"),
		DT_NOMIN("datenomin"),
		CORPS_ACCEUIL("corpsacc"),
		GRADE_ACCEUIL("gradeacc"),
		CORPS_ORIGINE("corpsori"),
		GRADE_ORIGINE("gradeori"),
		ECHELON_ORIGINE("echelonori"),         
		CHEVRON_ORIGINE("chevronori"),
		LIB_CLASS_ORIGINE("libclaori"),
		IBR_DETENU_ORIGINE("indbrutdet"), 
		IBR_ECH_SUP_ORIGINE("indbrutechelonsup"),

		FONC_DT_DEB_PREFIX("dateDeb"),
		FONC_DT_FIN_PREFIX("dateFin"),
		FONC_FONCTION_PREFIX("fonction"),

		FONC_ARTICLE_PREFIX("article"),
		FONC_QUOTITE_RET_PREFIX("quotiteretenue"),
		FONC_QUOTITE_REELLE_PREFIX("quotitereelle"),
		FONC_DUREE_RET_AA_PREFIX("dureeRetenueAA"),
		FONC_DUREE_RET_MM_PREFIX("dureeRetenueMM"),
		FONC_DUREE_RET_JJ_PREFIX("dureeRetenueJJ"),    	
		CALCUL_DUREE_DATE_DEB("calDateDeb"),
		CALCUL_DUREE_DATE_FIN("calDateFin")

		;


		private String nom;

		public String getNom(){
			return nom;
		}

		FORM_CALCULETTE_NAME(String pNom){
			nom = pNom;
		}
	}




	public static enum FONCTION_BITSET_INDX{
		ART_3_PRESENT,ART_4_PRESENT,ART_5_PRESENT,ART_10_PRESENT,ART_11_PRESENT,ART_12_PRESENT,ART_6_PRESENT,
		ART_4_DUREE_RETENUE_EMPTY,ART_4_DUREE_RETENUE_FILLED,
		ART_5_DUREE_RETENUE_EMPTY,ART_5_DUREE_RETENUE_FILLED,
		ART_6_DUREE_RETENUE_EMPTY,ART_6_DUREE_RETENUE_FILLED,
		ART_10_DUREE_RETENUE_EMPTY,ART_10_DUREE_RETENUE_FILLED,
		ART_12_DUREE_RETENUE_EMPTY,ART_12_DUREE_RETENUE_FILLED;		
	}

	// partie calculette de classement

	public static final String POINT_VIRGULE = ";";

	public static final String TIRET = " - ";

	public static int NB_MOIS_AN = 12;
	public static int NB_JOURS_PAR_MOIS = 30; //Nb de jours qui constituent un mois.
	public static final int NB_JOURS_PAR_AN = NB_JOURS_PAR_MOIS * 12; //Nb de jours qui constituent un an.

	

	public static final String ECR_DEMANDEE_KEY = "ecranDemande";
	public static final String ECR_CALCULETTE = "calculette";
	public static final String CALCULETTE_KEY = "calculette";


	public static boolean isEcranCalculette(String pEcran){
		return ECR_CALCULETTE.equals(pEcran);
	}

	

	public static enum MESSAGE{
		
		OK("",LEVEL.LEVEL_INFO), 
		KO("Echec", LEVEL.LEVEL_EXCEPTION),
		CALCUL_DUREE_DATE_DEBUT_MANDATORY("La date de début du calcul est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		CALCUL_DUREE_DATE_FIN_MANDATORY("La date de fin du calcul est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		CALCUL_FONCTIONE_DATE_DEBUT_MANDATORY("La date de début de la période du fonction est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		CALCUL_FONCTIONE_DATE_FIN_MANDATORY("La date de fin de la période du fonction est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		CALCUL_FONCTIONE_FORMAT_DATE_MANDATORY("La date est invalide.",LEVEL.LEVEL_USER_ERROR),
		NOM_MANDATORY("Le nom est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		NOM_MAX_LENGHT("Le nom ne peut avoir plus de 25 caractères.",LEVEL.LEVEL_USER_ERROR),
		PRENOM_MANDATORY("Le prénom est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		PRENOM_MAX_LENGHT("Le prénom ne peut avoir plus de 25 caractères.",LEVEL.LEVEL_USER_ERROR),
		DATE_NAISSANCE_MANDATORY("La date de naissance est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		DATE_NOMINATION_MANDATORY("La date de nomination est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		CONTROLE_FORMAT_DATE_NOMIN("Le format du champ date de nomination doit être une date valide (format jj/mm/aaaa).",LEVEL.LEVEL_USER_ERROR),
		CONTROLE_FORMAT_DATE_NAIS("Le format du champ date de naissance doit être une date valide (format jj/mm/aaaa).",LEVEL.LEVEL_USER_ERROR),
		CONTROLE_FORMAT_DATE_DEBUT("Le format du champ date de debut doit être une date valide (format jj/mm/aaaa).",LEVEL.LEVEL_USER_ERROR),
		CONTROLE_FORMAT_DATE_FIN("Le format du champ date de fin doit être une date valide (format jj/mm/aaaa).",LEVEL.LEVEL_USER_ERROR),
		CORPS_ACCEUIL_MANDATORY("Le corps d'accueil est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		GRADE_ACCEUIL_MANDATORY("Le grade d'accueil est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		PERIODE_MANDATORY("Pour les fonctions qui ne sont pas rattachées aux articles 15-II et libre, les date de début et de fin sont obligatoires.",LEVEL.LEVEL_USER_ERROR),
		QUOTITE_REELLE_MANDATORY("Pour les fonctions qui ne sont pas rattachées aux articles 15-II et libre, la quotité réelle est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		QUOTITE_REELLE_FORMAT("Les champs quotité sont des entiers compris entre 0 et 100.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_AA("Le champ durée retenue (AA) est un entier.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_MM("Le champ durée retenue (MM) est un entier compris entre 0 et 11.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_JJ("Le champ durée retenue (JJ) est un entier compris entre 0 et 29.",LEVEL.LEVEL_USER_ERROR),
		MCF_GRADE_MANDATORY("Le grade d'accueil doit correspondre à un grade de maître de conférences ou assimilé car les articles 8 ou 15-II ont été sélectionnés.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_FORBIDDEN("Pour les fonctions rattachées aux articles 15-II, 5-1 ou Non Retenue, la saisie de la durée retenue est interdite.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_MANDATORY_WITH_3("Pour les fonctions rattachées à l'article 3, vous devez indiquer dans la durée retenue l'ancienneté acquise dans l'échelon d'origine à la date de nomination de l'enseignant-chercheur.",LEVEL.LEVEL_USER_ERROR),
		DUREE_RETENUE_MANDATORY_WITH_LIBRE("Pour les fonctions rattachées à l'article libre, la saisie de la durée retenue est obligatoire.",LEVEL.LEVEL_USER_ERROR),
		RENVOI_ARTICLE_13("Pour les fonctions rattachées à l'article 13, vous devez indiquer l'article à appliquer par renvoi (parmi les articles 3 à 12) afin que l'outil puisse effectuer les calculs.",LEVEL.LEVEL_USER_ERROR),
		RENVOI_ARTICLE_14("Pour les fonctions rattachées à l'article 14, vous devez indiquer l'article à appliquer par renvoi (article 10 ou 12) afin que l'outil puisse effectuer les calculs.",LEVEL.LEVEL_USER_ERROR),
		WRONG_CORPS_ORIG_WITH_ART_9("Lorsque qu'une fonction est rattachée à l'article 9, le corps d'origine doit être le corps MCF associé ou PR associé.",LEVEL.LEVEL_USER_ERROR),
		CORPS_ACCEUIL_MCF_REQUESTED("Avec l'article 9 MCF, le corps d'accueil doit être de niveau MCF.",LEVEL.LEVEL_USER_ERROR),
		CORPS_ACCEUIL_PR_REQUESTED("Avec l'article 9 PR, le corps d'accueil doit être de niveau MCF ou PR.",LEVEL.LEVEL_USER_ERROR),
		ART_3_MULTIPLE("L'article 3 ne peut être rattaché qu'à une seule fonction.",LEVEL.LEVEL_USER_ERROR),
		ART_11_MULTIPLE("L'article 11 ne peut être rattaché qu'à une seule fonction.",LEVEL.LEVEL_USER_ERROR),
		ART_SNA_MULTIPLE("L'article SNA ne peut être rattaché qu'à une seule fonction.",LEVEL.LEVEL_USER_ERROR),
		ART_15_II_MULTIPLE("L'article 15-II ne peut être rattaché qu'à une seule fonction.",LEVEL.LEVEL_USER_ERROR),
		CLASSEMENT_ORIGINE_MANDATORY("Les informations concernant le classement d'origine sont obligatoires.",LEVEL.LEVEL_USER_ERROR),
		WARN_ART_11("Vous avez sélectionné l'article 11, assurez-vous que l'intéressé a bien la qualité de chercheur à la date de sa nomination.",LEVEL.LEVEL_WARNING),
		DUREE_RETENUE_NOT_FILLED("Les durées retenues de toutes les fonctions rattachées à l'article 4 doivent être soit toutes renseignées soit toutes non renseignées. La même règle est appliquée pour les articles 5, 10 ou 12.",LEVEL.LEVEL_USER_ERROR),
		QUOTITE_BAD_VALUE("La quotité doit être un entier supérieur à 0 et inférieur ou égal à 100",LEVEL.LEVEL_USER_ERROR),
		QUOTITE_RETENUE_TOO_BIG("La quotité retenue doit être inférieure ou égale à la quotité réelle.",LEVEL.LEVEL_USER_ERROR),
		DUREE_A_30_JRS_BAD_VALUE("Pour la durée retenue, le nombre de mois doit être inférieur à 12 et le nombre de jours doit être inférieur à 30.",LEVEL.LEVEL_USER_ERROR),
		ARTICLE_4_5_VALUE_NEEDED("L'article n'a pas le code adéquat (art. 4 ou art. 5 requis)",LEVEL.LEVEL_EXCEPTION),
		ARTICLE_10_12_VALUE_NEEDED("L'article n'a pas le code adéquat (art. 10 ou art. 12 requis)",LEVEL.LEVEL_EXCEPTION),
		ARTICLE_11_ONLY_WITH_LAST_PERIOD("L'article 11 n'est applicable qu'à la période la plus récente. Donc, rectifiez les dates ou choisissez un autre article (3, 10 ou 12). ",LEVEL.LEVEL_USER_ERROR),
		ARTICLE_11_LAST_PERIOD("L'article 11 n'est applicable que si la date de fin de période est égale à la veille de la date de nomination. Donc rectifiez les dates ou choisissez un autre article (3, 10 ou 12).",LEVEL.LEVEL_USER_ERROR),
		IBR_DET_SUP_IBR_SUP("L'indice brut détenu doit être inférieur à l'indice brut de l'échelon supérieur.",LEVEL.LEVEL_USER_ERROR),
		ORIGINE_DATA_MANDATORY("Toutes les données du classement d'origine sont obligatoires lorsque l'article 3 est présent.",LEVEL.LEVEL_USER_ERROR),
		FONCTION_ARTICLE_DOESNT_FIT("L'article rattaché à la fonction est incohérent.",LEVEL.LEVEL_USER_ERROR),
		QUOTITE_TOO_BIG("La somme des quotités retenues sur une même période ne peut pas dépasser 100%.",LEVEL.LEVEL_USER_ERROR),
		CHEVAUCHEMENT_DATE_FORBIDEN("Le chevauchement des périodes n'est autorisé que sur des périodes complètes (même date de début et de fin).",LEVEL.LEVEL_USER_ERROR),
		CALCUL_DUREE_PERIODE_MANDATORY("Les dates de début et de fin sont obligatoires pour le calcul des durées.",LEVEL.LEVEL_USER_ERROR),
		CALCUL_DUREE_DT_DEB_BEFORE_DT_FIN("La date de début doit être antérieure à la date de fin pour le calcul des durées.",LEVEL.LEVEL_USER_ERROR),
		FILE_MISS("Vous devez spécifier un fichier.",LEVEL.LEVEL_USER_ERROR),
		SELECTION_ARTICLE_15_II_ET_ARTICLE_8("L'article 15-II n'est pas compatible avec l'article 8.",LEVEL.LEVEL_USER_ERROR),
		SELECTION_ARTICLE_15_II_ET_ARTICLE_4("L'article 15-II n'est pas compatible avec l'article 4.",LEVEL.LEVEL_USER_ERROR),
		DATE_DEBUT_FIN_ARTICLE_3("La date de fin doit être supérieure ou égale à la date de début.",LEVEL.LEVEL_USER_ERROR),
		DATE_DEBUT_FIN_AUTRES_ARTICLES("La date de fin d'une fonction doit être strictement supérieure à sa date de début.",LEVEL.LEVEL_USER_ERROR),
		
		SI_QUANTITE_NO_EGALE("La somme des quotités retenues ne peut pas dépasser 100% sur une même période.",LEVEL.LEVEL_USER_ERROR),
		
		SI_DATE_DEBUT_RECENTE_DATE_FIN("Le chevauchement des périodes n'est autorisé que sur des périodes complètes (mêmes date de début et date de fin).",LEVEL.LEVEL_USER_ERROR),
		SI_DATE_DEBUT_ANTERIEUR_DATE_FIN("La date de début doit être antérieure à la date de fin.",LEVEL.LEVEL_USER_ERROR),
		WRONG_CORPS_ACCEUIL_WITH_ART_5_1("Les corps et grade d'accueil doivent correspondre à un corps/grade de maître de conférences de classe normale car l'article 5-1 a été sélectionné.",LEVEL.LEVEL_USER_ERROR),
		ART_5_1_MULTIPLE("L'article 5-1 ne peut être rattaché qu'à une seule fonction.",LEVEL.LEVEL_USER_ERROR)
		;


		protected String label;
		protected LEVEL niveau;
		protected String arg;
		private int nbpageAvis;
		private int nbpageInfosCnu;
		

		/** Constructeur */
		private MESSAGE(String pLabel, LEVEL niveau) {
			this.label = pLabel;
			this.niveau = niveau;

		}

		public String getLabel() {
			if(StringUtils.ok(arg)){
			  return this.label + arg;
			} else {
				return this.label;
			}
		}

		public LEVEL getLevel() {
			return this.niveau;
		}

		public void setArg(String pStr){
			arg = pStr;
		}
		
		public int getNbpageAvis() {
			return nbpageAvis;
		}
		
		public void setNbpageAvis(int nbpageAvis) {
			this.nbpageAvis = nbpageAvis;
		}

		public int getNbpageInfosCnu() {
			return nbpageInfosCnu;
		}

		public void setNbpageInfosCnu(int nbpageInfosCnu) {
			this.nbpageInfosCnu = nbpageInfosCnu;
		}
		
		
	}


	public static enum LEVEL{
		LEVEL_USER_ERROR,LEVEL_WARNING,LEVEL_INFO,LEVEL_EXCEPTION;

		public boolean isUserErrorLevel(){
			return (LEVEL_USER_ERROR == this);	    	                   

		}

		public boolean isErrorLevel(){
			boolean isError =     LEVEL_USER_ERROR == this
			|| LEVEL_EXCEPTION == this;
			return  isError;
		}

		public boolean isWarningLevel(){
			return  LEVEL_WARNING == this;
		}

		public boolean isInfoLevel(){
			return  LEVEL_INFO == this;
		}
	}



}
