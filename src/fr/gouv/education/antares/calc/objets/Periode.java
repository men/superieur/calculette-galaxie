package fr.gouv.education.antares.calc.objets;


import java.util.Date;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.calc.outils.UtilsCalculette;
import fr.gouv.education.antares.objets.ObjetBase;
import fr.gouv.education.antares.outils.Conversion;





public class Periode extends ObjetBase {
	
	private Date dateDebut;
	private Date dateFin;
	
	private String strDateDebut;
	private String strDateFin;
	
	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Periode(Date dateDebut, Date dateFin, String strDateDebut,
			String strDateFin) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.strDateDebut = strDateDebut;
		this.strDateFin = strDateFin;
	}

	public Periode(Date pDateDebut, Date pDateFin){
		dateDebut = pDateDebut;
		dateFin = pDateFin;
	}
	
	public Periode(){
		
	}
	
	
	public boolean isEmpty(){
		
		if( (dateDebut == null)
		&&  (dateFin == null)){
			return true;
		}
		return false;
	}
	
	 
	public float getNbJoursA30Jours(){
		if(isEmpty()){
			return 0;
		}
		return UtilsCalculette.differencierDatesA30jours(dateDebut, dateFin);
		
	}
	
	
	 /**
     *   Vérifie si la période est strictement antérieure à une date donnée
     *    => si la date de fin est null, on considère que la date est incluse dans la période
     * @param date
     * @return boolean
     */
    public boolean isBefore(final Date date){
         if (date == null) {
            return false;
        }
        boolean isBefore = true;

        if(dateDebut != null){
             isBefore = isBefore && date.after(dateDebut);
        }

        if(dateFin != null){
             isBefore = isBefore && date.after(dateFin);
        } else {
            isBefore = false;
        }
        return isBefore;

    }

    /**
     * détermine si une date appartient à la période
     * @param date Date
     * @return boolean
     */
    public boolean contains(final Date date) {
        if (date == null) {
            return false;
        }

        return ((this.getDateDebut() != null && this.getDateFin() != null
                 && (date.compareTo(this.getDateDebut()) >= 0) && (date.compareTo(this.getDateFin()) <= 0))
                || (this.getDateDebut() == null && (this.getDateFin() !=null) && (date.compareTo(this.getDateFin()) <= 0))
                || ( (this.getDateDebut() !=null) && (date.compareTo(this.getDateDebut()) >= 0) && this.getDateFin() == null));
    }
	
    
    
    /**
     * détermine si une date appartient à la période
     * @param date Date
     * @return boolean
     */
    public boolean compareToPeriode(final Date datedeb,final Date datefin) {
        if (datedeb == null || datefin == null) {
            return false;
        }
        
        if(this.getDateDebut() == null || this.getDateFin() == null) {
        	 return false;
        }

        if ((datedeb.compareTo(this.getDateDebut()) == 0) && (datefin.compareTo(this.getDateFin()) == 0)) {
             return false;
        }
        
        if (datedeb.compareTo(this.getDateFin()) > 0) {
            return false;
        }

        if (datefin.compareTo(this.getDateDebut()) < 0) {
            return false;
        }
        
        return true;
    }
    
    /**
     * renvoie l'intersection entre deux périodes à savoir la partie commune entre
     * deux périodes si elle existe.
     *
     * @param p Periode
     * @return Periode
     * 
     */
    public Periode inter(final Periode p){
        Periode result = null;
        Date d1 = p.getDateDebut();
        Date f1 = p.getDateFin();

        if (this.contains(d1) && this.contains(f1)) {
            result = new Periode(
                    d1,
                    f1);
        } else if (this.contains(d1) && !this.contains(f1)) {
            result = new Periode(
                    d1,
                    this.dateFin);
        } else if (!this.contains(d1) && this.contains(f1)) {
            result = new Periode(
                    this.dateDebut,
                    f1);
        } else if (p.contains(this.dateDebut) && p.contains(this.dateFin)) {
            result = new Periode(
                    this.dateDebut,
                    this.dateFin);
        } else if (p.contains(this.dateDebut) && !p.contains(this.dateFin)) {
            result = new Periode(
                    this.dateDebut,
                    p.getDateFin());
        } else if (!p.contains(this.dateDebut) && p.contains(this.dateFin)) {
            result = new Periode(
                    p.getDateDebut(),
                    this.dateFin);
        }

        return result;
    }
    
    /**
     * pour permettre de faire des tris descendants
     * @param o
     * @return
     */
    public int compareTo(Periode o){
      if(o == null)
        return 1;
//      return - toStringPeriod(ConstantesCalculette.POINT_VIRGULE).compareTo(((Periode) o).toStringPeriod(ConstantesCalculette.POINT_VIRGULE));
      return toStringPeriod(ConstantesCalculette.POINT_VIRGULE).compareTo(((Periode) o).toStringPeriod(ConstantesCalculette.POINT_VIRGULE));
    }
    
    public String toStringPeriod(String separator){
        
        StringBuffer buffer = new StringBuffer();
        buffer.append(Conversion.formatDate(dateDebut,Constantes.US_DATE_PATTERN));
        buffer.append(separator);
        buffer.append(Conversion.formatDate(dateFin,Constantes.US_DATE_PATTERN));        
        return buffer.toString();
    }
    
    public String toStringPeriod(String prefix, String separator){    
        StringBuffer buffer = new StringBuffer(prefix);
        buffer.append(Conversion.formatDate(dateDebut,Constantes.DATE_FORMAT_STRING));
        buffer.append(separator);
        buffer.append(Conversion.formatDate(dateFin,Constantes.DATE_FORMAT_STRING));        
        return buffer.toString();
    }

	public String getStrDateDebut() {
		return strDateDebut;
	}

	public void setStrDateDebut(String strDateDebut) {
		this.strDateDebut = strDateDebut;
	}

	public String getStrDateFin() {
		return strDateFin;
	}

	public void setStrDateFin(String strDateFin) {
		this.strDateFin = strDateFin;
	}
    
    

    
}
