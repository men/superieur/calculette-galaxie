package fr.gouv.education.antares.calc.objets;

import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.objets.ObjetBase;
import fr.gouv.education.antares.outils.sql.AntaresLogger;

public class DureeA30Jours extends ObjetBase {
	
	private int nbAnnees=-1;
	private int nbMois=-1;
	private int nbJours=-1;
	
	public DureeA30Jours(Integer pNbAnnee, Integer pNbMois, Integer pNbJours){
		
		if (pNbAnnee !=null) nbAnnees = pNbAnnee.intValue();
		if (pNbMois !=null) nbMois = pNbMois.intValue();
		if (pNbJours !=null) nbJours = pNbJours.intValue();		
		
	}

		
	
	public DureeA30Jours(){
		nbAnnees =0;
		nbMois =0;
		nbJours =0;	
	}
	
	public DureeA30Jours(float nbJours){
		
		if(nbJours > Integer.MAX_VALUE) {
			throw new ArithmeticException("Exception dans DureeA30Jours : integer overflow");
		}
		
		if(nbJours < Integer.MIN_VALUE) {
			throw new ArithmeticException("Exception dans DureeA30Jours : integer underflow");
		}
		
		float tmpNbJrs = nbJours;
		nbAnnees = (int)  tmpNbJrs /  ConstantesCalculette.NB_JOURS_PAR_AN ;
		tmpNbJrs -=   ConstantesCalculette.NB_JOURS_PAR_AN * nbAnnees;
		nbMois = (int)  tmpNbJrs /  ConstantesCalculette.NB_JOURS_PAR_MOIS ;
		this.nbJours = (int)  tmpNbJrs -  ConstantesCalculette.NB_JOURS_PAR_MOIS * nbMois;  
	}


	public int getNbAnnees() {
		return nbAnnees;
	}


	public void setNbAnnees(int nbAnnees) {
		this.nbAnnees = nbAnnees;
	}


	public int getNbMois() {
		return nbMois;
	}


	public void setNbMois(int nbMois) {
		this.nbMois = nbMois;
	}


	public int getNbJours() {
		return nbJours;
	}


	public void setNbJours(int nbJours) {
		this.nbJours = nbJours;
	}


	public float getNbJoursTotal(){
		if(nbAnnees!=-1 ||nbMois!=-1 ||nbJours!=-1  ){
			if(nbAnnees==-1)nbAnnees=0;
			if(nbMois==-1)nbMois=0;
			if(nbJours==-1)nbJours=0;
		}
		int result =  nbAnnees * 30 * 12 +
		              nbMois * 30 +
		              nbJours;
		return result;
		
	}
	
	public boolean isEmpty(){
		boolean result =     nbAnnees == -1 
		                  && nbMois == -1
		                  &&  nbJours == -1;
		return result;
		
	}
	
	public String toString(){
		return nbAnnees + " a " + nbMois + " m " + nbJours + " j ";
	}
	
	
	

}
