package fr.gouv.education.antares.calc.objets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.Messages;
import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.calc.ConstantesCalculette.DETAIL_CALCUL_MESSAGE;
import fr.gouv.education.antares.calc.ConstantesCalculette.MESSAGE;
import fr.gouv.education.antares.calc.outils.UtilsCalculette;
import fr.gouv.education.antares.objets.ObjetBase;
import fr.gouv.education.antares.outils.Conversion;




public class Calculette extends ObjetBase{
	
	// donnee etablissement
	private String numEtablissement;
	private String nomEtablissement;
	 
	// donnee identite	
	private String nom;
	private String prenom;
	private Date dateNaissance;
	
	// donnee acceuil. Utilisation des  identifiants 	
	private Date  dateNomination;
	private Long corpsAcceuilId;
	private Long gradeAcceuilId;
	
	// donnee origine
	private String corpsOrigineLibelleOrId;
	private String gradeOrigineLibelleOrId;
	private Integer echelonOrigine;
	private Integer chevronOrigine;
	private Integer indiceBrutDetenue;
	private Integer indiceBrutEchelonSup;
	
	// les variables utilisés pour le controle de surface
	private String dateFormat;
	private String date_Format_Nais;
	
	// donnee zone de calcul d'une période
	private Periode periodePourCalcul;
	
	// liste des fonctions de l'agent
	private List<Fonction> lstFonction;
	
	// Liste des messages de la partie calcul
	private String[] messageCalcul;
	
	// classement calculé
	private Integer echelonCalcule;
	private Integer chevronCalcule;
	private Integer indiceBrutCalcule;
	private float ancienneteCalcule;
	
	// total des nbs de jours calculés 
	private  float nbJourFinal;
	
	private boolean isCalculDone;
	private float reste4 =Float.valueOf(2160);
    private float reste5 =Float.valueOf(1440);
	
    
    public String getDate_Format_Nais() {
		return date_Format_Nais;
	}

	public void setDate_Format_Nais(String dateFormatNais) {
		date_Format_Nais = dateFormatNais;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public float getReste5() {
		return reste5;
	}

	public void setReste5(float reste) {
		this.reste5 = reste;
	}

	public float getReste4() {
		return reste4;
	}

	public void setReste4(float reste) {
		this.reste4 = reste;
	}

	// message pour l'utilisateur
	private MESSAGE userMessage;
	
	public MESSAGE getUserMessage() {
		if(userMessage == null){
			userMessage = MESSAGE.OK;
		}
		return userMessage;
	}

	public void setUserMessage(MESSAGE userMessage) {
		this.userMessage = userMessage;
	}

	public DureeA30Jours getDisplayedNbJoursFinal() {
		return UtilsCalculette.getDureeA30Jours(nbJourFinal);
	}

	public void setNbJoursFinal(float nbJourFinal) {
		this.nbJourFinal = nbJourFinal;
	}
	
	public float getNbJoursFinal() {
		return nbJourFinal;
	}

	public Calculette(){
		messageCalcul = new String[ConstantesCalculette.DETAIL_CALCUL_MESSAGE.values().length];
		addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_ALL_MESSAGE,ConstantesCalculette.APPLICATION_ART_ALL);
		isCalculDone = false;
		
	}
	
	public String getNumEtablissement() {
		return numEtablissement;
	}
	public void setNumEtablissement(String numEtablissement) {
		this.numEtablissement = numEtablissement;
	}
	public String getNomEtablissement() {
		return nomEtablissement;
	}
	public void setNomEtablissement(String nomEtablissement) {
		this.nomEtablissement = nomEtablissement;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public Date getDateNomination() {
		return dateNomination;
	}
	public void setDateNomination(Date dateNomination) {
		this.dateNomination = dateNomination;
	}

	public Long getCorspAcceuilId() {
		return corpsAcceuilId;
	}
	public void setCorspAcceuilId(Long corpAcceuilId) {
		this.corpsAcceuilId = corpAcceuilId;
	}
	public Long getGradeAcceuilId() {
		return gradeAcceuilId;
	}
	public void setGradeAcceuilId(Long gradeAcceuilId) {
		this.gradeAcceuilId = gradeAcceuilId;
	}
	public String getCorpsOrigineLibelleOrId() {
		return corpsOrigineLibelleOrId;
	}
	public void setCorpsOrigineLibelleOrId(String corpOrigine) {
		this.corpsOrigineLibelleOrId = corpOrigine;
	}
	public String getGradeOrigineLibelleOrId() {
		return gradeOrigineLibelleOrId;
	}
	public void setGradeOrigineLibelleOrId(String gradeOrigine) {
		this.gradeOrigineLibelleOrId = gradeOrigine;
	}
	public Integer getEchelonOrigine() {
		return echelonOrigine;
	}
	public void setEchelonOrigine(Integer echelon) {
		this.echelonOrigine = echelon;
	}
	public Integer getChevronOrigine() {
		return chevronOrigine;
	}
	public void setChevronOrigine(Integer chevron) {
		this.chevronOrigine = chevron;
	}
	
	public Integer getIndiceBrutEchelonSup() {
		return indiceBrutEchelonSup;
	}
	public void setIndiceBrutEchelonSup(Integer indiceBrutEchelonSup) {
		this.indiceBrutEchelonSup = indiceBrutEchelonSup;
	}
	public Integer getIndiceBrutDetenue() {
		return indiceBrutDetenue;
	}
	public void setIndiceBrutDetenue(Integer indiceBrutDetenue) {
		this.indiceBrutDetenue = indiceBrutDetenue;
	}
	public Periode getPeriodePourCalcul() {
		if(periodePourCalcul == null){
			periodePourCalcul =  new Periode(null,null);
		}
		return periodePourCalcul;
	}
	public void setPeriodePourCalcul(Periode periodePourCalcul) {
		this.periodePourCalcul = periodePourCalcul;
	}
	
	public List<Fonction> getLstFonction() {
		if(lstFonction == null){
			lstFonction = new ArrayList<Fonction>();
		}
		return lstFonction;
	}
	
	public void setLstFonction(List<Fonction> lstFonction) {
		
		this.lstFonction = lstFonction;
	}
	
	public void sortFonctions() {
	  for(int i = 0 ; i < this.lstFonction.size() ; i++)
	    for(int j = i ; j < this.lstFonction.size() ; j++)
	      {
	       Fonction f1 = this.lstFonction.get(i) ;
	       Fonction f2 = this.lstFonction.get(j) ;
	       if(f2.getPeriode().compareTo(f1.getPeriode()) < 0) {
	          this.lstFonction.set(i,f2) ;
	          this.lstFonction.set(j,f1) ;
	         }
	      }
	}
	
	public String getDetailCalcul() {
		StringBuilder builder = new StringBuilder();
		for(String message : messageCalcul){
			if(message !=null){
				builder.append(message);
				builder.append(ConstantesCalculette.LINEFEED);
			}
		}
		//System.out.println("**--message detail--** "+builder.toString());
		return builder.toString();
	}
	
	public Integer getEchelonCalcule() {
		return echelonCalcule;
	}
	public void setEchelonCalcule(Integer echelonCalcule) {
		this.echelonCalcule = echelonCalcule;
	}
	public Integer getChevronCalcule() {
		return chevronCalcule;
	}
	public void setChevronCalcule(Integer chevronCalcule) {
		this.chevronCalcule = chevronCalcule;
	}
	public Integer getIndiceBrutCalcule() {
		return indiceBrutCalcule;
	}
	public void setIndiceBrutCalcule(Integer indiceBrutCalcule) {
		this.indiceBrutCalcule = indiceBrutCalcule;
	}
	public float getAncienneteCalcule() {
		return ancienneteCalcule;
	}
	public void setAncienneteCalcule(float ancienneteCalcule) {
		this.ancienneteCalcule = ancienneteCalcule;
	}
	
	public DureeA30Jours getDureeAncienneteCalcule() {
		return UtilsCalculette.getDureeA30Jours(this.ancienneteCalcule);
	}
	
	public void addMessage(DETAIL_CALCUL_MESSAGE pMessage, String intituleMessage){
		messageCalcul[pMessage.ordinal()] = intituleMessage;
	}
	
	public String toString(){
		
		StringBuilder strBuilder = new StringBuilder();
		String tmpStr;
		
		strBuilder.append("numEtab :" + numEtablissement);
		strBuilder.append("\n");
		strBuilder.append("nomEtab :" + nomEtablissement);
		
		strBuilder.append("\n");
		strBuilder.append("nom :" + nom);
		
		strBuilder.append("\n");
		strBuilder.append("prenom :" + prenom);
		strBuilder.append("\n");
		strBuilder.append("dateNaissance :" + Conversion.formatDate(dateNaissance, Constantes.DATE_FORMAT_STRING));
		
		strBuilder.append("\n");
		strBuilder.append("dateNomination :" + Conversion.formatDate(dateNomination, Constantes.DATE_FORMAT_STRING));
		strBuilder.append("\n");
		tmpStr = (corpsAcceuilId != null) ?  corpsAcceuilId.toString():"null";
		strBuilder.append("corpsAcceuilId :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (gradeAcceuilId != null) ?  gradeAcceuilId.toString():"null";
		strBuilder.append("gradeAcceuilId :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (corpsOrigineLibelleOrId != null) ?  corpsOrigineLibelleOrId.toString():"null";
		strBuilder.append("corpsOrigineLibelleOrId :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (gradeOrigineLibelleOrId != null) ?  gradeOrigineLibelleOrId.toString():"null";
		strBuilder.append("gradeOrigineLibelleOrId :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (echelonOrigine != null) ?  echelonOrigine.toString():"null";
		strBuilder.append("echelonOrigine :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (chevronOrigine != null) ?  chevronOrigine.toString():"null";
		strBuilder.append("chevronOrigine :" +  tmpStr  );

		
		strBuilder.append("\n");		
		tmpStr = (indiceBrutDetenue != null) ?  indiceBrutDetenue.toString():"null";
		strBuilder.append("indiceBrutDetenue :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (indiceBrutEchelonSup != null) ?  indiceBrutEchelonSup.toString():"null";
		strBuilder.append("indiceBrutEchelonSup :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (periodePourCalcul != null) ?  periodePourCalcul.toString():"null";
		strBuilder.append("periodePourCalcul :" +  tmpStr  );
		
		strBuilder.append("\n");
		strBuilder.append("lst fonction \n");
		
		for(Fonction f : lstFonction){
			strBuilder.append(f.toString() + "\n");
		}
		
		for(String str : messageCalcul){
			strBuilder.append ("message : " + str + "\n");
		}
		
		strBuilder.append("\n");		
		tmpStr = (echelonCalcule != null) ?  echelonCalcule.toString():"null";
		strBuilder.append("echelonCalcule :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (chevronCalcule != null) ?  chevronCalcule.toString():"null";
		strBuilder.append("chevronCalcule :" +  tmpStr  );
		
		strBuilder.append("\n");		
		tmpStr = (indiceBrutCalcule != null) ?  indiceBrutCalcule.toString():"null";
		strBuilder.append("indiceBrutCalcule :" +  tmpStr  );
		
		strBuilder.append("\n");
		strBuilder.append("ancienneteCalcule :" +  ancienneteCalcule  );
		
		strBuilder.append("\n");
		strBuilder.append("nbJourFinal :" +  nbJourFinal  );
		
		strBuilder.append("\n");
		strBuilder.append("userMessage :" +  userMessage.getLabel());
		
		return strBuilder.toString();
	}
	
	public boolean isCalculDone() {
		return isCalculDone;
	}

	public void setCalculDone(boolean isCalculDone) {
		this.isCalculDone = isCalculDone;
	}
	
	public void resetCalcul(){
		isCalculDone = false;		
	    messageCalcul = new String[ConstantesCalculette.DETAIL_CALCUL_MESSAGE.values().length];		
		echelonCalcule = null ;
		chevronCalcule = null ;
		indiceBrutCalcule = null ;
		ancienneteCalcule = 0;		 
		nbJourFinal = 0;	
		reste4 =Float.valueOf(2160);
	    reste5 =Float.valueOf(1440);
		for(Fonction fonct : this.lstFonction){
			fonct.setNbJoursArticle(0);
			fonct.setNbJoursPrisEnCompte(0);
			fonct.setNbJoursResultat(0);
		}
		userMessage = MESSAGE.OK;
	}
	public String message() {
		List<Fonction> selectionArticle8 = new ArrayList<Fonction>();
		   List<Fonction> selectionArticle4 = new ArrayList<Fonction>();
		   List<Fonction> selectionArticle15 = new ArrayList<Fonction>();
		   // si article 15-II et article 8 alors message erreur
		   List<Fonction> fonctions=getLstFonction();
		   for (Fonction f : fonctions) {
			if (ConstantesCalculette.ARTICLE.ARTICLE_8.isArticleOK(f.getCodeArticle())) {
				selectionArticle8.add(f);
			}if(ConstantesCalculette.ARTICLE.ARTICLE_15_II.isArticleOK(f.getCodeArticle())) {
				selectionArticle15.add(f);
				
			}

			if (selectionArticle8.size()>=1 && selectionArticle15.size()>=1) {
				return Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_8;
			}
			
			
			// si article 15-II et article 4 alors message erreur
			if (ConstantesCalculette.ARTICLE.ARTICLE_4.isArticleOK(f.getCodeArticle())) {
				selectionArticle4.add(f);
			}if(ConstantesCalculette.ARTICLE.ARTICLE_15_II.isArticleOK(f.getCodeArticle())) {
				selectionArticle15.add(f);
				
			}

			if (selectionArticle4.size()>=1 && selectionArticle15.size()>=1) {
				return Messages.SELECTION_ARTICLE_15_II_ET_ARTICLE_4;
			}
			 if(f.getDateDeb()!=null && f.getDateFin()!=null && f.getDateDeb().equals(f.getDateFin())&& f.getDureeRetenueA30Jours().getNbJoursTotal()>0 ){
				return Messages.DUREE_RETENUE_MANDATORY_FEV_WITH_3;
			}
			 
		   }
		   return null;
		}

}
