package fr.gouv.education.antares.calc.objets;

import java.util.Date;

import fr.gouv.education.antares.calc.outils.UtilsCalculette;
import fr.gouv.education.antares.objets.ObjetBase;



public class Fonction extends ObjetBase {
	
	private Periode periode;
	private String  codeFonction;
	private String  liblFonction;
	private String  codeArticle;
	private String  observations ;
	private Integer quotiteReele;
	private Integer quotiteRetenue;	
		
	private float nbJoursPrisEnCompte;
	private float nbJoursArticle;
	//private float nbJoursRetenus;
	private float nbJoursResultat;
	
	private DureeA30Jours dureeRetenueA30Jours;
	
	// utilise pour permettre de faire le tri via le Sort
	private Date dateDeb;
	private Date dateFin;
	
	
	
	public Fonction(){
		
	}
	
	public String toString(){
		StringBuilder strBuilder = new StringBuilder();
		
				
		String tmpStr = (periode != null) ?  periode.toString():"null";
		strBuilder.append("periode :" +  tmpStr  );
		
		strBuilder.append("\n");			
		strBuilder.append("codeFonction :" +  codeFonction  );
		
		strBuilder.append("\n");			
		strBuilder.append("codeArticle :" +  codeArticle  );
		
		strBuilder.append("\n");
		tmpStr = (quotiteReele != null) ?  quotiteReele.toString():"null";
		strBuilder.append("quotiteReele :" +  tmpStr  );
		
		strBuilder.append("\n");
		tmpStr = (quotiteRetenue != null) ?  quotiteRetenue.toString():"null";
		strBuilder.append("quotiteRetenue :" +  tmpStr  );
		
		strBuilder.append("\n");
		strBuilder.append("nbJoursPrisEnCompte :" +  nbJoursPrisEnCompte  );
		strBuilder.append("\n");
		strBuilder.append("nbJoursArticle :" +  nbJoursArticle  );
		strBuilder.append("\n");
		strBuilder.append("nbJoursResultat :" +  nbJoursResultat  );
		
		strBuilder.append("\n");
		tmpStr = (dureeRetenueA30Jours != null) ?  dureeRetenueA30Jours.getNbAnnees()+ ":" + dureeRetenueA30Jours.getNbMois() + ":" + dureeRetenueA30Jours.getNbJours() :"null";
		strBuilder.append("quotiteRetenue :" +  tmpStr  );
		
		return strBuilder.toString();
		
	}


	public Periode getPeriode() {
		return periode;
	}


	public void setPeriode(Periode periode) {
		this.periode = periode;
		if(periode != null){
			dateDeb = periode.getDateDebut();
			dateFin = periode.getDateFin();
		}
	}
	
	public Date getDateDeb(){
		return dateDeb;
	}
	
	public Date getDateFin(){
		return dateFin;
	}
	
	


	public String getCodeFonction() {
		return codeFonction;
	}


	public void setCodeFonction(String codeFonction) {
		this.codeFonction = codeFonction;
	}

	public String getLibelleFonction() {
	    return liblFonction;
	}


	public void setLibelleFonction(String liblFonction) {
	    this.liblFonction = liblFonction;
	}


	public String getCodeArticle() {
		return codeArticle;
	}


	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}
	
    public String getObservations() {
	  return this.observations;
    }


    public void setObservations(String observations) {
      this.observations = observations;
    }


	public Integer getQuotiteReelle() {
		return quotiteReele;
	}


	public void setQuotiteReele(Integer quotiteReele) {
		this.quotiteReele = quotiteReele;
	}

	public Integer getQuotiteRetenue() {
		return quotiteRetenue;
	}

	public void setQuotiteRetenue(Integer quotiteRetenue) {
		this.quotiteRetenue = quotiteRetenue;
	}

	public float getNbJoursPrisEnCompte() {
		return nbJoursPrisEnCompte;
	}

	public void setNbJoursPrisEnCompte(float nbJoursPrisEnCompte) {
		this.nbJoursPrisEnCompte = nbJoursPrisEnCompte;
	}


	public float getNbJoursArticle() {
		return nbJoursArticle;
	}

	public void setNbJoursArticle(float nbJoursArticle) {
		this.nbJoursArticle = nbJoursArticle;
	}


	public float getNbJoursRetenus() {
		if(dureeRetenueA30Jours != null){
			return  dureeRetenueA30Jours.getNbJoursTotal();
		} 
		return 0;
	}



	public float getNbJoursResultat() {
		return nbJoursResultat;
	}


	public void setNbJoursResultat(float nbJoursResultat) {
		this.nbJoursResultat = nbJoursResultat;
	}
	
	public DureeA30Jours getDisplayedDureePriseEnCompte(){
		return UtilsCalculette.getDureeA30Jours(nbJoursPrisEnCompte);		
	}
	
	public DureeA30Jours getDisplayedDureeArticle(){
		return UtilsCalculette.getDureeA30Jours(nbJoursArticle);		
	}
	
	public DureeA30Jours getDisplayedDureeResultat(){
		return UtilsCalculette.getDureeA30Jours(nbJoursResultat);		
	}
	
	
	public void setDureeRetenueA30Jours(DureeA30Jours pDureeRetenue){		
		dureeRetenueA30Jours = pDureeRetenue;	
	}
	
	public DureeA30Jours getDureeRetenueA30Jours(){
		if(dureeRetenueA30Jours == null){
			dureeRetenueA30Jours = new DureeA30Jours();
		}
		return dureeRetenueA30Jours;
	}
	

   
	

}
