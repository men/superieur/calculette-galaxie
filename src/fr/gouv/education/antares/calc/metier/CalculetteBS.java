package fr.gouv.education.antares.calc.metier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.simpleLog;
import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.calc.ConstantesCalculette.ARTICLE;
import fr.gouv.education.antares.calc.ConstantesCalculette.DETAIL_CALCUL_MESSAGE;
import fr.gouv.education.antares.calc.ConstantesCalculette.FONCTION_BITSET_INDX;
import fr.gouv.education.antares.calc.ConstantesCalculette.FORM_CALCULETTE_NAME;
import fr.gouv.education.antares.calc.ConstantesCalculette.MESSAGE;
import fr.gouv.education.antares.calc.ConstantesCalculette.VALEUR_CALCUL;
import fr.gouv.education.antares.calc.objets.Calculette;
import fr.gouv.education.antares.calc.objets.DataCalculetteExport;
import fr.gouv.education.antares.calc.objets.DonneeOrigineExport;
import fr.gouv.education.antares.calc.objets.DureeA30Jours;
import fr.gouv.education.antares.calc.objets.Fonction;
import fr.gouv.education.antares.calc.objets.FonctionExport;
import fr.gouv.education.antares.calc.objets.ObjectFactory;
import fr.gouv.education.antares.calc.objets.Periode;
import fr.gouv.education.antares.calc.outils.UtilsCalculette;
import fr.gouv.education.antares.calc.outils.ref.metier.TableRefHelper;
import fr.gouv.education.antares.calc.outils.ref.objects.CorpsRef;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionArticleRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GradeRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GrilleIndiciaireRef;
import fr.gouv.education.antares.outils.Conversion;
import fr.gouv.education.antares.outils.StringUtils;

public class CalculetteBS {

	private final static CalculetteBS singleton = new CalculetteBS();

	public static CalculetteBS getSingleton() {
		return singleton;
	}

	private CalculetteBS() {

	}

	/**
	 * @param pCalculette
	 * @return MESSAGE
	 */
	public MESSAGE controleData(Calculette pCalculette) {

		MESSAGE message = MESSAGE.OK;
		simpleLog.println(">>> controleData");
		try {
			// TODO check format des champs de saisie

			// supprimer les messages antérieures
			pCalculette.setUserMessage(MESSAGE.OK);

			// Vérifie que les champs obligatoires sont renseignés
			message = checkMandatoryField(pCalculette);
			if (message.getLevel().isErrorLevel()) {
				simpleLog.println("<<< controleData (checkMandatoryField) : "
						+ message.getLabel());
				return message;
			}

			// Vérifie cohérence donnée de la zone fonction
			message = checkFonction(pCalculette);
			if (message.getLevel().isErrorLevel()) {
				simpleLog.println("<<< controleData (checkFonction) : "
						+ message.getLabel());
				return message;

			}

			// Vérifie cohérence donnée de la zone origine
			// il doit être fait après la vérif des fonctions
			message = checkDataOrigine(pCalculette);
			if (message.getLevel().isErrorLevel()) {
				simpleLog.println("<<< controleData (dataOrigine) : "
						+ message.getLabel());
				return message;
			}
		} catch (Exception ex) {
			simpleLog.println("<<< controleData (Exception) : "
					+ ex.getMessage());
			ex.printStackTrace();
		}

		simpleLog.println("<<< controleData : " + message.getLabel());
		return message;
	}

	public MESSAGE calculeAll(Calculette pCalculette) {

		simpleLog.println(">>> calculeAll ");
		MESSAGE tmpMessage = calculDuree(pCalculette);

		if (tmpMessage.getLevel().isErrorLevel()) {
			simpleLog.println("<<< calculeAll " + tmpMessage.getLabel());
			return tmpMessage;
		}
		calculClassement(pCalculette);

		simpleLog.println("<<< calculeAll done : nbJoursFinal : "
				+ pCalculette.getNbJoursFinal());
		pCalculette.setCalculDone(true);
		return tmpMessage;
	}

	/**
	 * Traitement permettant d'importer les donnees de la calculette a partir du
	 * fichier xml
	 * 
	 * @param pCalculette
	 * @param pFile
	 * @return
	 */
	public MESSAGE importDatasFromFile(Calculette pCalculette, File pFile) {
		MESSAGE tmpMessage = MESSAGE.OK;
		simpleLog.println(">>> importDatasFromFile");

		JAXBContext jaxbContext;
		DataCalculetteExport dataCalExport;

		String xsdFileName = Constantes.DATA_CLASSEMENT_XSD_FILE;
		Schema schema;
		SchemaFactory sf;
		Unmarshaller unmarshaller;

		try {
			//sf = SchemaFactory
				//	.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
			//schema = sf.newSchema(new File(xsdFileName));
			jaxbContext = JAXBContext.newInstance(DataCalculetteExport.class);
			unmarshaller = jaxbContext.createUnmarshaller();
			// unmarshaller.setEventHandler(new
			// CalculetteClassementValidationEventHandler());
			//unmarshaller.setSchema(schema);

			dataCalExport = (DataCalculetteExport) unmarshaller
					.unmarshal(pFile);
			mapDatasCalculetteToCalExport(pCalculette, dataCalExport);

		} catch (JAXBException e) {
			simpleLog.println(" importDatasFromFile JAXBException : "
					+ e.getMessage());
			e.printStackTrace();
		} /*catch (SAXException e) {
			simpleLog.println(" importDatasFromFile SAXException : "
					+ e.getMessage());
			e.printStackTrace();
		}*/

		simpleLog.println("<<< importDatasFromFile done ");
		return tmpMessage;
	}

	/**
	 * traitement permettant d'exporter les donnes de la calculette vers un
	 * fichier xml
	 * 
	 * @param pCalculette
	 * @param pFile
	 * @return
	 */
	public MESSAGE exportDatasToFile(Calculette pCalculette, File pFile) {
		simpleLog.println(">>> exportDatasToFile ");
		MESSAGE tmpMessage = MESSAGE.OK;
		FileOutputStream outputFile = null;
		try {

			Calendar today;
			JAXBContext context;
			Marshaller marshaller;

			//SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);

			String xsdFileName = Constantes.DATA_CLASSEMENT_XSD_FILE;
			//Schema schema = sf.newSchema(new File(xsdFileName));

			// Then you use the setSchema method to identify the JAXP 1.3 Schema
			// object (in this case, the po.xsd) that should be used to validate
			// subsequent unmarshal operations. If you pass null into this
			// method, it disables validation.
			ObjectFactory factory = new ObjectFactory();

			DataCalculetteExport dataCalExport = factory.createDataCalculetteExport();

			mapDatasCalExportToCalculette(pCalculette, dataCalExport);

			outputFile = new FileOutputStream(pFile);
			context = JAXBContext.newInstance(DataCalculetteExport.class);

			marshaller = context.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);

			//marshaller.setSchema(schema);
			marshaller.marshal(dataCalExport, outputFile);

			String str = outputFile.toString();
			System.out.println("xml genere = " + str);
			str = pFile.toString();
			System.out.println("xml genere = " + str);

		} /*catch (SAXException e) {
			simpleLog.println("exportDatasToFile SAXException : "
					+ e.getMessage());
			e.printStackTrace();
		}*/ catch (FileNotFoundException e) {
			simpleLog.println("exportDatasToFile FileNotFoundException : "
					+ e.getMessage());
			e.printStackTrace();
		} catch (JAXBException e) {
			simpleLog.println("exportDatasToFile JAXBException : "
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (outputFile != null) {
					outputFile.flush();
					outputFile.close();
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		simpleLog.println("<<< exportDatasToFile done ");

		return tmpMessage;

	}

	/**
	 * Traitement permettant de calculer les différentes durées 1- duree prise
	 * en compte 2- duree article 3- duree résultat
	 * 
	 * @param pCalculette
	 * @return MESSAGE
	 */
	private MESSAGE calculDuree(Calculette pCalculette) {

		simpleLog.println(">>> calculDuree");

		pCalculette.sortFonctions();
		List<Fonction> lstFonct = pCalculette.getLstFonction();
		BitSet bitSet = new BitSet(FONCTION_BITSET_INDX.values().length);
		MESSAGE tmpMessage = MESSAGE.OK;

		for (Fonction f : lstFonct) {
			// traitement permettant d'avoir des indicateurs sur la totalité des
			// fonctions
			tmpMessage = listeIndicateurArticle(bitSet, f);
			if (tmpMessage.getLevel().isErrorLevel()) {
				simpleLog.println("<<< calculDuree (listeIndicateurArticle)"
						+ tmpMessage.getLabel());
				return tmpMessage;
			}

			// calcul de la duree prise en compte
			tmpMessage = calculDureePriseEnCompte(f);
			if (tmpMessage.getLevel().isErrorLevel()) {
				simpleLog.println("<<< calculDuree (calculDureePriseEnCompte)"
						+ tmpMessage.getLabel());
				return tmpMessage;
			}

			// calcul de la duree article
			tmpMessage = calculDureeArticle(f);
			if (tmpMessage.getLevel().isErrorLevel()) {
				simpleLog.println("<<< calculDuree (calculDureeArticle)"
						+ tmpMessage.getLabel());
				return tmpMessage;
			}
		}

		// R402 : Détermination du résultat de chaque ligne
		// si durée retenue != null et non vide
		// alors RESULTAT = DUREE_RETENUE
		// sinon RESULTAT = DUREE_article
		for (Fonction f : lstFonct) {
			if (!f.getDureeRetenueA30Jours().isEmpty()) {
				f.setNbJoursResultat(f.getNbJoursRetenus());
			} else {
				f.setNbJoursResultat(f.getNbJoursArticle());
			}
			// simpleLog.println(" - fonction " +
			// f.getCodeFonction()+"/"+f.getCodeArticle()
			// +"   :   nbJours="+f.getNbJoursResultat() );
		}

		/*
		 * bug article 10 int index=0; float nbJours=0; for(Fonction f:
		 * lstFonct){ if(index!=lstFonct.size()-1){
		 * nbJours=f.getNbJoursResultat(); f.setNbJoursResultat(0); }else{
		 * f.setNbJoursResultat(nbJours); } index++; }
		 */

		// traitement spécifique pour les articles 4 et 5
		// R415
		tmpMessage = calculResultatArt4Art5(bitSet, pCalculette);
		if (tmpMessage.getLevel().isErrorLevel()) {
			simpleLog.println("<<< calculDuree (calculResultatArt4Art5)"
					+ tmpMessage.getLabel());
			return tmpMessage;
		}

		// traitement spécifique pour les articles 10 et 12 et 6
		// R420
		tmpMessage = calculResultatArt10Art12Art6(bitSet, pCalculette);
		if (tmpMessage.getLevel().isErrorLevel()) {
			simpleLog.println("<<< calculDuree (calculResultatArt10Art12)"
					+ tmpMessage.getLabel());
			return tmpMessage;
		}

		// traitement spécifique pour non cumul de l'article 3
		// R430
		tmpMessage = calculResultatArt3(bitSet, pCalculette);
		if (tmpMessage.getLevel().isErrorLevel()) {
			simpleLog.println("<<< calculDuree (calculResultatArt3)"
					+ tmpMessage.getLabel());
			return tmpMessage;
		}

		// R440
		tmpMessage = calculDureeFinale(pCalculette);
		if (tmpMessage.getLevel().isErrorLevel()) {
			simpleLog.println("<<< calculDuree (calculDureeFinale)"
					+ tmpMessage.getLabel());
			return tmpMessage;
		}

		int index10 = 0;
		int index12 = 0;

		float nbJoursArticle10 = 0;
		float nbJoursArticle12 = 0;

		List<Fonction> liste10 = new ArrayList<Fonction>();
		List<Fonction> liste12 = new ArrayList<Fonction>();
		List<Fonction> listeFonction = new ArrayList<Fonction>();

		for (Fonction f : lstFonct) {
			if (ConstantesCalculette.ARTICLE.ARTICLE_10.isArticleOK(f
					.getCodeArticle())) {
				liste10.add(f);
			} else if (ConstantesCalculette.ARTICLE.ARTICLE_12.isArticleOK(f
					.getCodeArticle())) {
				liste12.add(f);
			} else {
				listeFonction.add(f);
			}

		}

		for (Fonction f : liste10) {

			if (index10 != liste10.size() - 1) {
				nbJoursArticle10 = nbJoursArticle10 + f.getNbJoursResultat();
				f.setNbJoursResultat(0);
				listeFonction.add(f);
			} else {
				f.setNbJoursResultat(nbJoursArticle10 + f.getNbJoursResultat());
				listeFonction.add(f);
			}

			index10++;
		}

		for (Fonction f : liste12) {

			if (index12 != liste12.size() - 1) {
				nbJoursArticle12 = nbJoursArticle12 + f.getNbJoursResultat();
				f.setNbJoursResultat(0);
				listeFonction.add(f);
			} else {
				f.setNbJoursResultat(nbJoursArticle12 + f.getNbJoursResultat());
				listeFonction.add(f);
			}
			index12++;
		}

		lstFonct = listeFonction;

		simpleLog.println("<<< calculDuree");
		return tmpMessage;
	}

	/**
	 * Vérifie cohérence donnée de la zone origine
	 */
	private MESSAGE checkDataOrigine(Calculette pCalculette) {
		MESSAGE message = MESSAGE.OK;
		// R44
		if ((pCalculette.getIndiceBrutDetenue() != null)
				&& (pCalculette.getIndiceBrutEchelonSup() != null)) {
			if (pCalculette.getIndiceBrutDetenue().intValue() > pCalculette
					.getIndiceBrutEchelonSup().intValue()) {
				return MESSAGE.IBR_DET_SUP_IBR_SUP;
			}
		}

		// R200
		List<Fonction> lst = pCalculette.getLstFonction();
		String tmpCodeArticle;
		boolean isArticle3 = false;
		for (Fonction fonct : lst) {
			tmpCodeArticle = fonct.getCodeArticle();
			if ((ARTICLE.ARTICLE_3.isArticleOK(tmpCodeArticle))) {
				isArticle3 = true;
				break;
			}
		}

		boolean isOK = true;
		if (isArticle3) {
			isOK = StringUtils.ok(pCalculette.getCorpsOrigineLibelleOrId())
					&& StringUtils.ok(pCalculette.getGradeOrigineLibelleOrId())
					&& StringUtils.ok(pCalculette.getGradeOrigineLibelleOrId())
					&& pCalculette.getEchelonOrigine() != null
					&& pCalculette.getIndiceBrutDetenue() != null
					&& pCalculette.getIndiceBrutEchelonSup() != null;
			if (!isOK)
				return MESSAGE.ORIGINE_DATA_MANDATORY;
		}

		return message;
	}

	/**
	 * Vérifie que les champs obligatoires sont renseignés
	 * 
	 * @param pCalculette
	 * @return
	 */
	private MESSAGE checkMandatoryField(Calculette pCalculette) {

		MESSAGE message = MESSAGE.OK;

		// nom
		if (!StringUtils.ok(pCalculette.getNom()))
			message = MESSAGE.NOM_MANDATORY;
		else if (pCalculette.getNom().length() > 25)
			message = MESSAGE.NOM_MAX_LENGHT;

		// prenom
		else if (!StringUtils.ok(pCalculette.getPrenom()))
			message = MESSAGE.PRENOM_MANDATORY;
		else if (pCalculette.getPrenom().length() > 25)
			message = MESSAGE.PRENOM_MAX_LENGHT;
		else {
			String dateFormatNais = pCalculette.getDate_Format_Nais();
			String dateFormat = pCalculette.getDateFormat();
			if ((!dateFormatNais
					.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$")
					|| dateFormatNais.substring(3, 5).equals("00") || dateFormatNais
					.substring(0, 2).equals("00"))
					&& !dateFormatNais.isEmpty())
				message = MESSAGE.CONTROLE_FORMAT_DATE_NAIS;
			// date naissance
			else if (pCalculette.getDateNaissance() == null)
				message = MESSAGE.DATE_NAISSANCE_MANDATORY;

			else if ((!dateFormat
					.matches("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$")
					|| dateFormat.substring(3, 5).equals("00") || dateFormat
					.substring(0, 2).equals("00"))
					&& !dateFormat.isEmpty())
				message = MESSAGE.CONTROLE_FORMAT_DATE_NOMIN;
			// date de nomination
			else if (pCalculette.getDateNomination() == null)
				message = MESSAGE.DATE_NOMINATION_MANDATORY;

			// corps d'acceuil
			else if (pCalculette.getCorspAcceuilId() == null)
				message = MESSAGE.CORPS_ACCEUIL_MANDATORY;

			// grade d'acceuil
			else if (pCalculette.getGradeAcceuilId() == null)
				message = MESSAGE.GRADE_ACCEUIL_MANDATORY;
		}

		return message;
	}

	/**
	 * Vérifie cohérence donnée de la zone fonction
	 */
	private MESSAGE checkFonction(Calculette pCalculette) {

		Periode tmpPeriode;
		String tmpCodeArticle;
		boolean isOK;
		DureeA30Jours durA30Jrs;
		int nbJours;
		int nbMois;

		MESSAGE tmpMessage;

		// compteurs pour avoir le nb d'occurence de certains articles
		int nbOccurenceArt_SNA = 0;
		int nbOccurenceArt_3 = 0;
		int nbOccurenceArt_11 = 0;
		int nbOccurenceArt_15_II = 0;
		int nbOccurenceArt_5_1 = 0;

		// R213 init du bitset
		BitSet bitSet = new BitSet(FONCTION_BITSET_INDX.values().length);

		List<Fonction> fonctions = pCalculette.getLstFonction();
		MESSAGE message = MESSAGE.OK;

		// 1ere phase du controle
		// on vérifie que chaque fonction est cohérente de facon individuelle
		// on init aussi des indicateurs qui seront utiilsés par la suite pour
		// vérifier
		// la cohérence générale

		for (Fonction f : fonctions) {
			tmpPeriode = f.getPeriode();
			tmpCodeArticle = f.getCodeArticle();

			// R204 : quotite reelle obligatoire si la période est renseignée
			if (!tmpPeriode.isEmpty() && f.getQuotiteReelle() == null) {
				return MESSAGE.QUOTITE_REELLE_MANDATORY;
			}

			// R52 R53
			tmpMessage = checkQuotite(f.getQuotiteReelle(), f
					.getQuotiteRetenue());
			if (tmpMessage.getLevel().isErrorLevel()) {
				return tmpMessage;
			}

			// R54 R55
			// nb mois < 12
			// nb jours < 30
			durA30Jrs = f.getDureeRetenueA30Jours();
			if (durA30Jrs != null) {
				nbJours = durA30Jrs.getNbJours();
				nbMois = durA30Jrs.getNbMois();
				if (!(nbMois < 12) || !(nbJours < 30)) {
					return MESSAGE.DUREE_A_30_JRS_BAD_VALUE;
				}
			}

			// R200 Si l'article 3 a été sélectionné sur au moins une ligne du
			// tableau des fonctions exercées,
			// alors les informations sur le classement déorigine sont
			// obligatoires
			if (ConstantesCalculette.ARTICLE.ARTICLE_3
					.isArticleOK(tmpCodeArticle)) {
				// TODO cbu vérifier s'il faut le mettre ou pas controler aussi
				// le chevron origine
				// pCalculette.getChevronOrigine();

				isOK = StringUtils.ok(pCalculette.getCorpsOrigineLibelleOrId())
						&& StringUtils.ok(pCalculette
								.getGradeOrigineLibelleOrId())
						&& (pCalculette.getIndiceBrutDetenue() != null)
						&& (pCalculette.getIndiceBrutEchelonSup() != null)
						&& pCalculette.getEchelonOrigine() > 0;
				if (!isOK) {
					return MESSAGE.CLASSEMENT_ORIGINE_MANDATORY;
				}
			}

			/*
			 * FGU Règle de controle des dates de début et fin date début <=
			 * date fin si article 3 sinon date début < date fin
			 */
			if (f.getDateDeb() != null && f.getDateFin() != null) {
				if (ConstantesCalculette.ARTICLE.ARTICLE_3
						.isArticleOK(tmpCodeArticle)) {
					isOK = f.getDateDeb().before(f.getDateFin())
							|| f.getDateDeb().equals(f.getDateFin());
					if (!isOK) {
						return MESSAGE.DATE_DEBUT_FIN_ARTICLE_3;
					}
				} else {

					isOK = f.getDateDeb().before(f.getDateFin());

					if (!isOK) {
						return MESSAGE.DATE_DEBUT_FIN_AUTRES_ARTICLES;
					}
				}
			}
			/*
			 * FGU dans le cas du Chevaucheement des dates Règle de controle des
			 * dates de début et fin en cas de chevauchement date début 1 = date
			 * début 1 et date fin 1 = date fin 2 le total des quotités doit
			 * être inferieur ou égale à 100%
			 */

			/*
			 * if (f.getDateDeb().equals(dateDebut)&&
			 * f.getDateFin().equals(dateFin)) { int quantiteReelle1 =
			 * f.getQuotiteReelle() != null ? f.getQuotiteReelle() : 0; int
			 * quantiteReelle2 = quantiteReelle != null ? quantiteReelle: 0;
			 * 
			 * boolean isOk = (quantiteReelle1+quantiteReelle2)<=100?true:false;
			 * 
			 * if (!isOk) { MESSAGE.SI_QUANTITE_NO_EGALE.setArg(" (" +
			 * Conversion.formatDate(f.getDateDeb(),
			 * Constantes.DATE_FORMAT_STRING) + " au " +
			 * Conversion.formatDate(f.getDateFin(),
			 * Constantes.DATE_FORMAT_STRING) + ")"); return
			 * MESSAGE.SI_QUANTITE_NO_EGALE; }
			 * 
			 * } else if (dateFin != null && f.getDateDeb().before(dateFin)) {
			 * return MESSAGE.SI_DATE_DEBUT_RECENTE_DATE_FIN; } else if
			 * (f.getDateDeb().after(f.getDateFin())) { return
			 * MESSAGE.SI_DATE_DEBUT_ANTERIEUR_DATE_FIN; }
			 * 
			 * dateDebut = f.getDateDeb(); dateFin = f.getDateFin();
			 * quantiteReelle = f.getQuotiteReelle();
			 */
			if (f.getDateDeb() != null && f.getDateFin() != null) {
				if (ConstantesCalculette.ARTICLE.ARTICLE_3
						.isArticleOK(tmpCodeArticle)) {
					isOK = f.getDateDeb().before(f.getDateFin())
							|| f.getDateDeb().equals(f.getDateFin());
					if (!isOK) {
						return MESSAGE.DATE_DEBUT_FIN_ARTICLE_3;
					}
				} else {

					isOK = f.getDateDeb().before(f.getDateFin());

					if (!isOK) {
						return MESSAGE.DATE_DEBUT_FIN_AUTRES_ARTICLES;
					}
				}
			}
			// si article <> libre ET article <> 15-II ET article <> 5-1 alors
			if (!ConstantesCalculette.ARTICLE.ARTICLE_15_II
					.isArticleOK(tmpCodeArticle)
					&& !ConstantesCalculette.ARTICLE.ARTICLE_LIBRE
							.isArticleOK(tmpCodeArticle) 
					&& !ConstantesCalculette.ARTICLE.ARTICLE_5_1
							.isArticleOK(tmpCodeArticle)) {
				// R202
				// date obligatoire
				// vérifie que les dates sont renseignée
				if (tmpPeriode.isEmpty())
					return MESSAGE.PERIODE_MANDATORY;

				if (f.getPeriode().getNbJoursA30Jours() == 0
						&& (tmpPeriode.getDateDebut() == null || tmpPeriode
								.getDateFin() == null))
					return MESSAGE.PERIODE_MANDATORY;

				// R21 : dateDebut < date fin
				if (tmpPeriode.getDateDebut() != null
						&& tmpPeriode.getDateFin() != null
						&& tmpPeriode.getDateDebut().after(
								tmpPeriode.getDateFin()))
					return MESSAGE.PERIODE_MANDATORY;
			}

			// R203 appliquable si code fonction non null
			isOK = false;
			if (f.getCodeFonction() != null && !f.getCodeFonction().equals("")) {
				String fs = "";

				List<FonctionArticleRef> lst = TableRefHelper
						.getFonctionArticleByFonctionId(f.getCodeFonction());

				for (FonctionArticleRef ref : lst) {
					isOK |= ref.getLibelleCourtArticle().equals(
							f.getCodeArticle());
					fs += ref.getLibelleCourtArticle() + ",";
				}

				if (!isOK) {
					// MESSAGE.FONCTION_ARTICLE_DOESNT_FIT.setArg(" ( " +
					// TableRefHelper.getFonctionById(f.getCodeFonction()).getLibelleLong()+
					// ":"+
					// TableRefHelper.getArticleById(f.getCodeArticle()).getLibelleLong()
					// +" )");
					MESSAGE.FONCTION_ARTICLE_DOESNT_FIT.setArg(" ( "
							+ f.getCodeArticle() + " not in [" + fs + "] )");
					return MESSAGE.FONCTION_ARTICLE_DOESNT_FIT;
				}
			}

			// R207
			// si article == 8 OU article == 15-II alors
			// grade_acceuil == MCF

			if (ConstantesCalculette.ARTICLE.ARTICLE_15_II
					.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_8
							.isArticleOK(tmpCodeArticle)) {

				// R207 grade_acceuil == MCF
				GradeRef gradeRef = TableRefHelper.getGradeById(pCalculette
						.getGradeAcceuilId());

				if (gradeRef == null
						|| !ConstantesCalculette.isMaitreDeConf(gradeRef
								.getGradeAcceuil()))
					return MESSAGE.MCF_GRADE_MANDATORY;

			}

			// R206
			// si article == 15_II OU article == SNA OU article == Non retenu OU article == 5-1
			// durée retenue interdite
			if (ConstantesCalculette.ARTICLE.ARTICLE_15_II
					.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_NON_RETENU
							.isArticleOK(tmpCodeArticle)
							||  ConstantesCalculette.ARTICLE.ARTICLE_5_1
							.isArticleOK(tmpCodeArticle)) {
				if (!f.getDureeRetenueA30Jours().isEmpty())
					return MESSAGE.DUREE_RETENUE_FORBIDDEN;
			}

			// R206
			// si article == 3 OU article == libre
			// alors durée retenue obligatoire
			if (ConstantesCalculette.ARTICLE.ARTICLE_3
					.isArticleOK(tmpCodeArticle)
					&& f.getDateDeb() != null && f.getDateFin() != null) {

				if (f.getDureeRetenueA30Jours().isEmpty()
						&& !f.getDateDeb().equals(f.getDateFin())) {
					return MESSAGE.DUREE_RETENUE_MANDATORY_WITH_3;
				}
				if (f.getDateDeb().equals(f.getDateFin())
						&& f.getDureeRetenueA30Jours().toString().equals(
								new DureeA30Jours(-1, -1, -1).toString())) {
					return MESSAGE.DUREE_RETENUE_MANDATORY_WITH_3;
				}

			}

			if (ConstantesCalculette.ARTICLE.ARTICLE_LIBRE
					.isArticleOK(tmpCodeArticle)) {
				if (f.getDureeRetenueA30Jours().isEmpty()) {
					return MESSAGE.DUREE_RETENUE_MANDATORY_WITH_LIBRE;
				}
			}

			// R201
			// si article == 13 ou 14 alors afficher message pour le faire
			// remplacer par un autre
			if (ConstantesCalculette.ARTICLE.ARTICLE_13
					.isArticleOK(tmpCodeArticle))
				return MESSAGE.RENVOI_ARTICLE_13;

			if (ConstantesCalculette.ARTICLE.ARTICLE_14
					.isArticleOK(tmpCodeArticle))
				return MESSAGE.RENVOI_ARTICLE_14;

			/*
			 * R208 si article == 9 alors Pour chaque ligne du tableau des
			 * fonctions exercées, si l’article sélectionné est l’article 9 MCF
			 * ou 9 PR, le corps d’accueil doit être de niveau inférieur ou égal
			 * à celui de l'article : Si ARTICLE = 9 MCF, le corps d’accueil
			 * doit être de niveau MCF (CORPS.CORPS_ACCUEIL = MCF pour le CORPS
			 * sélectionné dans le champ corps d’accueil) Si ARTICLE = 9 PR, le
			 * corps d’accueil doit être de niveau MCF ou PR
			 * (CORPS.CORPS_ACCUEIL = MCF ou CORPS.CORPS_ACCUEIL = PR pour le
			 * CORPS sélectionné dans le champ corps d’accueil) (le contrôle par
			 * rapport au corps d'origine disparaît)
			 */

			if (ConstantesCalculette.ARTICLE.ARTICLE_9_MCF
					.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_9_PR
							.isArticleOK(tmpCodeArticle)) {

				CorpsRef corpsRef = TableRefHelper.getCorsRefById(pCalculette
						.getCorspAcceuilId());

				if (ConstantesCalculette.ARTICLE.ARTICLE_9_MCF
						.isArticleOK(tmpCodeArticle)) {

					if (!ConstantesCalculette.isMaitreDeConf(corpsRef
							.getCorpsAcceuil())) {

						return MESSAGE.CORPS_ACCEUIL_MCF_REQUESTED;
					}

				} else if (ConstantesCalculette.ARTICLE.ARTICLE_9_PR
						.isArticleOK(tmpCodeArticle)) {

					if (!ConstantesCalculette.isPR(corpsRef.getCorpsAcceuil())
							&& !ConstantesCalculette.isMaitreDeConf(corpsRef
									.getCorpsAcceuil())) {

						return MESSAGE.CORPS_ACCEUIL_PR_REQUESTED;
					}

				}
			}

			// R210 Chacun des articles 3, 11, 15-II, SNA ne peut figurer qu'une
			// seule fois dans la liste fonctions exercées.
			// plus compter le nombre d'occurence pour l'article 4,5,10,12
			if (ConstantesCalculette.ARTICLE.ARTICLE_3
					.isArticleOK(tmpCodeArticle)) {
				if (nbOccurenceArt_3 > 0) {
					return MESSAGE.ART_3_MULTIPLE;
				}
				nbOccurenceArt_3++;

			} else if (ConstantesCalculette.ARTICLE.ARTICLE_SNA
					.isArticleOK(tmpCodeArticle)) {
				if (nbOccurenceArt_SNA > 0) {
					return MESSAGE.ART_SNA_MULTIPLE;
				}
				nbOccurenceArt_SNA++;
			} else if (ConstantesCalculette.ARTICLE.ARTICLE_15_II
					.isArticleOK(tmpCodeArticle)) {
				if (nbOccurenceArt_15_II > 0) {
					return MESSAGE.ART_15_II_MULTIPLE;
				}
				nbOccurenceArt_15_II++;
			} else if(ConstantesCalculette.ARTICLE.ARTICLE_5_1
					.isArticleOK(tmpCodeArticle)){
				if (nbOccurenceArt_5_1 > 0) {
					return MESSAGE.ART_5_1_MULTIPLE;
				}
				nbOccurenceArt_5_1++;
			}
			
			// R221 si l'article 5-1 alors le corps d'accueil = MCF et 
			// grade d'accueil MCF CN 
			if (ConstantesCalculette.ARTICLE.ARTICLE_5_1
					.isArticleOK(tmpCodeArticle)) {
				
				
				CorpsRef corpsRef = TableRefHelper.getCorsRefById(pCalculette
						.getCorspAcceuilId());
				
				GradeRef gradeRef = TableRefHelper.getGradeById(pCalculette
						.getGradeAcceuilId());
				
				if (corpsRef == null || !ConstantesCalculette.isMaitreDeConf(corpsRef
								.getCorpsAcceuil()) || gradeRef == null
										|| !ConstantesCalculette.isMaitreDeConfNormal(gradeRef
												.getLibelleCourt())) {
					return MESSAGE.WRONG_CORPS_ACCEUIL_WITH_ART_5_1;
				}			

			}

			// R212 L'article 11 ne peut étre appliqué que si
			// l'enseignant-chercheur est chercheur à la date de sa nomination :
			// affichage d'un message de warning .
			if (ConstantesCalculette.ARTICLE.ARTICLE_11
					.isArticleOK(tmpCodeArticle))
				message = MESSAGE.WARN_ART_11;

			// R213
			listeIndicateurArticle(bitSet, f);

		}// for controle individuelle des fonctions

		// R213
		boolean isControl = bitSet.get(FONCTION_BITSET_INDX.ART_10_PRESENT
				.ordinal())
				|| bitSet.get(FONCTION_BITSET_INDX.ART_12_PRESENT.ordinal())
				|| bitSet.get(FONCTION_BITSET_INDX.ART_6_PRESENT.ordinal())
				|| (bitSet.get(FONCTION_BITSET_INDX.ART_4_PRESENT.ordinal()) && !bitSet
						.get(FONCTION_BITSET_INDX.ART_5_PRESENT.ordinal()))
				|| (bitSet.get(FONCTION_BITSET_INDX.ART_5_PRESENT.ordinal()) && !bitSet
						.get(FONCTION_BITSET_INDX.ART_5_PRESENT.ordinal()));

		if (isControl) {
			isOK = (bitSet.get(FONCTION_BITSET_INDX.ART_4_DUREE_RETENUE_EMPTY
					.ordinal()) == !bitSet
					.get(FONCTION_BITSET_INDX.ART_4_DUREE_RETENUE_FILLED
							.ordinal()))
					|| (bitSet
							.get(FONCTION_BITSET_INDX.ART_5_DUREE_RETENUE_EMPTY
									.ordinal()) == !bitSet
							.get(FONCTION_BITSET_INDX.ART_5_DUREE_RETENUE_FILLED
									.ordinal()))
					|| (bitSet
							.get(FONCTION_BITSET_INDX.ART_10_DUREE_RETENUE_EMPTY
									.ordinal()) == !bitSet
							.get(FONCTION_BITSET_INDX.ART_10_DUREE_RETENUE_FILLED
									.ordinal()))
					|| (bitSet
							.get(FONCTION_BITSET_INDX.ART_6_DUREE_RETENUE_EMPTY
									.ordinal()) == !bitSet
							.get(FONCTION_BITSET_INDX.ART_6_DUREE_RETENUE_FILLED
									.ordinal()))
					|| (bitSet
							.get(FONCTION_BITSET_INDX.ART_12_DUREE_RETENUE_EMPTY
									.ordinal()) == !bitSet
							.get(FONCTION_BITSET_INDX.ART_12_DUREE_RETENUE_FILLED
									.ordinal()));

			if (!isOK)
				return MESSAGE.DUREE_RETENUE_NOT_FILLED;
		}

		// R211 article 11 sur la période la plus récente
		Date dateFinArt11 = null;
		Date maxDatefin = null;
		for (Fonction f : fonctions) {
			tmpCodeArticle = f.getCodeArticle();
			if (maxDatefin == null) {
				maxDatefin = f.getPeriode().getDateFin();
			} else {
				if (maxDatefin.before(f.getPeriode().getDateFin())) {
					maxDatefin = f.getPeriode().getDateFin();
				}
			}
			if (ConstantesCalculette.ARTICLE.ARTICLE_11
					.isArticleOK(tmpCodeArticle)) {
				
				if (dateFinArt11 == null) {
					dateFinArt11 = f.getPeriode().getDateFin();
				} else {
					if (dateFinArt11.before(f.getPeriode().getDateFin())) {
						dateFinArt11 = f.getPeriode().getDateFin();
					}
				}
			}
		}

		if (dateFinArt11 != null) {
			if (dateFinArt11.before(maxDatefin)) {
				return MESSAGE.ARTICLE_11_ONLY_WITH_LAST_PERIOD;
			}
		}
		/*
		 * FGU Règle de controle des dates de début et fin si l'article 11 est
		 * sélectionné, alors contrôler que la fin de période de la dernière
		 * fonction d'article 11 est égale à la date de nomination ; si elle est
		 * différente, alors afficher un message similaire à celui affiché
		 * lorsque l'article 11 n'est pas la dernière fonction.
		 */
		
		
			Calendar cal = Calendar.getInstance();
			cal.setTime(pCalculette.getDateNomination());
			cal.add(cal.DAY_OF_MONTH, -1);
			Date dateNom = cal.getTime();
			if (dateFinArt11 != null
					&& !dateNom.equals(dateFinArt11)) {
				return MESSAGE.ARTICLE_11_LAST_PERIOD;
			}

		
		// 2nd phase du controle
		// On vérifie que les fonctions sont cohérentes les unes par rapports
		// aux autres
		// R220
		int size = fonctions.size();
		Periode currentPer;
		boolean isContinue;
		Integer tmpQutitie;
		HashMap<String, Integer> mapQutite = new HashMap<String, Integer>();

		for (int ii = 0; ii < size; ii++) {
			tmpCodeArticle = fonctions.get(ii).getCodeArticle();
			isContinue = ConstantesCalculette.ARTICLE.ARTICLE_LIBRE
					.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_NON_RETENU
							.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_15_II
							.isArticleOK(tmpCodeArticle)
					|| ConstantesCalculette.ARTICLE.ARTICLE_5_1
							.isArticleOK(tmpCodeArticle);
			// if (!isContinue)
			if (isContinue)
				break;

			currentPer = fonctions.get(ii).getPeriode();

			tmpQutitie = mapQutite.get(currentPer
					.toStringPeriod(ConstantesCalculette.TIRET));
			if (tmpQutitie == null) {
				tmpQutitie = new Integer(fonctions.get(ii).getQuotiteRetenue());
				mapQutite.put(currentPer.toString(), tmpQutitie);
			} else {
				tmpQutitie = Integer.valueOf(tmpQutitie
						+ fonctions.get(ii).getQuotiteRetenue());
			}

			int pourcentRetenu;
			try {
				pourcentRetenu = fonctions.get(ii).getQuotiteRetenue();
			} catch (Exception e) {
				pourcentRetenu = 0;
			}
			for (int jj = 0; jj < size; jj++) {
				if (ii != jj) {
					tmpCodeArticle = fonctions.get(jj).getCodeArticle();
					isContinue = ConstantesCalculette.ARTICLE.ARTICLE_LIBRE
							.isArticleOK(tmpCodeArticle)
							|| ConstantesCalculette.ARTICLE.ARTICLE_NON_RETENU
									.isArticleOK(tmpCodeArticle)
							|| ConstantesCalculette.ARTICLE.ARTICLE_15_II
									.isArticleOK(tmpCodeArticle)
							|| ConstantesCalculette.ARTICLE.ARTICLE_5_1
									.isArticleOK(tmpCodeArticle);
					// if (!isContinue)
					if (isContinue)
						break;

					tmpPeriode = fonctions.get(jj).getPeriode();
					if (currentPer != null
							&& tmpPeriode.getDateDebut() != null
							&& tmpPeriode.getDateFin() != null
							&& currentPer.compareToPeriode(tmpPeriode
									.getDateDebut(), tmpPeriode.getDateFin()))
						return MESSAGE.CHEVAUCHEMENT_DATE_FORBIDEN;

					// pour les périodes qui se chevauchent, le cumul des
					// quotités retenues ne doit pas dépasser 100
					if (fonctions.get(ii).getDateDeb() != null
							&& fonctions.get(jj).getDateDeb() != null
							&& fonctions.get(ii).getDateFin() != null
							&& fonctions.get(jj).getDateFin() != null
							&& fonctions.get(ii).getDateDeb().compareTo(
									fonctions.get(jj).getDateDeb()) == 0
							&& fonctions.get(ii).getDateFin().compareTo(
									fonctions.get(jj).getDateFin()) == 0) {

						pourcentRetenu = pourcentRetenu
								+ fonctions.get(jj).getQuotiteRetenue();

					}
				}
			}

			if (pourcentRetenu > 100) {

				return MESSAGE.QUOTITE_TOO_BIG;
			}
		}// for fonctions

		Set<String> keys = mapQutite.keySet();
		StringBuilder arg = new StringBuilder("Période concernée :");
		Integer quot;
		for (String key : keys) {
			quot = mapQutite.get(key);
			if (quot > 100) {
				arg.append(key);
				arg.append(" ");
			}
		}
		if (arg.length() > 19) {
			message = MESSAGE.QUOTITE_RETENUE_TOO_BIG;
			message.setArg(arg.toString());
			return message;
		}
		return message;
	}

	/**
	 * R213
	 * 
	 * @param pBitSet
	 * @param pFonction
	 */
	private MESSAGE listeIndicateurArticle(BitSet pBitSet, Fonction pFonction) {

		String tmpCodeArticle = pFonction.getCodeArticle();
		DureeA30Jours durA30Jrs = pFonction.getDureeRetenueA30Jours();
		MESSAGE tmpMessage = MESSAGE.OK;

		if (ConstantesCalculette.ARTICLE.ARTICLE_4.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 4
			pBitSet.set(FONCTION_BITSET_INDX.ART_4_PRESENT.ordinal(), true);
			if (durA30Jrs.isEmpty()) {
				// indicateur de présence d'un article 4 SANS une duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_4_DUREE_RETENUE_EMPTY
						.ordinal(), true);
			} else {
				// indicateur de présence d'un article 4 AVEC duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_4_DUREE_RETENUE_FILLED
						.ordinal(), true);
			}

		} else if (ConstantesCalculette.ARTICLE.ARTICLE_5
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 5
			pBitSet.set(FONCTION_BITSET_INDX.ART_5_PRESENT.ordinal(), true);
			if (durA30Jrs.isEmpty()) {
				// indicateur de présence d'un article 5 avec une duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_5_DUREE_RETENUE_EMPTY
						.ordinal(), true);
			} else {
				// indicateur de présence d'un article 5 SANS duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_5_DUREE_RETENUE_FILLED
						.ordinal(), true);
			}
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_6
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 6
			pBitSet.set(FONCTION_BITSET_INDX.ART_6_PRESENT.ordinal(), true);
			if (durA30Jrs.isEmpty()) {
				// indicateur de présence d'un article 6 avec une duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_6_DUREE_RETENUE_EMPTY
						.ordinal(), true);
			} else {
				// indicateur de présence d'un article 6 SANS duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_6_DUREE_RETENUE_FILLED
						.ordinal(), true);
			}
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_10
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 10
			pBitSet.set(FONCTION_BITSET_INDX.ART_10_PRESENT.ordinal(), true);
			if (durA30Jrs.isEmpty()) {
				// indicateur de présence d'un article 10 avec une duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_10_DUREE_RETENUE_EMPTY
						.ordinal(), true);
			} else {
				// indicateur de présence d'un article 10 SANS duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_10_DUREE_RETENUE_FILLED
						.ordinal(), true);
			}
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_12
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 12
			pBitSet.set(FONCTION_BITSET_INDX.ART_12_PRESENT.ordinal(), true);
			if (durA30Jrs.isEmpty()) {
				// indicateur de présence d'un article 12 avec une duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_12_DUREE_RETENUE_EMPTY
						.ordinal(), true);
			} else {
				// indicateur de présence d'un article 12 SANS duree retenue
				pBitSet.set(FONCTION_BITSET_INDX.ART_12_DUREE_RETENUE_FILLED
						.ordinal(), true);
			}
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_11
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 11
			pBitSet.set(FONCTION_BITSET_INDX.ART_11_PRESENT.ordinal(), true);
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_3
				.isArticleOK(tmpCodeArticle)) {
			// indicateur de présence de l'article 3
			pBitSet.set(FONCTION_BITSET_INDX.ART_3_PRESENT.ordinal(), true);
		}
		return tmpMessage;
	}

	/**
	 * Controle les quotitées reelle et retenue
	 * 
	 * @param pQuotiteReelle
	 * @param pQuotiteRetenue
	 * @return
	 */
	private MESSAGE checkQuotite(Integer pQuotiteReelle, Integer pQuotiteRetenue) {
		boolean isOk;

		// quotite > 0 et <= 100
		if (pQuotiteReelle != null) {
			isOk = pQuotiteReelle.intValue() > 0
					&& pQuotiteReelle.intValue() <= 100;

			if (!isOk) {
				return MESSAGE.QUOTITE_BAD_VALUE;
			}
		}

		if (pQuotiteRetenue != null) {
			isOk = pQuotiteRetenue.intValue() > 0
					&& pQuotiteRetenue.intValue() <= 100;

			if (!isOk) {
				return MESSAGE.QUOTITE_BAD_VALUE;
			}
		}

		// R53 si les deux sont renseignées
		// quotite retenue <= quetotie reelle
		if ((pQuotiteRetenue != null) && (pQuotiteReelle != null)) {
			isOk = pQuotiteRetenue.intValue() <= pQuotiteReelle.intValue();

			if (!isOk) {
				return MESSAGE.QUOTITE_RETENUE_TOO_BIG;
			}
		}
		return MESSAGE.OK;
	}

	/*
	 * méthode privée servant de point d'entrée pour le calcul des classements
	 */
	private void calculClassement(Calculette pCalculette) {

		simpleLog.println(">>> calculClassement ");

		List<Fonction> lstfonct = pCalculette.getLstFonction();
		String tmpCodeArticle;
		List<Fonction> lstArt15 = new ArrayList<Fonction>();
		Fonction art3 = null;
		for (Fonction f : lstfonct) {
			tmpCodeArticle = f.getCodeArticle();
			if (ConstantesCalculette.ARTICLE.ARTICLE_3
					.isArticleOK(tmpCodeArticle)) {
				art3 = f;
			} else {
				// if
				// (ConstantesCalculette.ARTICLE.ARTICLE_15_II.isArticleOK(tmpCodeArticle))
				// {
				if (f.getNbJoursResultat() > 0) {
					lstArt15.add(f);
				}
				// }
			}
		}

		// R600
		// calcul classement article 3
		if (art3 != null) {
			// calcul anciennete et classement specifique
			calculClassementWithArticle13_600(pCalculette, art3, lstArt15);
		} else {
			// R700
			List<GrilleIndiciaireRef> lst = TableRefHelper
					.getGrilleIndiciaireByGradeId(pCalculette
							.getGradeAcceuilId());
			GrilleIndiciaireRef minGrille = null;
			boolean isFound;
			int nbAnciennete = (int) pCalculette.getNbJoursFinal();
			int tmpAcienneteReel = 0;
			GrilleIndiciaireRef grille;
			int tmppasse = 0;
			for (int index = 0; index < lst.size(); index++) {
				grille = (GrilleIndiciaireRef) lst.get(index);
				if (grille.getTmpsPassageAnciennete() == null) {
					tmppasse = 0;
				} else {
					tmppasse = grille.getTmpsPassageAnciennete();
				}
				tmpAcienneteReel = tmpAcienneteReel + tmppasse * 30;

				if (((GrilleIndiciaireRef) lst.get(lst.size() - 1))
						.getEchelon() == grille.getEchelon()) {
					tmpAcienneteReel = tmpAcienneteReel - tmppasse * 30;
					minGrille = grille;
					break;
				} else if (tmpAcienneteReel >= nbAnciennete) {
					if (grille.getChevron() != null && grille.getChevron() > 1) {
						tmpAcienneteReel = tmpAcienneteReel - tmppasse * 30;
						continue;
					} else {
						if (tmpAcienneteReel > nbAnciennete) {
							tmpAcienneteReel = tmpAcienneteReel - tmppasse * 30;
							minGrille = grille;
							break;
						}
						if (index == lst.size() - 1) {
							minGrille = lst.get(index + 1);
							break;
						}
						if (index < lst.size() - 1) {
							index++;
							minGrille = lst.get(index);
							while (minGrille.getChevron() != null
									&& minGrille.getChevron() > 1) {
								index++;
								minGrille = lst.get(index);
							}
							break;
						}
					}
				} else if (grille.getChevron() != null
						&& grille.getChevron() > 1) {
					tmpAcienneteReel = tmpAcienneteReel - tmppasse * 30;

					continue;
				} else {
					minGrille = grille;
				}
			}
			GrilleIndiciaireRef grilleCalcule = calculClassementAutre(
					minGrille, nbAnciennete - tmpAcienneteReel);
			pCalculette.setAncienneteCalcule(nbAnciennete - tmpAcienneteReel);
			pCalculette.setChevronCalcule(grilleCalcule.getChevron());
			pCalculette.setEchelonCalcule(grilleCalcule.getEchelon());
			pCalculette.setIndiceBrutCalcule(grilleCalcule.getIndiceBrut());
			if (((GrilleIndiciaireRef) lst.get(lst.size() - 1)).getEchelon() == minGrille
					.getEchelon()) {
				pCalculette.setChevronCalcule(minGrille.getChevron());
				pCalculette.setEchelonCalcule(minGrille.getEchelon());
				pCalculette.setIndiceBrutCalcule(minGrille.getIndiceBrut());
			}
		}

	}

	private GrilleIndiciaireRef calculClassementAutre(
			GrilleIndiciaireRef pGrilleDepart, float pAncienneteConservee) {
		simpleLog
				.println(">>> calculClassement (long pGradeId, int pEchelon, int pChevron, float pAncienneteConservee)");
		// R500
		List<GrilleIndiciaireRef> lst = TableRefHelper
				.getGrilleIndiciaireByGradeId(pGrilleDepart.getGradeId());
		boolean isOK;
		Integer indiceBrutAtteint;
		GrilleIndiciaireRef nextGrille;
		Integer chevronAtteint;
		Integer echelonAtteint;

		// init du result avec donnée de la grille passé en paramètre
		GrilleIndiciaireRef resultAtteind = new GrilleIndiciaireRef();
		echelonAtteint = pGrilleDepart.getEchelon();
		chevronAtteint = pGrilleDepart.getChevron();
		indiceBrutAtteint = pGrilleDepart.getIndiceBrut();

		float ancienneteConservee = pAncienneteConservee;
		/*
		 * nextGrille = pGrilleDepart;
		 * 
		 * if(nextGrille != null){ echelonAtteint = nextGrille.getEchelon();
		 * chevronAtteint = nextGrille.getChevron(); ancienneteConservee =
		 * pAncienneteConservee; if(nextGrille.getTmpsPassageAnciennete() !=
		 * null){ nbJoursPassage = nextGrille.getTmpsPassageAnciennete() * 30; }
		 * else { nbJoursPassage = 0; } indiceBrutAtteint =
		 * nextGrille.getIndiceBrut(); } else { nbJoursPassage = 0; }
		 */

		nextGrille = pGrilleDepart;
		boolean value = false;
		float anciennete = 0;
		float ancienneteDernier = 0;
		GrilleIndiciaireRef grillemax = lst.get(lst.size() - 1);
		for (GrilleIndiciaireRef grille : lst) {
			// Correction ano art3 JCE 22/12/09 : ajout 2 lignes
			if (grille.getEchelon() < nextGrille.getEchelon())
				continue;
			if (grille.getChevron() > 1)
				continue;
			// recherche de l'echelon suivant
			isOK = (grille.getEchelon() == (echelonAtteint.intValue()));
			if (isOK) {
				anciennete = grille.getTmpsPassageAnciennete() != null ? grille
						.getTmpsPassageAnciennete() * 30 : 0;
				// Correction ano art3 JCE 22/12/09 : remplact 1 ligne if
				// (anciennete != 0 && ancienneteConservee > anciennete) {
				if (anciennete != 0 && ancienneteConservee >= anciennete) {
					value = true;
				}
			}
		}

		if (value) {
			for (GrilleIndiciaireRef grille : lst) {
				// Correction ano art3 JCE 22/12/09 : ajout 1 ligne
				if (grille.getEchelon() < nextGrille.getEchelon())
					continue;
				if (grille.getChevron() > 1) {
					continue;
				}
				anciennete = grille.getTmpsPassageAnciennete() != null ? grille
						.getTmpsPassageAnciennete() * 30 : 0;
				if (anciennete != 0 && ancienneteConservee >= anciennete) {
					if (grille.getEchelon() != grillemax.getEchelon()) {
						ancienneteConservee = ancienneteConservee - anciennete;
					}
					ancienneteDernier = anciennete;
					echelonAtteint = grille.getEchelon() + 1;
					nextGrille = grille;
				}
				if (grille.getEchelon() == echelonAtteint) {
					nextGrille = grille;
					break;
				}

				if (grille.getTmpsPassageAnciennete() != null
						&& grille.getTmpsPassageAnciennete() > 66) {

					nextGrille
							.setTmpsPassageAnciennete((int) ancienneteConservee);

				}

			}

		}

		if (grillemax.getEchelon() < echelonAtteint) {
			echelonAtteint = grillemax.getEchelon();
			ancienneteConservee = ancienneteConservee + ancienneteDernier;
		}

		if (nextGrille != null) {

			chevronAtteint = nextGrille.getChevron();
			indiceBrutAtteint = nextGrille.getIndiceBrut();
		}

		resultAtteind.setEchelon(echelonAtteint);
		resultAtteind.setChevron(chevronAtteint);
		resultAtteind.setTmpsPassageAnciennete((int) ancienneteConservee);
		resultAtteind.setIndiceBrut(indiceBrutAtteint);

		simpleLog
				.println("<<<< calculClassement (long pGradeId, int pEchelon, int pChevron, float pAncienneteConservee)");
		return resultAtteind;
	}

	/**
	 * @param pCalculette
	 * @param pGradeId
	 * @param pEchelon
	 * @param pChevron
	 * @param pAncienneteConservee
	 * @return
	 */
	private GrilleIndiciaireRef calculClassement(
			GrilleIndiciaireRef pGrilleDepart, float pAncienneteConservee,
			Calculette pCalculette) {
		simpleLog
				.println(">>> calculClassement (long pGradeId, int pEchelon, int pChevron, float pAncienneteConservee)");
		// R500

		List<GrilleIndiciaireRef> lst = TableRefHelper
				.getGrilleIndiciaireByGradeId(pGrilleDepart.getGradeId());
		boolean isOK;
		Integer indiceBrutAtteint;
		GrilleIndiciaireRef nextGrille;
		Integer chevronAtteint;
		Integer echelonAtteint;

		// init du result avec donnée de la grille passé en paramètre
		GrilleIndiciaireRef resultAtteind = new GrilleIndiciaireRef();
		echelonAtteint = pGrilleDepart.getEchelon();
		chevronAtteint = pGrilleDepart.getChevron();
		indiceBrutAtteint = pGrilleDepart.getIndiceBrut();

		float ancienneteConservee = pAncienneteConservee;
		/*
		 * nextGrille = pGrilleDepart;
		 * 
		 * if(nextGrille != null){ echelonAtteint = nextGrille.getEchelon();
		 * chevronAtteint = nextGrille.getChevron(); ancienneteConservee =
		 * pAncienneteConservee; if(nextGrille.getTmpsPassageAnciennete() !=
		 * null){ nbJoursPassage = nextGrille.getTmpsPassageAnciennete() * 30; }
		 * else { nbJoursPassage = 0; } indiceBrutAtteint =
		 * nextGrille.getIndiceBrut(); } else { nbJoursPassage = 0; }
		 */

		nextGrille = pGrilleDepart;
		boolean value = false;
		float anciennete = 0;
		float ancienneteDernier = 0;
		GrilleIndiciaireRef grillemax = lst.get(lst.size() - 1);
		for (GrilleIndiciaireRef grille : lst) {
			// Correction ano art3 JCE 22/12/09 : ajout 2 lignes
			if (grille.getEchelon() < nextGrille.getEchelon())
				continue;
			if (grille.getChevron() > 1)
				continue;
			// recherche de l'echelon suivant
			isOK = (grille.getEchelon() == (echelonAtteint.intValue()));
			if (isOK) {
				anciennete = grille.getTmpsPassageAnciennete() != null ? grille
						.getTmpsPassageAnciennete() * 30 : 0;
				// Correction ano art3 JCE 22/12/09 : remplact 1 ligne if
				// (anciennete != 0 && ancienneteConservee > anciennete) {
				if (anciennete != 0 && ancienneteConservee >= anciennete) {
					value = true;
				}
			}
		}

		if (value) {
			for (GrilleIndiciaireRef grille : lst) {
				// Correction ano art3 JCE 22/12/09 : ajout 1 ligne
				if (grille.getEchelon() < nextGrille.getEchelon())
					continue;
				if (grille.getChevron() > 1) {
					continue;
				}
				anciennete = grille.getTmpsPassageAnciennete() != null ? grille
						.getTmpsPassageAnciennete() * 30 : 0;
				if (anciennete != 0 && ancienneteConservee >= anciennete) {
					if (grille.getEchelon() != grillemax.getEchelon()) {
						ancienneteConservee = ancienneteConservee - anciennete;
					}
					ancienneteDernier = anciennete;
					echelonAtteint = grille.getEchelon() + 1;
					nextGrille = grille;
				}

				if (grille.getEchelon() == echelonAtteint) {
					nextGrille = grille;
					break;
				}

				if (grille.getTmpsPassageAnciennete() != null
						&& grille.getTmpsPassageAnciennete() > 66) {

					nextGrille
							.setTmpsPassageAnciennete((int) ancienneteConservee);

				}

			}

		}

		if (grillemax.getEchelon() < echelonAtteint
				&& grillemax.getIndiceBrut() <= pCalculette
						.getIndiceBrutDetenue()) {
			echelonAtteint = grillemax.getEchelon();
			chevronAtteint = grillemax.getChevron();
			indiceBrutAtteint = grillemax.getIndiceBrut();

		}

		else if (nextGrille != null) {
			if (nextGrille.getEchelon() > pGrilleDepart.getEchelon()) {

				echelonAtteint = nextGrille.getEchelon();
				chevronAtteint = nextGrille.getChevron();
				indiceBrutAtteint = nextGrille.getIndiceBrut();

			}

			else {
				echelonAtteint = pGrilleDepart.getEchelon();
				chevronAtteint = pGrilleDepart.getChevron();
				indiceBrutAtteint = pGrilleDepart.getIndiceBrut();
			}
		}

		resultAtteind.setEchelon(echelonAtteint);
		resultAtteind.setChevron(chevronAtteint);
		resultAtteind.setTmpsPassageAnciennete((int) ancienneteConservee);
		resultAtteind.setIndiceBrut(indiceBrutAtteint);

		simpleLog
				.println("<<<< calculClassement (long pGradeId, int pEchelon, int pChevron, float pAncienneteConservee)");
		return resultAtteind;
	}

	/**
	 * Traitement du classement lorsqu'une des fonctions est rattachée à
	 * l'article 3
	 * 
	 * @param pCalculette
	 * @param pArt3
	 * @param pListArt15
	 */
	private void calculClassementWithArticle13_600(Calculette pCalculette,
			Fonction pArt3, List<Fonction> pListArt15) {

		// R600
		// calcul classement article 3
		int ibrDetenu = pCalculette.getIndiceBrutDetenue();
		int ibrEchelonSup = pCalculette.getIndiceBrutEchelonSup();

		List<GrilleIndiciaireRef> lstGrilleIndiciaire = TableRefHelper
				.getGrilleIndiciaireByGradeId(pCalculette.getGradeAcceuilId());
		GrilleIndiciaireRef grilleFound;
		GrilleIndiciaireRef grilleSuivante = null;
		GrilleIndiciaireRef grilleCalcule = null;
		GrilleIndiciaireRef grilleFinale;

		grilleFound = rechercheLePlusPetit(lstGrilleIndiciaire, ibrDetenu);

		if (grilleFound != null) {

			grilleSuivante = rechercheByEchelonByChevron(lstGrilleIndiciaire,
					(grilleFound.getEchelon()), grilleFound.getChevron());/* +1 */

			if ((grilleSuivante != null)
					&& (grilleSuivante.getIndiceBrut() >= ibrDetenu)) {
				grilleCalcule = grilleSuivante;

			} else {

				grilleCalcule = grilleFound;
			}
		} else {

			grilleFound = rechercheLePlusGrand(lstGrilleIndiciaire, 0, 1);
			// Modif JCE 13/01/10
			// grilleFound = rechercheLePlusGrand(lstGrilleIndiciaire,
			// pCalculette
			// .getChevronOrigine(), pCalculette.getEchelonOrigine());

			if (grilleFound != null) {
				grilleCalcule = grilleFound;
			}
		}

		float ancienneteCalcule = 0;

		/**
		 * NOUVEAU ET ANCIEN CODE A GARDER EN CAS DE REGRESSION
		 */
		/*
		 * ancien code à garder if(grilleSuivante.getIndiceBrut()<=
		 * grilleCalcule.getIndiceBrut()){ ancienneteCalcule = 0; }else {
		 * ancienneteCalcule = grilleCalcule.getTmpsPassageAnciennete() * 30; }
		 */
		if (grilleCalcule != null) {
			if (ibrEchelonSup <= grilleCalcule.getIndiceBrut()) {
				ancienneteCalcule = 0;
			} else {
				if (grilleCalcule.getTmpsPassageAnciennete() != null
						&& (grilleCalcule.getChevron() == null || grilleCalcule
								.getChevron() == 0)) {
					ancienneteCalcule = grilleCalcule
							.getTmpsPassageAnciennete() * 30;
				} else {
					grilleFound = rechercheByEchelonByChevron(
							lstGrilleIndiciaire, grilleCalcule.getEchelon(), 1);
					if (grilleFound != null) {
						ancienneteCalcule = grilleFound
								.getTmpsPassageAnciennete() * 30;
					}
				}

			}
		}
		/**
		 * *********************************************************************
		 * ********
		 */

		if (ancienneteCalcule > pArt3.getNbJoursRetenus()) {
			ancienneteCalcule = pArt3.getNbJoursRetenus();

		}
		float ancienneteCalculFinal = 0;
		for (Fonction fonc : pCalculette.getLstFonction()) {
			if (!fonc.getCodeArticle().equals(
					ConstantesCalculette.ARTICLE.ARTICLE_3.getLabel())) {
				ancienneteCalculFinal = ancienneteCalculFinal
						+ fonc.getNbJoursResultat();
			}

		}
		ancienneteCalculFinal = ancienneteCalculFinal + ancienneteCalcule;
		if (grilleCalcule != null) {
			grilleFinale = calculClassement(grilleCalcule,
					ancienneteCalculFinal, pCalculette);

			pCalculette.setEchelonCalcule(grilleFinale.getEchelon());
			pCalculette.setChevronCalcule(grilleFinale.getChevron());
			pCalculette.setIndiceBrutCalcule(grilleFinale.getIndiceBrut());
			pCalculette.setAncienneteCalcule(grilleFinale
					.getTmpsPassageAnciennete());

			if (grilleCalcule.getEchelon() == (lstGrilleIndiciaire
					.get(lstGrilleIndiciaire.size() - 1).getEchelon())) {

				pCalculette.setAncienneteCalcule(pArt3.getNbJoursRetenus()
						+ sommeDurreRetenuWithoutArt3(pListArt15));
			}

		} else {
			GrilleIndiciaireRef grillemax = lstGrilleIndiciaire
					.get(lstGrilleIndiciaire.size() - 1);
			pCalculette.setEchelonCalcule(grillemax.getEchelon());
			pCalculette.setChevronCalcule(grillemax.getChevron());
			pCalculette.setIndiceBrutCalcule(grillemax.getIndiceBrut());

		}

	}

	private float sommeDurreRetenuFonction(List<Fonction> pListArt15) {
		float somme = 0;
		for (Fonction fonction : pListArt15) {

			somme = somme + fonction.getNbJoursRetenus();
		}
		return somme;
	}
	
	private float sommeDurreRetenuWithoutArt3(List<Fonction> pListArt15) {
		float somme = 0;
		for (Fonction fonction : pListArt15) {
			if(!fonction.getCodeArticle().equals(ARTICLE.ARTICLE_3.getLabel())){
			somme = somme + fonction.getNbJoursRetenus();
			}
		}
		return somme;
	}
	
	private void calculDureeWithArticle03(Calculette pCalculette,
			Fonction pArt3, List<Fonction> pListArt15) {

		// R600
		// calcul classement article 3
		int ibrDetenu = pCalculette.getIndiceBrutDetenue();
		int ibrEchelonSup = pCalculette.getIndiceBrutEchelonSup();

		List<GrilleIndiciaireRef> lstGrilleIndiciaire = TableRefHelper
				.getGrilleIndiciaireByGradeId(pCalculette.getGradeAcceuilId());
		GrilleIndiciaireRef grilleFound;
		GrilleIndiciaireRef grilleSuivante = null;
		GrilleIndiciaireRef grilleCalcule = null;

		grilleFound = rechercheLePlusPetit(lstGrilleIndiciaire, ibrDetenu);

		if (grilleFound != null) {

			grilleSuivante = rechercheByEchelonByChevron(lstGrilleIndiciaire,
					(grilleFound.getEchelon()), grilleFound.getChevron());/* +1 */

			if ((grilleSuivante != null)
					&& (grilleSuivante.getIndiceBrut() >= ibrDetenu)) {
				grilleCalcule = grilleSuivante;

			} else {

				grilleCalcule = grilleFound;
			}
		} else {

			grilleFound = rechercheLePlusGrand(lstGrilleIndiciaire, 0, 1);
			// Modif JCE 13/01/10
			// grilleFound = rechercheLePlusGrand(lstGrilleIndiciaire,
			// pCalculette
			// .getChevronOrigine(), pCalculette.getEchelonOrigine());

			if (grilleFound != null) {
				grilleCalcule = grilleFound;
			}
		}

		float ancienneteCalcule = 0;

		/**
		 * NOUVEAU ET ANCIEN CODE A GARDER EN CAS DE REGRESSION
		 */
		/*
		 * ancien code à garder if(grilleSuivante.getIndiceBrut()<=
		 * grilleCalcule.getIndiceBrut()){ ancienneteCalcule = 0; }else {
		 * ancienneteCalcule = grilleCalcule.getTmpsPassageAnciennete() * 30; }
		 */
		if (grilleCalcule != null) {
			if (ibrEchelonSup <= grilleCalcule.getIndiceBrut()) {
				ancienneteCalcule = 0;
			} else {
				if (grilleCalcule.getTmpsPassageAnciennete() != null
						&& (grilleCalcule.getChevron() == null || grilleCalcule
								.getChevron() == 0)) {
					ancienneteCalcule = grilleCalcule
							.getTmpsPassageAnciennete() * 30;
				} else {
					grilleFound = rechercheByEchelonByChevron(
							lstGrilleIndiciaire, grilleCalcule.getEchelon(), 1);
					if (grilleFound != null) {
						ancienneteCalcule = grilleFound
								.getTmpsPassageAnciennete() * 30;
					}
				}

			}
		}
	
		/**
		 * *********************************************************************
		 * ********
		 */

		if (pArt3!=null && ancienneteCalcule > pArt3.getNbJoursRetenus()) {
			ancienneteCalcule = pArt3.getNbJoursRetenus();

		}
		pCalculette.setAncienneteCalcule(ancienneteCalcule);
		if (grilleCalcule.getEchelon() == (lstGrilleIndiciaire
				.get(lstGrilleIndiciaire.size() - 1).getEchelon())) {

			pCalculette.setAncienneteCalcule(pArt3.getNbJoursRetenus()
					);
		}
		
	}
	/**
	 * @param pCalculette
	 * @return
	 */
	// R440
	private MESSAGE calculDureeFinale(Calculette pCalculette) {
		MESSAGE tmpMessage = MESSAGE.OK;
		float result = 0;
		Fonction pArt3=null;
		List<Fonction> lstFonction = pCalculette.getLstFonction();
		
		for (Fonction f : lstFonction) {
			
				result += f.getNbJoursResultat();
	
		}
		
		for (Fonction f : lstFonction) {
			if (f.getCodeArticle().equals(
					ConstantesCalculette.ARTICLE.ARTICLE_3.getLabel())) {
				 pArt3=f;
				 break;
			}
		}
		if(pArt3!=null){
		
		calculDureeWithArticle03(pCalculette, pArt3, lstFonction);
		result=sommeDurreRetenuWithoutArt3(lstFonction)+pCalculette.getAncienneteCalcule();
		}
		
		pCalculette.setNbJoursFinal((int) Math.ceil(result));
		DureeA30Jours tmpDurA30Jours = pCalculette.getDisplayedNbJoursFinal();
		
		String tmpStrMessage = String.format(
				ConstantesCalculette.APPLICATION_ART_ALL, tmpDurA30Jours
						.getNbAnnees(), tmpDurA30Jours.getNbMois(),
				tmpDurA30Jours.getNbJours());

		pCalculette.addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_ALL_MESSAGE,
				tmpStrMessage);

		return tmpMessage;

	}

	// R430
	private MESSAGE calculResultatArt3(BitSet bitSet, Calculette pCalculette) {
		MESSAGE tmpMessage = MESSAGE.OK;
		boolean isReset;
		String tmpCodeArticle;

		if (bitSet.get(FONCTION_BITSET_INDX.ART_3_PRESENT.ordinal())) {
			List<Fonction> lstFonct = pCalculette.getLstFonction();

			for (Fonction f : lstFonct) {
				tmpCodeArticle = f.getCodeArticle();

				isReset = (f.getDureeRetenueA30Jours().isEmpty())
						&& (!ConstantesCalculette.ARTICLE.ARTICLE_3
								.isArticleOK(tmpCodeArticle))
						&& (!ConstantesCalculette.ARTICLE.ARTICLE_15_II
								.isArticleOK(tmpCodeArticle))
						&& (!ConstantesCalculette.ARTICLE.ARTICLE_5_1
								.isArticleOK(tmpCodeArticle));

				if (isReset) {
					f.setNbJoursResultat(0);
				}
			}

		}
		return tmpMessage;
	}

	/**
	 * @param bitSet
	 * @param pCalculette
	 * @return
	 */
	private MESSAGE calculResultatArt4Art5(BitSet bitSet, Calculette pCalculette) {

		String tmpCodeArticle;

		Float resteArt4 = Float.valueOf(2160);
		Float resteArt5 = Float.valueOf(1440);

		MESSAGE tmpMessage = MESSAGE.OK;

		List<Fonction> lstFonct = pCalculette.getLstFonction();
		for (Fonction f : lstFonct) {

			if (f.getNbJoursResultat() == 0) {
				tmpCodeArticle = f.getCodeArticle();

				if (!ConstantesCalculette.ARTICLE.ARTICLE_4
						.isArticleOK(tmpCodeArticle)
						&& !ConstantesCalculette.ARTICLE.ARTICLE_5
								.isArticleOK(tmpCodeArticle)) {
					continue;
				}

				if (ConstantesCalculette.ARTICLE.ARTICLE_4
						.isArticleOK(tmpCodeArticle)) {

					tmpMessage = doResultArt4(
							f,
							resteArt4,
							pCalculette,
							bitSet.get(FONCTION_BITSET_INDX.ART_11_PRESENT
									.ordinal()),
							bitSet
									.get(FONCTION_BITSET_INDX.ART_4_DUREE_RETENUE_EMPTY
											.ordinal()));

				} else if (ConstantesCalculette.ARTICLE.ARTICLE_5
						.isArticleOK(tmpCodeArticle)) {
					tmpMessage = doResultArt5(
							f,
							resteArt5,
							pCalculette,
							bitSet.get(FONCTION_BITSET_INDX.ART_11_PRESENT
									.ordinal()),
							bitSet
									.get(FONCTION_BITSET_INDX.ART_5_DUREE_RETENUE_EMPTY
											.ordinal()));
				}
				if (tmpMessage.getLevel().isErrorLevel()) {
					return tmpMessage;
				}
			}
		}
		return tmpMessage;
	}

	/**
	 * @param pFonction
	 * @param reste
	 * @param bitSet
	 * @param pCalculette
	 */
	private MESSAGE doResultArt4(Fonction pFonction, Float resteArticle,
			Calculette pCalculette, boolean isArt11Present,
			boolean isDureeRetenuePresent) {
		MESSAGE tmpMessage = MESSAGE.OK;

		String tmpCodeArticle = pFonction.getCodeArticle();

		if (!ConstantesCalculette.ARTICLE.ARTICLE_4.isArticleOK(tmpCodeArticle)
				&& !ConstantesCalculette.ARTICLE.ARTICLE_5
						.isArticleOK(tmpCodeArticle)) {
			tmpMessage = MESSAGE.ARTICLE_4_5_VALUE_NEEDED;
		}

		// R410
		// et article 11 présent
		// et article courant = 4 ou 5
		// alors résultat = 2/3 nb jours pris en compte
		//

		if (isArt11Present && (isDureeRetenuePresent)) {

			pFonction
					.setNbJoursResultat(pFonction.getNbJoursPrisEnCompte() * 2 / 3);
			pCalculette.addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_11_MESSAGE,
					ConstantesCalculette.APPLICATION_ART_11);
		} else {
			// R415
			// si article courant (= 4 ou 5)
			// article 11 non présent
			// si durée retenue non renseigné
			// alors
			// duree article = dure prise en compte
			// calcul global pour les articles de même type.
			// float reste = resteArticle.floatValue();
			// pCalculette.setReste(resteArticle.floatValue());

			pFonction.setNbJoursArticle(pFonction.getNbJoursPrisEnCompte());
			
//			pFonction.setNbJoursResultat(pFonction.getNbJoursArticle());
			
			if (pCalculette.getReste4() >= pFonction.getNbJoursArticle()) {

				pFonction.setNbJoursResultat(pFonction.getNbJoursArticle());
				pCalculette.setReste4(pCalculette.getReste4()
						- pFonction.getNbJoursResultat());
				resteArticle = Float.valueOf(pCalculette.getReste4());
			} else if (pCalculette.getReste4() < pFonction.getNbJoursArticle()) {

				pFonction.setNbJoursResultat(pCalculette.getReste4());
				pFonction.setNbJoursArticle(pCalculette.getReste4());
				resteArticle = Float.valueOf(0);
			} else if (pCalculette.getReste4() == 0) {

				pFonction.setNbJoursResultat(0);
				pFonction.setNbJoursArticle(0);
			}
		}
		return tmpMessage;
	}

	/**
	 * @param pFonction
	 * @param reste
	 * @param bitSet
	 * @param pCalculette
	 */
	private MESSAGE doResultArt5(Fonction pFonction, Float resteArticle,
			Calculette pCalculette, boolean isArt11Present,
			boolean isDureeRetenuePresent) {
		MESSAGE tmpMessage = MESSAGE.OK;

		String tmpCodeArticle = pFonction.getCodeArticle();

		if (!ConstantesCalculette.ARTICLE.ARTICLE_4.isArticleOK(tmpCodeArticle)
				&& !ConstantesCalculette.ARTICLE.ARTICLE_5
						.isArticleOK(tmpCodeArticle)) {
			tmpMessage = MESSAGE.ARTICLE_4_5_VALUE_NEEDED;
		}

		// R410
		// et article 11 présent
		// et article courant = 4 ou 5
		// alors résultat = 2/3 nb jours pris en compte
		//

		if (isArt11Present && (isDureeRetenuePresent)) {
			pFonction
					.setNbJoursResultat(pFonction.getNbJoursPrisEnCompte() * 2 / 3);
			pCalculette.addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_11_MESSAGE,
					ConstantesCalculette.APPLICATION_ART_11);
		} else {
			// R415
			// si article courant (= 4 ou 5)
			// article 11 non présent
			// si durée retenue non renseigné
			// alors
			// duree article = dure prise en compte
			// calcul global pour les articles de même type.
			// float reste = resteArticle.floatValue();
			// pCalculette.setReste(resteArticle.floatValue());

			pFonction.setNbJoursArticle(pFonction.getNbJoursPrisEnCompte());
			pFonction.setNbJoursResultat(pFonction.getNbJoursArticle());
			
			// Dans la limite de 4 ans pour le classement dans un corps différent de MCF ou assimilé
			GradeRef gradeRef = TableRefHelper.getGradeById(pCalculette
					.getGradeAcceuilId());
			
			if (pCalculette.getReste5() >= pFonction.getNbJoursArticle()) {
				

				if (gradeRef == null
						|| !ConstantesCalculette.isMaitreDeConf(gradeRef
								.getGradeAcceuil())) {
//					pFonction.setNbJoursResultat(pFonction.getNbJoursArticle());					
				}
				pCalculette.setReste5(pCalculette.getReste5()
						- pFonction.getNbJoursResultat());
				resteArticle = Float.valueOf(pCalculette.getReste5());
			} else if (pCalculette.getReste5() < pFonction.getNbJoursArticle()) {
				
				if (gradeRef == null
						|| !ConstantesCalculette.isMaitreDeConf(gradeRef
								.getGradeAcceuil())) {
//				pFonction.setNbJoursResultat(pCalculette.getReste5());
//				pFonction.setNbJoursArticle(pCalculette.getReste5());
				}
				resteArticle = Float.valueOf(0);
			} else if (pCalculette.getReste5() == 0) {

				pFonction.setNbJoursResultat(0);
				pFonction.setNbJoursArticle(0);
			}
		}
		return tmpMessage;
	}

	/**
	 * @param bitSet
	 * @param pCalculette
	 * @return
	 */
	private MESSAGE calculResultatArt10Art12Art6(BitSet bitSet,
			Calculette pCalculette) {
		// R420
		String tmpCodeArticle;
		Periode tmpPeriode;
		Periode maxPeriode;
		MESSAGE tmpMessage = MESSAGE.OK;

		float total_Art10 = 0;
		float total_Art12 = 0;
		float total_Art6 = 0;
		Fonction maxFonctionArticle10 = null;
		Fonction maxFonctionArticle12 = null;
		Fonction maxFonctionArticle6 = null;

		List<Fonction> lstFonct = pCalculette.getLstFonction();
		for (Fonction f : lstFonct) {
			tmpCodeArticle = f.getCodeArticle();
			if (f.getNbJoursResultat() == 0) {
				if (!ConstantesCalculette.ARTICLE.ARTICLE_10
						.isArticleOK(tmpCodeArticle)
						&& !ConstantesCalculette.ARTICLE.ARTICLE_6
								.isArticleOK(tmpCodeArticle)
						&& !ConstantesCalculette.ARTICLE.ARTICLE_12
								.isArticleOK(tmpCodeArticle)) {
					continue;
				}

				if (ConstantesCalculette.ARTICLE.ARTICLE_6
						.isArticleOK(tmpCodeArticle)) {
					// recherche de la fonction ratachée à l'article 6
					// avec la période max
					if (maxFonctionArticle6 == null) {
						maxFonctionArticle6 = f;
					} else {
						maxPeriode = maxFonctionArticle6.getPeriode();
						tmpPeriode = f.getPeriode();

						if (maxPeriode.compareTo(tmpPeriode) < 0) {
							maxFonctionArticle6 = f;
						}
					}
					f.setNbJoursResultat(0);
					total_Art6 += f.getNbJoursPrisEnCompte();
				}

				if (ConstantesCalculette.ARTICLE.ARTICLE_10
						.isArticleOK(tmpCodeArticle)) {
					// recherche de la fonction ratachée à l'article 10
					// avec la période max
					if (maxFonctionArticle10 == null) {
						maxFonctionArticle10 = f;
					} else {
						maxPeriode = maxFonctionArticle10.getPeriode();
						tmpPeriode = f.getPeriode();

						if (maxPeriode.compareTo(tmpPeriode) < 0) {
							maxFonctionArticle10 = f;
						}
					}
					f.setNbJoursResultat(0);
					total_Art10 += f.getNbJoursPrisEnCompte();
				}

				if (ConstantesCalculette.ARTICLE.ARTICLE_12
						.isArticleOK(tmpCodeArticle)) {
					f.setNbJoursResultat(0);
					total_Art12 += f.getNbJoursPrisEnCompte();

					// recherche de la fonction ratachée à l'article 12
					// avec la période max
					if (maxFonctionArticle12 == null) {
						maxFonctionArticle12 = f;
					} else {
						maxPeriode = maxFonctionArticle12.getPeriode();
						tmpPeriode = f.getPeriode();

						if (maxPeriode.compareTo(tmpPeriode) < 0) {
							maxFonctionArticle12 = f;
						}
					}
				}
			}
		} // for

		if (maxFonctionArticle10 != null) {
			maxFonctionArticle10.setNbJoursResultat(doResultArt10Art12Art6(
					total_Art10, ConstantesCalculette.ARTICLE.ARTICLE_10,
					pCalculette));
		}
		if (maxFonctionArticle12 != null) {
			maxFonctionArticle12.setNbJoursResultat(doResultArt10Art12Art6(
					total_Art12, ConstantesCalculette.ARTICLE.ARTICLE_12,
					pCalculette));
		}

		if (maxFonctionArticle6 != null) {
			maxFonctionArticle6.setNbJoursResultat(doResultArt10Art12Art6(
					total_Art6, ConstantesCalculette.ARTICLE.ARTICLE_6,
					pCalculette));
		}
		return tmpMessage;
	}

	private float doResultArt10Art12Art6(float totalArticle,
			ConstantesCalculette.ARTICLE pArticle, Calculette pCalculette) {

		String tmpMessage;
		DureeA30Jours tmpDureeTotalArticle30jrs;
		DureeA30Jours tmpDureeTaux1A30jrs;
		DureeA30Jours tmpDureeTaux2A30jrs;
		DureeA30Jours tmpDureeTotalTauxA30jrs;

		float result = 0;
		float taux1 = 0;
		float taux2 = 0;

		switch (pArticle) {
		case ARTICLE_10:
		case ARTICLE_6:

			if (totalArticle <= VALEUR_CALCUL.NB_JRS_12_ANS.getValue()) {
				taux1 = totalArticle
						* VALEUR_CALCUL.TAUX_12_ANS_ART_10_ET_6.getValue();
			} else {
				taux1 = VALEUR_CALCUL.NB_JRS_12_ANS.getValue()
						* VALEUR_CALCUL.TAUX_12_ANS_ART_10_ET_6.getValue();
				taux2 += (totalArticle - VALEUR_CALCUL.NB_JRS_12_ANS.getValue())
						* VALEUR_CALCUL.TAUX_PLUS_12_ANS_ART_10_ET_6.getValue();
			}
			result = taux2 + taux1;
			tmpDureeTotalArticle30jrs = UtilsCalculette
					.getDureeA30Jours(totalArticle);
			tmpDureeTaux1A30jrs = UtilsCalculette.getDureeA30Jours(taux1);
			tmpDureeTaux2A30jrs = UtilsCalculette.getDureeA30Jours(taux2);
			tmpDureeTotalTauxA30jrs = UtilsCalculette.getDureeA30Jours(result);

			tmpMessage = String.format(
					ConstantesCalculette.APPLICATION_ART_10_12, pArticle
							.getLabel(), tmpDureeTotalArticle30jrs
							.getNbAnnees(), tmpDureeTotalArticle30jrs
							.getNbMois(), tmpDureeTotalArticle30jrs
							.getNbJours(), tmpDureeTaux1A30jrs.getNbAnnees(),
					tmpDureeTaux1A30jrs.getNbMois(), tmpDureeTaux1A30jrs
							.getNbJours(), tmpDureeTaux2A30jrs.getNbAnnees(),
					tmpDureeTaux2A30jrs.getNbMois(), tmpDureeTaux2A30jrs
							.getNbJours(), pArticle.getLabel(),
					tmpDureeTotalTauxA30jrs.getNbAnnees(),
					tmpDureeTotalTauxA30jrs.getNbMois(),
					tmpDureeTotalTauxA30jrs.getNbJours());

			if (ARTICLE.ARTICLE_10.compareTo(pArticle) == 0) {
				pCalculette.addMessage(
						DETAIL_CALCUL_MESSAGE.ARTICLE_10_MESSAGE, tmpMessage);
			} else {
				pCalculette.addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_6_MESSAGE,
						tmpMessage);
			}
			break;
		case ARTICLE_12:
			if (totalArticle <= VALEUR_CALCUL.NB_JRS_12_ANS.getValue()) {
				taux1 = totalArticle
						* VALEUR_CALCUL.TAUX_12_ANS_ART_12.getValue();
			} else {
				taux1 = VALEUR_CALCUL.NB_JRS_12_ANS.getValue()
						* VALEUR_CALCUL.TAUX_12_ANS_ART_12.getValue();
				taux2 = (totalArticle - VALEUR_CALCUL.NB_JRS_12_ANS.getValue())
						* VALEUR_CALCUL.TAUX_PLUS_12_ANS_ART_12.getValue();
			}

			result = taux2 + taux1;
			tmpDureeTotalArticle30jrs = UtilsCalculette
					.getDureeA30Jours(totalArticle);
			tmpDureeTaux1A30jrs = UtilsCalculette.getDureeA30Jours(taux1);
			tmpDureeTaux2A30jrs = UtilsCalculette.getDureeA30Jours(taux2);
			tmpDureeTotalTauxA30jrs = UtilsCalculette.getDureeA30Jours(result);

			tmpMessage = String.format(
					ConstantesCalculette.APPLICATION_ART_10_12, pArticle
							.getLabel(), tmpDureeTotalArticle30jrs
							.getNbAnnees(), tmpDureeTotalArticle30jrs
							.getNbMois(), tmpDureeTotalArticle30jrs
							.getNbJours(), tmpDureeTaux1A30jrs.getNbAnnees(),
					tmpDureeTaux1A30jrs.getNbMois(), tmpDureeTaux1A30jrs
							.getNbJours(), tmpDureeTaux2A30jrs.getNbAnnees(),
					tmpDureeTaux2A30jrs.getNbMois(), tmpDureeTaux2A30jrs
							.getNbJours(), pArticle.getLabel(),
					tmpDureeTotalTauxA30jrs.getNbAnnees(),
					tmpDureeTotalTauxA30jrs.getNbMois(),
					tmpDureeTotalTauxA30jrs.getNbJours());

			pCalculette.addMessage(DETAIL_CALCUL_MESSAGE.ARTICLE_12_MESSAGE,
					tmpMessage);

			break;

		default:
			break;
		}

		return result;

	}

	/*
	 * Calcul de la duree de la zone article pour chaque fonction
	 */
	private MESSAGE calculDureeArticle(Fonction pFonction) {

		MESSAGE tmpMessage = MESSAGE.OK;

		// R401 : Calcul de la durée résultant de l'application de l'article
		// sélectionné
		String tmpCodeArticle = pFonction.getCodeArticle();

		// Article 3 : pas de calcul (la durée résultant de l'application de
		// l'article 3 reste vide),
		// Article 4 : pas de calcul (durée résultant de l'application de
		// l'article 4 reste vide),
		// Article 5 : pas de calcul (durée résultant de l'application de
		// l'article 5 reste vide),
		// Article 10 : pas de calcul (durée résultant de l'application de
		// l'article 10 reste vide),
		// Article 12 : pas de calcul (durée résultant de l'application de
		// l'article 12 reste vide),
		// Articles Libre et Non retenue : pas de calcul (durée résultant de
		// l'application de l'article de ces articles reste vide)

		if ((ConstantesCalculette.ARTICLE.ARTICLE_3.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_4
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_5
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_10
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_12
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_LIBRE
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_NON_RETENU
						.isArticleOK(tmpCodeArticle))) {
			// rien à faire. Durée est nulle
		} else if ((ConstantesCalculette.ARTICLE.ARTICLE_8
				.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_9_MCF
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_9_PR
						.isArticleOK(tmpCodeArticle))
				|| (ConstantesCalculette.ARTICLE.ARTICLE_SNA
						.isArticleOK(tmpCodeArticle))) {
			// Article 8 : la durée résultant de l'application de l'article 8
			// est égale à la durée prise en compte calculée
			// Article 9 : la durée résultant de l'application de l'article 9
			// est égale à la durée prise en compte calculée
			// Article SNA : la durée résultant de l'application de l'article
			// SNA est égale à la durée prise en compte calculée
			pFonction.setNbJoursArticle(pFonction.getNbJoursPrisEnCompte());
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_15_II
				.isArticleOK(tmpCodeArticle)) {
			// Article 15-II : la durée résultant de l'application de l'article
			// 15-II est toujours égale é 2 ans
			pFonction
					.setNbJoursArticle(ConstantesCalculette.NB_JOURS_PAR_AN * 2);
		} else if (ConstantesCalculette.ARTICLE.ARTICLE_11
				.isArticleOK(tmpCodeArticle)) {
			// Article 11 : la durée résultant de l'application de l'article 11
			// est égale aux 2/3 de la durée prise en compte calculée
			pFonction
					.setNbJoursArticle(pFonction.getNbJoursPrisEnCompte() * 2 / 3);
		} else if(ConstantesCalculette.ARTICLE.ARTICLE_5_1
				.isArticleOK(tmpCodeArticle)) {
			pFonction
			.setNbJoursArticle(ConstantesCalculette.NB_JOURS_PAR_AN);
		}
		return tmpMessage;
	}

	/**
	 * @param pFonction
	 */
	private MESSAGE calculDureePriseEnCompte(Fonction pFonction) {

		MESSAGE tmpMessage = MESSAGE.OK;

		if (pFonction == null) {
			return tmpMessage;
		}
		float nbDays;
		Periode tmpPeriode = pFonction.getPeriode();

		if (!tmpPeriode.isEmpty()) {

			// R205 Pour chaque ligne du tableau des fonctions exercées dont
			// la période est renseignée et la quotité retenue néest pas
			// renseignée,
			// la quotité retenue est initialisée é la valeur de la quotité
			// réelle.
			if (pFonction.getQuotiteRetenue() == null) {
				pFonction.setQuotiteRetenue(pFonction.getQuotiteReelle());
			}

			// R400 calcul duree prise en compte
			// duree prise en compte = periode.nbJoursA30 * quotiteRetenue / 100

			nbDays = tmpPeriode.getNbJoursA30Jours();
			nbDays = nbDays * pFonction.getQuotiteRetenue() / 100f;
			pFonction.setNbJoursPrisEnCompte(nbDays);
		}
		return tmpMessage;
	}

	// / traitement utilitaires
	// //////////////////////////
	/**
	 * Traitement permettant de rechercher le dernier element de la liste des
	 * fonctions
	 * 
	 * @param pCalculette
	 * @return
	 */
	private Fonction rechercheDernierElementListFonctions(Calculette pCalculette) {
		List<Fonction> fonctions = pCalculette.getLstFonction();
		Fonction dernierFonction = null;
		for (Fonction fonction : fonctions) {
			dernierFonction = fonction;
		}

		return dernierFonction;
	}

	/**
	 * Traitement permettant de rechercher le plus petit echelon/chevron dont
	 * l'ibr est >= à l'ibr passé en paramétre
	 * 
	 * @param lstGrilleIndiciaire
	 * @param ibr
	 * @param chevron
	 * @param echelon
	 * @return
	 */
	private GrilleIndiciaireRef rechercheLePlusPetit(
			List<GrilleIndiciaireRef> lstGrilleIndiciaire, int ibr) {
		GrilleIndiciaireRef grilleFound = null;
		boolean isFound;

		for (GrilleIndiciaireRef grille : lstGrilleIndiciaire) {
			if ((grille.getIndiceBrut() == ibr)
					|| (grille.getIndiceBrut() > ibr)) {

				if (grilleFound == null) {
					grilleFound = grille;
				} else {
					isFound = (grille.getEchelon() < grilleFound.getEchelon())
							|| ((grille.getEchelon() == grilleFound
									.getEchelon()) && (grille.getChevron() < grilleFound
									.getChevron()));
					if (isFound) {
						grilleFound = grille;
					}
				}
			}
		}// for
		return grilleFound;
	}

	/**
	 * recherche le plus grand élément
	 * 
	 * @param pLstGrilleIndiciaire
	 * @param pChevron
	 * @param pEchelon
	 * @return
	 */
	private GrilleIndiciaireRef rechercheLePlusGrand(
			List<GrilleIndiciaireRef> pLstGrilleIndiciaire, int pChevron,
			int pEchelon) {
		boolean isFound;
		GrilleIndiciaireRef grilleFound = null;
		int currentChevron = pChevron;
		int currentEchelon = pEchelon;

		for (GrilleIndiciaireRef grille : pLstGrilleIndiciaire) {

			isFound = (grille.getEchelon() > currentEchelon)
					|| ((grille.getChevron() > currentChevron) && (grille
							.getEchelon() == currentEchelon));
			// Modif JCE 13/01/10
			// isFound = (grille.getChevron() > currentChevron)
			// && (grille.getEchelon() == currentEchelon);

			if (isFound) {
				grilleFound = grille;
				currentChevron = grilleFound.getChevron();
				currentEchelon = grilleFound.getEchelon();
			}
		}// for
		return grilleFound;
	}

	/**
	 * Recherche dans une liste de grille, l'élément ayant le même chevron et le
	 * même échelon que ceux passé en paramètre
	 * 
	 * @param lstGrilleIndiciaire
	 *            : la liste des grilles indiciaires. elles ont toutes le même
	 *            grade id
	 * @param pEchelon
	 * @param pChevron
	 * @return
	 */
	private GrilleIndiciaireRef rechercheByEchelonByChevron(
			List<GrilleIndiciaireRef> lstGrilleIndiciaire, int pEchelon,
			int pChevron) {
		boolean isFound;
		GrilleIndiciaireRef grilleFound = null;

		for (GrilleIndiciaireRef grille : lstGrilleIndiciaire) {
			isFound = (grille.getEchelon() == pEchelon)
					&& (grille.getChevron() == pChevron);

			if (isFound) {
				grilleFound = grille;
				break;
			}
		}// for
		return grilleFound;
	}

	public void mapDatasCalculette(HttpServletRequest request, Calculette pCalculette) {
		try {

			String tmpStr = request.getParameter(FORM_CALCULETTE_NAME.NOM.getNom());
			// simpleLog.println("  - request nom *" + tmpStr);
			pCalculette.setNom(tmpStr);
			tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.PRENOM.getNom());
			// simpleLog.println("  - request prenom *" + tmpStr + "*");
			pCalculette.setPrenom(tmpStr);

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.DT_NAISS.getNom());

			pCalculette.setDateNaissance(Conversion.stringToDate(tmpStr));
			// simpleLog.println("  - request dt naissance *" + tmpStr + "*");

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.DT_NOMIN.getNom());

			pCalculette.setDateNomination(Conversion.stringToDate(tmpStr));
			// simpleLog.println("  - request dt nomination *" + tmpStr + "*");
			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.CORPS_ACCEUIL.getNom());
			pCalculette.setCorspAcceuilId(Conversion.stringToLong(tmpStr));
			// simpleLog.println("  - request corps d'acceuil *" + tmpStr +
			// "*");

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.GRADE_ACCEUIL.getNom());
			// simpleLog.println("  - request grade d'acceuil *" + tmpStr +
			// "*");
			pCalculette.setGradeAcceuilId(Conversion.stringToLong(tmpStr));

			tmpStr = request.getParameter("id_"+ FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom());
			if (tmpStr == null || tmpStr.equals(""))
				tmpStr = request.getParameter(FORM_CALCULETTE_NAME.CORPS_ORIGINE.getNom());
			pCalculette.setCorpsOrigineLibelleOrId(tmpStr);

			tmpStr = request.getParameter("id_"+ FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom());
			if (tmpStr == null || tmpStr.equals(""))
				tmpStr = request.getParameter(FORM_CALCULETTE_NAME.GRADE_ORIGINE.getNom());
			pCalculette.setGradeOrigineLibelleOrId(tmpStr);

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.ECHELON_ORIGINE.getNom());
			// simpleLog.println("request echelon d'origine *" + tmpStr + "*");
			pCalculette.setEchelonOrigine(Conversion.stringToInteger(tmpStr));

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.CHEVRON_ORIGINE.getNom());
			// simpleLog.println("request chevron d'origine :" + tmpStr);
			pCalculette.setChevronOrigine(Conversion.stringToInteger(tmpStr));

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.IBR_DETENU_ORIGINE.getNom());
			// simpleLog.println("request indice brut détenu  :" + tmpStr);
			pCalculette.setIndiceBrutDetenue(Conversion.stringToInteger(tmpStr));

			tmpStr = request.getParameter(FORM_CALCULETTE_NAME.IBR_ECH_SUP_ORIGINE.getNom());
			// simpleLog.println("request indice brut ech sup  :" + tmpStr);
			pCalculette.setIndiceBrutEchelonSup(Conversion.stringToInteger(tmpStr));

			// fonction
			tmpStr = request.getParameter("nbFonctions");
			simpleLog.println("nbFonctions = " + tmpStr);

			int nbFonction = Conversion.stringToInteger(tmpStr);

			List<Fonction> lst = new ArrayList<Fonction>();
			pCalculette.setLstFonction(lst);

			Fonction tmpFonc;
			Periode tmpPeriode;
			Date tmpDateDeb;
			Date tmpDateFin;
			String strTmpDateDeb;
			String strTmpDateFin;
			DureeA30Jours tmpDureeA30Jrs;
			Integer nbAA;
			Integer nbMM;
			Integer nbJJ;

			for (int ii = 1; ii < nbFonction + 1; ii++) {
				tmpFonc = new Fonction();

				tmpStr = (String) request.getParameter("f_" + ii);
				if (tmpStr != null && tmpStr.equals("f_" + ii)) {
					// Periode
					tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_DT_DEB_PREFIX.getNom()+ ii);
					tmpDateDeb = Conversion.stringToDate(tmpStr);
					strTmpDateDeb = tmpStr;
					if (tmpDateDeb==null) {
						tmpDateDeb = new Date(0);
					}
					tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_DT_FIN_PREFIX.getNom()+ ii);
					tmpDateFin = Conversion.stringToDate(tmpStr);
					strTmpDateFin = tmpStr;
					if (tmpDateFin==null) {
						tmpDateFin = new Date(1);
					}
					tmpPeriode = new Periode(tmpDateDeb, tmpDateFin, strTmpDateDeb,strTmpDateFin);
					tmpFonc.setPeriode(tmpPeriode);

					// Fonction
					tmpStr = (String) request.getParameter("id_"+ FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom() + ii);
					tmpFonc.setCodeFonction(tmpStr);
					tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_FONCTION_PREFIX.getNom()+ ii);
					tmpFonc.setLibelleFonction(tmpStr);

					// Article
					tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+ ii);
					tmpFonc.setCodeArticle(tmpStr);
					tmpStr = (String) request.getParameter("obs_"+FORM_CALCULETTE_NAME.FONC_ARTICLE_PREFIX.getNom()+ ii);
                    tmpFonc.setObservations(tmpStr);

					// Quotite reelle
					try 
					  {
						tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_QUOTITE_REELLE_PREFIX.getNom()+ ii);
					  } 
					catch (NumberFormatException nfe) 
					  {
					   nfe.printStackTrace();
					  }
					tmpFonc.setQuotiteReele(Conversion.stringToInteger(tmpStr));

					// Quotite retenue
					tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_QUOTITE_RET_PREFIX.getNom()+ ii);
					tmpFonc.setQuotiteRetenue(Conversion.stringToInteger(tmpStr));

					try {
						tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_AA_PREFIX.getNom()+ ii);
						// if(tmpStr.trim().equals("")) throw new
						// NullPointerException(FORM_CALCULETTE_NAME.FONC_DUREE_RET_AA_PREFIX.getNom()+" non renseigne")
						// ;
						nbAA = Conversion.stringToInteger(tmpStr);

						tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_MM_PREFIX.getNom()+ ii);
						// if(tmpStr.trim().equals("")) throw new
						// NullPointerException(FORM_CALCULETTE_NAME.FONC_DUREE_RET_MM_PREFIX.getNom()+" non renseigne")
						// ;
						nbMM = Conversion.stringToInteger(tmpStr);

						tmpStr = (String) request.getParameter(FORM_CALCULETTE_NAME.FONC_DUREE_RET_JJ_PREFIX.getNom()+ ii);
						// if(tmpStr.trim().equals("")) throw new
						// NullPointerException(FORM_CALCULETTE_NAME.FONC_DUREE_RET_JJ_PREFIX.getNom()+" non renseigne")
						// ;
						nbJJ = Conversion.stringToInteger(tmpStr);

						tmpDureeA30Jrs = new DureeA30Jours(nbAA, nbMM, nbJJ);
						tmpFonc.setDureeRetenueA30Jours(tmpDureeA30Jrs);
					} catch (Exception ex) {
						simpleLog.println(ex.getMessage());
						ex.printStackTrace();
					}

					simpleLog.println(" - fonction : "
							+ tmpFonc.getCodeFonction() + "  "
							+ tmpFonc.getCodeArticle() + "    "
							+ tmpFonc.getPeriode().getDateDebut() + " -> "
							+ tmpFonc.getPeriode().getDateFin());

					lst.add(tmpFonc);
				}
			}

			/* Sort.trier(lst, "dateDeb,dateFin", false); */

		} catch (Exception ex) {
			simpleLog.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			// simpleLog.println(pCalculette.toString());
		}
	}

	/**
	 * Traitement permettant de mapper les donnes des structures jaxb vers la
	 * calculette
	 * 
	 * @param pCalculette
	 * @param pCalExport
	 */
	private void mapDatasCalculetteToCalExport(Calculette pCalculette, DataCalculetteExport pCalExport) {

		pCalculette.setNom(pCalExport.getNom());
		pCalculette.setPrenom(pCalExport.getPrenom());
		pCalculette.setDateNaissance(pCalExport.getDateDeNaissance().getTime());

		pCalculette.setDateNomination(pCalExport.getDateDeNomination().getTime());
		pCalculette.setCorspAcceuilId(pCalExport.getCorpsAcceuilId());
		pCalculette.setGradeAcceuilId(pCalExport.getGradeAcceuilId());

		// zone donne origine
		DonneeOrigineExport donneeOrigine = pCalExport.getDonneeOrigineExport();
		pCalculette.setCorpsOrigineLibelleOrId(donneeOrigine.getCorpsOrigine());
		pCalculette.setGradeOrigineLibelleOrId(donneeOrigine.getGradeOrigine());
		pCalculette.setIndiceBrutDetenue(Conversion.stringToInteger(donneeOrigine.getIndiceBrutDetenu()));
		pCalculette.setIndiceBrutEchelonSup(Conversion.stringToInteger(donneeOrigine.getIndiceBrutEchelonSup()));
		pCalculette.setChevronOrigine(donneeOrigine.getChevronOrigine());
		pCalculette.setEchelonOrigine(donneeOrigine.getEchelonOrigine());

		// zone fonction
		List<Fonction> lstFonct = new ArrayList<Fonction>();
		pCalculette.setLstFonction(lstFonct);
		List<FonctionExport> lstDataExport = pCalExport.getFonctionExport();

		Fonction tmpFonct;
		Periode tmpPeriode;
		Date tmpDateDeb;
		Date tmpDateFin;
		DureeA30Jours tmpDure30Jrs;

		for (FonctionExport tmpFonctExport : lstDataExport) {
			tmpFonct = new Fonction();

			tmpDateDeb = (tmpFonctExport.getDateDeDebut() != null) ? tmpFonctExport.getDateDeDebut().getTime(): null;
			tmpDateFin = (tmpFonctExport.getDateDeFin() != null) ? tmpFonctExport.getDateDeFin().getTime(): null;
			tmpPeriode = new Periode(tmpDateDeb, tmpDateFin);
			tmpFonct.setPeriode(tmpPeriode);

			tmpFonct.setCodeFonction(tmpFonctExport.getCodeFonction());
			tmpFonct.setLibelleFonction(tmpFonctExport.getLibelleFonction());

			tmpFonct.setCodeArticle(tmpFonctExport.getLibelleArticle());
			tmpFonct.setObservations(tmpFonctExport.getObservations()) ;
			tmpFonct.setQuotiteReele(tmpFonctExport.getQuotiteReelle());
			tmpFonct.setQuotiteRetenue(tmpFonctExport.getQuotiteRetenue());

			tmpDure30Jrs = new DureeA30Jours(tmpFonctExport.getDureeRetenueAnnee(), 
			                                 tmpFonctExport.getDureeRetenueMois(), 
			                                 tmpFonctExport.getDureeRetenueJour());
			tmpFonct.setDureeRetenueA30Jours(tmpDure30Jrs);
			lstFonct.add(tmpFonct);

		}
	}

	private void mapDatasCalExportToCalculette(Calculette pCalculette, DataCalculetteExport pCalExport) {

		pCalExport.setEntete("entete");

		ObjectFactory factory = new ObjectFactory();

		pCalExport.setNom(pCalculette.getNom());
		pCalExport.setPrenom(pCalculette.getPrenom());

		Calendar tmpCal = Calendar.getInstance();
		tmpCal.setTime(pCalculette.getDateNaissance());
		pCalExport.setDateDeNaissance(tmpCal);

		Calendar tmpCal1 = Calendar.getInstance();
		tmpCal1.setTime(pCalculette.getDateNomination());
		pCalExport.setDateDeNomination(tmpCal1);

		pCalExport.setCorpsAcceuilId(pCalculette.getCorspAcceuilId());
		pCalExport.setGradeAcceuilId(pCalculette.getGradeAcceuilId());

		// zone donne origine
		DonneeOrigineExport donneExport = factory.createDonneeOrigineExport();
		pCalExport.setDonneeOrigineExport(donneExport);

		donneExport.setCorpsOrigine(pCalculette.getCorpsOrigineLibelleOrId());
		donneExport.setGradeOrigine(pCalculette.getGradeOrigineLibelleOrId());
		donneExport.setIndiceBrutDetenu(Conversion.integerToString(pCalculette.getIndiceBrutDetenue()));
		donneExport.setIndiceBrutEchelonSup(Conversion.integerToString(pCalculette.getIndiceBrutEchelonSup()));
		donneExport.setChevronOrigine(pCalculette.getChevronOrigine());
		donneExport.setEchelonOrigine(pCalculette.getEchelonOrigine());

		// zone fonction
		List<FonctionExport> lstFonctionExport = pCalExport.getFonctionExport();
		FonctionExport tmpFonctionExport;
		Periode tmpPeriode;
		DureeA30Jours tmpDureeA30Jrs;

		List<Fonction> lstFonction = pCalculette.getLstFonction();

		for (Fonction tmpFonct : lstFonction) {
			tmpFonctionExport = factory.createFonctionExport();
			lstFonctionExport.add(tmpFonctionExport);

			tmpPeriode = tmpFonct.getPeriode();
			if (tmpPeriode != null) {

				if (tmpPeriode.getDateDebut() != null) {
					tmpCal = Calendar.getInstance();
					tmpCal.setTime(tmpPeriode.getDateDebut());
					tmpFonctionExport.setDateDeDebut(tmpCal);
				}

				if (tmpPeriode.getDateFin() != null) {
					tmpCal = Calendar.getInstance();
					tmpCal.setTime(tmpPeriode.getDateFin());
					tmpFonctionExport.setDateDeFin(tmpCal);
				}
			}

			tmpFonctionExport.setLibelleArticle(tmpFonct.getCodeArticle());
			tmpFonctionExport.setObservations(tmpFonct.getObservations()) ;
			tmpFonctionExport.setCodeFonction(tmpFonct.getCodeFonction());
			tmpFonctionExport.setLibelleFonction(tmpFonct.getLibelleFonction());
			tmpFonctionExport.setQuotiteReelle(tmpFonct.getQuotiteReelle());
			tmpFonctionExport.setQuotiteRetenue(tmpFonct.getQuotiteRetenue());

			tmpDureeA30Jrs = tmpFonct.getDureeRetenueA30Jours();

			tmpFonctionExport.setDureeRetenueAnnee(tmpDureeA30Jrs.getNbAnnees());
			tmpFonctionExport.setDureeRetenueMois(tmpDureeA30Jrs.getNbMois());
			tmpFonctionExport.setDureeRetenueJour(tmpDureeA30Jrs.getNbJours());
		}
	}

}
