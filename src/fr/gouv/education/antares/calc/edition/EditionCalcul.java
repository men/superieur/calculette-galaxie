package fr.gouv.education.antares.calc.edition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.simpleLog;
import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.calc.ConstantesCalculette.ARTICLE;
import fr.gouv.education.antares.calc.ConstantesCalculette.MESSAGE;
import fr.gouv.education.antares.calc.objets.Calculette;
import fr.gouv.education.antares.calc.objets.DureeA30Jours;
import fr.gouv.education.antares.calc.objets.Fonction;
import fr.gouv.education.antares.calc.objets.Periode;
import fr.gouv.education.antares.calc.outils.ref.metier.TableRefHelper;
import fr.gouv.education.antares.calc.outils.ref.objects.CorpsRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GradeRef;
import fr.gouv.education.antares.outils.Conversion;


/**
 * 
 */
public class EditionCalcul {
	
	private final static String ENTETE1_ARTICLE = "Règles simplifiées de prise en compte pour chaque article :";
	private final static String ENTETE2_ARTICLE = "(pour plus de détails, voir le décret n°2009-462 du 23 avril 2009)";

	private final static String FOOTER1_ARTICLE ="NB : Règles qui s’appliquent à tous les articles :";
	private final static String FOOTER2_ARTICLE ="- Les fonctions exercées à temps partiel sont prises à concurrence des services réellement effectués.";
	private final static String FOOTER3_ARTICLE ="- Une même période n’est prise en compte qu’une seule fois.";


	private final static String TITRE1 = "Reclassement";
	private final static String TITRE2 = "(au titre du décret n°2009-462 du 23 avril 2009)";

	private final static String CHAMP_ETABLISSEMENT_1 = "Établissement : ";
	private final static String CHAMP_ETABLISSEMENT_2 = " (ex RNE) - ";
	
	private final static String CHAMP_NOM = "NOM : ";

	private final static String CHAMP_PRENOM = "Prénom : ";

	private final static String CHAMP_DT_NAISSANCE = "Date de naissance : ";
	
	private final static String CHAMP_DT_NOMINATION = "Date de nomination : ";

	private final static String CHAMP_GRADE_ACCUEIL = "Grade d’accueil : ";

	private final static String CHAMP_CORPS_ACCUEIL = "Corps d’accueil :";

	private final static String CHAMP_CAS_ECHEANT = "Le cas échéant";

	private final static String CHAMP_CORPS_ORIGINE = "Corps d’origine :";

	private final static String CHAMP_GRADE_ORIGINE = "Grade d’origine : ";

	private final static String LIBELLE_ECHELON = "Échelon : ";
	
	private final static String LIBELLE_CHEVRON = "Chevron : ";

	private final static String CHAMP_INDICE_ORIGINE = "Indice d’origine (IB) : ";
	
	private final static String CHAMP_INDICE_BRUT = "IB : ";

	private final static String CHAMP_TITRE_SERV_RETENU = "Services retenus pour le classement" + ConstantesCalculette.LINEFEED;

	private final static String CHAMP_TOTAL = "Total";
	

	private final static String CHAMP_TITRE_SERV_NON_RETENU  = "Services non retenus pour le classement" + ConstantesCalculette.LINEFEED;

	private final static String CHAMP_ANCIENNETE_CONSERV = "Ancienneté conservée : ";

	private final static String CHAMP_CLASSEMENT_PROPOSE = "Au vu des éléments ci-dessus, classement proposé : ";
	
	private final static String CHAMP_DETAILS_DES_CALCULS = "Détail des calculs : ";


	private static final String[] SERVICE_RETENU_HEADER = {"Fonctions","Article","Quotité","Période","Durée comptabilisée","Durée retenue"};


	private static final String[] SERVICE_NON_RETENU_HEADER ={"Fonctions","Période","Observations éventuelles"};




	private static enum DESCRIPTION_ARTICLE{

		DESC_ART3("Article 3","Fonctionnaire","Classement à l’indice égal ou immédiatement supérieur à celui détenu dans le corps d’origine avec ancienneté d’échelon acquise dans l’ancien échelon conservée, dans la limite de l’ancienneté exigée pour le passage à l’échelon supérieur dans le nouveau corps, si l’augmentation de traitement est inférieure à celle pouvant résulter d’un avancement d’échelon dans son ancien corps"),

		DESC_ART4("Article 4","Préparation du doctorat dans le cadre d’un contrat de travail ayant fait l’objet d’une convention avec une personne publique (CIFRE)","Temps consacré à la préparation du doctorat pris en compte dans la limite de la durée de la convention, ne pouvant excéder une durée totale de 6 ans et après avis du conseil académique ou l'organe en tenant lieu."),

		DESC_ART5("Article 5","Recherche après obtention du doctorat dans le cadre d’un contrat de travail","Post doc dans le cadre d’un contrat de travail pris en compte dans la totalité pour le classement et après avis du conseil scientifique ou l'organe en tenant lieu."),
		
		DESC_ART5_1("Article 5-1","Obtention du doctorat","Bonification d’ancienneté d’un an au titre de l’obtention du doctorat pour le classement dans la classe normale du corps des maîtres de conférences."),

		DESC_ART6("Article 6","Services de praticiens hospitaliers","Services de catégorie A pris en compte à ½ de la durée des fonctions jusqu’à 12 ans et au 3/4 au-delà de 12 ans, Services de catégorie B : rien pour les 7 premières années, 6/16 de la durée entre 7 ans et 16 ans et 9/16 de la durée au-delà de 16 ans"),

		DESC_ART8("Article 8","Services d’ATER, allocataire de recherche, moniteur, doctorant contractuel, pour une nomination de MCF",""),

		DESC_ART9("Article 9","Services d’enseignant associé en application du décret du 17 juillet 1985","Service pris en compte en totalité pour le classement dans le corps de niveau correspondant au niveau de l’association."),

		DESC_ART10("Article 10","Services d’agent non titulaire de l’État, des collectivités locales ou de leurs établissements publics","Services de catégorie A pris en compte à ½ de la durée des fonctions jusqu’à 12 ans et au 3/4 au-delà de 12 ans, Services de catégorie B : rien pour les 7 premières années, 6/16 de la durée entre 7 ans et 16 ans et 9/16 de la durée au-delà de 16 ans"),

		DESC_ART11("Article 11","Services de chercheurs des EPST ou personnels scientifiques contractuels des établissements publics de recherche et des GIP au moment de leur nomination en qualité de MCF ou de PR","Si le niveau de fonctions requis est au moins équivalent à celui du corps d’accueil, l’ancienneté est retenue au 2/3 des services effectifs et dans sa totalité si le niveau et la nature des fonctions sont confirmés par le conseil scientifique."),

		DESC_ART12("Article 12","Services dans le secteur privé","Services dans des fonctions et domaines d’activité de niveau et de nature comparables à ceux dans lesquels exercent les membres du corps d’accueil pris en compte à ½ de la durée des fonctions jusqu’à 12 ans et au 2/3 au-delà de 12 ans après avis du conseil scientifique."),

		DESC_ART13("Article 13","Services accomplis dans une administration ou un organisme ou un établissement d’un État membre de la Communauté européenne, autre que la France, ou d’un autre État partie à l’accord sur l’Espace économique européen (+ Suisse, Monaco et Andorre)","Après avis du conseil scientifique, services pris en compte au titre des articles 3 à 12 suivant les fonctions effectuées."),

		DESC_ART14("Article 14","Services accomplis dans un pays non membre de l’union européenne et de l’EEE","Après avis du conseil scientifique, services pris en compte au titre des articles 10 et 12 suivant les fonctions effectuées."),

		DESC_ART15_II("Article 15 - II","Préparation doctorat, pour une nomination de MCF", "Bonification de deux ans accordée si la préparation au doctorat n’a pas fait l’objet d’un classement plus favorable au titre d’un autre article.")

		;

		private String codeArticle;
		private String libelleArticle;
		private String desciptionArticle;

		private DESCRIPTION_ARTICLE(String pCode, String pLib, String pDesc){
			codeArticle = pCode;
			libelleArticle = pLib;
			desciptionArticle = pDesc;
		}

		public String getLibelle(){
			return libelleArticle;
		}

		public String getDesciption(){
			return desciptionArticle;
		}

		public String getCode(){
			return codeArticle;
		}
	}



    
  public static MESSAGE lanceEdition(Calculette pCal, File tmpFile) {
	  
	  MESSAGE tmpMessage = MESSAGE.OK;
	  FileOutputStream fos = null;
	  Document document = null;
	  String tmpCodeArticle;
	  Periode tmpPeriode;
	  DureeA30Jours tmpDureeA30Jours;
	  PdfPCell cell;
	  
	  try{
		// step 1: creation of a document-object
		   
		  document = new Document(PageSize.A4,20,20,20,20);

		// step 2:
		// we create a writer that listens to the document
		// and directs a PDF-stream to a file
		
		fos = new FileOutputStream(tmpFile) ;
		PdfWriter.getInstance(document,fos);
		// step 3: we grab the ContentByte and do some stuff with it
		// we create a PdfTemplate
		// we add some graphics
		// we add some text

		Font fontUnderlineGras = new Font(Font.TIMES_ROMAN, 10, Font.BOLD | Font.UNDERLINE);
		
		Font fontItalic = new Font(Font.TIMES_ROMAN, 10, Font.ITALIC);
		Font font10         = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		//            Font font6         = new Font(Font.TIMES_ROMAN, 6, Font.NORMAL);
		Font fontGras       = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);

		// step 4: we open the document
		document.open();
		
		// ajout du titre
		Paragraph paragraph = new Paragraph(TITRE1,fontUnderlineGras);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);
		document.add(paragraph);

		paragraph = new Paragraph(TITRE2,font10);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);            
		document.add(paragraph);
		
		document.add(Chunk.NEWLINE);
		
		float[] colWidths = new float[] {0.18f, 0.15f, 0.10f,0.18f, 0.15f, 0.15f} ;
		PdfPTable tableIdentite = new PdfPTable(colWidths);

		tableIdentite.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		tableIdentite.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		// ajout etablissement		
				
		paragraph = new Paragraph(CHAMP_ETABLISSEMENT_1,font10);
		cell = new PdfPCell(paragraph);
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(pCal.getNumEtablissement() + CHAMP_ETABLISSEMENT_2  + pCal.getNomEtablissement() ,font10);
		cell = new PdfPCell(paragraph);
		cell.setColspan(5);
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		
		// ajout du nom et prénom
		paragraph = new Paragraph(CHAMP_NOM ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(pCal.getNom() ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);				
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_PRENOM ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(pCal.getPrenom() ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		// date de naisance	
		paragraph = new Paragraph(CHAMP_DT_NAISSANCE ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph( Conversion.formatDate(pCal.getDateNaissance(),Constantes.DATE_FORMAT_STRING) ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		cell.setColspan(5);
		tableIdentite.addCell(cell);
		
		// date de nomination	
		paragraph = new Paragraph(CHAMP_DT_NOMINATION ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph( Conversion.formatDate(pCal.getDateNomination(),Constantes.DATE_FORMAT_STRING) ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		cell.setColspan(5);
		tableIdentite.addCell(cell);
		
		// corps grade acceuil
		CorpsRef tmpCorpsRef = TableRefHelper.getCorsRefById(pCal.getCorspAcceuilId()) ;
		GradeRef tmpGradeRef = TableRefHelper.getGradeById(pCal.getGradeAcceuilId()) ;
		
		paragraph = new Paragraph(CHAMP_CORPS_ACCUEIL ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(tmpCorpsRef.getLibelleLong() ,font10);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);	
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_GRADE_ACCUEIL ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(tmpGradeRef.getLibelleLong() ,font10);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		
		
// Donnee origine
		Long tmpId = null;
		try
		  {
		   tmpId = Long.parseLong(pCal.getCorpsOrigineLibelleOrId());
		  }
		catch(Exception e){ tmpId =null; }
		String corpsLibelle="";
		if(tmpId != null)
		  {
		   CorpsRef tmpCorps = TableRefHelper.getCorsRefById(tmpId);
		   if(tmpCorps == null)
		     corpsLibelle = pCal.getCorpsOrigineLibelleOrId();
		   else 
		     corpsLibelle = tmpCorps.getLibelleLong();
		  }
		else
		  corpsLibelle = pCal.getCorpsOrigineLibelleOrId() ;
		
		try
		  {
		   tmpId = Long.parseLong(pCal.getGradeOrigineLibelleOrId());
		  }
		catch(Exception e){ tmpId =null; }
		String gradeLibelle="";
		if(tmpId != null)
		  {
		   GradeRef tmpGrade = TableRefHelper.getGradeById(tmpId);
		   if(tmpGrade == null)
			 gradeLibelle = pCal.getGradeOrigineLibelleOrId();
		   else 
			 gradeLibelle = tmpGrade.getLibelleLong();
		  }
		else
		   gradeLibelle = pCal.getGradeOrigineLibelleOrId();
		
		paragraph = new Paragraph(CHAMP_CAS_ECHEANT ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		cell.setColspan(6);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_CORPS_ORIGINE ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		
		//System.out.println("*corpsLibelle* "+corpsLibelle);
		
		paragraph = new Paragraph(corpsLibelle ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_GRADE_ORIGINE ,fontGras);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		
		
		System.out.println("*gradeLibelle* "+gradeLibelle);
		paragraph = new Paragraph(gradeLibelle ,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(LIBELLE_ECHELON ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		System.out.println("*getEchelonOrigine != null* "+pCal.getEchelonOrigine());
		paragraph = new Paragraph(Conversion.integerToString(pCal.getEchelonOrigine()) ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(LIBELLE_CHEVRON ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		System.out.println("*getChevronOrigine != null* "+pCal.getChevronOrigine());
		paragraph = new Paragraph(Conversion.integerToString(pCal.getChevronOrigine()) ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableIdentite.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_INDICE_ORIGINE + Conversion.integerToString(pCal.getIndiceBrutDetenue()) ,fontItalic);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);	
		cell.setColspan(2);
		tableIdentite.addCell(cell);
		
		document.add(tableIdentite);
		document.add(Chunk.NEWLINE);
		
		// remplissage des différentes tables
		List<Fonction> lstFonct = (List<Fonction>) pCal.getLstFonction();				

		// gestion table service retenu			
		
		PdfPTable tableServiceRetenu = new PdfPTable(SERVICE_RETENU_HEADER.length);
		tableServiceRetenu.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		tableServiceRetenu.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		paragraph = new Paragraph(CHAMP_TITRE_SERV_RETENU,font10);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);
		
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(6);
		tableServiceRetenu.addCell(cell);
		
		// ajout de l'entete du tableu pour service retenu
		for(String header : SERVICE_RETENU_HEADER){
			cell = new PdfPCell(new Paragraph(header,fontGras));                
			tableServiceRetenu.addCell(cell);
		}
		
		
		for(Fonction f: lstFonct){			
			tmpCodeArticle = f.getCodeArticle();
			if(!ARTICLE.ARTICLE_NON_RETENU.isArticleOK(tmpCodeArticle)){				
				
			    if(f.getCodeFonction() != null && !f.getCodeFonction().equals(""))
				  cell = new PdfPCell(new Paragraph(TableRefHelper.getFonctionById(f.getCodeFonction()).getLibelleLong(),font10));
			    else
			      cell = new PdfPCell(new Paragraph(f.getLibelleFonction(),font10));  
				tableServiceRetenu.addCell(cell);
				
				cell = new PdfPCell(new Paragraph(TableRefHelper.getArticleById(f.getCodeArticle()).getLibelleCourt()+" - " +TableRefHelper.getArticleById(f.getCodeArticle()).getLibelleLong(),font10));
				tableServiceRetenu.addCell(cell);
				
				cell = new PdfPCell(new Paragraph(f.getQuotiteRetenue().toString(),font10));
				tableServiceRetenu.addCell(cell);
				
				tmpPeriode = f.getPeriode();

				if ( tmpPeriode != null){
					cell = new PdfPCell(new Paragraph(tmpPeriode.toStringPeriod("Du ", ConstantesCalculette.TIRET),font10));
					tableServiceRetenu.addCell(cell);
				} else {
					cell = new PdfPCell(new Paragraph(""));
					tableServiceRetenu.addCell(cell);
				}

				tmpDureeA30Jours = new DureeA30Jours(f.getNbJoursPrisEnCompte());  
				cell = new PdfPCell(new Paragraph(tmpDureeA30Jours.toString(),font10));
				tableServiceRetenu.addCell(cell);
				
				tmpDureeA30Jours = new DureeA30Jours(f.getNbJoursResultat());          
				cell = new PdfPCell(new Paragraph(tmpDureeA30Jours.toString(),font10));
				tableServiceRetenu.addCell(cell);				
			}

		}// for
		
		cell = new PdfPCell(new Paragraph(CHAMP_TOTAL));
		cell.setColspan(5);
		tableServiceRetenu.addCell(cell);
		// total duree retenue		
		tmpDureeA30Jours = new DureeA30Jours(pCal.getNbJoursFinal());
		cell = new PdfPCell(new Paragraph(tmpDureeA30Jours.toString(),font10));
		tableServiceRetenu.addCell(cell);
		
		document.add(tableServiceRetenu);
		
		document.add(Chunk.NEWLINE);
				

		// table service non retenu
		
		PdfPTable tableServiceNonRetenu = new PdfPTable(SERVICE_NON_RETENU_HEADER.length);
		tableServiceNonRetenu.getDefaultCell().setBorder(Rectangle.NO_BORDER);				
		tableServiceNonRetenu.setHorizontalAlignment(Element.ALIGN_LEFT);		
		
		paragraph = new Paragraph(CHAMP_TITRE_SERV_NON_RETENU,font10);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);
		
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(6);
		tableServiceNonRetenu.addCell(cell);
		
		// ajout de l'entete

		for(String header : SERVICE_NON_RETENU_HEADER){
			cell = new PdfPCell(new Paragraph(header,fontGras));                
			tableServiceNonRetenu.addCell(cell);
		}
		
				
		for(Fonction f: lstFonct){			
			tmpCodeArticle = f.getCodeArticle();
			if(ARTICLE.ARTICLE_NON_RETENU.isArticleOK(tmpCodeArticle)){
				if (TableRefHelper.getFonctionById(f.getCodeFonction())!=null) {
					cell = new PdfPCell(new Paragraph(TableRefHelper.getFonctionById(f.getCodeFonction()).getLibelleLong(),font10));
					
				} else {
					cell = new PdfPCell(new Paragraph("",font10));
				}
				tableServiceNonRetenu.addCell(cell);
								
				tmpPeriode = f.getPeriode();
				if ( tmpPeriode != null){
					cell = new PdfPCell(new Paragraph(tmpPeriode.toStringPeriod("Du ", ConstantesCalculette.TIRET),font10));
					tableServiceNonRetenu.addCell(cell);
				} else {
					cell = new PdfPCell(new Paragraph("",font10));
					tableServiceNonRetenu.addCell(cell);
				}
				cell = new PdfPCell(new Paragraph( f.getObservations()!=null?f.getObservations():"" ,font10));
				tableServiceNonRetenu.addCell(cell);
			} 

		}// for
		
		document.add(tableServiceNonRetenu);
		
		
		document.add(Chunk.NEWLINE);
		
		colWidths = new float[] {0.10f, 0.10f, 0.10f,0.20f} ;
		PdfPTable tableNextEchelon = new PdfPTable(colWidths);
		tableNextEchelon.getDefaultCell().setBorder(Rectangle.NO_BORDER);				
		tableNextEchelon.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		paragraph = new Paragraph(CHAMP_CLASSEMENT_PROPOSE,font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		cell.setColspan(4);
		tableNextEchelon.addCell(cell);
		
		
		paragraph = new Paragraph(LIBELLE_ECHELON + Conversion.integerToString(pCal.getEchelonCalcule()),font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableNextEchelon.addCell(cell);
		
		paragraph = new Paragraph(LIBELLE_CHEVRON + Conversion.integerToString(pCal.getChevronCalcule()),font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableNextEchelon.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_INDICE_BRUT + Conversion.integerToString(pCal.getIndiceBrutCalcule()),font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);
		tableNextEchelon.addCell(cell);
		
		paragraph = new Paragraph(CHAMP_ANCIENNETE_CONSERV + pCal.getDureeAncienneteCalcule().toString(),font10);
		cell = new PdfPCell(paragraph);		
		cell.setBorder(0);		
		tableNextEchelon.addCell(cell);
		
		document.add(tableNextEchelon);
		
		document.add(Chunk.NEWLINE);
		
	
		
		paragraph = new Paragraph(CHAMP_DETAILS_DES_CALCULS,font10);
		cell = new PdfPCell(paragraph);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);            
		document.add(paragraph);
		
		
		
		
		
		paragraph = new Paragraph(pCal.getDetailCalcul(),font10);
		cell = new PdfPCell(paragraph);
		paragraph.setAlignment(Paragraph.ALIGN_LEFT);            
		document.add(paragraph);
		
		document.add(Chunk.NEWLINE);
		
		paragraph = new Paragraph(ENTETE1_ARTICLE,fontGras);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);				
		document.add(paragraph);
				
		paragraph = new Paragraph(ENTETE2_ARTICLE,font10);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);			
		document.add(paragraph);
		
		DESCRIPTION_ARTICLE[] arrayArticle = DESCRIPTION_ARTICLE.values();

		for(DESCRIPTION_ARTICLE article : arrayArticle){
			
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(article.getCode(),fontGras));			
			document.add(new Paragraph(article.getLibelle(),fontItalic));
			document.add(new Paragraph(article.getDesciption(),font10));            	
		}

		document.add(Chunk.NEWLINE);
		document.add(new Paragraph(FOOTER1_ARTICLE,fontGras));
		document.add(new Paragraph(FOOTER2_ARTICLE,font10));
		document.add(new Paragraph(FOOTER3_ARTICLE,font10));  


	} catch(DocumentException de) {		
		simpleLog.println(de.getMessage());
	} catch(IOException ioe) {
		simpleLog.println(ioe.getMessage());
	} finally  {
		
		// step 5: we close the document
		if(document!=null){
			document.close();
		}
		
		if(fos!=null){
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				simpleLog.println(e.getMessage());
				e.printStackTrace();
			}			
		}
	}
	
	return tmpMessage;

  }    
  
}
        


//Je sais pas si ça intéresse encore quelqu'un ici mais, pour faire un saut de page manuel, 2 méthodes :
//- doc.newPage(); // (où doc est un object Document)
//- doc.add(Chunk.NEXTPAGE);
//
//Pour spécifier qu'un paragraphe ne doit pas être coupé par un saut de page en plein milieu :
//paragraph.setKeepTogether(true)