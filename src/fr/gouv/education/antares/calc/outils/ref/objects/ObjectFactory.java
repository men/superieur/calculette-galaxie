//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.08.17 at 02:32:46 PM CEST 
//


package fr.gouv.education.antares.calc.outils.ref.objects;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.gouv.education.antares.reference.objet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GrilleIndiciaireTypeEchelon_QNAME = new QName("", "echelon");
    private final static QName _GrilleIndiciaireTypeChevron_QNAME = new QName("", "chevron");
    private final static QName _GrilleIndiciaireTypeIndiceBrut_QNAME = new QName("", "indiceBrut");
    private final static QName _GrilleIndiciaireTypeTmpsPassageAnciennete_QNAME = new QName("", "tmpsPassageAnciennete");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.gouv.education.antares.reference.objet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GradeType }
     * 
     */
    public GradeType createGradeType() {
        return new GradeType();
    }

    /**
     * Create an instance of {@link ArticleType }
     * 
     */
    public ArticleType createArticleType() {
        return new ArticleType();
    }

    /**
     * Create an instance of {@link CalculetteClassementType }
     * 
     */
    public CalculetteClassementType createCalculetteClassementType() {
        return new CalculetteClassementType();
    }

    /**
     * Create an instance of {@link FonctionArticleType }
     * 
     */
    public FonctionArticleType createFonctionArticleType() {
        return new FonctionArticleType();
    }

    /**
     * Create an instance of {@link FonctionType }
     * 
     */
    public FonctionType createFonctionType() {
        return new FonctionType();
    }

    /**
     * Create an instance of {@link GrilleIndiciaireType }
     * 
     */
    public GrilleIndiciaireType createGrilleIndiciaireType() {
        return new GrilleIndiciaireType();
    }

    /**
     * Create an instance of {@link CorpsType }
     * 
     */
    public CorpsType createCorpsType() {
        return new CorpsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "echelon", scope = GrilleIndiciaireType.class)
    public JAXBElement<Integer> createGrilleIndiciaireTypeEchelon(Integer value) {
        return new JAXBElement<Integer>(_GrilleIndiciaireTypeEchelon_QNAME, Integer.class, GrilleIndiciaireType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "chevron", scope = GrilleIndiciaireType.class)
    public JAXBElement<Integer> createGrilleIndiciaireTypeChevron(Integer value) {
        return new JAXBElement<Integer>(_GrilleIndiciaireTypeChevron_QNAME, Integer.class, GrilleIndiciaireType.class, value);
    }
    
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indiceBrut", scope = GrilleIndiciaireType.class)
    public JAXBElement<Integer> createGrilleIndiciaireIndiceBrut(Integer value) {
        return new JAXBElement<Integer>(_GrilleIndiciaireTypeIndiceBrut_QNAME, Integer.class, GrilleIndiciaireType.class, value);
    }
    
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tmpsPassageAnciennete", scope = GrilleIndiciaireType.class)
    public JAXBElement<Integer> createGrilleIndiciaireTmpsPassageAnciennete(Integer value) {
        return new JAXBElement<Integer>(_GrilleIndiciaireTypeTmpsPassageAnciennete_QNAME, Integer.class, GrilleIndiciaireType.class, value);
    }

}
