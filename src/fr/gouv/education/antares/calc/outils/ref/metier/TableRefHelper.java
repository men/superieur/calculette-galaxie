package fr.gouv.education.antares.calc.outils.ref.metier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import fr.gouv.education.antares.calc.outils.ref.ConstantesRef.TAB_REF;
import fr.gouv.education.antares.calc.outils.ref.objects.ArticleRef;
import fr.gouv.education.antares.calc.outils.ref.objects.CorpsRef;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionArticleRef;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GradeRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GrilleIndiciaireRef;
import fr.gouv.education.antares.outils.sort.Sort;



public class TableRefHelper {
	
	
	
	
	public static List<?> getAllRef(TAB_REF pTabRef){
		List<?> lst;
		Collection<?>  coll;
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<?, ?> tmpMap;
		switch (pTabRef) {
		case FONCTION :
			 tmpMap =  (HashMap<String, FonctionRef>) tabRef.getFonctionRef();
			coll = (Collection<FonctionRef>) tmpMap.values();
			lst = new ArrayList<FonctionRef>((Collection<FonctionRef>)coll);
			Sort.trier(lst, "libelleCourt", true);
			return lst;	
			
		case GRADE:
			tmpMap = (HashMap<Long, GradeRef>)   tabRef.getGradeRef();
			coll = (Collection<GradeRef>) tmpMap.values();			
			lst =  new ArrayList<GradeRef>((Collection<GradeRef>)coll);
			Sort.trier(lst, "libelleCourt", true);
			return lst;
		case CORPS:
			tmpMap = (HashMap<Long, CorpsRef>)tabRef.getCorpsRef();
			 coll = (Collection<CorpsRef>) tmpMap.values();			 
			 lst =  new ArrayList<CorpsRef>((Collection<CorpsRef>)coll);
			 Sort.trier(lst, "libelleCourt", true);
			 return lst;
		case ARTICLE:
			tmpMap = (HashMap<String, ArticleRef>)  tabRef.getArticleRef();
			coll = (Collection<ArticleRef>) tmpMap.values();			
			lst = new ArrayList<ArticleRef>((Collection<ArticleRef>)coll);
			Sort.trier(lst, "libelleCourt", true);
			return lst;
		case GRILLE_INDICIAIRE:
			tmpMap = (HashMap<Long, GrilleIndiciaireRef>)  tabRef.getGrilleIndiciaireRef();
			coll = (Collection<GrilleIndiciaireRef>) tmpMap.values();			
			lst = new ArrayList<GrilleIndiciaireRef>((Collection<GrilleIndiciaireRef>)coll);
			Sort.trier(lst, "gradeId,echelon,chevron", true);
			
			return lst;			
		default:
			return new ArrayList<CorpsRef>();
		}
		
	}
	
	public static ArticleRef getArticleById(String articleId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<String, ArticleRef> articleHash =  tabRef.getArticleRef();
		ArticleRef article = (ArticleRef) articleHash.get(articleId);
		return article;
		
	}
	
	public static CorpsRef getCorsRefById(Long corpsId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, CorpsRef> corpsHash =  tabRef.getCorpsRef();
		CorpsRef corps = (CorpsRef) corpsHash.get(corpsId);
		//System.out.println("!!!!!!!!!!! corps libelle "+corps.getLibelleCourt());
		return corps;
	}
	
	
	public static List<CorpsRef> getCorpsAcceuilRef(){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, CorpsRef> corpsHash =  tabRef.getCorpsRef();
		Collection<CorpsRef>  coll = (Collection<CorpsRef>) corpsHash.values();
		List<CorpsRef>  lst= new ArrayList<CorpsRef>();		
		for(CorpsRef tmpCorpsRef : coll){
			if( (tmpCorpsRef.getCorpsAcceuil() != null)
			&&  (tmpCorpsRef.getCorpsAcceuil().length() >0)){
			   lst.add(tmpCorpsRef);
			}
		}
		Sort.trier(lst, "libelleCourt", true);
		
		return lst;		
	}
	
	public static List<GradeRef> getGradeAcceuilRef(){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, GradeRef> corpsHash =  tabRef.getGradeRef();
		Collection<GradeRef>  coll = (Collection<GradeRef>) corpsHash.values();
		List<GradeRef>  lst= new ArrayList<GradeRef>();		
		for(GradeRef tmpGradeRef : coll){
			if( (tmpGradeRef.getGradeAcceuil() != null)
			&&  (tmpGradeRef.getGradeAcceuil().length() >0)){
			   lst.add(tmpGradeRef);
			}
		}
		Sort.trier(lst, "libelleCourt", true);
		
		return lst;		
	}
	
	
	public static List<FonctionArticleRef> getFonctionArticleByFonctionId(String fonctionId){

		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<String, List<FonctionArticleRef>> articleHash =  tabRef.getFonctionArticleRef();
		List<FonctionArticleRef> fonct = (List<FonctionArticleRef>) articleHash.get(fonctionId);
		return fonct;
		
	}
	
	public static List<FonctionArticleRef> getFonctionArticleByArticleId(String articleId){

		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<String, List<FonctionArticleRef>> articleHash =  tabRef.getFonctionArticleByArticleHash();
		List<FonctionArticleRef> fonct = (List<FonctionArticleRef>) articleHash.get(articleId);
		return fonct;
		
	}
	
	
	public static FonctionRef getFonctionById(String fonctionId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<String, FonctionRef> fonctionHash2 =  tabRef.getFonctionRef();
		FonctionRef fonct = (FonctionRef) fonctionHash2.get(fonctionId); 
		return fonct;
		
	}
	
	public static GradeRef getGradeById(Long gradeId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, GradeRef> gradeHash =  tabRef.getGradeRef();
		GradeRef grade = (GradeRef) gradeHash.get(gradeId);		
		
		//System.out.println("grade origine ****** "+grade.getLibelleCourt());
		return grade;
		
	}
	
	
	public static List<GradeRef> getGradeByCorpsId(Long corpsId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, List<GradeRef>> gradeHash =  tabRef.getGradeByCorpsHash();
		List<GradeRef> grade = (List<GradeRef>) gradeHash.get(corpsId);		
		return grade;
		
	}
	
	public static GrilleIndiciaireRef getGrilleIndiciaireById(Long grilleId){
		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, GrilleIndiciaireRef> grilleIndiciaireRefHash =  tabRef.getGrilleIndiciaireRef();
		GrilleIndiciaireRef grille = (GrilleIndiciaireRef) grilleIndiciaireRefHash.get(grilleId); 
		return grille;
		
	}
	
	
	public static List<GrilleIndiciaireRef> getGrilleIndiciaireByGradeId(Long gradeId){

		TableRefBS tabRef = TableRefBS.getSingleton();
		HashMap<Long, List<GrilleIndiciaireRef>> grilleIndiciaireRefHash =  tabRef.getGrilleIndiciaireByGradeHash();
		List<GrilleIndiciaireRef> lst = (List<GrilleIndiciaireRef>) grilleIndiciaireRefHash.get(gradeId); 
		return lst;
	
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		
//			JAXBContext jaxbContext;
//			Schema schema;
//			SchemaFactory sf;
//			CalculetteClassementType calculetteClassement;
//			 List<FonctionType> lstFonctionType ;
//			
//			List<CorpsType> lstCorpsRef ;
//			 List<ArticleType> lstArticleType;
//			 
//			 List<FonctionArticleType> lstFonctionArticleType ;

//			 List<GradeType> lstGradeType ;
//			 List<GrilleIndiciaireType> lstGrilleIndiciaireType ;
//			 
//			try {
//				
//				sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);				
//				schema = sf.newSchema(new File("/appli/galaxie/dev/workspace/Antares/ressources/calc/calculette.xsd"));
//						
//				jaxbContext = JAXBContext.newInstance(CalculetteClassementType.class);
//			
//				 Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//				 
//				 unmarshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 unmarshaller.setSchema(schema);
//				 
//				 
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("E:/chauworkspace/Calculette/src/xml/corpsRef.xml"));				  				 			
//				 lstCorpsRef =  calculetteClassement.getCorpsType();
//				 
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("E:/chauworkspace/Calculette/src/xml/articleRef.xml"));				 
//				 lstArticleType =  calculetteClassement.getArticleType();
//
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("E:/chauworkspace/Calculette/src/xml/fonctionArticleRef.xml"));				 
//				 lstFonctionArticleType =  calculetteClassement.getFonctionArticleType();
//				 
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("/appli/galaxie/dev/workspace/Antares/ressources/calc/fonctionRef.xml"));
//				 lstFonctionType =  calculetteClassement.getFonctionType();
//				 
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("E:/chauworkspace/Calculette/src/xml/gradeRef.xml"));
//				 lstGradeType =  calculetteClassement.getGradeType();
//				 
//				 calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File("E:/chauworkspace/Calculette/src/xml/grilleIndiciaireRef.xml"));
//				 lstGrilleIndiciaireType =  calculetteClassement.getGrilleIndiciaireType();
//				 
//				 
//				 
//				 int ii=0;
//				 ii++;
//			} catch (JAXBException e) {
//				e.printStackTrace();
//			}catch (SAXException e) {
//				e.printStackTrace();
//			}
		
			 
			 
			 
			 ///////////////////////////////////////////////////
			
//			SchemaFactory sf = SchemaFactory.newInstance(
//				      javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
//
//				//The SchemaFactory instance is used to create a new JAXP 1.3 Schema object:
//				   Schema schema;
//			try {
//					schema = sf.newSchema(new File("E:/chauworkspace/Calculette/src/ref/jaxb/calculette.xsd"));
//			
//				   
//
//				//Then you use the setSchema method to identify the JAXP 1.3 Schema object (in this case, the po.xsd) that should be used to validate subsequent unmarshal operations. If you pass null into this method, it disables validation.
//				   
//				   
//				
//			
//			 List<CorpsType> lstCorpsRef;
//			 CalculetteClassementType calculetteClassementType;
//			 File file;
//			 FileOutputStream outputFile;
//			 JAXBContext context;
//			 Marshaller marshaller;
//			 
//			 ObjectFactory factory = new ObjectFactory();
//			 
//			 
//			 List<ArticleType> lstArticle;			 
//			 ArticleType articleType = factory.createArticleType();
//			 articleType.setLibelleCourt("libelleCourtArt");
//			 articleType.setLibelleLong("libelleLongArticle");
//			 			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("Liste des articles");
//			 lstArticle = calculetteClassementType.getArticleType();
//			 lstArticle.add(articleType);			 
//			 			 
//		     
//		
//				file = new File("c:/ref/articleRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);				 
//				 marshaller.marshal(calculetteClassementType, outputFile);
//
//		
//			
//			CorpsType corpsType = factory.createCorpsType();
//			 corpsType.setCorpsAcceuil("CrpAc");
//			 corpsType.setCorpsId(1);
//			 corpsType.setLibelleCourt("libelleCourtListe des articles");
//			 corpsType.setLibelleLong("libelleLongListe des articles");
//			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("corpsRef");
//			 lstCorpsRef = calculetteClassementType.getCorpsType();
//			 lstCorpsRef.add(corpsType);			 
//			 			 
//		 
//
//				file = new File("c:/ref/corpsRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);
//				 marshaller.marshal(calculetteClassementType, outputFile);
//
//			
//			
//			 List<FonctionArticleType> lstFonctionArticle;			 
//			 FonctionArticleType fonctionArticleType = factory.createFonctionArticleType();
//			 fonctionArticleType.setLibelleCourtArticle("libCrtArt");
//			 fonctionArticleType.setLibelleCourtFonction("libCrtfct");
//			 			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("Liste des fonctions-articles");
//			 lstFonctionArticle = calculetteClassementType.getFonctionArticleType();
//			 lstFonctionArticle.add(fonctionArticleType);	
//			 fonctionArticleType = factory.createFonctionArticleType();
//			 fonctionArticleType.setLibelleCourtArticle("libCrtArt2");
//			 fonctionArticleType.setLibelleCourtFonction("libCrtfct2");
//			 lstFonctionArticle.add(fonctionArticleType);
//			 			 
//		     			
//				file = new File("c:/ref/fonctionArticleRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);
//				 marshaller.marshal(calculetteClassementType, outputFile);
//			 
//
//			 List<FonctionType> lstFonction;			 
//			 FonctionType fonctionType = factory.createFonctionType();
//			 fonctionType.setLibelleCourt("libCrtFonction");
//			 fonctionType.setLibelleLong("libLongFonction");
//			 			 			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("Liste des fonctions");
//			 lstFonction = calculetteClassementType.getFonctionType();
//			 lstFonction.add(fonctionType);	
//		     
//			 
//				file = new File("c:/ref/fonctionRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);
//				 marshaller.marshal(calculetteClassementType, outputFile);
//
//			
//			
//			
//			List<GradeType> lstGrade;			 
//			GradeType gradeType = factory.createGradeType();
//			gradeType.setCorpsId(1);
//			gradeType.setGradeAcceuil("gracl");
//			gradeType.setGradeId(1);
//			gradeType.setLibelleCourt("LibelleCourtgrade");
//			gradeType.setLibelleLong("LibelleLonggrade");
//			
//			 			 			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("Liste des grades");
//			 lstGrade = calculetteClassementType.getGradeType();
//			 lstGrade.add(gradeType);	
//		     
//			
//				file = new File("c:/ref/gradeRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);
//				 marshaller.marshal(calculetteClassementType, outputFile);
//
//			
//			
//			
//			List<GrilleIndiciaireType> lstGrille;			 
//			GrilleIndiciaireType grilleType = factory.createGrilleIndiciaireType();
//			grilleType.setGrilleIndiciaireId(1);
//			grilleType.setChevron(BigInteger.valueOf(1));
//			grilleType.setEchelon(BigInteger.valueOf(2));
//			grilleType.setGradeId(1);
//			grilleType.setIndiceBrut("ibr");
//			grilleType.setTmpsPassageAnciennete(BigInteger.valueOf(22));
//			
//			 			 			 
//			 calculetteClassementType = factory.createCalculetteClassementType();
//			 
//			 calculetteClassementType.setEntete("Liste des grades");
//			 lstGrille = calculetteClassementType.getGrilleIndiciaireType();
//			 lstGrille.add(grilleType);	
//		     
//			
//				file = new File("c:/ref/grilleRef.xml");
//				outputFile = new FileOutputStream(file);
//				 context = JAXBContext.newInstance(CalculetteClassementType.class);
//				 marshaller = context.createMarshaller();
//				 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//				 marshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
//				 marshaller.setSchema(schema);
//				 marshaller.marshal(calculetteClassementType, outputFile);
//
//			} catch (SAXException e1) {

//					e1.printStackTrace();				
//			 } catch (FileNotFoundException e) {

//				e.printStackTrace();
//			}catch (Exception e) {

//				e.printStackTrace();
//			}


			 
	}

		


	
	
	


}
