package fr.gouv.education.antares.calc.outils.ref.metier;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import fr.gouv.education.antares.Constantes;
import fr.gouv.education.antares.simpleLog;
import fr.gouv.education.antares.calc.outils.ref.ConstantesRef;
import fr.gouv.education.antares.calc.outils.ref.objects.ArticleRef;
import fr.gouv.education.antares.calc.outils.ref.objects.ArticleType;
import fr.gouv.education.antares.calc.outils.ref.objects.CalculetteClassementType;
import fr.gouv.education.antares.calc.outils.ref.objects.CalculetteClassementValidationEventHandler;
import fr.gouv.education.antares.calc.outils.ref.objects.CorpsRef;
import fr.gouv.education.antares.calc.outils.ref.objects.CorpsType;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionArticleRef;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionArticleType;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionRef;
import fr.gouv.education.antares.calc.outils.ref.objects.FonctionType;
import fr.gouv.education.antares.calc.outils.ref.objects.GradeRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GradeType;
import fr.gouv.education.antares.calc.outils.ref.objects.GrilleIndiciaireRef;
import fr.gouv.education.antares.calc.outils.ref.objects.GrilleIndiciaireType;
import fr.gouv.education.antares.outils.sort.Sort;




public class TableRefBS {
	
	private static TableRefBS singleton = null; //= new TableRefBS("c");
	// cle =   fonctionid
	private HashMap<String, FonctionRef> fonctionRefHash = null;
	// cle =   articleid
	private HashMap<String, ArticleRef> articleRefHash = null;
	// cle =  corpsid
	private HashMap<Long, CorpsRef> corpsRefHash = null;
	
	// cle = fonction
	private HashMap<String, List<FonctionArticleRef>> fonctionArticleRefHash = null;	
	// cle = article
	private HashMap<String, List<FonctionArticleRef>> fonctionArticleByArticleHash = null;
	
	// cle =   grilleindiciaireid
	private HashMap<Long, GrilleIndiciaireRef> grilleIndiciaireRefHash = null;
	// cle =   gradeid
	private HashMap<Long, List<GrilleIndiciaireRef>> grilleIndiciairebyGradeHash = null;
	//cle =  gradeid
	private HashMap<Long, GradeRef> gradeRefHash = null;
	
	//cle =  corpsid
	private HashMap<Long, List<GradeRef>> gradeByCorpsHash = null;
	
	
	public static TableRefBS getSingleton(){
		if (TableRefBS.singleton == null) {
			TableRefBS.singleton = new TableRefBS("ss");
		}
		return TableRefBS.singleton;
	}
	
	private TableRefBS(String s){	
		this.initArticleRef();
		this.initCorpsRef();
		this.initFonctionArticleRef();
		this.initFonctionRef();
		this.initGradeRef();
		this.initGrilleIndiciaireRef();
				
	} 
	
	
	
	private List<?> initRef(ConstantesRef.TAB_REF ref){
		
		//simpleLog.println(">>> TableRefBS:initRef");
		JAXBContext jaxbContext;				
		CalculetteClassementType calculetteClassement;
		List<?> result = null;
		String fileName;
		String xsdFileName = Constantes.CALCUL_CLASSEMENT_XSD_FILE;
		//Schema schema;
		//SchemaFactory sf;
		Unmarshaller unmarshaller;
		
		try {
			//sf = SchemaFactory.newInstance(/*javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI*/);								
			//schema = sf.newSchema(new File(xsdFileName));				
			jaxbContext = JAXBContext.newInstance(CalculetteClassementType.class);			
			unmarshaller = jaxbContext.createUnmarshaller();				 
			unmarshaller.setEventHandler(new CalculetteClassementValidationEventHandler());
			//unmarshaller.setSchema(schema);
		
			switch (ref) {
			case ARTICLE:
				fileName = Constantes.ARTICLE_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getArticleType();
				break;
			case CORPS:
				fileName = Constantes.CORPS_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getCorpsType();
				break;
			case FONCTION_ARTICLE:
				fileName = Constantes.FONCTION_ARTICLE_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getFonctionArticleType();
				break;
			case FONCTION:
				fileName = Constantes.FONCTION_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getFonctionType();
				break;
			case GRADE:
				fileName = Constantes.GRADE_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getGradeType();
				break;

			case GRILLE_INDICIAIRE:
				fileName = Constantes.GRILLE_INDICIAIRE_REF_FILE;
				calculetteClassement = (CalculetteClassementType) unmarshaller.unmarshal(new File(fileName));				  				 			
				result =  calculetteClassement.getGrilleIndiciaireType();
				break;
			default:
				result = new ArrayList<Object>();
				break;
			}
			return result;
		
		} catch (JAXBException e) {
			simpleLog.println("TableRefBS:initRef JAXBException" +  e.getMessage());
			e.printStackTrace();
		} /*catch (SAXException e) {
			simpleLog.println("TableRefBS:initRef SAXException" +  e.getMessage());
			simpleLog.println(e.getMessage());
			e.printStackTrace();
		}	*/
		
		//simpleLog.println("<<<< TableRefBS:initRef");
		return new ArrayList<Object>();
		 
	}
	
	
	@SuppressWarnings("unchecked")
	private void initFonctionRef(){
		
		if(fonctionRefHash == null){
			fonctionRefHash = new HashMap<String, FonctionRef>();
			
			FonctionRef tmpFonctionRef;
			List<FonctionType> lst = (List<FonctionType>)initRef(ConstantesRef.TAB_REF.FONCTION);
			for(FonctionType fonctionType : lst){
				tmpFonctionRef = new FonctionRef();
				tmpFonctionRef.setLibelleCourt(fonctionType.getLibelleCourt());
				tmpFonctionRef.setLibelleLong(fonctionType.getLibelleLong());			
				fonctionRefHash.put(tmpFonctionRef.getLibelleCourt(), tmpFonctionRef);
			}						
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initArticleRef(){
		
		if(articleRefHash == null || articleRefHash.isEmpty() || articleRefHash.size() == 0){
		
			articleRefHash = new HashMap<String, ArticleRef>();
	
			ArticleRef tmpArticleRef;
			List<ArticleType> lst = (List<ArticleType>)initRef(ConstantesRef.TAB_REF.ARTICLE);
			for(ArticleType articleType : lst){
				tmpArticleRef = new ArticleRef();
				tmpArticleRef.setLibelleCourt(articleType.getLibelleCourt());
				tmpArticleRef.setLibelleLong(articleType.getLibelleLong());			
				articleRefHash.put(tmpArticleRef.getLibelleCourt(), tmpArticleRef);
			}
		}
	}
	@SuppressWarnings("unchecked")
	private void initCorpsRef(){
		
		if(corpsRefHash == null){
			corpsRefHash = new HashMap<Long, CorpsRef>();
			
			List<CorpsType> lst = (List<CorpsType>)initRef(ConstantesRef.TAB_REF.CORPS);
			CorpsRef tmpCorpsRef;
			for(CorpsType corpsType : lst){
				tmpCorpsRef = new CorpsRef();
				tmpCorpsRef.setLibelleCourt(corpsType.getLibelleCourt());
				tmpCorpsRef.setLibelleLong(corpsType.getLibelleLong());
				tmpCorpsRef.setCorpsAcceuil(corpsType.getCorpsAcceuil());
				tmpCorpsRef.setCorpsId(corpsType.getCorpsId());
				corpsRefHash.put(tmpCorpsRef.getCorpsId(), tmpCorpsRef);
			}
		}
		 
	}
	
	
	@SuppressWarnings("unchecked")
	private void initFonctionArticleRef(){
		
		if(fonctionArticleRefHash == null){
			
			fonctionArticleRefHash = new HashMap<String, List<FonctionArticleRef>>();
			
			fonctionArticleByArticleHash = new HashMap<String, List<FonctionArticleRef>>();
			
			List<FonctionArticleRef> tmpList;
			
			List<FonctionArticleType> lst = (List<FonctionArticleType>)initRef(ConstantesRef.TAB_REF.FONCTION_ARTICLE);
			FonctionArticleRef tmpFonctionArticleRef;
			
			for(FonctionArticleType fonctArtType : lst){
				tmpFonctionArticleRef = new FonctionArticleRef();
				tmpFonctionArticleRef.setLibelleCourtArticle(fonctArtType.getLibelleCourtArticle());
				tmpFonctionArticleRef.setLibelleCourtFonction(fonctArtType.getLibelleCourtFonction());
				
				tmpList = fonctionArticleRefHash.get(tmpFonctionArticleRef.getLibelleCourtFonction());
				if(tmpList == null){
					tmpList = new ArrayList<FonctionArticleRef>();
					fonctionArticleRefHash.put(tmpFonctionArticleRef.getLibelleCourtFonction(), tmpList);
				}
				tmpList.add(tmpFonctionArticleRef);
				
				
				tmpList = fonctionArticleByArticleHash.get(tmpFonctionArticleRef.getLibelleCourtArticle());
				if(tmpList == null){
					tmpList = new ArrayList<FonctionArticleRef>();
					fonctionArticleByArticleHash.put(tmpFonctionArticleRef.getLibelleCourtArticle(), tmpList);
				}
				tmpList.add(tmpFonctionArticleRef);
			}
			
			Collection<List<FonctionArticleRef>> coll =  fonctionArticleByArticleHash.values();			
			for(List<FonctionArticleRef> lstRef : coll){
				Sort.trier(lstRef, "libelleCourtFonction", true);
				
			}
			
			coll =  fonctionArticleRefHash.values();
			for(List<FonctionArticleRef> lstRef : coll){
				Sort.trier(lstRef, "libelleCourtArticle", true);
				
			}
			
			
			
			
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initGrilleIndiciaireRef(){
		
		if(grilleIndiciaireRefHash == null){
			
			grilleIndiciaireRefHash = new HashMap<Long, GrilleIndiciaireRef>();
			grilleIndiciairebyGradeHash = new HashMap<Long, List<GrilleIndiciaireRef>>();
			
			List<GrilleIndiciaireType> lst = (List<GrilleIndiciaireType>)initRef(ConstantesRef.TAB_REF.GRILLE_INDICIAIRE);
			GrilleIndiciaireRef tmpGrilleIndiciaireRef;
			
			List<GrilleIndiciaireRef> tmpList;
			
			JAXBElement<Integer> tmpElement;
			
			for(GrilleIndiciaireType grilleIndiciaireType : lst){
				
				// l'indice brute de la grille indiciaire n'est pas renseignee
				// ne pas prendre l'element en compte
				tmpElement = grilleIndiciaireType.getIndiceBrut();
				if( (tmpElement ==null)
				|| (tmpElement.getValue() == -1)){
					continue;
				}
				
				
				tmpGrilleIndiciaireRef = new GrilleIndiciaireRef();
				
				if( (tmpElement !=null)
				&& (tmpElement.getValue()>-1)){
					tmpGrilleIndiciaireRef.setIndiceBrut(tmpElement.getValue());
				}
				
				tmpElement = grilleIndiciaireType.getChevron();
				
				if( (tmpElement !=null)
				&& (tmpElement.getValue()>-1)){
					tmpGrilleIndiciaireRef.setChevron(tmpElement.getValue());
				} else {
					tmpGrilleIndiciaireRef.setChevron(null);
				}
				
				tmpElement = grilleIndiciaireType.getEchelon();
				if( (tmpElement !=null)
				&& (tmpElement.getValue()>-1)){
					tmpGrilleIndiciaireRef.setEchelon(tmpElement.getValue());
				} else {
					tmpGrilleIndiciaireRef.setEchelon(null);
				}				
				
				tmpGrilleIndiciaireRef.setGradeId(grilleIndiciaireType.getGradeId());				
				tmpGrilleIndiciaireRef.setGrilleIndiciaireId(grilleIndiciaireType.getGrilleIndiciaireId());
				
				
				
				tmpElement = grilleIndiciaireType.getTmpsPassageAnciennete();
				if( (tmpElement !=null)
				&& (tmpElement.getValue()>-1)){
					tmpGrilleIndiciaireRef.setTmpsPassageAnciennete(tmpElement.getValue());
				}else {
					tmpGrilleIndiciaireRef.setTmpsPassageAnciennete(null);
				}
								
				
				grilleIndiciaireRefHash.put(tmpGrilleIndiciaireRef.getGrilleIndiciaireId(), tmpGrilleIndiciaireRef);
				
				tmpList = (List<GrilleIndiciaireRef>) grilleIndiciairebyGradeHash.get(tmpGrilleIndiciaireRef.getGradeId());
				if(tmpList == null){
					tmpList = new ArrayList<GrilleIndiciaireRef>();
					grilleIndiciairebyGradeHash.put(tmpGrilleIndiciaireRef.getGradeId(), tmpList);
				}
				tmpList.add(tmpGrilleIndiciaireRef);				
				
			}
			
			Collection<List<GrilleIndiciaireRef>> coll =  grilleIndiciairebyGradeHash.values();			
			for(List<GrilleIndiciaireRef> lstGrilleRef : coll){
				Sort.trier(lstGrilleRef, "echelon,chevron", true);
				
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void initGradeRef(){
		if(gradeRefHash == null){			
			gradeRefHash = new HashMap<Long, GradeRef>();
			
			gradeByCorpsHash = new HashMap<Long, List<GradeRef>>();
			
			List<GradeType> lst = (List<GradeType>)initRef(ConstantesRef.TAB_REF.GRADE);
			GradeRef tmpGradeRef;
			List<GradeRef> tmpLst;
			
			for(GradeType gradeType : lst){
				tmpGradeRef = new GradeRef();
				tmpGradeRef.setCorpsId(gradeType.getCorpsId());
				tmpGradeRef.setGradeAcceuil(gradeType.getGradeAcceuil());
				tmpGradeRef.setGradeId(gradeType.getGradeId());
				
				tmpGradeRef.setLibelleCourt(gradeType.getLibelleCourt());
				tmpGradeRef.setLibelleLong(gradeType.getLibelleLong());
				
				gradeRefHash.put(tmpGradeRef.getGradeId(), tmpGradeRef);
				//System.out.println("*** ("+tmpGradeRef.getGradeId()+"),("+tmpGradeRef.getLibelleCourt()+")");
				
				tmpLst = gradeByCorpsHash.get(tmpGradeRef.getCorpsId());
				if(tmpLst == null){
					tmpLst = new ArrayList<GradeRef>();
					 gradeByCorpsHash.put(tmpGradeRef.getCorpsId(),tmpLst);					
				}
				tmpLst.add(tmpGradeRef);				
			}		
			
			Collection<List<GradeRef>> coll =  gradeByCorpsHash.values();
			// tri de la liste sur le libelle court
			for(List<GradeRef> lstGrad : coll){
			       Sort.trier(lstGrad, "libelleCourt", true);			       
			}
			
		}
	}
	
	
	
	
	public HashMap<String, ArticleRef> getArticleRef(){
		initArticleRef();
		HashMap<String, ArticleRef> tmpHashmap = articleRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<Long, CorpsRef> getCorpsRef(){
		initCorpsRef();
		HashMap<Long, CorpsRef> tmpHashmap = corpsRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<String, List<FonctionArticleRef>> getFonctionArticleRef(){
		initFonctionArticleRef();
		HashMap<String, List<FonctionArticleRef>> tmpHashmap = fonctionArticleRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<String, FonctionRef> getFonctionRef(){
		initFonctionRef();
		HashMap<String, FonctionRef> tmpHashmap = fonctionRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<Long, GradeRef> getGradeRef(){
		initGradeRef();
		HashMap<Long, GradeRef> tmpHashmap = gradeRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<Long, GrilleIndiciaireRef> getGrilleIndiciaireRef(){
		initGrilleIndiciaireRef();
		HashMap<Long, GrilleIndiciaireRef> tmpHashmap = grilleIndiciaireRefHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<Long, List<GrilleIndiciaireRef>> getGrilleIndiciaireByGradeHash(){
		
		HashMap<Long, List<GrilleIndiciaireRef>> tmpHashmap = grilleIndiciairebyGradeHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<Long, List<GradeRef>> getGradeByCorpsHash(){
		
		HashMap<Long, List<GradeRef>> tmpHashmap = gradeByCorpsHash; 
		return tmpHashmap ;				
	}
	
	public HashMap<String, List<FonctionArticleRef>> getFonctionArticleByArticleHash(){		
		HashMap<String, List<FonctionArticleRef>> tmpHashmap = fonctionArticleByArticleHash; 
		return tmpHashmap ;				
	}
	
	
	/*
	public static void main(String[] args) {
		TableRefBS tab = TableRefBS.getSingleton();
		HashMap<?, ?> map = tab.getArticleRef();
		
		map = tab.getCorpsRef();
		map = tab.getFonctionArticleRef();
		map = tab.getFonctionRef();
		map = tab.getGradeRef();
		map = tab.getGrilleIndiciaireRef();

	}
	*/

}
