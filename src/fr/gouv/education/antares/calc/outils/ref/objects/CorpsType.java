//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.08.17 at 02:32:46 PM CEST 
//


package fr.gouv.education.antares.calc.outils.ref.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import fr.gouv.education.antares.objets.ObjetBase;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="corpsId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}long">
 *               &lt;minInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="libelleCourt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="libelleLong">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="corpsAcceuil" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corpsId",
    "libelleCourt",
    "libelleLong",
    "corpsAcceuil"
})
@XmlRootElement(name = "corpsType")
public class CorpsType extends ObjetBase{

    protected long corpsId;
    @XmlElement(required = true)
    protected String libelleCourt;
    @XmlElement(required = true)
    protected String libelleLong;
    protected String corpsAcceuil;

    /**
     * Gets the value of the corpsId property.
     * 
     */
    public long getCorpsId() {
        return corpsId;
    }

    /**
     * Sets the value of the corpsId property.
     * 
     */
    public void setCorpsId(long value) {
        this.corpsId = value;
    }

    /**
     * Gets the value of the libelleCourt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * Sets the value of the libelleCourt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLibelleCourt(String value) {
        this.libelleCourt = value;
    }

    /**
     * Gets the value of the libelleLong property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * Sets the value of the libelleLong property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLibelleLong(String value) {
        this.libelleLong = value;
    }

    /**
     * Gets the value of the corpsAcceuil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpsAcceuil() {
        return corpsAcceuil;
    }

    /**
     * Sets the value of the corpsAcceuil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpsAcceuil(String value) {
        this.corpsAcceuil = value;
    }

}
