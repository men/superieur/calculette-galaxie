package fr.gouv.education.antares.calc.outils.ref.objects;

import fr.gouv.education.antares.objets.ObjetBase;

public class GradeRef extends ObjetBase{
	
	private Long gradeId;
	private Long corpsId;
	
	private String libelleCourt;
	private String libelleLong;
	private String gradeAcceuil;
	
	public Long getGradeId() {
		return gradeId;
	}
	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}
	public Long getCorpsId() {
		return corpsId;
	}
	public void setCorpsId(Long corpsId) {
		this.corpsId = corpsId;
	}
	public String getLibelleCourt() {
		return libelleCourt;
	}
	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}
	public String getLibelleLong() {
		return libelleLong;
	}
	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}
	public String getGradeAcceuil() {
		return gradeAcceuil;
	}
	public void setGradeAcceuil(String gradeAcceuil) {
		this.gradeAcceuil = gradeAcceuil;
	}
	
	
	

}
