package fr.gouv.education.antares.calc.outils.ref.objects;

import fr.gouv.education.antares.objets.ObjetBase;

public class FonctionArticleRef extends ObjetBase{
	
	String libelleCourtFonction;
	String libelleCourtArticle;
	
	public String getLibelleCourtFonction() {
		return libelleCourtFonction;
	}
	public void setLibelleCourtFonction(String libelleCourtFonction) {
		this.libelleCourtFonction = libelleCourtFonction;
	}
	public String getLibelleCourtArticle() {
		return libelleCourtArticle;
	}
	public void setLibelleCourtArticle(String libelleCourtArticle) {
		this.libelleCourtArticle = libelleCourtArticle;
	}

}
