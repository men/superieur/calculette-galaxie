package fr.gouv.education.antares.calc.outils.ref.objects;

import fr.gouv.education.antares.objets.ObjetBase;

public class ArticleRef extends ObjetBase{
	
	String libelleCourt;
	String libelleLong;
	
	public String getLibelleCourt() {
		return libelleCourt;
	}
	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}
	public String getLibelleLong() {
		return libelleLong;
	}
	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}
	

}
