package fr.gouv.education.antares.calc.outils.ref.objects;

import fr.gouv.education.antares.objets.ObjetBase;

public class GrilleIndiciaireRef extends ObjetBase{
	
	private Long grilleIndiciaireId;
	private Long gradeId;
	private Integer echelon;
	private Integer chevron;
	private Integer indiceBrut;
	private Integer tmpsPassageAnciennete;
	
	public Long getGrilleIndiciaireId() {
		return grilleIndiciaireId;
	}
	public void setGrilleIndiciaireId(Long grilleIndiciaireId) {
		this.grilleIndiciaireId = grilleIndiciaireId;
	}
	public Long getGradeId() {
		return gradeId;
	}
	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}
	public Integer getEchelon() {
		return echelon;
	}
	public void setEchelon(Integer echelon) {
		this.echelon = echelon;
	}
	public Integer getChevron() {
		if(chevron == null){
			chevron =  new Integer(0);
		}
		return chevron;
	}
	public void setChevron(Integer chevron) {
		this.chevron = chevron;
	}
	public Integer getIndiceBrut() {
		return indiceBrut;
	}
	public void setIndiceBrut(Integer indiceBrut) {
		this.indiceBrut = indiceBrut;
	}
	public Integer getTmpsPassageAnciennete() {
		return tmpsPassageAnciennete;
	}
	public void setTmpsPassageAnciennete(Integer tmpsPassageAnciennete) {
		this.tmpsPassageAnciennete = tmpsPassageAnciennete;
	}
	
	
	

}
