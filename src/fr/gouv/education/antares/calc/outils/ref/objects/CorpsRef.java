package fr.gouv.education.antares.calc.outils.ref.objects;

import fr.gouv.education.antares.objets.ObjetBase;

public class CorpsRef extends ObjetBase{
	
	private Long corpsId;
	private String libelleCourt;
	private String libelleLong;
	private String corpsAcceuil;
	
	public Long getCorpsId() {
		return corpsId;
	}
	public void setCorpsId(Long corpsId) {
		this.corpsId = corpsId;
	}
	public String getLibelleCourt() {
		return libelleCourt;
	}
	public void setLibelleCourt(String libelleCourt) {
		this.libelleCourt = libelleCourt;
	}
	public String getLibelleLong() {
		return libelleLong;
	}
	public void setLibelleLong(String libelleLong) {
		this.libelleLong = libelleLong;
	}
	public String getCorpsAcceuil() {
		return corpsAcceuil;
	}
	public void setCorpsAcceuil(String corpsAcceuil) {
		this.corpsAcceuil = corpsAcceuil;
	}

}
