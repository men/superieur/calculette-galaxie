package fr.gouv.education.antares.calc.outils;

import java.util.Date;
import java.util.GregorianCalendar;

import fr.gouv.education.antares.calc.ConstantesCalculette;
import fr.gouv.education.antares.calc.objets.DureeA30Jours;
import fr.gouv.education.antares.outils.Conversion;

public class UtilsCalculette {
	
	  /**
     * Retourne le nombre de jours entre deux dates en considérant les mois à 30 jours
     * s'il s'agit du mois de février, le 28 est remplacé par 30 pour une année non bissextile,
     * le 29 est remplacé par 30 pour une année bissextile.
     *
     * @param date1 Date
     * @param date2 Date
     * @return int (-1 en cas de problème)
     */
    public static int differencierDatesA30jours(final Date date1, final Date date2) {
        int result = 0;

        if (date1 == null || date2 == null) {
            return -1;
        }

        String[] parseDate1 = Conversion.parseDate(date1);
        String[] parseDate2 = Conversion.parseDate(date2);
        
        

        if (parseDate1[0].compareTo("30") > 0 ||
            isFinFevrier(parseDate1)) {
            parseDate1[0] = "30";
        }

        if (parseDate2[0].compareTo("30") > 0 ||
            isFinFevrier(parseDate2)) {
            parseDate2[0] = "30";
        }

        long nbJours1 = Long.parseLong(parseDate1[0])
            + (Long.parseLong(parseDate1[1]) * ConstantesCalculette.NB_JOURS_PAR_MOIS)
            + (Long.parseLong(parseDate1[2]) * ConstantesCalculette.NB_JOURS_PAR_AN);

        long nbJours2 = Long.parseLong(parseDate2[0])
            + (Long.parseLong(parseDate2[1]) * ConstantesCalculette.NB_JOURS_PAR_MOIS)
            + (Long.parseLong(parseDate2[2]) * ConstantesCalculette.NB_JOURS_PAR_AN);

        result = Math.abs((int) (nbJours1 - nbJours2)) + 1;

        return result;
    }
    
    /**
     * @param pNbJour
     * @return
     */
    public static DureeA30Jours getDureeA30Jours(float pNbJour){
		DureeA30Jours duree = new DureeA30Jours(pNbJour);				
		return duree;
	}
    
    /**
     * Détermine si la date parsée en paramètre est le dernier jour de février
     * 28 pour une année non bissextile
     * 29 pour une année bissextile
     *
     * @param parseDate String[]
     * @return boolean
     */
    public static boolean isFinFevrier(String[] parseDate) {
      boolean retour = false;
      int jour = Integer.parseInt(parseDate[0]);
      int mois = Integer.parseInt(parseDate[1]);
      int annee = Integer.parseInt(parseDate[2]);
      boolean isLeapYear = isLeapYear(annee);
      if (((isLeapYear && jour == 29) ||  jour == 28)
          && mois == 2) {
          retour = true;
        }

      return retour;
    }
    
    /**
     * Détermine si l'année passée en paramètre est bissextile
     *
     * @param year int
     * @return boolean
     */
    public static boolean isLeapYear (int year) {
      GregorianCalendar cal = new GregorianCalendar();
      boolean retour = cal.isLeapYear(year);
      cal = null;
      return retour;
    }

}
